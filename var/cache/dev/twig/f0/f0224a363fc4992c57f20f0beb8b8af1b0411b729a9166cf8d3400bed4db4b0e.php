<?php

/* niveau/new.html.twig */
class __TwigTemplate_2b9363a2b050d6bd34d2aef2e0a49afaeb5679c4d20f58ea0759f435cdc08c78 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "niveau/new.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d7ca22713d0d6ececc14bf2545f149313e4dd475c1cf23b21b01f738817f8cfd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d7ca22713d0d6ececc14bf2545f149313e4dd475c1cf23b21b01f738817f8cfd->enter($__internal_d7ca22713d0d6ececc14bf2545f149313e4dd475c1cf23b21b01f738817f8cfd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "niveau/new.html.twig"));

        $__internal_58aa2d92a11150d754b5b73ebdfb30a3f4206340e1668108a307dabe3ae5a983 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_58aa2d92a11150d754b5b73ebdfb30a3f4206340e1668108a307dabe3ae5a983->enter($__internal_58aa2d92a11150d754b5b73ebdfb30a3f4206340e1668108a307dabe3ae5a983_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "niveau/new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d7ca22713d0d6ececc14bf2545f149313e4dd475c1cf23b21b01f738817f8cfd->leave($__internal_d7ca22713d0d6ececc14bf2545f149313e4dd475c1cf23b21b01f738817f8cfd_prof);

        
        $__internal_58aa2d92a11150d754b5b73ebdfb30a3f4206340e1668108a307dabe3ae5a983->leave($__internal_58aa2d92a11150d754b5b73ebdfb30a3f4206340e1668108a307dabe3ae5a983_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_b59e5091b3462ea1ff75b1fc98b949547458832f5dfc5fb6bd3505cc9a28e547 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b59e5091b3462ea1ff75b1fc98b949547458832f5dfc5fb6bd3505cc9a28e547->enter($__internal_b59e5091b3462ea1ff75b1fc98b949547458832f5dfc5fb6bd3505cc9a28e547_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_fd06a90bdced169e5de524cbf7ff172058963cbfcbed7231b272694eca28644d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fd06a90bdced169e5de524cbf7ff172058963cbfcbed7231b272694eca28644d->enter($__internal_fd06a90bdced169e5de524cbf7ff172058963cbfcbed7231b272694eca28644d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<br><br><br>
<div class=\"container\">


    <h2>Niveau creation</h2>
    <br>

    ";
        // line 11
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
        ";
        // line 12
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo "
        <input type=\"submit\" value=\"Create\" />
    ";
        // line 14
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
    <br>


            <a href=\"";
        // line 18
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_niveau_index");
        echo "\" class=\"btn btn-large btn-info\">Back to the list</a>

</div>
";
        
        $__internal_fd06a90bdced169e5de524cbf7ff172058963cbfcbed7231b272694eca28644d->leave($__internal_fd06a90bdced169e5de524cbf7ff172058963cbfcbed7231b272694eca28644d_prof);

        
        $__internal_b59e5091b3462ea1ff75b1fc98b949547458832f5dfc5fb6bd3505cc9a28e547->leave($__internal_b59e5091b3462ea1ff75b1fc98b949547458832f5dfc5fb6bd3505cc9a28e547_prof);

    }

    public function getTemplateName()
    {
        return "niveau/new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  74 => 18,  67 => 14,  62 => 12,  58 => 11,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
<br><br><br>
<div class=\"container\">


    <h2>Niveau creation</h2>
    <br>

    {{ form_start(form) }}
        {{ form_widget(form) }}
        <input type=\"submit\" value=\"Create\" />
    {{ form_end(form) }}
    <br>


            <a href=\"{{ path('admin_niveau_index') }}\" class=\"btn btn-large btn-info\">Back to the list</a>

</div>
{% endblock %}
", "niveau/new.html.twig", "/home/fonguen/symfony projet/infotels/app/Resources/views/niveau/new.html.twig");
    }
}
