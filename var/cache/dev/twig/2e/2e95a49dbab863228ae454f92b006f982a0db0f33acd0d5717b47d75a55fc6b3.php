<?php

/* @FOSUser/Group/show.html.twig */
class __TwigTemplate_bada1e559640b7c821aae8f14bd5e02afd53fe3f467725d5a425165448bdea1b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Group/show.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5770c2e6d080c336ecab7f341398b7d81e1245ab905dfbf550a4a4a786ddcea3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5770c2e6d080c336ecab7f341398b7d81e1245ab905dfbf550a4a4a786ddcea3->enter($__internal_5770c2e6d080c336ecab7f341398b7d81e1245ab905dfbf550a4a4a786ddcea3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Group/show.html.twig"));

        $__internal_3b519e3b82f2900dbb1b6f0c38bf2ba00d157a66a3e2bac8ba554f4e743be6ed = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3b519e3b82f2900dbb1b6f0c38bf2ba00d157a66a3e2bac8ba554f4e743be6ed->enter($__internal_3b519e3b82f2900dbb1b6f0c38bf2ba00d157a66a3e2bac8ba554f4e743be6ed_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Group/show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5770c2e6d080c336ecab7f341398b7d81e1245ab905dfbf550a4a4a786ddcea3->leave($__internal_5770c2e6d080c336ecab7f341398b7d81e1245ab905dfbf550a4a4a786ddcea3_prof);

        
        $__internal_3b519e3b82f2900dbb1b6f0c38bf2ba00d157a66a3e2bac8ba554f4e743be6ed->leave($__internal_3b519e3b82f2900dbb1b6f0c38bf2ba00d157a66a3e2bac8ba554f4e743be6ed_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_ea653e72d8008a8d7e9737fd0caaf8c58851a24cf6be7a8177af4b75a55512a4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ea653e72d8008a8d7e9737fd0caaf8c58851a24cf6be7a8177af4b75a55512a4->enter($__internal_ea653e72d8008a8d7e9737fd0caaf8c58851a24cf6be7a8177af4b75a55512a4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_4e04f99a38e7cec5c77a90f8b8dbf2fc1418cfa355d6d210a0c3fa1290f0eb6e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4e04f99a38e7cec5c77a90f8b8dbf2fc1418cfa355d6d210a0c3fa1290f0eb6e->enter($__internal_4e04f99a38e7cec5c77a90f8b8dbf2fc1418cfa355d6d210a0c3fa1290f0eb6e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/show_content.html.twig", "@FOSUser/Group/show.html.twig", 4)->display($context);
        
        $__internal_4e04f99a38e7cec5c77a90f8b8dbf2fc1418cfa355d6d210a0c3fa1290f0eb6e->leave($__internal_4e04f99a38e7cec5c77a90f8b8dbf2fc1418cfa355d6d210a0c3fa1290f0eb6e_prof);

        
        $__internal_ea653e72d8008a8d7e9737fd0caaf8c58851a24cf6be7a8177af4b75a55512a4->leave($__internal_ea653e72d8008a8d7e9737fd0caaf8c58851a24cf6be7a8177af4b75a55512a4_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Group/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/show_content.html.twig\" %}
{% endblock fos_user_content %}
", "@FOSUser/Group/show.html.twig", "/home/fonguen/symfony projet/infotels/vendor/friendsofsymfony/user-bundle/Resources/views/Group/show.html.twig");
    }
}
