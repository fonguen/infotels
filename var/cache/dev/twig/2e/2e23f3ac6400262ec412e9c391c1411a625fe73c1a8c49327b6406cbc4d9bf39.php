<?php

/* @FOSUser/ChangePassword/change_password.html.twig */
class __TwigTemplate_30f090bf8d14e1c96453c523ff0292ced10d376590a34f1647dfebeded451294 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/ChangePassword/change_password.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c652c90cce604ef9087bc667ba9d56b610098e2fe376247da8d7eea30bf5f0ae = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c652c90cce604ef9087bc667ba9d56b610098e2fe376247da8d7eea30bf5f0ae->enter($__internal_c652c90cce604ef9087bc667ba9d56b610098e2fe376247da8d7eea30bf5f0ae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/ChangePassword/change_password.html.twig"));

        $__internal_3aee5a4d2cd6b20029590c6b69075798b0c56a99a24748820cc2a66d9a554518 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3aee5a4d2cd6b20029590c6b69075798b0c56a99a24748820cc2a66d9a554518->enter($__internal_3aee5a4d2cd6b20029590c6b69075798b0c56a99a24748820cc2a66d9a554518_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/ChangePassword/change_password.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c652c90cce604ef9087bc667ba9d56b610098e2fe376247da8d7eea30bf5f0ae->leave($__internal_c652c90cce604ef9087bc667ba9d56b610098e2fe376247da8d7eea30bf5f0ae_prof);

        
        $__internal_3aee5a4d2cd6b20029590c6b69075798b0c56a99a24748820cc2a66d9a554518->leave($__internal_3aee5a4d2cd6b20029590c6b69075798b0c56a99a24748820cc2a66d9a554518_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_7d340aa0681218c0bc5e2c6f453c3730c3b42f5081e2a61d10dd850bd7b486bb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7d340aa0681218c0bc5e2c6f453c3730c3b42f5081e2a61d10dd850bd7b486bb->enter($__internal_7d340aa0681218c0bc5e2c6f453c3730c3b42f5081e2a61d10dd850bd7b486bb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_6671731e1f3f6aaf1b2de15e162a27d366f4e1f9619fefc073ced9ad2215597e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6671731e1f3f6aaf1b2de15e162a27d366f4e1f9619fefc073ced9ad2215597e->enter($__internal_6671731e1f3f6aaf1b2de15e162a27d366f4e1f9619fefc073ced9ad2215597e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/ChangePassword/change_password_content.html.twig", "@FOSUser/ChangePassword/change_password.html.twig", 4)->display($context);
        
        $__internal_6671731e1f3f6aaf1b2de15e162a27d366f4e1f9619fefc073ced9ad2215597e->leave($__internal_6671731e1f3f6aaf1b2de15e162a27d366f4e1f9619fefc073ced9ad2215597e_prof);

        
        $__internal_7d340aa0681218c0bc5e2c6f453c3730c3b42f5081e2a61d10dd850bd7b486bb->leave($__internal_7d340aa0681218c0bc5e2c6f453c3730c3b42f5081e2a61d10dd850bd7b486bb_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/ChangePassword/change_password.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/ChangePassword/change_password_content.html.twig\" %}
{% endblock fos_user_content %}
", "@FOSUser/ChangePassword/change_password.html.twig", "/home/fonguen/symfony projet/infotels/vendor/friendsofsymfony/user-bundle/Resources/views/ChangePassword/change_password.html.twig");
    }
}
