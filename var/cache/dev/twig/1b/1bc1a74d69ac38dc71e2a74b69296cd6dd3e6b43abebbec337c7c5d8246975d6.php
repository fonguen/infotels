<?php

/* @FOSUser/Resetting/reset.html.twig */
class __TwigTemplate_7333160e7561d53914fe5ee91f9727fc9242851de67316328b6c2ad4b8abbf77 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Resetting/reset.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e903d0703fa81fe0de26b2d996fc3e25aac4008ab5540168808c03bc0e90d6f8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e903d0703fa81fe0de26b2d996fc3e25aac4008ab5540168808c03bc0e90d6f8->enter($__internal_e903d0703fa81fe0de26b2d996fc3e25aac4008ab5540168808c03bc0e90d6f8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Resetting/reset.html.twig"));

        $__internal_c3b1ffc1fa70d8048afddb3180fc441905021f87aa1f77c20c050e975d692488 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c3b1ffc1fa70d8048afddb3180fc441905021f87aa1f77c20c050e975d692488->enter($__internal_c3b1ffc1fa70d8048afddb3180fc441905021f87aa1f77c20c050e975d692488_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Resetting/reset.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e903d0703fa81fe0de26b2d996fc3e25aac4008ab5540168808c03bc0e90d6f8->leave($__internal_e903d0703fa81fe0de26b2d996fc3e25aac4008ab5540168808c03bc0e90d6f8_prof);

        
        $__internal_c3b1ffc1fa70d8048afddb3180fc441905021f87aa1f77c20c050e975d692488->leave($__internal_c3b1ffc1fa70d8048afddb3180fc441905021f87aa1f77c20c050e975d692488_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_72559a21c480c453ffc4a70be46e256e16f12c951731f7dca1a21a5f8ac6f84c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_72559a21c480c453ffc4a70be46e256e16f12c951731f7dca1a21a5f8ac6f84c->enter($__internal_72559a21c480c453ffc4a70be46e256e16f12c951731f7dca1a21a5f8ac6f84c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_23f4b810fd481371699368a0a46abf4e03810d72a9492645366c503bb74702e9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_23f4b810fd481371699368a0a46abf4e03810d72a9492645366c503bb74702e9->enter($__internal_23f4b810fd481371699368a0a46abf4e03810d72a9492645366c503bb74702e9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Resetting/reset_content.html.twig", "@FOSUser/Resetting/reset.html.twig", 4)->display($context);
        
        $__internal_23f4b810fd481371699368a0a46abf4e03810d72a9492645366c503bb74702e9->leave($__internal_23f4b810fd481371699368a0a46abf4e03810d72a9492645366c503bb74702e9_prof);

        
        $__internal_72559a21c480c453ffc4a70be46e256e16f12c951731f7dca1a21a5f8ac6f84c->leave($__internal_72559a21c480c453ffc4a70be46e256e16f12c951731f7dca1a21a5f8ac6f84c_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Resetting/reset.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Resetting/reset_content.html.twig\" %}
{% endblock fos_user_content %}
", "@FOSUser/Resetting/reset.html.twig", "/home/fonguen/symfony projet/infotels/vendor/friendsofsymfony/user-bundle/Resources/views/Resetting/reset.html.twig");
    }
}
