<?php

/* @Framework/Form/checkbox_widget.html.php */
class __TwigTemplate_30338179ed67d19f78b0804d2d3cc78b225a62e429a457cfb539a0738c49d4ba extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_47d9307a0895160961f18a155c25377f066d6cde685618a30e4d674698c5f012 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_47d9307a0895160961f18a155c25377f066d6cde685618a30e4d674698c5f012->enter($__internal_47d9307a0895160961f18a155c25377f066d6cde685618a30e4d674698c5f012_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/checkbox_widget.html.php"));

        $__internal_b85ca238a39947ab6fcf8cbcff9d0042c91c82b1b356d98d0dbf31d293a6aace = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b85ca238a39947ab6fcf8cbcff9d0042c91c82b1b356d98d0dbf31d293a6aace->enter($__internal_b85ca238a39947ab6fcf8cbcff9d0042c91c82b1b356d98d0dbf31d293a6aace_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/checkbox_widget.html.php"));

        // line 1
        echo "<input type=\"checkbox\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    <?php if (strlen(\$value) > 0): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?>
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $__internal_47d9307a0895160961f18a155c25377f066d6cde685618a30e4d674698c5f012->leave($__internal_47d9307a0895160961f18a155c25377f066d6cde685618a30e4d674698c5f012_prof);

        
        $__internal_b85ca238a39947ab6fcf8cbcff9d0042c91c82b1b356d98d0dbf31d293a6aace->leave($__internal_b85ca238a39947ab6fcf8cbcff9d0042c91c82b1b356d98d0dbf31d293a6aace_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/checkbox_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<input type=\"checkbox\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    <?php if (strlen(\$value) > 0): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?>
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
", "@Framework/Form/checkbox_widget.html.php", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/checkbox_widget.html.php");
    }
}
