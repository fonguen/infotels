<?php

/* @WebProfiler/Profiler/toolbar_redirect.html.twig */
class __TwigTemplate_1a30439df8ddc570c6af10484fc2edb816c86552ca03cc26aa0640fc87f29c99 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@WebProfiler/Profiler/toolbar_redirect.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_331dca5f3a1fda21311cf1b7c9c8223703f9d93de63e34f79833cef41f7472a0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_331dca5f3a1fda21311cf1b7c9c8223703f9d93de63e34f79833cef41f7472a0->enter($__internal_331dca5f3a1fda21311cf1b7c9c8223703f9d93de63e34f79833cef41f7472a0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/toolbar_redirect.html.twig"));

        $__internal_d10bb1089619230a4b0656617090302708047ea15ca056d1e4fbad8c8cdc4f87 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d10bb1089619230a4b0656617090302708047ea15ca056d1e4fbad8c8cdc4f87->enter($__internal_d10bb1089619230a4b0656617090302708047ea15ca056d1e4fbad8c8cdc4f87_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/toolbar_redirect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_331dca5f3a1fda21311cf1b7c9c8223703f9d93de63e34f79833cef41f7472a0->leave($__internal_331dca5f3a1fda21311cf1b7c9c8223703f9d93de63e34f79833cef41f7472a0_prof);

        
        $__internal_d10bb1089619230a4b0656617090302708047ea15ca056d1e4fbad8c8cdc4f87->leave($__internal_d10bb1089619230a4b0656617090302708047ea15ca056d1e4fbad8c8cdc4f87_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_8af8d0f6bffb37a30925e785d71a398a7b4de2b5e5d9d3b0634c1e237e893c8b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8af8d0f6bffb37a30925e785d71a398a7b4de2b5e5d9d3b0634c1e237e893c8b->enter($__internal_8af8d0f6bffb37a30925e785d71a398a7b4de2b5e5d9d3b0634c1e237e893c8b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_9748f0411bcc1a632eb89aca2a8f9126021019cf5b46a109b715aff91d331392 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9748f0411bcc1a632eb89aca2a8f9126021019cf5b46a109b715aff91d331392->enter($__internal_9748f0411bcc1a632eb89aca2a8f9126021019cf5b46a109b715aff91d331392_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Redirection Intercepted";
        
        $__internal_9748f0411bcc1a632eb89aca2a8f9126021019cf5b46a109b715aff91d331392->leave($__internal_9748f0411bcc1a632eb89aca2a8f9126021019cf5b46a109b715aff91d331392_prof);

        
        $__internal_8af8d0f6bffb37a30925e785d71a398a7b4de2b5e5d9d3b0634c1e237e893c8b->leave($__internal_8af8d0f6bffb37a30925e785d71a398a7b4de2b5e5d9d3b0634c1e237e893c8b_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_7c85a5181571b75916fc04008237719eec3abbac8a8d4eca0d66aad8c9fe84e5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7c85a5181571b75916fc04008237719eec3abbac8a8d4eca0d66aad8c9fe84e5->enter($__internal_7c85a5181571b75916fc04008237719eec3abbac8a8d4eca0d66aad8c9fe84e5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_d3990de55da7da9b848c55cb04e8733981d02351a440219dea6456e947aaba3a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d3990de55da7da9b848c55cb04e8733981d02351a440219dea6456e947aaba3a->enter($__internal_d3990de55da7da9b848c55cb04e8733981d02351a440219dea6456e947aaba3a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, ($context["location"] ?? $this->getContext($context, "location")), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, ($context["location"] ?? $this->getContext($context, "location")), "html", null, true);
        echo "</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
";
        
        $__internal_d3990de55da7da9b848c55cb04e8733981d02351a440219dea6456e947aaba3a->leave($__internal_d3990de55da7da9b848c55cb04e8733981d02351a440219dea6456e947aaba3a_prof);

        
        $__internal_7c85a5181571b75916fc04008237719eec3abbac8a8d4eca0d66aad8c9fe84e5->leave($__internal_7c85a5181571b75916fc04008237719eec3abbac8a8d4eca0d66aad8c9fe84e5_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/toolbar_redirect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 8,  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block title 'Redirection Intercepted' %}

{% block body %}
    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"{{ location }}\">{{ location }}</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
{% endblock %}
", "@WebProfiler/Profiler/toolbar_redirect.html.twig", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/toolbar_redirect.html.twig");
    }
}
