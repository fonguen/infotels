<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_ebdd36ba70293ed99abaa4cff9ff66e5404f25c8540e9af8077cd31a57fbe3fd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_83f3e125e167079dce990182414e108755605b6ffaee25fee106524c0e062df9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_83f3e125e167079dce990182414e108755605b6ffaee25fee106524c0e062df9->enter($__internal_83f3e125e167079dce990182414e108755605b6ffaee25fee106524c0e062df9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $__internal_3768b199bcdeddc7e10efc9431bcfd590d3a7439cabc78774e4dfe2af7b91aca = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3768b199bcdeddc7e10efc9431bcfd590d3a7439cabc78774e4dfe2af7b91aca->enter($__internal_3768b199bcdeddc7e10efc9431bcfd590d3a7439cabc78774e4dfe2af7b91aca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_83f3e125e167079dce990182414e108755605b6ffaee25fee106524c0e062df9->leave($__internal_83f3e125e167079dce990182414e108755605b6ffaee25fee106524c0e062df9_prof);

        
        $__internal_3768b199bcdeddc7e10efc9431bcfd590d3a7439cabc78774e4dfe2af7b91aca->leave($__internal_3768b199bcdeddc7e10efc9431bcfd590d3a7439cabc78774e4dfe2af7b91aca_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_3f571d57f11c40d3128157ee2a5f60c0cdabb6c3e095d3d754a91842301406a5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3f571d57f11c40d3128157ee2a5f60c0cdabb6c3e095d3d754a91842301406a5->enter($__internal_3f571d57f11c40d3128157ee2a5f60c0cdabb6c3e095d3d754a91842301406a5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_f5da33f30fff9a459f1e45f98cd94011e1c638910dff02ec27f3713d37a98b7d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f5da33f30fff9a459f1e45f98cd94011e1c638910dff02ec27f3713d37a98b7d->enter($__internal_f5da33f30fff9a459f1e45f98cd94011e1c638910dff02ec27f3713d37a98b7d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_f5da33f30fff9a459f1e45f98cd94011e1c638910dff02ec27f3713d37a98b7d->leave($__internal_f5da33f30fff9a459f1e45f98cd94011e1c638910dff02ec27f3713d37a98b7d_prof);

        
        $__internal_3f571d57f11c40d3128157ee2a5f60c0cdabb6c3e095d3d754a91842301406a5->leave($__internal_3f571d57f11c40d3128157ee2a5f60c0cdabb6c3e095d3d754a91842301406a5_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_0be5c430d6a9a62fabad3d9dbc1c428122b1f9bb5260325b70ed9b64409f4ec4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0be5c430d6a9a62fabad3d9dbc1c428122b1f9bb5260325b70ed9b64409f4ec4->enter($__internal_0be5c430d6a9a62fabad3d9dbc1c428122b1f9bb5260325b70ed9b64409f4ec4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_6e4fe9298553acd9b5888e7a15ee4021bab0ab9acaeb81d668d1f66f1c55b077 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6e4fe9298553acd9b5888e7a15ee4021bab0ab9acaeb81d668d1f66f1c55b077->enter($__internal_6e4fe9298553acd9b5888e7a15ee4021bab0ab9acaeb81d668d1f66f1c55b077_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_6e4fe9298553acd9b5888e7a15ee4021bab0ab9acaeb81d668d1f66f1c55b077->leave($__internal_6e4fe9298553acd9b5888e7a15ee4021bab0ab9acaeb81d668d1f66f1c55b077_prof);

        
        $__internal_0be5c430d6a9a62fabad3d9dbc1c428122b1f9bb5260325b70ed9b64409f4ec4->leave($__internal_0be5c430d6a9a62fabad3d9dbc1c428122b1f9bb5260325b70ed9b64409f4ec4_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_60b48757d60fd2af1710b07ed89cb115cfd63b7f064e9d237d7e7166d786cc40 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_60b48757d60fd2af1710b07ed89cb115cfd63b7f064e9d237d7e7166d786cc40->enter($__internal_60b48757d60fd2af1710b07ed89cb115cfd63b7f064e9d237d7e7166d786cc40_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_f2688bc91386de1386faec59dade223027c6158d298e4028cd0ab58df6e032c0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f2688bc91386de1386faec59dade223027c6158d298e4028cd0ab58df6e032c0->enter($__internal_f2688bc91386de1386faec59dade223027c6158d298e4028cd0ab58df6e032c0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_f2688bc91386de1386faec59dade223027c6158d298e4028cd0ab58df6e032c0->leave($__internal_f2688bc91386de1386faec59dade223027c6158d298e4028cd0ab58df6e032c0_prof);

        
        $__internal_60b48757d60fd2af1710b07ed89cb115cfd63b7f064e9d237d7e7166d786cc40->leave($__internal_60b48757d60fd2af1710b07ed89cb115cfd63b7f064e9d237d7e7166d786cc40_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/router.html.twig");
    }
}
