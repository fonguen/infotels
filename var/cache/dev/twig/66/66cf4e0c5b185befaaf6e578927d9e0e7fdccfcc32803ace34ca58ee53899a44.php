<?php

/* document/index.html.twig */
class __TwigTemplate_4eb71363f6f1234caf5ea0f8984696f7a7b383caa91b9240e23018bf24a11665 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "document/index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_54a9d4540572d88d224bd1aa33880f920427a4889d76d83211c5f4b5943fc28e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_54a9d4540572d88d224bd1aa33880f920427a4889d76d83211c5f4b5943fc28e->enter($__internal_54a9d4540572d88d224bd1aa33880f920427a4889d76d83211c5f4b5943fc28e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "document/index.html.twig"));

        $__internal_411530189605ed23b5e2974ae4e6301d6e7a294b99a1d949af1b3e66187b8e88 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_411530189605ed23b5e2974ae4e6301d6e7a294b99a1d949af1b3e66187b8e88->enter($__internal_411530189605ed23b5e2974ae4e6301d6e7a294b99a1d949af1b3e66187b8e88_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "document/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_54a9d4540572d88d224bd1aa33880f920427a4889d76d83211c5f4b5943fc28e->leave($__internal_54a9d4540572d88d224bd1aa33880f920427a4889d76d83211c5f4b5943fc28e_prof);

        
        $__internal_411530189605ed23b5e2974ae4e6301d6e7a294b99a1d949af1b3e66187b8e88->leave($__internal_411530189605ed23b5e2974ae4e6301d6e7a294b99a1d949af1b3e66187b8e88_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_c2997f7869db87dec8caf92eb58312d0f9ff27934b5f04aa3428b55704446220 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c2997f7869db87dec8caf92eb58312d0f9ff27934b5f04aa3428b55704446220->enter($__internal_c2997f7869db87dec8caf92eb58312d0f9ff27934b5f04aa3428b55704446220_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_d5188b06f96ee40115c31eb6b89bc9ff3d89b4dac5e706983ce530e8ed06632b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d5188b06f96ee40115c31eb6b89bc9ff3d89b4dac5e706983ce530e8ed06632b->enter($__internal_d5188b06f96ee40115c31eb6b89bc9ff3d89b4dac5e706983ce530e8ed06632b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<div class=\"container\">


    <h4 class=\"list-group-item active\">Documents list</h4>
      <a href=\"";
        // line 8
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_document_new");
        echo "\" class=\"btn btn-large btn-info\">Create a new document</a>

    <table class=\"table\">
        <thead>
            <tr>
                <th>Id</th>
                <th>Nom</th>
                <th>Path</th>
                <th>Type</th>
                <th>Datepublication</th>
                <th>Description</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        ";
        // line 23
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["documents"] ?? $this->getContext($context, "documents")));
        foreach ($context['_seq'] as $context["_key"] => $context["document"]) {
            // line 24
            echo "            <tr>
                <td><a href=\"";
            // line 25
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_document_show", array("id" => $this->getAttribute($context["document"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["document"], "id", array()), "html", null, true);
            echo "</a></td>
                <td>";
            // line 26
            echo twig_escape_filter($this->env, $this->getAttribute($context["document"], "nom", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 27
            echo twig_escape_filter($this->env, $this->getAttribute($context["document"], "path", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 28
            echo twig_escape_filter($this->env, $this->getAttribute($context["document"], "type", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 29
            if ($this->getAttribute($context["document"], "datePublication", array())) {
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["document"], "datePublication", array()), "Y-m-d H:i:s"), "html", null, true);
            }
            echo "</td>
                <td>";
            // line 30
            echo twig_escape_filter($this->env, $this->getAttribute($context["document"], "description", array()), "html", null, true);
            echo "</td>
                <td>

                            <a href=\"";
            // line 33
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_document_show", array("id" => $this->getAttribute($context["document"], "id", array()))), "html", null, true);
            echo "\"  class=\"label label-warning\">show</a>

                            <a href=\"";
            // line 35
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_document_edit", array("id" => $this->getAttribute($context["document"], "id", array()))), "html", null, true);
            echo "\" class=\"label label-danger\" >edit</a>

                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['document'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 40
        echo "        </tbody>
    </table>
</div>
";
        
        $__internal_d5188b06f96ee40115c31eb6b89bc9ff3d89b4dac5e706983ce530e8ed06632b->leave($__internal_d5188b06f96ee40115c31eb6b89bc9ff3d89b4dac5e706983ce530e8ed06632b_prof);

        
        $__internal_c2997f7869db87dec8caf92eb58312d0f9ff27934b5f04aa3428b55704446220->leave($__internal_c2997f7869db87dec8caf92eb58312d0f9ff27934b5f04aa3428b55704446220_prof);

    }

    public function getTemplateName()
    {
        return "document/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  126 => 40,  115 => 35,  110 => 33,  104 => 30,  98 => 29,  94 => 28,  90 => 27,  86 => 26,  80 => 25,  77 => 24,  73 => 23,  55 => 8,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
<div class=\"container\">


    <h4 class=\"list-group-item active\">Documents list</h4>
      <a href=\"{{ path('admin_document_new') }}\" class=\"btn btn-large btn-info\">Create a new document</a>

    <table class=\"table\">
        <thead>
            <tr>
                <th>Id</th>
                <th>Nom</th>
                <th>Path</th>
                <th>Type</th>
                <th>Datepublication</th>
                <th>Description</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        {% for document in documents %}
            <tr>
                <td><a href=\"{{ path('admin_document_show', { 'id': document.id }) }}\">{{ document.id }}</a></td>
                <td>{{ document.nom }}</td>
                <td>{{ document.path }}</td>
                <td>{{ document.type }}</td>
                <td>{% if document.datePublication %}{{ document.datePublication|date('Y-m-d H:i:s') }}{% endif %}</td>
                <td>{{ document.description }}</td>
                <td>

                            <a href=\"{{ path('admin_document_show', { 'id': document.id }) }}\"  class=\"label label-warning\">show</a>

                            <a href=\"{{ path('admin_document_edit', { 'id': document.id }) }}\" class=\"label label-danger\" >edit</a>

                </td>
            </tr>
        {% endfor %}
        </tbody>
    </table>
</div>
{% endblock %}
", "document/index.html.twig", "/home/fonguen/symfony projet/infotels/app/Resources/views/document/index.html.twig");
    }
}
