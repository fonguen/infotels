<?php

/* @Framework/Form/form_label.html.php */
class __TwigTemplate_fcf472df22fba8e0892d42b00db237d26a5538ba493eeac3f2c56d1858a252e0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_039dfc9b6cb39871f9e1e6897a106e0fe4ba0b2d09a31db86cd9e1d78f23bec9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_039dfc9b6cb39871f9e1e6897a106e0fe4ba0b2d09a31db86cd9e1d78f23bec9->enter($__internal_039dfc9b6cb39871f9e1e6897a106e0fe4ba0b2d09a31db86cd9e1d78f23bec9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_label.html.php"));

        $__internal_35ba1aa69e9abe7dae9c668998b2b4781c1f0c8c01ecd4d7372d51abe1c0475a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_35ba1aa69e9abe7dae9c668998b2b4781c1f0c8c01ecd4d7372d51abe1c0475a->enter($__internal_35ba1aa69e9abe7dae9c668998b2b4781c1f0c8c01ecd4d7372d51abe1c0475a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_label.html.php"));

        // line 1
        echo "<?php if (false !== \$label): ?>
<?php if (\$required) { \$label_attr['class'] = trim((isset(\$label_attr['class']) ? \$label_attr['class'] : '').' required'); } ?>
<?php if (!\$compound) { \$label_attr['for'] = \$id; } ?>
<?php if (!\$label) { \$label = isset(\$label_format)
    ? strtr(\$label_format, array('%name%' => \$name, '%id%' => \$id))
    : \$view['form']->humanize(\$name); } ?>
<label <?php foreach (\$label_attr as \$k => \$v) { printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$v)); } ?>><?php echo \$view->escape(false !== \$translation_domain ? \$view['translator']->trans(\$label, array(), \$translation_domain) : \$label) ?></label>
<?php endif ?>
";
        
        $__internal_039dfc9b6cb39871f9e1e6897a106e0fe4ba0b2d09a31db86cd9e1d78f23bec9->leave($__internal_039dfc9b6cb39871f9e1e6897a106e0fe4ba0b2d09a31db86cd9e1d78f23bec9_prof);

        
        $__internal_35ba1aa69e9abe7dae9c668998b2b4781c1f0c8c01ecd4d7372d51abe1c0475a->leave($__internal_35ba1aa69e9abe7dae9c668998b2b4781c1f0c8c01ecd4d7372d51abe1c0475a_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_label.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (false !== \$label): ?>
<?php if (\$required) { \$label_attr['class'] = trim((isset(\$label_attr['class']) ? \$label_attr['class'] : '').' required'); } ?>
<?php if (!\$compound) { \$label_attr['for'] = \$id; } ?>
<?php if (!\$label) { \$label = isset(\$label_format)
    ? strtr(\$label_format, array('%name%' => \$name, '%id%' => \$id))
    : \$view['form']->humanize(\$name); } ?>
<label <?php foreach (\$label_attr as \$k => \$v) { printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$v)); } ?>><?php echo \$view->escape(false !== \$translation_domain ? \$view['translator']->trans(\$label, array(), \$translation_domain) : \$label) ?></label>
<?php endif ?>
", "@Framework/Form/form_label.html.php", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_label.html.php");
    }
}
