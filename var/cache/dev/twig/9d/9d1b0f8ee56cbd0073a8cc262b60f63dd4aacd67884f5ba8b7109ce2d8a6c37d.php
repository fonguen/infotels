<?php

/* niveau/index.html.twig */
class __TwigTemplate_6913e2604d015f8b922352a2f9e6ff5909e29fa8abda44a5a6a58b3639108d60 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "niveau/index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_25b52e7faa3316983cc7d550a24967a47e683ef765d27c7fdd327e7afa4534ad = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_25b52e7faa3316983cc7d550a24967a47e683ef765d27c7fdd327e7afa4534ad->enter($__internal_25b52e7faa3316983cc7d550a24967a47e683ef765d27c7fdd327e7afa4534ad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "niveau/index.html.twig"));

        $__internal_e904d5e372f3902eff26466d5339cddeee28027a72f3bf9da86966210c10dc65 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e904d5e372f3902eff26466d5339cddeee28027a72f3bf9da86966210c10dc65->enter($__internal_e904d5e372f3902eff26466d5339cddeee28027a72f3bf9da86966210c10dc65_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "niveau/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_25b52e7faa3316983cc7d550a24967a47e683ef765d27c7fdd327e7afa4534ad->leave($__internal_25b52e7faa3316983cc7d550a24967a47e683ef765d27c7fdd327e7afa4534ad_prof);

        
        $__internal_e904d5e372f3902eff26466d5339cddeee28027a72f3bf9da86966210c10dc65->leave($__internal_e904d5e372f3902eff26466d5339cddeee28027a72f3bf9da86966210c10dc65_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_50fb7a43555512e390c5742a3c3e31957a73a97f25994b1bfae25bd1e32ec92e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_50fb7a43555512e390c5742a3c3e31957a73a97f25994b1bfae25bd1e32ec92e->enter($__internal_50fb7a43555512e390c5742a3c3e31957a73a97f25994b1bfae25bd1e32ec92e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_169750a4440565c8a780bcbdcdb4b282e7019dc10cd5507f033d790c5878bdd4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_169750a4440565c8a780bcbdcdb4b282e7019dc10cd5507f033d790c5878bdd4->enter($__internal_169750a4440565c8a780bcbdcdb4b282e7019dc10cd5507f033d790c5878bdd4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<br><br>
<div class=\"container\">
    <h2>Niveaus list</h2>
    <a href=\"";
        // line 7
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_niveau_new");
        echo "\" class=\"btn btn-large btn-info\">Create a new niveau</a>


    <table class=\"container\">
        <thead>
            <tr>
                <th>Id</th>
                <th>Nom</th>
                <th>Options</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        ";
        // line 20
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["niveaus"] ?? $this->getContext($context, "niveaus")));
        foreach ($context['_seq'] as $context["_key"] => $context["niveau"]) {
            // line 21
            echo "            <tr>
                <td><a href=\"";
            // line 22
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_niveau_show", array("id" => $this->getAttribute($context["niveau"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["niveau"], "id", array()), "html", null, true);
            echo "</a></td>
                <td>";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute($context["niveau"], "nom", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute($context["niveau"], "options", array()), "html", null, true);
            echo "</td>
                <td>

                            <a href=\"";
            // line 27
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_niveau_show", array("id" => $this->getAttribute($context["niveau"], "id", array()))), "html", null, true);
            echo "\" class=\"label label-warning\">show</a>

                            <a href=\"";
            // line 29
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_niveau_edit", array("id" => $this->getAttribute($context["niveau"], "id", array()))), "html", null, true);
            echo "\" class=\"label label-danger\">edit</a>
                        <
                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['niveau'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 34
        echo "        </tbody>
    </table>

    </div>
";
        
        $__internal_169750a4440565c8a780bcbdcdb4b282e7019dc10cd5507f033d790c5878bdd4->leave($__internal_169750a4440565c8a780bcbdcdb4b282e7019dc10cd5507f033d790c5878bdd4_prof);

        
        $__internal_50fb7a43555512e390c5742a3c3e31957a73a97f25994b1bfae25bd1e32ec92e->leave($__internal_50fb7a43555512e390c5742a3c3e31957a73a97f25994b1bfae25bd1e32ec92e_prof);

    }

    public function getTemplateName()
    {
        return "niveau/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  109 => 34,  98 => 29,  93 => 27,  87 => 24,  83 => 23,  77 => 22,  74 => 21,  70 => 20,  54 => 7,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
<br><br>
<div class=\"container\">
    <h2>Niveaus list</h2>
    <a href=\"{{ path('admin_niveau_new') }}\" class=\"btn btn-large btn-info\">Create a new niveau</a>


    <table class=\"container\">
        <thead>
            <tr>
                <th>Id</th>
                <th>Nom</th>
                <th>Options</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        {% for niveau in niveaus %}
            <tr>
                <td><a href=\"{{ path('admin_niveau_show', { 'id': niveau.id }) }}\">{{ niveau.id }}</a></td>
                <td>{{ niveau.nom }}</td>
                <td>{{ niveau.options }}</td>
                <td>

                            <a href=\"{{ path('admin_niveau_show', { 'id': niveau.id }) }}\" class=\"label label-warning\">show</a>

                            <a href=\"{{ path('admin_niveau_edit', { 'id': niveau.id }) }}\" class=\"label label-danger\">edit</a>
                        <
                </td>
            </tr>
        {% endfor %}
        </tbody>
    </table>

    </div>
{% endblock %}
", "niveau/index.html.twig", "/home/fonguen/symfony projet/infotels/app/Resources/views/niveau/index.html.twig");
    }
}
