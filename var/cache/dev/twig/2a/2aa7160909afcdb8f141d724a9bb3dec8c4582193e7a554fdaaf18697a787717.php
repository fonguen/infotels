<?php

/* @Framework/FormTable/button_row.html.php */
class __TwigTemplate_f3161debb94d4c63d9081c336a893b39ef75176e4d9d6924351c026375c6383c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c3cda8fc24a05f857d2077f25aaf8a8897fc187114dcfb6bd385521eb48aad94 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c3cda8fc24a05f857d2077f25aaf8a8897fc187114dcfb6bd385521eb48aad94->enter($__internal_c3cda8fc24a05f857d2077f25aaf8a8897fc187114dcfb6bd385521eb48aad94_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/button_row.html.php"));

        $__internal_79a7b5b41dbdba68b4241278f9d75ed0a2ddfd9cc9b35e2fde7b9b4bb04e73f8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_79a7b5b41dbdba68b4241278f9d75ed0a2ddfd9cc9b35e2fde7b9b4bb04e73f8->enter($__internal_79a7b5b41dbdba68b4241278f9d75ed0a2ddfd9cc9b35e2fde7b9b4bb04e73f8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/button_row.html.php"));

        // line 1
        echo "<tr>
    <td></td>
    <td>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_c3cda8fc24a05f857d2077f25aaf8a8897fc187114dcfb6bd385521eb48aad94->leave($__internal_c3cda8fc24a05f857d2077f25aaf8a8897fc187114dcfb6bd385521eb48aad94_prof);

        
        $__internal_79a7b5b41dbdba68b4241278f9d75ed0a2ddfd9cc9b35e2fde7b9b4bb04e73f8->leave($__internal_79a7b5b41dbdba68b4241278f9d75ed0a2ddfd9cc9b35e2fde7b9b4bb04e73f8_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<tr>
    <td></td>
    <td>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
", "@Framework/FormTable/button_row.html.php", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/button_row.html.php");
    }
}
