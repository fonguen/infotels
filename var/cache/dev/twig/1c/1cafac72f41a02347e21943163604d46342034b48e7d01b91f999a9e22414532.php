<?php

/* cours/show.html.twig */
class __TwigTemplate_0970fed301408f8e14cc58596560016dbdef4c981e1c64ba2ddeafd66ef7916b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "cours/show.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_89b7cd7d5e31ce2f5b748604d2c61558e02f09e319aaa04e81ccbc81e619158d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_89b7cd7d5e31ce2f5b748604d2c61558e02f09e319aaa04e81ccbc81e619158d->enter($__internal_89b7cd7d5e31ce2f5b748604d2c61558e02f09e319aaa04e81ccbc81e619158d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "cours/show.html.twig"));

        $__internal_ce7c6382366fd614483eabea00ecc52576659acef8c174a14e91fd5f73c0b22a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ce7c6382366fd614483eabea00ecc52576659acef8c174a14e91fd5f73c0b22a->enter($__internal_ce7c6382366fd614483eabea00ecc52576659acef8c174a14e91fd5f73c0b22a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "cours/show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_89b7cd7d5e31ce2f5b748604d2c61558e02f09e319aaa04e81ccbc81e619158d->leave($__internal_89b7cd7d5e31ce2f5b748604d2c61558e02f09e319aaa04e81ccbc81e619158d_prof);

        
        $__internal_ce7c6382366fd614483eabea00ecc52576659acef8c174a14e91fd5f73c0b22a->leave($__internal_ce7c6382366fd614483eabea00ecc52576659acef8c174a14e91fd5f73c0b22a_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_438ee7db768425e36a37092f40385438a299b59d8fd345e9b7dd9cae8b24928d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_438ee7db768425e36a37092f40385438a299b59d8fd345e9b7dd9cae8b24928d->enter($__internal_438ee7db768425e36a37092f40385438a299b59d8fd345e9b7dd9cae8b24928d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_9dbd0f6b5c5b7db2ab24a6e77ea98fd218930301326c7cbf6fde4ede251dac63 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9dbd0f6b5c5b7db2ab24a6e77ea98fd218930301326c7cbf6fde4ede251dac63->enter($__internal_9dbd0f6b5c5b7db2ab24a6e77ea98fd218930301326c7cbf6fde4ede251dac63_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Cour</h1>

    <table>
        <tbody>
            <tr>
                <th>Id</th>
                <td>";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute(($context["cour"] ?? $this->getContext($context, "cour")), "id", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Intitulecours</th>
                <td>";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute(($context["cour"] ?? $this->getContext($context, "cour")), "intituleCours", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Code</th>
                <td>";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute(($context["cour"] ?? $this->getContext($context, "cour")), "code", array()), "html", null, true);
        echo "</td>
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"";
        // line 25
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_cours_index");
        echo "\">Back to the list</a>
        </li>
        <li>
            <a href=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_cours_edit", array("id" => $this->getAttribute(($context["cour"] ?? $this->getContext($context, "cour")), "id", array()))), "html", null, true);
        echo "\">Edit</a>
        </li>
        <li>
            ";
        // line 31
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_start');
        echo "
                <input type=\"submit\" value=\"Delete\">
            ";
        // line 33
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_end');
        echo "
        </li>
    </ul>
";
        
        $__internal_9dbd0f6b5c5b7db2ab24a6e77ea98fd218930301326c7cbf6fde4ede251dac63->leave($__internal_9dbd0f6b5c5b7db2ab24a6e77ea98fd218930301326c7cbf6fde4ede251dac63_prof);

        
        $__internal_438ee7db768425e36a37092f40385438a299b59d8fd345e9b7dd9cae8b24928d->leave($__internal_438ee7db768425e36a37092f40385438a299b59d8fd345e9b7dd9cae8b24928d_prof);

    }

    public function getTemplateName()
    {
        return "cours/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  98 => 33,  93 => 31,  87 => 28,  81 => 25,  71 => 18,  64 => 14,  57 => 10,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Cour</h1>

    <table>
        <tbody>
            <tr>
                <th>Id</th>
                <td>{{ cour.id }}</td>
            </tr>
            <tr>
                <th>Intitulecours</th>
                <td>{{ cour.intituleCours }}</td>
            </tr>
            <tr>
                <th>Code</th>
                <td>{{ cour.code }}</td>
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"{{ path('admin_cours_index') }}\">Back to the list</a>
        </li>
        <li>
            <a href=\"{{ path('admin_cours_edit', { 'id': cour.id }) }}\">Edit</a>
        </li>
        <li>
            {{ form_start(delete_form) }}
                <input type=\"submit\" value=\"Delete\">
            {{ form_end(delete_form) }}
        </li>
    </ul>
{% endblock %}
", "cours/show.html.twig", "/home/fonguen/symfony projet/infotels/app/Resources/views/cours/show.html.twig");
    }
}
