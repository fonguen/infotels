<?php

/* cours/new.html.twig */
class __TwigTemplate_fa1aa84d7778bd1ac04df0df234599cc876f461c6ef569560517a498290267f1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "cours/new.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_115820165d7ab29c15d732db197a711fd6ebc7bf64c8170f6804d927e01f3b50 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_115820165d7ab29c15d732db197a711fd6ebc7bf64c8170f6804d927e01f3b50->enter($__internal_115820165d7ab29c15d732db197a711fd6ebc7bf64c8170f6804d927e01f3b50_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "cours/new.html.twig"));

        $__internal_5fd1b5c87b12d685c138b4992df18220c7514179422a9bdc11d9cea3fd8f8ead = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5fd1b5c87b12d685c138b4992df18220c7514179422a9bdc11d9cea3fd8f8ead->enter($__internal_5fd1b5c87b12d685c138b4992df18220c7514179422a9bdc11d9cea3fd8f8ead_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "cours/new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_115820165d7ab29c15d732db197a711fd6ebc7bf64c8170f6804d927e01f3b50->leave($__internal_115820165d7ab29c15d732db197a711fd6ebc7bf64c8170f6804d927e01f3b50_prof);

        
        $__internal_5fd1b5c87b12d685c138b4992df18220c7514179422a9bdc11d9cea3fd8f8ead->leave($__internal_5fd1b5c87b12d685c138b4992df18220c7514179422a9bdc11d9cea3fd8f8ead_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_2a9c7cd27e7737119eb735588065abc30610ca19d078b7c08a77518948773b35 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2a9c7cd27e7737119eb735588065abc30610ca19d078b7c08a77518948773b35->enter($__internal_2a9c7cd27e7737119eb735588065abc30610ca19d078b7c08a77518948773b35_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_8a41ef7da8efb03df817368441f94414a7f56d98db82571f2aac45828ef64a90 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8a41ef7da8efb03df817368441f94414a7f56d98db82571f2aac45828ef64a90->enter($__internal_8a41ef7da8efb03df817368441f94414a7f56d98db82571f2aac45828ef64a90_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<div class=\"container\">


    <h4 class=\"list-group-item active\">Cour creation</h4>

    ";
        // line 9
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
        ";
        // line 10
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo "
        <input type=\"submit\" value=\"Create\" />
    ";
        // line 12
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
    <br>


            <a href=\"";
        // line 16
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_cours_index");
        echo "\" class=\"btn btn-large btn-info\">Back to the list</a>
</div>
";
        
        $__internal_8a41ef7da8efb03df817368441f94414a7f56d98db82571f2aac45828ef64a90->leave($__internal_8a41ef7da8efb03df817368441f94414a7f56d98db82571f2aac45828ef64a90_prof);

        
        $__internal_2a9c7cd27e7737119eb735588065abc30610ca19d078b7c08a77518948773b35->leave($__internal_2a9c7cd27e7737119eb735588065abc30610ca19d078b7c08a77518948773b35_prof);

    }

    public function getTemplateName()
    {
        return "cours/new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 16,  65 => 12,  60 => 10,  56 => 9,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
<div class=\"container\">


    <h4 class=\"list-group-item active\">Cour creation</h4>

    {{ form_start(form) }}
        {{ form_widget(form) }}
        <input type=\"submit\" value=\"Create\" />
    {{ form_end(form) }}
    <br>


            <a href=\"{{ path('admin_cours_index') }}\" class=\"btn btn-large btn-info\">Back to the list</a>
</div>
{% endblock %}
", "cours/new.html.twig", "/home/fonguen/symfony projet/infotels/app/Resources/views/cours/new.html.twig");
    }
}
