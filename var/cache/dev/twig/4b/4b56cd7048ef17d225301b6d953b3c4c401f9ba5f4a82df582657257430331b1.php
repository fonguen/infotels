<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_1a71dadacce9ebf407743d76f418c214e3fee6c1111cacf1e26c4aa74e5313aa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_86f783c4eb7546108d03c8b52f4ace2e64a4f6c7fc41d176f2c49909f5e87757 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_86f783c4eb7546108d03c8b52f4ace2e64a4f6c7fc41d176f2c49909f5e87757->enter($__internal_86f783c4eb7546108d03c8b52f4ace2e64a4f6c7fc41d176f2c49909f5e87757_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $__internal_27f5f2c5b826912143c4035f364e02d98e3ffd93c26d40e23e2f182aadcb4ef2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_27f5f2c5b826912143c4035f364e02d98e3ffd93c26d40e23e2f182aadcb4ef2->enter($__internal_27f5f2c5b826912143c4035f364e02d98e3ffd93c26d40e23e2f182aadcb4ef2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_86f783c4eb7546108d03c8b52f4ace2e64a4f6c7fc41d176f2c49909f5e87757->leave($__internal_86f783c4eb7546108d03c8b52f4ace2e64a4f6c7fc41d176f2c49909f5e87757_prof);

        
        $__internal_27f5f2c5b826912143c4035f364e02d98e3ffd93c26d40e23e2f182aadcb4ef2->leave($__internal_27f5f2c5b826912143c4035f364e02d98e3ffd93c26d40e23e2f182aadcb4ef2_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_c3cfc0d32022d63ed164bf5265b8696dabd6b0ab02a9daff8540d32e01964b25 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c3cfc0d32022d63ed164bf5265b8696dabd6b0ab02a9daff8540d32e01964b25->enter($__internal_c3cfc0d32022d63ed164bf5265b8696dabd6b0ab02a9daff8540d32e01964b25_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_04abb1fedcc0db6870ff6db356e6cb2271259c68c63c3c10b13e617ddc64b142 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_04abb1fedcc0db6870ff6db356e6cb2271259c68c63c3c10b13e617ddc64b142->enter($__internal_04abb1fedcc0db6870ff6db356e6cb2271259c68c63c3c10b13e617ddc64b142_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_04abb1fedcc0db6870ff6db356e6cb2271259c68c63c3c10b13e617ddc64b142->leave($__internal_04abb1fedcc0db6870ff6db356e6cb2271259c68c63c3c10b13e617ddc64b142_prof);

        
        $__internal_c3cfc0d32022d63ed164bf5265b8696dabd6b0ab02a9daff8540d32e01964b25->leave($__internal_c3cfc0d32022d63ed164bf5265b8696dabd6b0ab02a9daff8540d32e01964b25_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_da849227b8978d3111e5ccef41ccc5a37ae5ffccc83ec54611d426464645efde = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_da849227b8978d3111e5ccef41ccc5a37ae5ffccc83ec54611d426464645efde->enter($__internal_da849227b8978d3111e5ccef41ccc5a37ae5ffccc83ec54611d426464645efde_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_ed2e6bb996eecabb1185e97b24227a5b501164d90e94b1d2ff31dda2c70a7940 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ed2e6bb996eecabb1185e97b24227a5b501164d90e94b1d2ff31dda2c70a7940->enter($__internal_ed2e6bb996eecabb1185e97b24227a5b501164d90e94b1d2ff31dda2c70a7940_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_ed2e6bb996eecabb1185e97b24227a5b501164d90e94b1d2ff31dda2c70a7940->leave($__internal_ed2e6bb996eecabb1185e97b24227a5b501164d90e94b1d2ff31dda2c70a7940_prof);

        
        $__internal_da849227b8978d3111e5ccef41ccc5a37ae5ffccc83ec54611d426464645efde->leave($__internal_da849227b8978d3111e5ccef41ccc5a37ae5ffccc83ec54611d426464645efde_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_f5cdd311f7f0f0b470a6e42825ed036b7b0b4f824dd71d333cff7c60406ea9d3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f5cdd311f7f0f0b470a6e42825ed036b7b0b4f824dd71d333cff7c60406ea9d3->enter($__internal_f5cdd311f7f0f0b470a6e42825ed036b7b0b4f824dd71d333cff7c60406ea9d3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_507d220a788291d90ed51d8e8ad64ea89554104f7ffb78afcf03018dd21e4dc8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_507d220a788291d90ed51d8e8ad64ea89554104f7ffb78afcf03018dd21e4dc8->enter($__internal_507d220a788291d90ed51d8e8ad64ea89554104f7ffb78afcf03018dd21e4dc8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_507d220a788291d90ed51d8e8ad64ea89554104f7ffb78afcf03018dd21e4dc8->leave($__internal_507d220a788291d90ed51d8e8ad64ea89554104f7ffb78afcf03018dd21e4dc8_prof);

        
        $__internal_f5cdd311f7f0f0b470a6e42825ed036b7b0b4f824dd71d333cff7c60406ea9d3->leave($__internal_f5cdd311f7f0f0b470a6e42825ed036b7b0b4f824dd71d333cff7c60406ea9d3_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/router.html.twig");
    }
}
