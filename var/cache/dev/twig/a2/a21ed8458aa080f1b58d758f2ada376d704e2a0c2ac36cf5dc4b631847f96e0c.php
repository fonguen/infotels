<?php

/* departement/show.html.twig */
class __TwigTemplate_939f0e0e024761d4ba3843d796fc4b05ff100d4ba04279321ab43f3b3dae97c1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "departement/show.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_227292c08a48344e1022150ac1ca3a26856470c08508ae66557ad02bf5da69db = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_227292c08a48344e1022150ac1ca3a26856470c08508ae66557ad02bf5da69db->enter($__internal_227292c08a48344e1022150ac1ca3a26856470c08508ae66557ad02bf5da69db_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "departement/show.html.twig"));

        $__internal_b7a0c95f292aea277ad6a67a50b7b895140dfa8dbbf8e9302cc15065d6eb983c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b7a0c95f292aea277ad6a67a50b7b895140dfa8dbbf8e9302cc15065d6eb983c->enter($__internal_b7a0c95f292aea277ad6a67a50b7b895140dfa8dbbf8e9302cc15065d6eb983c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "departement/show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_227292c08a48344e1022150ac1ca3a26856470c08508ae66557ad02bf5da69db->leave($__internal_227292c08a48344e1022150ac1ca3a26856470c08508ae66557ad02bf5da69db_prof);

        
        $__internal_b7a0c95f292aea277ad6a67a50b7b895140dfa8dbbf8e9302cc15065d6eb983c->leave($__internal_b7a0c95f292aea277ad6a67a50b7b895140dfa8dbbf8e9302cc15065d6eb983c_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_69d3f0a1d3a3971e6ad08c2bac700667277581c4134eb858004dac9c3cf548b6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_69d3f0a1d3a3971e6ad08c2bac700667277581c4134eb858004dac9c3cf548b6->enter($__internal_69d3f0a1d3a3971e6ad08c2bac700667277581c4134eb858004dac9c3cf548b6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_70378affca0822c1674a9f648a6903140626c4244f8bdf9e392294d425ab57b4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_70378affca0822c1674a9f648a6903140626c4244f8bdf9e392294d425ab57b4->enter($__internal_70378affca0822c1674a9f648a6903140626c4244f8bdf9e392294d425ab57b4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<br><br>
<div class=\"container\">


    <h1>Departement</h1>

    <table class=\"table\">
        <tbody>
            <tr>
                <th>Id</th>
                <td>";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute(($context["departement"] ?? $this->getContext($context, "departement")), "id", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Nom</th>
                <td>";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute(($context["departement"] ?? $this->getContext($context, "departement")), "nom", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Code</th>
                <td>";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute(($context["departement"] ?? $this->getContext($context, "departement")), "code", array()), "html", null, true);
        echo "</td>
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"";
        // line 29
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_departement_index");
        echo "\" class=\"btn btn-success\">Back to the list</a>
        </li>
        <li>
            <a href=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_departement_edit", array("id" => $this->getAttribute(($context["departement"] ?? $this->getContext($context, "departement")), "id", array()))), "html", null, true);
        echo "\"class=\"btn btn-warning\">Edit</a>
        </li>
        <li>
            ";
        // line 35
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_start');
        echo "
                <input type=\"submit\" value=\"Delete\" class=\"btn btn-danger\">
            ";
        // line 37
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_end');
        echo "
        </li>
    </ul>
  </div>
";
        
        $__internal_70378affca0822c1674a9f648a6903140626c4244f8bdf9e392294d425ab57b4->leave($__internal_70378affca0822c1674a9f648a6903140626c4244f8bdf9e392294d425ab57b4_prof);

        
        $__internal_69d3f0a1d3a3971e6ad08c2bac700667277581c4134eb858004dac9c3cf548b6->leave($__internal_69d3f0a1d3a3971e6ad08c2bac700667277581c4134eb858004dac9c3cf548b6_prof);

    }

    public function getTemplateName()
    {
        return "departement/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  102 => 37,  97 => 35,  91 => 32,  85 => 29,  75 => 22,  68 => 18,  61 => 14,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
<br><br>
<div class=\"container\">


    <h1>Departement</h1>

    <table class=\"table\">
        <tbody>
            <tr>
                <th>Id</th>
                <td>{{ departement.id }}</td>
            </tr>
            <tr>
                <th>Nom</th>
                <td>{{ departement.nom }}</td>
            </tr>
            <tr>
                <th>Code</th>
                <td>{{ departement.code }}</td>
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"{{ path('admin_departement_index') }}\" class=\"btn btn-success\">Back to the list</a>
        </li>
        <li>
            <a href=\"{{ path('admin_departement_edit', { 'id': departement.id }) }}\"class=\"btn btn-warning\">Edit</a>
        </li>
        <li>
            {{ form_start(delete_form) }}
                <input type=\"submit\" value=\"Delete\" class=\"btn btn-danger\">
            {{ form_end(delete_form) }}
        </li>
    </ul>
  </div>
{% endblock %}
", "departement/show.html.twig", "/home/fonguen/symfony projet/infotels/app/Resources/views/departement/show.html.twig");
    }
}
