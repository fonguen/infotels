<?php

/* @Framework/Form/search_widget.html.php */
class __TwigTemplate_5a89ca134e1b34d90e214940f699436d8ad0558cd2ad8be884526e64f33d3a71 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0304f0c0f9ff2e78d61a2be5c364c879f88055b1c58395a37078bacc62daeb36 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0304f0c0f9ff2e78d61a2be5c364c879f88055b1c58395a37078bacc62daeb36->enter($__internal_0304f0c0f9ff2e78d61a2be5c364c879f88055b1c58395a37078bacc62daeb36_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        $__internal_1ad6d2461566522ff2f04284ae85bffd8584205043509161ba1059909e493743 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1ad6d2461566522ff2f04284ae85bffd8584205043509161ba1059909e493743->enter($__internal_1ad6d2461566522ff2f04284ae85bffd8584205043509161ba1059909e493743_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'search')) ?>
";
        
        $__internal_0304f0c0f9ff2e78d61a2be5c364c879f88055b1c58395a37078bacc62daeb36->leave($__internal_0304f0c0f9ff2e78d61a2be5c364c879f88055b1c58395a37078bacc62daeb36_prof);

        
        $__internal_1ad6d2461566522ff2f04284ae85bffd8584205043509161ba1059909e493743->leave($__internal_1ad6d2461566522ff2f04284ae85bffd8584205043509161ba1059909e493743_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/search_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'search')) ?>
", "@Framework/Form/search_widget.html.php", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/search_widget.html.php");
    }
}
