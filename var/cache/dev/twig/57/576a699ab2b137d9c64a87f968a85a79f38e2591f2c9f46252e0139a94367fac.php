<?php

/* @Framework/Form/textarea_widget.html.php */
class __TwigTemplate_7b60534083b335480532bb78ee021ad0a22c4476efdc0a72238137a4e2ab6fc8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0335df75b906d12ce3f7b3dcee7b066f7896529ec5b6980f47748b34f71ddf8b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0335df75b906d12ce3f7b3dcee7b066f7896529ec5b6980f47748b34f71ddf8b->enter($__internal_0335df75b906d12ce3f7b3dcee7b066f7896529ec5b6980f47748b34f71ddf8b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/textarea_widget.html.php"));

        $__internal_76a36f570e1eacbb85a2f6e77b30b5bbb2b3372ce8344587692b600221cebb58 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_76a36f570e1eacbb85a2f6e77b30b5bbb2b3372ce8344587692b600221cebb58->enter($__internal_76a36f570e1eacbb85a2f6e77b30b5bbb2b3372ce8344587692b600221cebb58_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/textarea_widget.html.php"));

        // line 1
        echo "<textarea <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>><?php echo \$view->escape(\$value) ?></textarea>
";
        
        $__internal_0335df75b906d12ce3f7b3dcee7b066f7896529ec5b6980f47748b34f71ddf8b->leave($__internal_0335df75b906d12ce3f7b3dcee7b066f7896529ec5b6980f47748b34f71ddf8b_prof);

        
        $__internal_76a36f570e1eacbb85a2f6e77b30b5bbb2b3372ce8344587692b600221cebb58->leave($__internal_76a36f570e1eacbb85a2f6e77b30b5bbb2b3372ce8344587692b600221cebb58_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/textarea_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<textarea <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>><?php echo \$view->escape(\$value) ?></textarea>
", "@Framework/Form/textarea_widget.html.php", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/textarea_widget.html.php");
    }
}
