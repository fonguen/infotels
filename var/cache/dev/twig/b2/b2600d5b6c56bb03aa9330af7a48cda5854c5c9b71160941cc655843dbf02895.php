<?php

/* @Framework/Form/hidden_row.html.php */
class __TwigTemplate_2611c59e491bdb9c48833946d62e3790d6b9c510c7521dbc947d125f28640f32 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3157433ad6691143358be608b3fe06eac3699e138093891c6d928fa4a3c60ec1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3157433ad6691143358be608b3fe06eac3699e138093891c6d928fa4a3c60ec1->enter($__internal_3157433ad6691143358be608b3fe06eac3699e138093891c6d928fa4a3c60ec1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_row.html.php"));

        $__internal_c01001f12a26985bd57dd0f843abe6e62fb06f41619f25429aa5f7f00bc13381 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c01001f12a26985bd57dd0f843abe6e62fb06f41619f25429aa5f7f00bc13381->enter($__internal_c01001f12a26985bd57dd0f843abe6e62fb06f41619f25429aa5f7f00bc13381_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_row.html.php"));

        // line 1
        echo "<?php echo \$view['form']->widget(\$form) ?>
";
        
        $__internal_3157433ad6691143358be608b3fe06eac3699e138093891c6d928fa4a3c60ec1->leave($__internal_3157433ad6691143358be608b3fe06eac3699e138093891c6d928fa4a3c60ec1_prof);

        
        $__internal_c01001f12a26985bd57dd0f843abe6e62fb06f41619f25429aa5f7f00bc13381->leave($__internal_c01001f12a26985bd57dd0f843abe6e62fb06f41619f25429aa5f7f00bc13381_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->widget(\$form) ?>
", "@Framework/Form/hidden_row.html.php", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/hidden_row.html.php");
    }
}
