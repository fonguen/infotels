<?php

/* @FOSUser/Resetting/check_email.html.twig */
class __TwigTemplate_bda3b87bc1d63c830c8ab860b3d2a71e0121d0a404b2aa39907905e95204c81a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Resetting/check_email.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_31e53bb416536f9eb4a00526a5256b522543ba74a8579e8858ed41fbb6dff072 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_31e53bb416536f9eb4a00526a5256b522543ba74a8579e8858ed41fbb6dff072->enter($__internal_31e53bb416536f9eb4a00526a5256b522543ba74a8579e8858ed41fbb6dff072_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Resetting/check_email.html.twig"));

        $__internal_7ef6198a04a9a5cc2a2ee442a4f4c5b55dda25d522773073a833f177c1256766 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7ef6198a04a9a5cc2a2ee442a4f4c5b55dda25d522773073a833f177c1256766->enter($__internal_7ef6198a04a9a5cc2a2ee442a4f4c5b55dda25d522773073a833f177c1256766_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Resetting/check_email.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_31e53bb416536f9eb4a00526a5256b522543ba74a8579e8858ed41fbb6dff072->leave($__internal_31e53bb416536f9eb4a00526a5256b522543ba74a8579e8858ed41fbb6dff072_prof);

        
        $__internal_7ef6198a04a9a5cc2a2ee442a4f4c5b55dda25d522773073a833f177c1256766->leave($__internal_7ef6198a04a9a5cc2a2ee442a4f4c5b55dda25d522773073a833f177c1256766_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_ebf0abed930d969b4e3c1aed75df662b89f5563ddf1d6569b21d7f942d52ea6b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ebf0abed930d969b4e3c1aed75df662b89f5563ddf1d6569b21d7f942d52ea6b->enter($__internal_ebf0abed930d969b4e3c1aed75df662b89f5563ddf1d6569b21d7f942d52ea6b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_f55c41d76413ea0d99528dae8147a47b5495399dc292be3089d234bb1404df9e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f55c41d76413ea0d99528dae8147a47b5495399dc292be3089d234bb1404df9e->enter($__internal_f55c41d76413ea0d99528dae8147a47b5495399dc292be3089d234bb1404df9e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "<p>
";
        // line 7
        echo nl2br(twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.check_email", array("%tokenLifetime%" => ($context["tokenLifetime"] ?? $this->getContext($context, "tokenLifetime"))), "FOSUserBundle"), "html", null, true));
        echo "
</p>
";
        
        $__internal_f55c41d76413ea0d99528dae8147a47b5495399dc292be3089d234bb1404df9e->leave($__internal_f55c41d76413ea0d99528dae8147a47b5495399dc292be3089d234bb1404df9e_prof);

        
        $__internal_ebf0abed930d969b4e3c1aed75df662b89f5563ddf1d6569b21d7f942d52ea6b->leave($__internal_ebf0abed930d969b4e3c1aed75df662b89f5563ddf1d6569b21d7f942d52ea6b_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Resetting/check_email.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 7,  49 => 6,  40 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% trans_default_domain 'FOSUserBundle' %}

{% block fos_user_content %}
<p>
{{ 'resetting.check_email'|trans({'%tokenLifetime%': tokenLifetime})|nl2br }}
</p>
{% endblock %}
", "@FOSUser/Resetting/check_email.html.twig", "/home/fonguen/symfony projet/infotels/vendor/friendsofsymfony/user-bundle/Resources/views/Resetting/check_email.html.twig");
    }
}
