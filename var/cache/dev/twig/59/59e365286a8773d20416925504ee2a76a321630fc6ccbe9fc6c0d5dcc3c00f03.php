<?php

/* @Framework/Form/attributes.html.php */
class __TwigTemplate_86d336e7c6159819b44d5a42350277ba3434c395973d7c7ad165dc40d4ff83d0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1ca6fd60671a913b72f30655fb3775d03c014bead27064e9c3aeb72d3507e90d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1ca6fd60671a913b72f30655fb3775d03c014bead27064e9c3aeb72d3507e90d->enter($__internal_1ca6fd60671a913b72f30655fb3775d03c014bead27064e9c3aeb72d3507e90d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/attributes.html.php"));

        $__internal_0b06ec3f99c802e8104959a4574b269334b9a79c60068b8d9b30b9fc11f4b420 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0b06ec3f99c802e8104959a4574b269334b9a79c60068b8d9b30b9fc11f4b420->enter($__internal_0b06ec3f99c802e8104959a4574b269334b9a79c60068b8d9b30b9fc11f4b420_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
";
        
        $__internal_1ca6fd60671a913b72f30655fb3775d03c014bead27064e9c3aeb72d3507e90d->leave($__internal_1ca6fd60671a913b72f30655fb3775d03c014bead27064e9c3aeb72d3507e90d_prof);

        
        $__internal_0b06ec3f99c802e8104959a4574b269334b9a79c60068b8d9b30b9fc11f4b420->leave($__internal_0b06ec3f99c802e8104959a4574b269334b9a79c60068b8d9b30b9fc11f4b420_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
", "@Framework/Form/attributes.html.php", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/attributes.html.php");
    }
}
