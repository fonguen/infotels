<?php

/* @FOSUser/Group/new_content.html.twig */
class __TwigTemplate_4efcdeae2e6dbbb6c4e2a5d843c082c54db5f4e737d6d87790ec22bd34ff4b14 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d84cb4f9d992998e87206322e2d3a96a92819b97f28af1c1ee444e319b0630d6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d84cb4f9d992998e87206322e2d3a96a92819b97f28af1c1ee444e319b0630d6->enter($__internal_d84cb4f9d992998e87206322e2d3a96a92819b97f28af1c1ee444e319b0630d6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Group/new_content.html.twig"));

        $__internal_8e9f8689dd19acbbcf69e63a7bdfda53b11c3706298ee1b5890d91eb9737bb8e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8e9f8689dd19acbbcf69e63a7bdfda53b11c3706298ee1b5890d91eb9737bb8e->enter($__internal_8e9f8689dd19acbbcf69e63a7bdfda53b11c3706298ee1b5890d91eb9737bb8e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Group/new_content.html.twig"));

        // line 2
        echo "
";
        // line 3
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start', array("action" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_group_new"), "attr" => array("class" => "fos_user_group_new")));
        echo "
    ";
        // line 4
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo "
    <div>
        <input type=\"submit\" value=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("group.new.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\" />
    </div>
";
        // line 8
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
";
        
        $__internal_d84cb4f9d992998e87206322e2d3a96a92819b97f28af1c1ee444e319b0630d6->leave($__internal_d84cb4f9d992998e87206322e2d3a96a92819b97f28af1c1ee444e319b0630d6_prof);

        
        $__internal_8e9f8689dd19acbbcf69e63a7bdfda53b11c3706298ee1b5890d91eb9737bb8e->leave($__internal_8e9f8689dd19acbbcf69e63a7bdfda53b11c3706298ee1b5890d91eb9737bb8e_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Group/new_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 8,  37 => 6,  32 => 4,  28 => 3,  25 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}

{{ form_start(form, { 'action': path('fos_user_group_new'), 'attr': { 'class': 'fos_user_group_new' } }) }}
    {{ form_widget(form) }}
    <div>
        <input type=\"submit\" value=\"{{ 'group.new.submit'|trans }}\" />
    </div>
{{ form_end(form) }}
", "@FOSUser/Group/new_content.html.twig", "/home/fonguen/symfony projet/infotels/vendor/friendsofsymfony/user-bundle/Resources/views/Group/new_content.html.twig");
    }
}
