<?php

/* @WebProfiler/Collector/exception.html.twig */
class __TwigTemplate_82c0fbbddbd18664184b634e0612ed30aad41511b9b796096c811f8bc292f130 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7ab6e59ce0e9de46ce5c269941bc71a66dc3d3b457baaa9c7561cd4440e9d1dd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7ab6e59ce0e9de46ce5c269941bc71a66dc3d3b457baaa9c7561cd4440e9d1dd->enter($__internal_7ab6e59ce0e9de46ce5c269941bc71a66dc3d3b457baaa9c7561cd4440e9d1dd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $__internal_8a8adebfb4ab02821b246b404cf1753664728efa2d4d062c02d2aca137154c77 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8a8adebfb4ab02821b246b404cf1753664728efa2d4d062c02d2aca137154c77->enter($__internal_8a8adebfb4ab02821b246b404cf1753664728efa2d4d062c02d2aca137154c77_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7ab6e59ce0e9de46ce5c269941bc71a66dc3d3b457baaa9c7561cd4440e9d1dd->leave($__internal_7ab6e59ce0e9de46ce5c269941bc71a66dc3d3b457baaa9c7561cd4440e9d1dd_prof);

        
        $__internal_8a8adebfb4ab02821b246b404cf1753664728efa2d4d062c02d2aca137154c77->leave($__internal_8a8adebfb4ab02821b246b404cf1753664728efa2d4d062c02d2aca137154c77_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_a15aec6916a660a9df14d86a14e12ee893ee2af3b8aead3855daf394231b4779 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a15aec6916a660a9df14d86a14e12ee893ee2af3b8aead3855daf394231b4779->enter($__internal_a15aec6916a660a9df14d86a14e12ee893ee2af3b8aead3855daf394231b4779_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_262386f2d3d30d79ae83bc72538872686e81ec6659a0a5bb663fa0f431e4b89b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_262386f2d3d30d79ae83bc72538872686e81ec6659a0a5bb663fa0f431e4b89b->enter($__internal_262386f2d3d30d79ae83bc72538872686e81ec6659a0a5bb663fa0f431e4b89b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_262386f2d3d30d79ae83bc72538872686e81ec6659a0a5bb663fa0f431e4b89b->leave($__internal_262386f2d3d30d79ae83bc72538872686e81ec6659a0a5bb663fa0f431e4b89b_prof);

        
        $__internal_a15aec6916a660a9df14d86a14e12ee893ee2af3b8aead3855daf394231b4779->leave($__internal_a15aec6916a660a9df14d86a14e12ee893ee2af3b8aead3855daf394231b4779_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_b3bf43d2383b77cdf8aa11163ef96c384d434f4355aaadce1d47b8e23c86b376 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b3bf43d2383b77cdf8aa11163ef96c384d434f4355aaadce1d47b8e23c86b376->enter($__internal_b3bf43d2383b77cdf8aa11163ef96c384d434f4355aaadce1d47b8e23c86b376_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_401b81a536980a5be543e41ec9fd69b43b86d5ebbd0113b8f1a412e0dbf1b820 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_401b81a536980a5be543e41ec9fd69b43b86d5ebbd0113b8f1a412e0dbf1b820->enter($__internal_401b81a536980a5be543e41ec9fd69b43b86d5ebbd0113b8f1a412e0dbf1b820_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo (($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_401b81a536980a5be543e41ec9fd69b43b86d5ebbd0113b8f1a412e0dbf1b820->leave($__internal_401b81a536980a5be543e41ec9fd69b43b86d5ebbd0113b8f1a412e0dbf1b820_prof);

        
        $__internal_b3bf43d2383b77cdf8aa11163ef96c384d434f4355aaadce1d47b8e23c86b376->leave($__internal_b3bf43d2383b77cdf8aa11163ef96c384d434f4355aaadce1d47b8e23c86b376_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_c61e8a998acba43873cb9a13fbf0f6e4972e56e47f658713bb5940c9ed9100d9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c61e8a998acba43873cb9a13fbf0f6e4972e56e47f658713bb5940c9ed9100d9->enter($__internal_c61e8a998acba43873cb9a13fbf0f6e4972e56e47f658713bb5940c9ed9100d9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_781e2b17dcaa4698b14fe991c9df56dedad9781b36ba98f8456c534f5aec3570 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_781e2b17dcaa4698b14fe991c9df56dedad9781b36ba98f8456c534f5aec3570->enter($__internal_781e2b17dcaa4698b14fe991c9df56dedad9781b36ba98f8456c534f5aec3570_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !$this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </div>
    ";
        }
        
        $__internal_781e2b17dcaa4698b14fe991c9df56dedad9781b36ba98f8456c534f5aec3570->leave($__internal_781e2b17dcaa4698b14fe991c9df56dedad9781b36ba98f8456c534f5aec3570_prof);

        
        $__internal_c61e8a998acba43873cb9a13fbf0f6e4972e56e47f658713bb5940c9ed9100d9->leave($__internal_c61e8a998acba43873cb9a13fbf0f6e4972e56e47f658713bb5940c9ed9100d9_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "@WebProfiler/Collector/exception.html.twig", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/exception.html.twig");
    }
}
