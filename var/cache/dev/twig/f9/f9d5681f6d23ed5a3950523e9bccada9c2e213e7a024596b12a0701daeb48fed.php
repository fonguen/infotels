<?php

/* @Framework/Form/button_row.html.php */
class __TwigTemplate_e64bc898ac73521c6dddbf582551beeee761f2b82faa1d5ee869a53297c896f5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4ce24ea1c3a61af0a079b2ea134c28ad1b35e3493e5eea911731feb651de3f24 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4ce24ea1c3a61af0a079b2ea134c28ad1b35e3493e5eea911731feb651de3f24->enter($__internal_4ce24ea1c3a61af0a079b2ea134c28ad1b35e3493e5eea911731feb651de3f24_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        $__internal_64e636034e6ec96d98a0da21b10c5f8f9e6f3ad814e02da37eebedbc618d87fa = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_64e636034e6ec96d98a0da21b10c5f8f9e6f3ad814e02da37eebedbc618d87fa->enter($__internal_64e636034e6ec96d98a0da21b10c5f8f9e6f3ad814e02da37eebedbc618d87fa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_4ce24ea1c3a61af0a079b2ea134c28ad1b35e3493e5eea911731feb651de3f24->leave($__internal_4ce24ea1c3a61af0a079b2ea134c28ad1b35e3493e5eea911731feb651de3f24_prof);

        
        $__internal_64e636034e6ec96d98a0da21b10c5f8f9e6f3ad814e02da37eebedbc618d87fa->leave($__internal_64e636034e6ec96d98a0da21b10c5f8f9e6f3ad814e02da37eebedbc618d87fa_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
", "@Framework/Form/button_row.html.php", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/button_row.html.php");
    }
}
