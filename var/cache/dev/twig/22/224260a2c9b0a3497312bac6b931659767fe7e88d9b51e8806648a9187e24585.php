<?php

/* document/show.html.twig */
class __TwigTemplate_0b0e7de87f724b22330a6be51c96b1f66a58614f5c6809e8d65ff277df98d68e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "document/show.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d2cc8c05b8c16573370dee5886d19660a77fcd37015e41172a0bec0472d50a2d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d2cc8c05b8c16573370dee5886d19660a77fcd37015e41172a0bec0472d50a2d->enter($__internal_d2cc8c05b8c16573370dee5886d19660a77fcd37015e41172a0bec0472d50a2d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "document/show.html.twig"));

        $__internal_0e57bd5ab0da4932f8052fc911e2824b03b78d12d73512c2ee87c4c2773b62e2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0e57bd5ab0da4932f8052fc911e2824b03b78d12d73512c2ee87c4c2773b62e2->enter($__internal_0e57bd5ab0da4932f8052fc911e2824b03b78d12d73512c2ee87c4c2773b62e2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "document/show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d2cc8c05b8c16573370dee5886d19660a77fcd37015e41172a0bec0472d50a2d->leave($__internal_d2cc8c05b8c16573370dee5886d19660a77fcd37015e41172a0bec0472d50a2d_prof);

        
        $__internal_0e57bd5ab0da4932f8052fc911e2824b03b78d12d73512c2ee87c4c2773b62e2->leave($__internal_0e57bd5ab0da4932f8052fc911e2824b03b78d12d73512c2ee87c4c2773b62e2_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_f5047e4eefb2c8161ec6f717949abb13552fa2a879368223eb644b07f6c6ca02 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f5047e4eefb2c8161ec6f717949abb13552fa2a879368223eb644b07f6c6ca02->enter($__internal_f5047e4eefb2c8161ec6f717949abb13552fa2a879368223eb644b07f6c6ca02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_0230f30662c40ff3f3f34bf219b243548e9bbf24f91e8c3c475f292ede3f3e02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0230f30662c40ff3f3f34bf219b243548e9bbf24f91e8c3c475f292ede3f3e02->enter($__internal_0230f30662c40ff3f3f34bf219b243548e9bbf24f91e8c3c475f292ede3f3e02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Document</h1>

    <table>
        <tbody>
            <tr>
                <th>Id</th>
                <td>";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute(($context["document"] ?? $this->getContext($context, "document")), "id", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Nom</th>
                <td>";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute(($context["document"] ?? $this->getContext($context, "document")), "nom", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Path</th>
                <td>";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute(($context["document"] ?? $this->getContext($context, "document")), "path", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Type</th>
                <td>";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute(($context["document"] ?? $this->getContext($context, "document")), "type", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Datepublication</th>
                <td>";
        // line 26
        if ($this->getAttribute(($context["document"] ?? $this->getContext($context, "document")), "datePublication", array())) {
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute(($context["document"] ?? $this->getContext($context, "document")), "datePublication", array()), "Y-m-d H:i:s"), "html", null, true);
        }
        echo "</td>
            </tr>
            <tr>
                <th>Description</th>
                <td>";
        // line 30
        echo twig_escape_filter($this->env, $this->getAttribute(($context["document"] ?? $this->getContext($context, "document")), "description", array()), "html", null, true);
        echo "</td>
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"";
        // line 37
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_document_index");
        echo "\">Back to the list</a>
        </li>
        <li>
            <a href=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_document_edit", array("id" => $this->getAttribute(($context["document"] ?? $this->getContext($context, "document")), "id", array()))), "html", null, true);
        echo "\">Edit</a>
        </li>
        <li>
            ";
        // line 43
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_start');
        echo "
                <input type=\"submit\" value=\"Delete\">
            ";
        // line 45
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_end');
        echo "
        </li>
    </ul>
";
        
        $__internal_0230f30662c40ff3f3f34bf219b243548e9bbf24f91e8c3c475f292ede3f3e02->leave($__internal_0230f30662c40ff3f3f34bf219b243548e9bbf24f91e8c3c475f292ede3f3e02_prof);

        
        $__internal_f5047e4eefb2c8161ec6f717949abb13552fa2a879368223eb644b07f6c6ca02->leave($__internal_f5047e4eefb2c8161ec6f717949abb13552fa2a879368223eb644b07f6c6ca02_prof);

    }

    public function getTemplateName()
    {
        return "document/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  121 => 45,  116 => 43,  110 => 40,  104 => 37,  94 => 30,  85 => 26,  78 => 22,  71 => 18,  64 => 14,  57 => 10,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Document</h1>

    <table>
        <tbody>
            <tr>
                <th>Id</th>
                <td>{{ document.id }}</td>
            </tr>
            <tr>
                <th>Nom</th>
                <td>{{ document.nom }}</td>
            </tr>
            <tr>
                <th>Path</th>
                <td>{{ document.path }}</td>
            </tr>
            <tr>
                <th>Type</th>
                <td>{{ document.type }}</td>
            </tr>
            <tr>
                <th>Datepublication</th>
                <td>{% if document.datePublication %}{{ document.datePublication|date('Y-m-d H:i:s') }}{% endif %}</td>
            </tr>
            <tr>
                <th>Description</th>
                <td>{{ document.description }}</td>
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"{{ path('admin_document_index') }}\">Back to the list</a>
        </li>
        <li>
            <a href=\"{{ path('admin_document_edit', { 'id': document.id }) }}\">Edit</a>
        </li>
        <li>
            {{ form_start(delete_form) }}
                <input type=\"submit\" value=\"Delete\">
            {{ form_end(delete_form) }}
        </li>
    </ul>
{% endblock %}
", "document/show.html.twig", "/home/fonguen/symfony projet/infotels/app/Resources/views/document/show.html.twig");
    }
}
