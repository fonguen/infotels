<?php

/* @Framework/Form/choice_widget_expanded.html.php */
class __TwigTemplate_3d3651894fa5e21eebaafff7559498cdc38bd6c90b589e05432246d98e7bbcb3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c174e722cb8e72d93166ed7cd7057d90fe7276e09f0e42fb2a208711aae2502d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c174e722cb8e72d93166ed7cd7057d90fe7276e09f0e42fb2a208711aae2502d->enter($__internal_c174e722cb8e72d93166ed7cd7057d90fe7276e09f0e42fb2a208711aae2502d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget_expanded.html.php"));

        $__internal_c98dfc8e17ad30d24261619f6a07584f40aa1339af9d6d89acbb1a690a7b392f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c98dfc8e17ad30d24261619f6a07584f40aa1339af9d6d89acbb1a690a7b392f->enter($__internal_c98dfc8e17ad30d24261619f6a07584f40aa1339af9d6d89acbb1a690a7b392f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget_expanded.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
<?php foreach (\$form as \$child): ?>
    <?php echo \$view['form']->widget(\$child) ?>
    <?php echo \$view['form']->label(\$child, null, array('translation_domain' => \$choice_translation_domain)) ?>
<?php endforeach ?>
</div>
";
        
        $__internal_c174e722cb8e72d93166ed7cd7057d90fe7276e09f0e42fb2a208711aae2502d->leave($__internal_c174e722cb8e72d93166ed7cd7057d90fe7276e09f0e42fb2a208711aae2502d_prof);

        
        $__internal_c98dfc8e17ad30d24261619f6a07584f40aa1339af9d6d89acbb1a690a7b392f->leave($__internal_c98dfc8e17ad30d24261619f6a07584f40aa1339af9d6d89acbb1a690a7b392f_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget_expanded.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
<?php foreach (\$form as \$child): ?>
    <?php echo \$view['form']->widget(\$child) ?>
    <?php echo \$view['form']->label(\$child, null, array('translation_domain' => \$choice_translation_domain)) ?>
<?php endforeach ?>
</div>
", "@Framework/Form/choice_widget_expanded.html.php", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choice_widget_expanded.html.php");
    }
}
