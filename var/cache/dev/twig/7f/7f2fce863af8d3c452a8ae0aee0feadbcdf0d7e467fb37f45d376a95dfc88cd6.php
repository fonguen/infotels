<?php

/* @Twig/Exception/exception.rdf.twig */
class __TwigTemplate_c432f3ced93da82ccddc309344bc2021217aad62a3785c393cbc25a2d60f429f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5f5399b3415e4a74ce3a79d9b4b9a062ef3876d8653938f1dff25212c66996a5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5f5399b3415e4a74ce3a79d9b4b9a062ef3876d8653938f1dff25212c66996a5->enter($__internal_5f5399b3415e4a74ce3a79d9b4b9a062ef3876d8653938f1dff25212c66996a5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.rdf.twig"));

        $__internal_e076ef248a027d4e71d5e513eb65e4e0b6e9de82ba0a4d32431595f451539c7c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e076ef248a027d4e71d5e513eb65e4e0b6e9de82ba0a4d32431595f451539c7c->enter($__internal_e076ef248a027d4e71d5e513eb65e4e0b6e9de82ba0a4d32431595f451539c7c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.rdf.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/exception.xml.twig", "@Twig/Exception/exception.rdf.twig", 1)->display(array_merge($context, array("exception" => ($context["exception"] ?? $this->getContext($context, "exception")))));
        
        $__internal_5f5399b3415e4a74ce3a79d9b4b9a062ef3876d8653938f1dff25212c66996a5->leave($__internal_5f5399b3415e4a74ce3a79d9b4b9a062ef3876d8653938f1dff25212c66996a5_prof);

        
        $__internal_e076ef248a027d4e71d5e513eb65e4e0b6e9de82ba0a4d32431595f451539c7c->leave($__internal_e076ef248a027d4e71d5e513eb65e4e0b6e9de82ba0a4d32431595f451539c7c_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% include '@Twig/Exception/exception.xml.twig' with { 'exception': exception } %}
", "@Twig/Exception/exception.rdf.twig", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.rdf.twig");
    }
}
