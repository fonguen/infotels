<?php

/* @Framework/Form/form_rest.html.php */
class __TwigTemplate_94585b7a2d94abadc1bc2d17173def765919d635e67ae00b342a274d64c7652e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bbbc8bbb3906a0d379f91808a06b2c30edb4d5114934520810768257d110deb1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bbbc8bbb3906a0d379f91808a06b2c30edb4d5114934520810768257d110deb1->enter($__internal_bbbc8bbb3906a0d379f91808a06b2c30edb4d5114934520810768257d110deb1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rest.html.php"));

        $__internal_712215474661f4866795c80c8c4d6d5cb0ed487b9065c2b2b31e59a1d1d7308f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_712215474661f4866795c80c8c4d6d5cb0ed487b9065c2b2b31e59a1d1d7308f->enter($__internal_712215474661f4866795c80c8c4d6d5cb0ed487b9065c2b2b31e59a1d1d7308f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rest.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
";
        
        $__internal_bbbc8bbb3906a0d379f91808a06b2c30edb4d5114934520810768257d110deb1->leave($__internal_bbbc8bbb3906a0d379f91808a06b2c30edb4d5114934520810768257d110deb1_prof);

        
        $__internal_712215474661f4866795c80c8c4d6d5cb0ed487b9065c2b2b31e59a1d1d7308f->leave($__internal_712215474661f4866795c80c8c4d6d5cb0ed487b9065c2b2b31e59a1d1d7308f_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rest.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
", "@Framework/Form/form_rest.html.php", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_rest.html.php");
    }
}
