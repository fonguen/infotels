<?php

/* @FOSUser/Security/login.html.twig */
class __TwigTemplate_41bc06055c8a2fde9f7e5164d728c044acadec06d3da7a52abde2ca17bc830aa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Security/login.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_005e186efe6dd897ae602f6d3a464271081b04e07e91480fecc999bb0b069f7a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_005e186efe6dd897ae602f6d3a464271081b04e07e91480fecc999bb0b069f7a->enter($__internal_005e186efe6dd897ae602f6d3a464271081b04e07e91480fecc999bb0b069f7a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login.html.twig"));

        $__internal_f00cca4422b7730b752ced1acbae151b3b086f0125e5d11188e362e246241e2a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f00cca4422b7730b752ced1acbae151b3b086f0125e5d11188e362e246241e2a->enter($__internal_f00cca4422b7730b752ced1acbae151b3b086f0125e5d11188e362e246241e2a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_005e186efe6dd897ae602f6d3a464271081b04e07e91480fecc999bb0b069f7a->leave($__internal_005e186efe6dd897ae602f6d3a464271081b04e07e91480fecc999bb0b069f7a_prof);

        
        $__internal_f00cca4422b7730b752ced1acbae151b3b086f0125e5d11188e362e246241e2a->leave($__internal_f00cca4422b7730b752ced1acbae151b3b086f0125e5d11188e362e246241e2a_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_ff72cd76c91db7275bade84f1ac603a04ac7bde5af6727bd09faafbf55a9ff70 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ff72cd76c91db7275bade84f1ac603a04ac7bde5af6727bd09faafbf55a9ff70->enter($__internal_ff72cd76c91db7275bade84f1ac603a04ac7bde5af6727bd09faafbf55a9ff70_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_56cefc7c15acf4c99f23693fd3efb4f0ba125ab380ad3d77b31e049df9be2435 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_56cefc7c15acf4c99f23693fd3efb4f0ba125ab380ad3d77b31e049df9be2435->enter($__internal_56cefc7c15acf4c99f23693fd3efb4f0ba125ab380ad3d77b31e049df9be2435_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        echo "    ";
        echo twig_include($this->env, $context, "@FOSUser/Security/login_content.html.twig");
        echo "
";
        
        $__internal_56cefc7c15acf4c99f23693fd3efb4f0ba125ab380ad3d77b31e049df9be2435->leave($__internal_56cefc7c15acf4c99f23693fd3efb4f0ba125ab380ad3d77b31e049df9be2435_prof);

        
        $__internal_ff72cd76c91db7275bade84f1ac603a04ac7bde5af6727bd09faafbf55a9ff70->leave($__internal_ff72cd76c91db7275bade84f1ac603a04ac7bde5af6727bd09faafbf55a9ff70_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Security/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
    {{ include('@FOSUser/Security/login_content.html.twig') }}
{% endblock fos_user_content %}
", "@FOSUser/Security/login.html.twig", "/home/fonguen/symfony projet/infotels/vendor/friendsofsymfony/user-bundle/Resources/views/Security/login.html.twig");
    }
}
