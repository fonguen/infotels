<?php

/* formation/index.html.twig */
class __TwigTemplate_79fed2863c2d2daff6b221a3acf4df7f95f7ebd2e712f55be6106b35be3a599b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "formation/index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_dd62f282d1b42eb3ad0425ed694ae328d71ca133e2405cb0befdd30575f2630a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dd62f282d1b42eb3ad0425ed694ae328d71ca133e2405cb0befdd30575f2630a->enter($__internal_dd62f282d1b42eb3ad0425ed694ae328d71ca133e2405cb0befdd30575f2630a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "formation/index.html.twig"));

        $__internal_d52738e5bc7a93359eb88828673e9b4ee712e061df09bd06ceaac5379a3773e8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d52738e5bc7a93359eb88828673e9b4ee712e061df09bd06ceaac5379a3773e8->enter($__internal_d52738e5bc7a93359eb88828673e9b4ee712e061df09bd06ceaac5379a3773e8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "formation/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_dd62f282d1b42eb3ad0425ed694ae328d71ca133e2405cb0befdd30575f2630a->leave($__internal_dd62f282d1b42eb3ad0425ed694ae328d71ca133e2405cb0befdd30575f2630a_prof);

        
        $__internal_d52738e5bc7a93359eb88828673e9b4ee712e061df09bd06ceaac5379a3773e8->leave($__internal_d52738e5bc7a93359eb88828673e9b4ee712e061df09bd06ceaac5379a3773e8_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_8283d9f4d2dc43f9e2397490a96083d5101630b6661ce26a2abc43acef44dd30 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8283d9f4d2dc43f9e2397490a96083d5101630b6661ce26a2abc43acef44dd30->enter($__internal_8283d9f4d2dc43f9e2397490a96083d5101630b6661ce26a2abc43acef44dd30_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_8108be0b3ad5b6417acde0f1909f395afc28fcdcf7543ccafdab814f06b0746b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8108be0b3ad5b6417acde0f1909f395afc28fcdcf7543ccafdab814f06b0746b->enter($__internal_8108be0b3ad5b6417acde0f1909f395afc28fcdcf7543ccafdab814f06b0746b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<br><br><br>
<div class=\"container\">


    <h2>Formations list</h2>
    <a href=\"";
        // line 9
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_formation_new");
        echo "\" class=\"btn btn-large btn-info\">Create a new formation</a>

    <table class=\"table\">
        <thead>
            <tr>
                <th>Id</th>
                <th>Nom</th>
                <th>Description</th>
                <th>Path</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        ";
        // line 22
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["formations"] ?? $this->getContext($context, "formations")));
        foreach ($context['_seq'] as $context["_key"] => $context["formation"]) {
            // line 23
            echo "            <tr>
                <td><a href=\"";
            // line 24
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_formation_show", array("id" => $this->getAttribute($context["formation"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["formation"], "id", array()), "html", null, true);
            echo "</a></td>
                <td>";
            // line 25
            echo twig_escape_filter($this->env, $this->getAttribute($context["formation"], "nom", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 26
            echo twig_escape_filter($this->env, $this->getAttribute($context["formation"], "description", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 27
            echo twig_escape_filter($this->env, $this->getAttribute($context["formation"], "path", array()), "html", null, true);
            echo "</td>
                <td>

                      <a href=\"";
            // line 30
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_formation_show", array("id" => $this->getAttribute($context["formation"], "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-warning\">show</a>

                      <a href=\"";
            // line 32
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_formation_edit", array("id" => $this->getAttribute($context["formation"], "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-danger\">edit</a>

                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['formation'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 37
        echo "        </tbody>
    </table>

</div>
";
        
        $__internal_8108be0b3ad5b6417acde0f1909f395afc28fcdcf7543ccafdab814f06b0746b->leave($__internal_8108be0b3ad5b6417acde0f1909f395afc28fcdcf7543ccafdab814f06b0746b_prof);

        
        $__internal_8283d9f4d2dc43f9e2397490a96083d5101630b6661ce26a2abc43acef44dd30->leave($__internal_8283d9f4d2dc43f9e2397490a96083d5101630b6661ce26a2abc43acef44dd30_prof);

    }

    public function getTemplateName()
    {
        return "formation/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  115 => 37,  104 => 32,  99 => 30,  93 => 27,  89 => 26,  85 => 25,  79 => 24,  76 => 23,  72 => 22,  56 => 9,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
<br><br><br>
<div class=\"container\">


    <h2>Formations list</h2>
    <a href=\"{{ path('admin_formation_new') }}\" class=\"btn btn-large btn-info\">Create a new formation</a>

    <table class=\"table\">
        <thead>
            <tr>
                <th>Id</th>
                <th>Nom</th>
                <th>Description</th>
                <th>Path</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        {% for formation in formations %}
            <tr>
                <td><a href=\"{{ path('admin_formation_show', { 'id': formation.id }) }}\">{{ formation.id }}</a></td>
                <td>{{ formation.nom }}</td>
                <td>{{ formation.description }}</td>
                <td>{{ formation.path }}</td>
                <td>

                      <a href=\"{{ path('admin_formation_show', { 'id': formation.id }) }}\" class=\"btn btn-warning\">show</a>

                      <a href=\"{{ path('admin_formation_edit', { 'id': formation.id }) }}\" class=\"btn btn-danger\">edit</a>

                </td>
            </tr>
        {% endfor %}
        </tbody>
    </table>

</div>
{% endblock %}
", "formation/index.html.twig", "/home/fonguen/symfony projet/infotels/app/Resources/views/formation/index.html.twig");
    }
}
