<?php

/* @FOSUser/Profile/edit.html.twig */
class __TwigTemplate_0fddfdaaed1e32a1fba62715f8fd6f0f68d916b8e86e7f21cfbdb6989cd3d0c1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Profile/edit.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c4779101f8c434a1e7dd91c26b207efdf548152919c77ad7b0547c061c566c3d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c4779101f8c434a1e7dd91c26b207efdf548152919c77ad7b0547c061c566c3d->enter($__internal_c4779101f8c434a1e7dd91c26b207efdf548152919c77ad7b0547c061c566c3d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Profile/edit.html.twig"));

        $__internal_2a0326947f6421872038a1d4ae892c12fd1c4e682cf7bc7e816de95cbf5b41da = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2a0326947f6421872038a1d4ae892c12fd1c4e682cf7bc7e816de95cbf5b41da->enter($__internal_2a0326947f6421872038a1d4ae892c12fd1c4e682cf7bc7e816de95cbf5b41da_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Profile/edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c4779101f8c434a1e7dd91c26b207efdf548152919c77ad7b0547c061c566c3d->leave($__internal_c4779101f8c434a1e7dd91c26b207efdf548152919c77ad7b0547c061c566c3d_prof);

        
        $__internal_2a0326947f6421872038a1d4ae892c12fd1c4e682cf7bc7e816de95cbf5b41da->leave($__internal_2a0326947f6421872038a1d4ae892c12fd1c4e682cf7bc7e816de95cbf5b41da_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_d0cec8c3991e2de7fe99aa432a0fcac1df572779c74999f1132c343e6fde1ee6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d0cec8c3991e2de7fe99aa432a0fcac1df572779c74999f1132c343e6fde1ee6->enter($__internal_d0cec8c3991e2de7fe99aa432a0fcac1df572779c74999f1132c343e6fde1ee6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_f2cfa98c6a54bf90b47400f608bc7fbf678945ce91bb02391572797f1e225257 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f2cfa98c6a54bf90b47400f608bc7fbf678945ce91bb02391572797f1e225257->enter($__internal_f2cfa98c6a54bf90b47400f608bc7fbf678945ce91bb02391572797f1e225257_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Profile/edit_content.html.twig", "@FOSUser/Profile/edit.html.twig", 4)->display($context);
        
        $__internal_f2cfa98c6a54bf90b47400f608bc7fbf678945ce91bb02391572797f1e225257->leave($__internal_f2cfa98c6a54bf90b47400f608bc7fbf678945ce91bb02391572797f1e225257_prof);

        
        $__internal_d0cec8c3991e2de7fe99aa432a0fcac1df572779c74999f1132c343e6fde1ee6->leave($__internal_d0cec8c3991e2de7fe99aa432a0fcac1df572779c74999f1132c343e6fde1ee6_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Profile/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Profile/edit_content.html.twig\" %}
{% endblock fos_user_content %}
", "@FOSUser/Profile/edit.html.twig", "/home/fonguen/symfony projet/infotels/vendor/friendsofsymfony/user-bundle/Resources/views/Profile/edit.html.twig");
    }
}
