<?php

/* @WebProfiler/Collector/ajax.html.twig */
class __TwigTemplate_8593ce2fb331cf97038070ec06960b6e4e12fcb54ec2fff19bee11a3dbec615e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/ajax.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9573a17b8137116701721264ece83b5b4efbf89fdc68582277bde9bcba3c2781 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9573a17b8137116701721264ece83b5b4efbf89fdc68582277bde9bcba3c2781->enter($__internal_9573a17b8137116701721264ece83b5b4efbf89fdc68582277bde9bcba3c2781_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/ajax.html.twig"));

        $__internal_b42a1996e9f59b11c38df2c3893113fda342ff4b3d6f23ae2b493a642a6203b1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b42a1996e9f59b11c38df2c3893113fda342ff4b3d6f23ae2b493a642a6203b1->enter($__internal_b42a1996e9f59b11c38df2c3893113fda342ff4b3d6f23ae2b493a642a6203b1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/ajax.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9573a17b8137116701721264ece83b5b4efbf89fdc68582277bde9bcba3c2781->leave($__internal_9573a17b8137116701721264ece83b5b4efbf89fdc68582277bde9bcba3c2781_prof);

        
        $__internal_b42a1996e9f59b11c38df2c3893113fda342ff4b3d6f23ae2b493a642a6203b1->leave($__internal_b42a1996e9f59b11c38df2c3893113fda342ff4b3d6f23ae2b493a642a6203b1_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_da6076a9afdb9b02b0d691a486db6ef41a35595b9e4713c0ba8d77379877385d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_da6076a9afdb9b02b0d691a486db6ef41a35595b9e4713c0ba8d77379877385d->enter($__internal_da6076a9afdb9b02b0d691a486db6ef41a35595b9e4713c0ba8d77379877385d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_d1a85bb3a24970fb8ce7a7cd3354df552d0da1542eb340234361800abea950b2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d1a85bb3a24970fb8ce7a7cd3354df552d0da1542eb340234361800abea950b2->enter($__internal_d1a85bb3a24970fb8ce7a7cd3354df552d0da1542eb340234361800abea950b2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        // line 4
        echo "    ";
        ob_start();
        // line 5
        echo "        ";
        echo twig_include($this->env, $context, "@WebProfiler/Icon/ajax.svg");
        echo "
        <span class=\"sf-toolbar-value sf-toolbar-ajax-requests\">0</span>
    ";
        $context["icon"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 8
        echo "
    ";
        // line 9
        $context["text"] = ('' === $tmp = "        <div class=\"sf-toolbar-info-piece\">
            <b class=\"sf-toolbar-ajax-info\"></b>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <table class=\"sf-toolbar-ajax-requests\">
                <thead>
                    <tr>
                        <th>Method</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>URL</th>
                        <th>Time</th>
                        <th>Profile</th>
                    </tr>
                </thead>
                <tbody class=\"sf-toolbar-ajax-request-list\"></tbody>
            </table>
        </div>
    ") ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 29
        echo "
    ";
        // line 30
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/toolbar_item.html.twig", array("link" => false));
        echo "
";
        
        $__internal_d1a85bb3a24970fb8ce7a7cd3354df552d0da1542eb340234361800abea950b2->leave($__internal_d1a85bb3a24970fb8ce7a7cd3354df552d0da1542eb340234361800abea950b2_prof);

        
        $__internal_da6076a9afdb9b02b0d691a486db6ef41a35595b9e4713c0ba8d77379877385d->leave($__internal_da6076a9afdb9b02b0d691a486db6ef41a35595b9e4713c0ba8d77379877385d_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/ajax.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 30,  82 => 29,  62 => 9,  59 => 8,  52 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}
    {% set icon %}
        {{ include('@WebProfiler/Icon/ajax.svg') }}
        <span class=\"sf-toolbar-value sf-toolbar-ajax-requests\">0</span>
    {% endset %}

    {% set text %}
        <div class=\"sf-toolbar-info-piece\">
            <b class=\"sf-toolbar-ajax-info\"></b>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <table class=\"sf-toolbar-ajax-requests\">
                <thead>
                    <tr>
                        <th>Method</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>URL</th>
                        <th>Time</th>
                        <th>Profile</th>
                    </tr>
                </thead>
                <tbody class=\"sf-toolbar-ajax-request-list\"></tbody>
            </table>
        </div>
    {% endset %}

    {{ include('@WebProfiler/Profiler/toolbar_item.html.twig', { link: false }) }}
{% endblock %}
", "@WebProfiler/Collector/ajax.html.twig", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/ajax.html.twig");
    }
}
