<?php

/* @Framework/Form/form_enctype.html.php */
class __TwigTemplate_a9f48008ebcec8de0410d71fa0ce5b0564817ca774095f94660b14a0f7f4209c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6eeae890600a8285516ef83b32c47bde6dca20de6debd7f558189c84aaa215c3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6eeae890600a8285516ef83b32c47bde6dca20de6debd7f558189c84aaa215c3->enter($__internal_6eeae890600a8285516ef83b32c47bde6dca20de6debd7f558189c84aaa215c3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_enctype.html.php"));

        $__internal_6da41cc6501e68505ee24a99797efc6db7f8616bb018b1a5076db63fe5c83cc1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6da41cc6501e68505ee24a99797efc6db7f8616bb018b1a5076db63fe5c83cc1->enter($__internal_6da41cc6501e68505ee24a99797efc6db7f8616bb018b1a5076db63fe5c83cc1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_enctype.html.php"));

        // line 1
        echo "<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
";
        
        $__internal_6eeae890600a8285516ef83b32c47bde6dca20de6debd7f558189c84aaa215c3->leave($__internal_6eeae890600a8285516ef83b32c47bde6dca20de6debd7f558189c84aaa215c3_prof);

        
        $__internal_6da41cc6501e68505ee24a99797efc6db7f8616bb018b1a5076db63fe5c83cc1->leave($__internal_6da41cc6501e68505ee24a99797efc6db7f8616bb018b1a5076db63fe5c83cc1_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_enctype.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
", "@Framework/Form/form_enctype.html.php", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_enctype.html.php");
    }
}
