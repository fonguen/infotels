<?php

/* @Framework/Form/submit_widget.html.php */
class __TwigTemplate_9cf7a8b83182fe7e6c850a694f2ac098138a7afa3fa042a2dd3130e55f540645 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6a610d28ff08da12d44840b93211f0b6a7b92eadad92ba418e1a0d41a93d251a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6a610d28ff08da12d44840b93211f0b6a7b92eadad92ba418e1a0d41a93d251a->enter($__internal_6a610d28ff08da12d44840b93211f0b6a7b92eadad92ba418e1a0d41a93d251a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/submit_widget.html.php"));

        $__internal_eadae3f37e78adcabe6054859301b433a2749c532d55a6e5da26af459f237b3d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_eadae3f37e78adcabe6054859301b433a2749c532d55a6e5da26af459f237b3d->enter($__internal_eadae3f37e78adcabe6054859301b433a2749c532d55a6e5da26af459f237b3d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/submit_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'submit')) ?>
";
        
        $__internal_6a610d28ff08da12d44840b93211f0b6a7b92eadad92ba418e1a0d41a93d251a->leave($__internal_6a610d28ff08da12d44840b93211f0b6a7b92eadad92ba418e1a0d41a93d251a_prof);

        
        $__internal_eadae3f37e78adcabe6054859301b433a2749c532d55a6e5da26af459f237b3d->leave($__internal_eadae3f37e78adcabe6054859301b433a2749c532d55a6e5da26af459f237b3d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/submit_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'submit')) ?>
", "@Framework/Form/submit_widget.html.php", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/submit_widget.html.php");
    }
}
