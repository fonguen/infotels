<?php

/* @Framework/Form/percent_widget.html.php */
class __TwigTemplate_f72da7b4fb4e2cff9383981c082ff59341cc9999a17f597e8ffb92b567321b98 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_83840f8d3a9098bd8a6fc0cd53af823802ad82564dd77701160254cd98ec080a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_83840f8d3a9098bd8a6fc0cd53af823802ad82564dd77701160254cd98ec080a->enter($__internal_83840f8d3a9098bd8a6fc0cd53af823802ad82564dd77701160254cd98ec080a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/percent_widget.html.php"));

        $__internal_e84f8240d933f358156003e17aa2d775aa50f799295de2cfbeee13283e0d4255 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e84f8240d933f358156003e17aa2d775aa50f799295de2cfbeee13283e0d4255->enter($__internal_e84f8240d933f358156003e17aa2d775aa50f799295de2cfbeee13283e0d4255_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/percent_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?> %
";
        
        $__internal_83840f8d3a9098bd8a6fc0cd53af823802ad82564dd77701160254cd98ec080a->leave($__internal_83840f8d3a9098bd8a6fc0cd53af823802ad82564dd77701160254cd98ec080a_prof);

        
        $__internal_e84f8240d933f358156003e17aa2d775aa50f799295de2cfbeee13283e0d4255->leave($__internal_e84f8240d933f358156003e17aa2d775aa50f799295de2cfbeee13283e0d4255_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/percent_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?> %
", "@Framework/Form/percent_widget.html.php", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/percent_widget.html.php");
    }
}
