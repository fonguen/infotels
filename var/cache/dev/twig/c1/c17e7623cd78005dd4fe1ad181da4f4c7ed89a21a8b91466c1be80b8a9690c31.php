<?php

/* @Twig/Exception/error.atom.twig */
class __TwigTemplate_856e09e24948105c30c7e2ea336c0032950cc0b8cd03e07bbe70dd4f5fdb3418 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_53585c0f6981a68c7c8530c696f68d60011a65f0703596fa173e7f2d7e0e030a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_53585c0f6981a68c7c8530c696f68d60011a65f0703596fa173e7f2d7e0e030a->enter($__internal_53585c0f6981a68c7c8530c696f68d60011a65f0703596fa173e7f2d7e0e030a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.atom.twig"));

        $__internal_4a332f3a1be1d07aebce26d1b907e46d2c0122dc9a7f0abc428402662bb77b4b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4a332f3a1be1d07aebce26d1b907e46d2c0122dc9a7f0abc428402662bb77b4b->enter($__internal_4a332f3a1be1d07aebce26d1b907e46d2c0122dc9a7f0abc428402662bb77b4b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.atom.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/error.xml.twig", "@Twig/Exception/error.atom.twig", 1)->display($context);
        
        $__internal_53585c0f6981a68c7c8530c696f68d60011a65f0703596fa173e7f2d7e0e030a->leave($__internal_53585c0f6981a68c7c8530c696f68d60011a65f0703596fa173e7f2d7e0e030a_prof);

        
        $__internal_4a332f3a1be1d07aebce26d1b907e46d2c0122dc9a7f0abc428402662bb77b4b->leave($__internal_4a332f3a1be1d07aebce26d1b907e46d2c0122dc9a7f0abc428402662bb77b4b_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.atom.twig";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% include '@Twig/Exception/error.xml.twig' %}
", "@Twig/Exception/error.atom.twig", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.atom.twig");
    }
}
