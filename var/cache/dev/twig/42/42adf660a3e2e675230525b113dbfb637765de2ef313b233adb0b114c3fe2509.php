<?php

/* @Framework/Form/form_errors.html.php */
class __TwigTemplate_474d8c700dd2e812983328cac81eb540b8f10dae0efb1605844bbae86650d671 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2945d89a8ff9c394d8abf151c577389a75c458493b36dc44cfa002a8134efbd8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2945d89a8ff9c394d8abf151c577389a75c458493b36dc44cfa002a8134efbd8->enter($__internal_2945d89a8ff9c394d8abf151c577389a75c458493b36dc44cfa002a8134efbd8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_errors.html.php"));

        $__internal_98a0b6858fc80eaad8e2596938403dd1ff12f3319e4d6218446b992a5f1f2b02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_98a0b6858fc80eaad8e2596938403dd1ff12f3319e4d6218446b992a5f1f2b02->enter($__internal_98a0b6858fc80eaad8e2596938403dd1ff12f3319e4d6218446b992a5f1f2b02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_errors.html.php"));

        // line 1
        echo "<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
";
        
        $__internal_2945d89a8ff9c394d8abf151c577389a75c458493b36dc44cfa002a8134efbd8->leave($__internal_2945d89a8ff9c394d8abf151c577389a75c458493b36dc44cfa002a8134efbd8_prof);

        
        $__internal_98a0b6858fc80eaad8e2596938403dd1ff12f3319e4d6218446b992a5f1f2b02->leave($__internal_98a0b6858fc80eaad8e2596938403dd1ff12f3319e4d6218446b992a5f1f2b02_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_errors.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
", "@Framework/Form/form_errors.html.php", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_errors.html.php");
    }
}
