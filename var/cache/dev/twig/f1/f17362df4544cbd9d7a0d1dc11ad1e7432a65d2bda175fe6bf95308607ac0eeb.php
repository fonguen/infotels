<?php

/* @Twig/Exception/error.rdf.twig */
class __TwigTemplate_7540b80184a120883fe7618bcd9bd55da383b98c13ed6cae018f6f8db90ee8c1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_783c6c6d9d924afe1379a0e29a8e832fb41ffb1c46a8c3d409b93a53f1601823 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_783c6c6d9d924afe1379a0e29a8e832fb41ffb1c46a8c3d409b93a53f1601823->enter($__internal_783c6c6d9d924afe1379a0e29a8e832fb41ffb1c46a8c3d409b93a53f1601823_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.rdf.twig"));

        $__internal_5dbc2c8f554d597ed03fdfe443c68d6d4dab0692ca42c93d2bf4a34667fa9541 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5dbc2c8f554d597ed03fdfe443c68d6d4dab0692ca42c93d2bf4a34667fa9541->enter($__internal_5dbc2c8f554d597ed03fdfe443c68d6d4dab0692ca42c93d2bf4a34667fa9541_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.rdf.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/error.xml.twig", "@Twig/Exception/error.rdf.twig", 1)->display($context);
        
        $__internal_783c6c6d9d924afe1379a0e29a8e832fb41ffb1c46a8c3d409b93a53f1601823->leave($__internal_783c6c6d9d924afe1379a0e29a8e832fb41ffb1c46a8c3d409b93a53f1601823_prof);

        
        $__internal_5dbc2c8f554d597ed03fdfe443c68d6d4dab0692ca42c93d2bf4a34667fa9541->leave($__internal_5dbc2c8f554d597ed03fdfe443c68d6d4dab0692ca42c93d2bf4a34667fa9541_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.rdf.twig";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% include '@Twig/Exception/error.xml.twig' %}
", "@Twig/Exception/error.rdf.twig", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.rdf.twig");
    }
}
