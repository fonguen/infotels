<?php

/* evenement/index.html.twig */
class __TwigTemplate_f94d90ee206a150ba3a2444022dfd8a89699a25e33796dc4373328bd7edab09f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "evenement/index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c6e17d6fa4ffe492785014efb6c735e757f40ad06d3ae8326c879ddda14793fc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c6e17d6fa4ffe492785014efb6c735e757f40ad06d3ae8326c879ddda14793fc->enter($__internal_c6e17d6fa4ffe492785014efb6c735e757f40ad06d3ae8326c879ddda14793fc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "evenement/index.html.twig"));

        $__internal_41136578f0efdfcc62faa5a0d2ea90a96494b6125c5a666c48898e34b76951ee = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_41136578f0efdfcc62faa5a0d2ea90a96494b6125c5a666c48898e34b76951ee->enter($__internal_41136578f0efdfcc62faa5a0d2ea90a96494b6125c5a666c48898e34b76951ee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "evenement/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c6e17d6fa4ffe492785014efb6c735e757f40ad06d3ae8326c879ddda14793fc->leave($__internal_c6e17d6fa4ffe492785014efb6c735e757f40ad06d3ae8326c879ddda14793fc_prof);

        
        $__internal_41136578f0efdfcc62faa5a0d2ea90a96494b6125c5a666c48898e34b76951ee->leave($__internal_41136578f0efdfcc62faa5a0d2ea90a96494b6125c5a666c48898e34b76951ee_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_0dc6f9075bc054885a63f4d90408dfccea92e40ccd3b81dc3cdecb90f777ea85 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0dc6f9075bc054885a63f4d90408dfccea92e40ccd3b81dc3cdecb90f777ea85->enter($__internal_0dc6f9075bc054885a63f4d90408dfccea92e40ccd3b81dc3cdecb90f777ea85_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_7024310fcbc447d4690bd567d03289d29affd728998c297763b5f72f81e8b0c9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7024310fcbc447d4690bd567d03289d29affd728998c297763b5f72f81e8b0c9->enter($__internal_7024310fcbc447d4690bd567d03289d29affd728998c297763b5f72f81e8b0c9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<div class=\"container\">


    <h1>Evenements list</h1>
    <a href=\"";
        // line 8
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_evenement_new");
        echo "\" class=\"btn btn-large btn-info\">Create a new evenement</a>
    <table class=\"table\">
        <thead>
            <tr>
                <th>Id</th>
                <th>Intituleevenement</th>
                <th>Dateevenement</th>
                <th>Description</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        ";
        // line 20
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["evenements"] ?? $this->getContext($context, "evenements")));
        foreach ($context['_seq'] as $context["_key"] => $context["evenement"]) {
            // line 21
            echo "            <tr>
                <td><a href=\"";
            // line 22
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_evenement_show", array("id" => $this->getAttribute($context["evenement"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["evenement"], "id", array()), "html", null, true);
            echo "</a></td>
                <td>";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute($context["evenement"], "intituleEvenement", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 24
            if ($this->getAttribute($context["evenement"], "dateEvenement", array())) {
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["evenement"], "dateEvenement", array()), "Y-m-d H:i:s"), "html", null, true);
            }
            echo "</td>
                <td>";
            // line 25
            echo twig_escape_filter($this->env, $this->getAttribute($context["evenement"], "description", array()), "html", null, true);
            echo "</td>
                <td>

                            <a href=\"";
            // line 28
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_evenement_show", array("id" => $this->getAttribute($context["evenement"], "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-warning\">show</a>

                            <a href=\"";
            // line 30
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_evenement_edit", array("id" => $this->getAttribute($context["evenement"], "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-danger\">edit</a>
                      
                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['evenement'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 35
        echo "        </tbody>
    </table>



</div>
";
        
        $__internal_7024310fcbc447d4690bd567d03289d29affd728998c297763b5f72f81e8b0c9->leave($__internal_7024310fcbc447d4690bd567d03289d29affd728998c297763b5f72f81e8b0c9_prof);

        
        $__internal_0dc6f9075bc054885a63f4d90408dfccea92e40ccd3b81dc3cdecb90f777ea85->leave($__internal_0dc6f9075bc054885a63f4d90408dfccea92e40ccd3b81dc3cdecb90f777ea85_prof);

    }

    public function getTemplateName()
    {
        return "evenement/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  115 => 35,  104 => 30,  99 => 28,  93 => 25,  87 => 24,  83 => 23,  77 => 22,  74 => 21,  70 => 20,  55 => 8,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
<div class=\"container\">


    <h1>Evenements list</h1>
    <a href=\"{{ path('admin_evenement_new') }}\" class=\"btn btn-large btn-info\">Create a new evenement</a>
    <table class=\"table\">
        <thead>
            <tr>
                <th>Id</th>
                <th>Intituleevenement</th>
                <th>Dateevenement</th>
                <th>Description</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        {% for evenement in evenements %}
            <tr>
                <td><a href=\"{{ path('admin_evenement_show', { 'id': evenement.id }) }}\">{{ evenement.id }}</a></td>
                <td>{{ evenement.intituleEvenement }}</td>
                <td>{% if evenement.dateEvenement %}{{ evenement.dateEvenement|date('Y-m-d H:i:s') }}{% endif %}</td>
                <td>{{ evenement.description }}</td>
                <td>

                            <a href=\"{{ path('admin_evenement_show', { 'id': evenement.id }) }}\" class=\"btn btn-warning\">show</a>

                            <a href=\"{{ path('admin_evenement_edit', { 'id': evenement.id }) }}\" class=\"btn btn-danger\">edit</a>
                      
                </td>
            </tr>
        {% endfor %}
        </tbody>
    </table>



</div>
{% endblock %}
", "evenement/index.html.twig", "/home/fonguen/symfony projet/infotels/app/Resources/views/evenement/index.html.twig");
    }
}
