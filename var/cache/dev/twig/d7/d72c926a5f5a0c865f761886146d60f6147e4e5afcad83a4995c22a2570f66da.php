<?php

/* evenement/new.html.twig */
class __TwigTemplate_bf65f43d50dd150567e223f617382fc47674ae2dc8cbe284be231a98158b01bf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "evenement/new.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_117bf34ee38ec5d17203bc62adae8275cac81a9457fee163be04d60268686b42 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_117bf34ee38ec5d17203bc62adae8275cac81a9457fee163be04d60268686b42->enter($__internal_117bf34ee38ec5d17203bc62adae8275cac81a9457fee163be04d60268686b42_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "evenement/new.html.twig"));

        $__internal_b1acc50df809378aa5a848e18129e67983cb10034f478b01d7eb1fa299286be7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b1acc50df809378aa5a848e18129e67983cb10034f478b01d7eb1fa299286be7->enter($__internal_b1acc50df809378aa5a848e18129e67983cb10034f478b01d7eb1fa299286be7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "evenement/new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_117bf34ee38ec5d17203bc62adae8275cac81a9457fee163be04d60268686b42->leave($__internal_117bf34ee38ec5d17203bc62adae8275cac81a9457fee163be04d60268686b42_prof);

        
        $__internal_b1acc50df809378aa5a848e18129e67983cb10034f478b01d7eb1fa299286be7->leave($__internal_b1acc50df809378aa5a848e18129e67983cb10034f478b01d7eb1fa299286be7_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_ff3fdf8bf6454037edb8713f3d2252f16eaa188cf0a95bb694adcab077a35d3e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ff3fdf8bf6454037edb8713f3d2252f16eaa188cf0a95bb694adcab077a35d3e->enter($__internal_ff3fdf8bf6454037edb8713f3d2252f16eaa188cf0a95bb694adcab077a35d3e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_75b8349f162917ab1ae653de6087575dd295cc2c9c54bb7e07442bd4f28f2945 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_75b8349f162917ab1ae653de6087575dd295cc2c9c54bb7e07442bd4f28f2945->enter($__internal_75b8349f162917ab1ae653de6087575dd295cc2c9c54bb7e07442bd4f28f2945_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<div class=\"container\">


    <h1>Evenement creation</h1>

    ";
        // line 9
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
        ";
        // line 10
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo "
        <input type=\"submit\" value=\"Create\" class=\"btn btn-large btn-info\"/>
    ";
        // line 12
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "

    <ul>
        <li>
            <a href=\"";
        // line 16
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_evenement_index");
        echo "\" class=\"btn btn-success\">Back to the list</a>
        </li>
    </ul>
</div>
";
        
        $__internal_75b8349f162917ab1ae653de6087575dd295cc2c9c54bb7e07442bd4f28f2945->leave($__internal_75b8349f162917ab1ae653de6087575dd295cc2c9c54bb7e07442bd4f28f2945_prof);

        
        $__internal_ff3fdf8bf6454037edb8713f3d2252f16eaa188cf0a95bb694adcab077a35d3e->leave($__internal_ff3fdf8bf6454037edb8713f3d2252f16eaa188cf0a95bb694adcab077a35d3e_prof);

    }

    public function getTemplateName()
    {
        return "evenement/new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 16,  65 => 12,  60 => 10,  56 => 9,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
<div class=\"container\">


    <h1>Evenement creation</h1>

    {{ form_start(form) }}
        {{ form_widget(form) }}
        <input type=\"submit\" value=\"Create\" class=\"btn btn-large btn-info\"/>
    {{ form_end(form) }}

    <ul>
        <li>
            <a href=\"{{ path('admin_evenement_index') }}\" class=\"btn btn-success\">Back to the list</a>
        </li>
    </ul>
</div>
{% endblock %}
", "evenement/new.html.twig", "/home/fonguen/symfony projet/infotels/app/Resources/views/evenement/new.html.twig");
    }
}
