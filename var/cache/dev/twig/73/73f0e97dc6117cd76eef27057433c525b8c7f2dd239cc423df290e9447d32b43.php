<?php

/* @FOSUser/Group/new.html.twig */
class __TwigTemplate_5fc8345c0d27f1ec5578aa5b88093fe984823b1e92e1d822b5b028af5b70cc88 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Group/new.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0b168b50131eb11e74db42b5370ce52edb7f60febd82a9862796403e8964f866 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0b168b50131eb11e74db42b5370ce52edb7f60febd82a9862796403e8964f866->enter($__internal_0b168b50131eb11e74db42b5370ce52edb7f60febd82a9862796403e8964f866_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Group/new.html.twig"));

        $__internal_4002653c9de257061dc75528c4e8e198074782e42d1d8e262ad70061144f18f2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4002653c9de257061dc75528c4e8e198074782e42d1d8e262ad70061144f18f2->enter($__internal_4002653c9de257061dc75528c4e8e198074782e42d1d8e262ad70061144f18f2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Group/new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0b168b50131eb11e74db42b5370ce52edb7f60febd82a9862796403e8964f866->leave($__internal_0b168b50131eb11e74db42b5370ce52edb7f60febd82a9862796403e8964f866_prof);

        
        $__internal_4002653c9de257061dc75528c4e8e198074782e42d1d8e262ad70061144f18f2->leave($__internal_4002653c9de257061dc75528c4e8e198074782e42d1d8e262ad70061144f18f2_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_c31e8f88a60f116e6956b4b67027daa94a340ce47f7d83aed7003afa6470b969 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c31e8f88a60f116e6956b4b67027daa94a340ce47f7d83aed7003afa6470b969->enter($__internal_c31e8f88a60f116e6956b4b67027daa94a340ce47f7d83aed7003afa6470b969_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_bda96dc777e0f009feb1bc982dbb0a15cd2fb18ae0adf80eabbc289b61237c66 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bda96dc777e0f009feb1bc982dbb0a15cd2fb18ae0adf80eabbc289b61237c66->enter($__internal_bda96dc777e0f009feb1bc982dbb0a15cd2fb18ae0adf80eabbc289b61237c66_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/new_content.html.twig", "@FOSUser/Group/new.html.twig", 4)->display($context);
        
        $__internal_bda96dc777e0f009feb1bc982dbb0a15cd2fb18ae0adf80eabbc289b61237c66->leave($__internal_bda96dc777e0f009feb1bc982dbb0a15cd2fb18ae0adf80eabbc289b61237c66_prof);

        
        $__internal_c31e8f88a60f116e6956b4b67027daa94a340ce47f7d83aed7003afa6470b969->leave($__internal_c31e8f88a60f116e6956b4b67027daa94a340ce47f7d83aed7003afa6470b969_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Group/new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/new_content.html.twig\" %}
{% endblock fos_user_content %}
", "@FOSUser/Group/new.html.twig", "/home/fonguen/symfony projet/infotels/vendor/friendsofsymfony/user-bundle/Resources/views/Group/new.html.twig");
    }
}
