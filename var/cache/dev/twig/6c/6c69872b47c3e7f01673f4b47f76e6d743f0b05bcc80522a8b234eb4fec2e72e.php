<?php

/* @WebProfiler/Collector/ajax.html.twig */
class __TwigTemplate_8034ec46c8e284c9234e28342e5948df3376e47bd8ddb83e684844eac5b8a57a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/ajax.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f583c859a9209f438b83f7e5fba9765fd2ad2bf63f3bcb81733d84161fb81170 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f583c859a9209f438b83f7e5fba9765fd2ad2bf63f3bcb81733d84161fb81170->enter($__internal_f583c859a9209f438b83f7e5fba9765fd2ad2bf63f3bcb81733d84161fb81170_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/ajax.html.twig"));

        $__internal_4a75d8a431b6f6beae5b73a302efb286593634a404cf98b5401e756524cd2f21 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4a75d8a431b6f6beae5b73a302efb286593634a404cf98b5401e756524cd2f21->enter($__internal_4a75d8a431b6f6beae5b73a302efb286593634a404cf98b5401e756524cd2f21_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/ajax.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f583c859a9209f438b83f7e5fba9765fd2ad2bf63f3bcb81733d84161fb81170->leave($__internal_f583c859a9209f438b83f7e5fba9765fd2ad2bf63f3bcb81733d84161fb81170_prof);

        
        $__internal_4a75d8a431b6f6beae5b73a302efb286593634a404cf98b5401e756524cd2f21->leave($__internal_4a75d8a431b6f6beae5b73a302efb286593634a404cf98b5401e756524cd2f21_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_0207822e5ec13099ed116a7d09a7c74e8d2d81edd8c1d8160e0945b767140bf6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0207822e5ec13099ed116a7d09a7c74e8d2d81edd8c1d8160e0945b767140bf6->enter($__internal_0207822e5ec13099ed116a7d09a7c74e8d2d81edd8c1d8160e0945b767140bf6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_8101eaba627bd51693a55045a2db88cb13e638ed9342c2fa303d9dea72146902 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8101eaba627bd51693a55045a2db88cb13e638ed9342c2fa303d9dea72146902->enter($__internal_8101eaba627bd51693a55045a2db88cb13e638ed9342c2fa303d9dea72146902_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        // line 4
        echo "    ";
        ob_start();
        // line 5
        echo "        ";
        echo twig_include($this->env, $context, "@WebProfiler/Icon/ajax.svg");
        echo "
        <span class=\"sf-toolbar-value sf-toolbar-ajax-requests\">0</span>
    ";
        $context["icon"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 8
        echo "
    ";
        // line 9
        $context["text"] = ('' === $tmp = "        <div class=\"sf-toolbar-info-piece\">
            <b class=\"sf-toolbar-ajax-info\"></b>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <table class=\"sf-toolbar-ajax-requests\">
                <thead>
                    <tr>
                        <th>Method</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>URL</th>
                        <th>Time</th>
                        <th>Profile</th>
                    </tr>
                </thead>
                <tbody class=\"sf-toolbar-ajax-request-list\"></tbody>
            </table>
        </div>
    ") ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 29
        echo "
    ";
        // line 30
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/toolbar_item.html.twig", array("link" => false));
        echo "
";
        
        $__internal_8101eaba627bd51693a55045a2db88cb13e638ed9342c2fa303d9dea72146902->leave($__internal_8101eaba627bd51693a55045a2db88cb13e638ed9342c2fa303d9dea72146902_prof);

        
        $__internal_0207822e5ec13099ed116a7d09a7c74e8d2d81edd8c1d8160e0945b767140bf6->leave($__internal_0207822e5ec13099ed116a7d09a7c74e8d2d81edd8c1d8160e0945b767140bf6_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/ajax.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 30,  82 => 29,  62 => 9,  59 => 8,  52 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}
    {% set icon %}
        {{ include('@WebProfiler/Icon/ajax.svg') }}
        <span class=\"sf-toolbar-value sf-toolbar-ajax-requests\">0</span>
    {% endset %}

    {% set text %}
        <div class=\"sf-toolbar-info-piece\">
            <b class=\"sf-toolbar-ajax-info\"></b>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <table class=\"sf-toolbar-ajax-requests\">
                <thead>
                    <tr>
                        <th>Method</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>URL</th>
                        <th>Time</th>
                        <th>Profile</th>
                    </tr>
                </thead>
                <tbody class=\"sf-toolbar-ajax-request-list\"></tbody>
            </table>
        </div>
    {% endset %}

    {{ include('@WebProfiler/Profiler/toolbar_item.html.twig', { link: false }) }}
{% endblock %}
", "@WebProfiler/Collector/ajax.html.twig", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/ajax.html.twig");
    }
}
