<?php

/* @Framework/Form/password_widget.html.php */
class __TwigTemplate_82944e7f1ab1920c058ca0864ec0c11db80c00d7ee42f997e5beb96a2d8d36e2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8ab6fee072ebba41e154a8333db2efda4ba701a15d4d5633ad378eecc235019b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8ab6fee072ebba41e154a8333db2efda4ba701a15d4d5633ad378eecc235019b->enter($__internal_8ab6fee072ebba41e154a8333db2efda4ba701a15d4d5633ad378eecc235019b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/password_widget.html.php"));

        $__internal_d21526a86c8e784f72ab5239610169b1db001da9dfa7411703bb375db17eeaf2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d21526a86c8e784f72ab5239610169b1db001da9dfa7411703bb375db17eeaf2->enter($__internal_d21526a86c8e784f72ab5239610169b1db001da9dfa7411703bb375db17eeaf2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/password_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'password')) ?>
";
        
        $__internal_8ab6fee072ebba41e154a8333db2efda4ba701a15d4d5633ad378eecc235019b->leave($__internal_8ab6fee072ebba41e154a8333db2efda4ba701a15d4d5633ad378eecc235019b_prof);

        
        $__internal_d21526a86c8e784f72ab5239610169b1db001da9dfa7411703bb375db17eeaf2->leave($__internal_d21526a86c8e784f72ab5239610169b1db001da9dfa7411703bb375db17eeaf2_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/password_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'password')) ?>
", "@Framework/Form/password_widget.html.php", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/password_widget.html.php");
    }
}
