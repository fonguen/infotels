<?php

/* @Twig/Exception/exception.css.twig */
class __TwigTemplate_4071eed322c4e279d72a484d24da57d37ab1c53870c0410dff6c1de1ade92132 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9a1f1a5f91c5bbf97199736c30ca09c14c08bb7be6d7eba0806bc89215ffc6af = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9a1f1a5f91c5bbf97199736c30ca09c14c08bb7be6d7eba0806bc89215ffc6af->enter($__internal_9a1f1a5f91c5bbf97199736c30ca09c14c08bb7be6d7eba0806bc89215ffc6af_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.css.twig"));

        $__internal_9d8a761f2f814aff14d895551c8e7a6ac9ff820bcc1a29feab58f43b88546f3f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9d8a761f2f814aff14d895551c8e7a6ac9ff820bcc1a29feab58f43b88546f3f->enter($__internal_9d8a761f2f814aff14d895551c8e7a6ac9ff820bcc1a29feab58f43b88546f3f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        $this->loadTemplate("@Twig/Exception/exception.txt.twig", "@Twig/Exception/exception.css.twig", 2)->display(array_merge($context, array("exception" => ($context["exception"] ?? $this->getContext($context, "exception")))));
        // line 3
        echo "*/
";
        
        $__internal_9a1f1a5f91c5bbf97199736c30ca09c14c08bb7be6d7eba0806bc89215ffc6af->leave($__internal_9a1f1a5f91c5bbf97199736c30ca09c14c08bb7be6d7eba0806bc89215ffc6af_prof);

        
        $__internal_9d8a761f2f814aff14d895551c8e7a6ac9ff820bcc1a29feab58f43b88546f3f->leave($__internal_9d8a761f2f814aff14d895551c8e7a6ac9ff820bcc1a29feab58f43b88546f3f_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 3,  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{% include '@Twig/Exception/exception.txt.twig' with { 'exception': exception } %}
*/
", "@Twig/Exception/exception.css.twig", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.css.twig");
    }
}
