<?php

/* @App/Blog/affichecour.html.twig */
class __TwigTemplate_1afc8c7ed93e2abb81045a252874a29e1e147103d79784c5eb3a6654e409637c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "@App/Blog/affichecour.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2b16e889d1cf273df64dc19d6be80eb2cbf23b5bd7b0d81b3abb0a6f3720c2f6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2b16e889d1cf273df64dc19d6be80eb2cbf23b5bd7b0d81b3abb0a6f3720c2f6->enter($__internal_2b16e889d1cf273df64dc19d6be80eb2cbf23b5bd7b0d81b3abb0a6f3720c2f6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@App/Blog/affichecour.html.twig"));

        $__internal_2184a8b927f700caa94645e6581f6ffc8f730458e1431fd0871c92fab0dbf439 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2184a8b927f700caa94645e6581f6ffc8f730458e1431fd0871c92fab0dbf439->enter($__internal_2184a8b927f700caa94645e6581f6ffc8f730458e1431fd0871c92fab0dbf439_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@App/Blog/affichecour.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2b16e889d1cf273df64dc19d6be80eb2cbf23b5bd7b0d81b3abb0a6f3720c2f6->leave($__internal_2b16e889d1cf273df64dc19d6be80eb2cbf23b5bd7b0d81b3abb0a6f3720c2f6_prof);

        
        $__internal_2184a8b927f700caa94645e6581f6ffc8f730458e1431fd0871c92fab0dbf439->leave($__internal_2184a8b927f700caa94645e6581f6ffc8f730458e1431fd0871c92fab0dbf439_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_af6411a67f1ca479a320576088894b35fe3df99e048dc8d8e536cf784369b024 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_af6411a67f1ca479a320576088894b35fe3df99e048dc8d8e536cf784369b024->enter($__internal_af6411a67f1ca479a320576088894b35fe3df99e048dc8d8e536cf784369b024_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_3c02c579578281028316555b05bb3a57eee99501d59f3a90bdcc27666417567b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3c02c579578281028316555b05bb3a57eee99501d59f3a90bdcc27666417567b->enter($__internal_3c02c579578281028316555b05bb3a57eee99501d59f3a90bdcc27666417567b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "AppBundle:Blog:affichecour";
        
        $__internal_3c02c579578281028316555b05bb3a57eee99501d59f3a90bdcc27666417567b->leave($__internal_3c02c579578281028316555b05bb3a57eee99501d59f3a90bdcc27666417567b_prof);

        
        $__internal_af6411a67f1ca479a320576088894b35fe3df99e048dc8d8e536cf784369b024->leave($__internal_af6411a67f1ca479a320576088894b35fe3df99e048dc8d8e536cf784369b024_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_1f1be5f7de0fc0546bb0887a6b786c6d0266f9f07f052352c07243d7bbfb81ef = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1f1be5f7de0fc0546bb0887a6b786c6d0266f9f07f052352c07243d7bbfb81ef->enter($__internal_1f1be5f7de0fc0546bb0887a6b786c6d0266f9f07f052352c07243d7bbfb81ef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_d5056d62f49de6ad15656881fce44aa153ffdf417f4bf401d88e04909d952e22 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d5056d62f49de6ad15656881fce44aa153ffdf417f4bf401d88e04909d952e22->enter($__internal_d5056d62f49de6ad15656881fce44aa153ffdf417f4bf401d88e04909d952e22_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "<div class=\"container\">
  <form class=\"col-lg-6\" method=\"post\" action=\"\">

  <div class=\"form-group\">
      <table>
          <tr>
              <td width=\"100%\">
                  <input id=\"text\" type=\"text\" class=\"form-control\" name=\"motcle\" >
              </td>
              <td>
                  <button type=\"submit\" class=\"btn btn-info\" ><i class=\"fas fa-search\"></i></button>

              </td>
          </tr>
      </table>



  </div>
  </form>

  <table class=\"menuuser\">
    <tr>
      <td><a href=\"user/affichecour\" class=\"btn btn-large btn-info\">doccumment</a></td>
      <td><a href=\"user/affichepragramme\" class=\"btn btn-large btn-success\">programmes</a></td>
      <td><a href=\"\" class=\"btn btn-primary\">resultat</a></td>
      <td><a href=\"user/affichvideo\" class=\"btn btn-warning\">videos</a></td>
      <td><a href=\"\" class=\"btn btn-large btn-info\">déconnexion</a></td>
    </tr>

  </table><br><br>
  <a href=\"\" class=\"btn btn-large btn-info\">Doccumentation</a>


    \t\t<table class=\"table\" >
    \t\t\t<tr>
    \t\t\t\t<td> <b>intitulé du document</b></td><td> <b>action</b></td>
    \t\t\t</tr>

    \t\t\t";
        // line 45
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["document"]);
        foreach ($context['_seq'] as $context["_key"] => $context["document"]) {
            // line 46
            echo "
    \t\t\t<tr>
    \t\t\t\t<td> ";
            // line 48
            echo twig_escape_filter($this->env, $this->getAttribute($context["document"], "nom", array()), "html", null, true);
            echo "</td>
    \t\t\t\t<td><a href=\"";
            // line 49
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl(("uploads/document/" . $this->getAttribute($context["document"], "path", array()))), "html", null, true);
            echo "\" >Telecharger</a> </td>
    \t\t\t</tr>
    \t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['document'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 52
        echo "
    \t\t</table>

    \t<div class=\"navigation\">
    ";
        // line 56
        echo $this->env->getExtension('Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension')->render($this->env, ($context["document"] ?? $this->getContext($context, "document")));
        echo "
</div>
</div>
";
        
        $__internal_d5056d62f49de6ad15656881fce44aa153ffdf417f4bf401d88e04909d952e22->leave($__internal_d5056d62f49de6ad15656881fce44aa153ffdf417f4bf401d88e04909d952e22_prof);

        
        $__internal_1f1be5f7de0fc0546bb0887a6b786c6d0266f9f07f052352c07243d7bbfb81ef->leave($__internal_1f1be5f7de0fc0546bb0887a6b786c6d0266f9f07f052352c07243d7bbfb81ef_prof);

    }

    public function getTemplateName()
    {
        return "@App/Blog/affichecour.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  136 => 56,  130 => 52,  121 => 49,  117 => 48,  113 => 46,  109 => 45,  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"::base.html.twig\" %}

{% block title %}AppBundle:Blog:affichecour{% endblock %}

{% block body %}
<div class=\"container\">
  <form class=\"col-lg-6\" method=\"post\" action=\"\">

  <div class=\"form-group\">
      <table>
          <tr>
              <td width=\"100%\">
                  <input id=\"text\" type=\"text\" class=\"form-control\" name=\"motcle\" >
              </td>
              <td>
                  <button type=\"submit\" class=\"btn btn-info\" ><i class=\"fas fa-search\"></i></button>

              </td>
          </tr>
      </table>



  </div>
  </form>

  <table class=\"menuuser\">
    <tr>
      <td><a href=\"user/affichecour\" class=\"btn btn-large btn-info\">doccumment</a></td>
      <td><a href=\"user/affichepragramme\" class=\"btn btn-large btn-success\">programmes</a></td>
      <td><a href=\"\" class=\"btn btn-primary\">resultat</a></td>
      <td><a href=\"user/affichvideo\" class=\"btn btn-warning\">videos</a></td>
      <td><a href=\"\" class=\"btn btn-large btn-info\">déconnexion</a></td>
    </tr>

  </table><br><br>
  <a href=\"\" class=\"btn btn-large btn-info\">Doccumentation</a>


    \t\t<table class=\"table\" >
    \t\t\t<tr>
    \t\t\t\t<td> <b>intitulé du document</b></td><td> <b>action</b></td>
    \t\t\t</tr>

    \t\t\t{% for document in document %}

    \t\t\t<tr>
    \t\t\t\t<td> {{ document.nom }}</td>
    \t\t\t\t<td><a href=\"{{ asset('uploads/document/' ~ document.path) }}\" >Telecharger</a> </td>
    \t\t\t</tr>
    \t\t\t{% endfor %}

    \t\t</table>

    \t<div class=\"navigation\">
    {{ knp_pagination_render(document) }}
</div>
</div>
{% endblock %}
", "@App/Blog/affichecour.html.twig", "/home/fonguen/symfony projet/infotels/src/AppBundle/Resources/views/Blog/affichecour.html.twig");
    }
}
