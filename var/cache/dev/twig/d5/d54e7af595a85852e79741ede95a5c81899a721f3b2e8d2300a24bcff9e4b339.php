<?php

/* AppBundle:Blog:affichecour.html.twig */
class __TwigTemplate_4aa78c220c93afe62593ec1170d05fb636676ea1aa74821f68fc6d77f8250a62 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "AppBundle:Blog:affichecour.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8594f58052aa4bf252218d06bd744178d060a1941c6960badba9a0893fa41263 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8594f58052aa4bf252218d06bd744178d060a1941c6960badba9a0893fa41263->enter($__internal_8594f58052aa4bf252218d06bd744178d060a1941c6960badba9a0893fa41263_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:Blog:affichecour.html.twig"));

        $__internal_c5263750f9892248424f9838bbf40d4200964e1491ee7f34a67d40e6a8b637e5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c5263750f9892248424f9838bbf40d4200964e1491ee7f34a67d40e6a8b637e5->enter($__internal_c5263750f9892248424f9838bbf40d4200964e1491ee7f34a67d40e6a8b637e5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:Blog:affichecour.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8594f58052aa4bf252218d06bd744178d060a1941c6960badba9a0893fa41263->leave($__internal_8594f58052aa4bf252218d06bd744178d060a1941c6960badba9a0893fa41263_prof);

        
        $__internal_c5263750f9892248424f9838bbf40d4200964e1491ee7f34a67d40e6a8b637e5->leave($__internal_c5263750f9892248424f9838bbf40d4200964e1491ee7f34a67d40e6a8b637e5_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_1fb11061724e159f6f5ed188e16b27ac97ce06814707aed2986f70bd59bb504f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1fb11061724e159f6f5ed188e16b27ac97ce06814707aed2986f70bd59bb504f->enter($__internal_1fb11061724e159f6f5ed188e16b27ac97ce06814707aed2986f70bd59bb504f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_067b35bd3e582285b713285a49e9a5931b8f78018ac95d4764cb4a26068343e0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_067b35bd3e582285b713285a49e9a5931b8f78018ac95d4764cb4a26068343e0->enter($__internal_067b35bd3e582285b713285a49e9a5931b8f78018ac95d4764cb4a26068343e0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "AppBundle:Blog:affichecour";
        
        $__internal_067b35bd3e582285b713285a49e9a5931b8f78018ac95d4764cb4a26068343e0->leave($__internal_067b35bd3e582285b713285a49e9a5931b8f78018ac95d4764cb4a26068343e0_prof);

        
        $__internal_1fb11061724e159f6f5ed188e16b27ac97ce06814707aed2986f70bd59bb504f->leave($__internal_1fb11061724e159f6f5ed188e16b27ac97ce06814707aed2986f70bd59bb504f_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_c4083b7762a9d79eac52e9fae2bbc3b4a424864177b938042da33b6a3ef6fe58 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c4083b7762a9d79eac52e9fae2bbc3b4a424864177b938042da33b6a3ef6fe58->enter($__internal_c4083b7762a9d79eac52e9fae2bbc3b4a424864177b938042da33b6a3ef6fe58_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_cd94fdfcae05da14ae21538317eb3a229839ebc78e2394df4af4d92db007eb9d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cd94fdfcae05da14ae21538317eb3a229839ebc78e2394df4af4d92db007eb9d->enter($__internal_cd94fdfcae05da14ae21538317eb3a229839ebc78e2394df4af4d92db007eb9d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "<div class=\"container\">
  <form class=\"col-lg-6\" method=\"post\" action=\"\">

  <div class=\"form-group\">
      <table>
          <tr>
              <td width=\"100%\">
                  <input id=\"text\" type=\"text\" class=\"form-control\" name=\"motcle\" >
              </td>
              <td>
                  <button type=\"submit\" class=\"btn btn-info\" ><i class=\"fas fa-search\"></i></button>

              </td>
          </tr>
      </table>



  </div>
  </form>

  <table class=\"menuuser\">
    <tr>
      <td><a href=\"user_affichecour\" class=\"btn btn-large btn-info\">doccumment</a></td>
      <td><a href=\"user_affichepragramme\" class=\"btn btn-large btn-success\">programmes</a></td>
      <td><a href=\"user_afficheresultat\" class=\"btn btn-primary\">resultat</a></td>
      <td><a href=\"user_affichvideo\" class=\"btn btn-warning\">videos</a></td>
      <td><a href=\"\" class=\"btn btn-large btn-info\">déconnexion</a></td>
    </tr>

  </table><br><br>
  <a href=\"\" class=\"btn btn-large btn-info\">Doccumentation</a>


    \t\t<table class=\"table\" >
    \t\t\t<tr>
    \t\t\t\t<td> <b>intitulé du document</b></td><td> <b>action</b></td>
    \t\t\t</tr>

    \t\t\t";
        // line 45
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["document"]);
        foreach ($context['_seq'] as $context["_key"] => $context["document"]) {
            // line 46
            echo "
    \t\t\t<tr>
    \t\t\t\t<td> ";
            // line 48
            echo twig_escape_filter($this->env, $this->getAttribute($context["document"], "nom", array()), "html", null, true);
            echo "</td>
    \t\t\t\t<td><a href=\"";
            // line 49
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl(("uploads/document/" . $this->getAttribute($context["document"], "path", array()))), "html", null, true);
            echo "\" >Telecharger</a> </td>
    \t\t\t</tr>
    \t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['document'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 52
        echo "
    \t\t</table>

    \t<div class=\"navigation\">
    ";
        // line 56
        echo $this->env->getExtension('Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension')->render($this->env, ($context["document"] ?? $this->getContext($context, "document")));
        echo "
</div>
</div>
";
        
        $__internal_cd94fdfcae05da14ae21538317eb3a229839ebc78e2394df4af4d92db007eb9d->leave($__internal_cd94fdfcae05da14ae21538317eb3a229839ebc78e2394df4af4d92db007eb9d_prof);

        
        $__internal_c4083b7762a9d79eac52e9fae2bbc3b4a424864177b938042da33b6a3ef6fe58->leave($__internal_c4083b7762a9d79eac52e9fae2bbc3b4a424864177b938042da33b6a3ef6fe58_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle:Blog:affichecour.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  136 => 56,  130 => 52,  121 => 49,  117 => 48,  113 => 46,  109 => 45,  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"::base.html.twig\" %}

{% block title %}AppBundle:Blog:affichecour{% endblock %}

{% block body %}
<div class=\"container\">
  <form class=\"col-lg-6\" method=\"post\" action=\"\">

  <div class=\"form-group\">
      <table>
          <tr>
              <td width=\"100%\">
                  <input id=\"text\" type=\"text\" class=\"form-control\" name=\"motcle\" >
              </td>
              <td>
                  <button type=\"submit\" class=\"btn btn-info\" ><i class=\"fas fa-search\"></i></button>

              </td>
          </tr>
      </table>



  </div>
  </form>

  <table class=\"menuuser\">
    <tr>
      <td><a href=\"user_affichecour\" class=\"btn btn-large btn-info\">doccumment</a></td>
      <td><a href=\"user_affichepragramme\" class=\"btn btn-large btn-success\">programmes</a></td>
      <td><a href=\"user_afficheresultat\" class=\"btn btn-primary\">resultat</a></td>
      <td><a href=\"user_affichvideo\" class=\"btn btn-warning\">videos</a></td>
      <td><a href=\"\" class=\"btn btn-large btn-info\">déconnexion</a></td>
    </tr>

  </table><br><br>
  <a href=\"\" class=\"btn btn-large btn-info\">Doccumentation</a>


    \t\t<table class=\"table\" >
    \t\t\t<tr>
    \t\t\t\t<td> <b>intitulé du document</b></td><td> <b>action</b></td>
    \t\t\t</tr>

    \t\t\t{% for document in document %}

    \t\t\t<tr>
    \t\t\t\t<td> {{ document.nom }}</td>
    \t\t\t\t<td><a href=\"{{ asset('uploads/document/' ~ document.path) }}\" >Telecharger</a> </td>
    \t\t\t</tr>
    \t\t\t{% endfor %}

    \t\t</table>

    \t<div class=\"navigation\">
    {{ knp_pagination_render(document) }}
</div>
</div>
{% endblock %}
", "AppBundle:Blog:affichecour.html.twig", "/home/fonguen/symfony projet/infotels/var/cache/dev/../../../src/AppBundle/Resources/views/Blog/affichecour.html.twig");
    }
}
