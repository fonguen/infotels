<?php

/* @Framework/Form/email_widget.html.php */
class __TwigTemplate_90c0ff43ba9da8c03d9c24b9b614fb204c04be09db84a1486670faef5fe6da29 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b8bd2381bea93d79e35e11e91de5de25b4055362985a22c5c8dbca032ef9cf20 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b8bd2381bea93d79e35e11e91de5de25b4055362985a22c5c8dbca032ef9cf20->enter($__internal_b8bd2381bea93d79e35e11e91de5de25b4055362985a22c5c8dbca032ef9cf20_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/email_widget.html.php"));

        $__internal_3d5887fdae81424b814c2431ed6707c338762bc99399e7874a3361dacf5c82a8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3d5887fdae81424b814c2431ed6707c338762bc99399e7874a3361dacf5c82a8->enter($__internal_3d5887fdae81424b814c2431ed6707c338762bc99399e7874a3361dacf5c82a8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/email_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'email')) ?>
";
        
        $__internal_b8bd2381bea93d79e35e11e91de5de25b4055362985a22c5c8dbca032ef9cf20->leave($__internal_b8bd2381bea93d79e35e11e91de5de25b4055362985a22c5c8dbca032ef9cf20_prof);

        
        $__internal_3d5887fdae81424b814c2431ed6707c338762bc99399e7874a3361dacf5c82a8->leave($__internal_3d5887fdae81424b814c2431ed6707c338762bc99399e7874a3361dacf5c82a8_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/email_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'email')) ?>
", "@Framework/Form/email_widget.html.php", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/email_widget.html.php");
    }
}
