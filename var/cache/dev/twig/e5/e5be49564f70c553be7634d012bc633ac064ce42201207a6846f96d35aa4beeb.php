<?php

/* @Framework/Form/hidden_widget.html.php */
class __TwigTemplate_880be461590b2ce4bf69245f98a244a7dbadf1234dcd87c1d0874215996e3e08 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d9ffc312522144f32274b4b2548a626d194258e06b51d038237ac2175b472063 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d9ffc312522144f32274b4b2548a626d194258e06b51d038237ac2175b472063->enter($__internal_d9ffc312522144f32274b4b2548a626d194258e06b51d038237ac2175b472063_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        $__internal_a2bd234543edbe673a6facb24eea59f72f7f45219618138753b99b6fd855e174 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a2bd234543edbe673a6facb24eea59f72f7f45219618138753b99b6fd855e174->enter($__internal_a2bd234543edbe673a6facb24eea59f72f7f45219618138753b99b6fd855e174_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
";
        
        $__internal_d9ffc312522144f32274b4b2548a626d194258e06b51d038237ac2175b472063->leave($__internal_d9ffc312522144f32274b4b2548a626d194258e06b51d038237ac2175b472063_prof);

        
        $__internal_a2bd234543edbe673a6facb24eea59f72f7f45219618138753b99b6fd855e174->leave($__internal_a2bd234543edbe673a6facb24eea59f72f7f45219618138753b99b6fd855e174_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
", "@Framework/Form/hidden_widget.html.php", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/hidden_widget.html.php");
    }
}
