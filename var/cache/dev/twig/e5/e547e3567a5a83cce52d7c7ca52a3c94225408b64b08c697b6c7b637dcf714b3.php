<?php

/* @FOSUser/Profile/show.html.twig */
class __TwigTemplate_d32a9f020f4989c688554fa5f76087052dd3f8be2dacc9a4302b5d43f03a9405 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Profile/show.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cdc3f045260ea6e6c3d37d8fc6799ea7158603391776d5ea92d67df8b25db69e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cdc3f045260ea6e6c3d37d8fc6799ea7158603391776d5ea92d67df8b25db69e->enter($__internal_cdc3f045260ea6e6c3d37d8fc6799ea7158603391776d5ea92d67df8b25db69e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Profile/show.html.twig"));

        $__internal_64ed26fe28c44234a1407bb21feffb4d3afc0cab3840e711c257436de31662af = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_64ed26fe28c44234a1407bb21feffb4d3afc0cab3840e711c257436de31662af->enter($__internal_64ed26fe28c44234a1407bb21feffb4d3afc0cab3840e711c257436de31662af_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Profile/show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_cdc3f045260ea6e6c3d37d8fc6799ea7158603391776d5ea92d67df8b25db69e->leave($__internal_cdc3f045260ea6e6c3d37d8fc6799ea7158603391776d5ea92d67df8b25db69e_prof);

        
        $__internal_64ed26fe28c44234a1407bb21feffb4d3afc0cab3840e711c257436de31662af->leave($__internal_64ed26fe28c44234a1407bb21feffb4d3afc0cab3840e711c257436de31662af_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_6f897346d34d2b8d44f1963d4e4ef02bcf07a3421dffbc1e7412ed7991b00f9d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6f897346d34d2b8d44f1963d4e4ef02bcf07a3421dffbc1e7412ed7991b00f9d->enter($__internal_6f897346d34d2b8d44f1963d4e4ef02bcf07a3421dffbc1e7412ed7991b00f9d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_e8ed9080a019323cf1d2467a39afde894a83c2cacae0d532bbeac777411a4a1a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e8ed9080a019323cf1d2467a39afde894a83c2cacae0d532bbeac777411a4a1a->enter($__internal_e8ed9080a019323cf1d2467a39afde894a83c2cacae0d532bbeac777411a4a1a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Profile/show_content.html.twig", "@FOSUser/Profile/show.html.twig", 4)->display($context);
        
        $__internal_e8ed9080a019323cf1d2467a39afde894a83c2cacae0d532bbeac777411a4a1a->leave($__internal_e8ed9080a019323cf1d2467a39afde894a83c2cacae0d532bbeac777411a4a1a_prof);

        
        $__internal_6f897346d34d2b8d44f1963d4e4ef02bcf07a3421dffbc1e7412ed7991b00f9d->leave($__internal_6f897346d34d2b8d44f1963d4e4ef02bcf07a3421dffbc1e7412ed7991b00f9d_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Profile/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Profile/show_content.html.twig\" %}
{% endblock fos_user_content %}
", "@FOSUser/Profile/show.html.twig", "/home/fonguen/symfony projet/infotels/vendor/friendsofsymfony/user-bundle/Resources/views/Profile/show.html.twig");
    }
}
