<?php

/* @Framework/Form/container_attributes.html.php */
class __TwigTemplate_086d1aea2505ec17460b249042ae6f631b8829745d30c2b0ef4b403b82db269d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_42af712c5dea59a48bbeb5b95e05af5c983da554cde0364870ad7d2bdd9a2d4c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_42af712c5dea59a48bbeb5b95e05af5c983da554cde0364870ad7d2bdd9a2d4c->enter($__internal_42af712c5dea59a48bbeb5b95e05af5c983da554cde0364870ad7d2bdd9a2d4c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/container_attributes.html.php"));

        $__internal_cc9a035c2ec5d773ab4a042ccdfb97f8e0f6f0b7d1e16d515add8f54f4673e95 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cc9a035c2ec5d773ab4a042ccdfb97f8e0f6f0b7d1e16d515add8f54f4673e95->enter($__internal_cc9a035c2ec5d773ab4a042ccdfb97f8e0f6f0b7d1e16d515add8f54f4673e95_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/container_attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
";
        
        $__internal_42af712c5dea59a48bbeb5b95e05af5c983da554cde0364870ad7d2bdd9a2d4c->leave($__internal_42af712c5dea59a48bbeb5b95e05af5c983da554cde0364870ad7d2bdd9a2d4c_prof);

        
        $__internal_cc9a035c2ec5d773ab4a042ccdfb97f8e0f6f0b7d1e16d515add8f54f4673e95->leave($__internal_cc9a035c2ec5d773ab4a042ccdfb97f8e0f6f0b7d1e16d515add8f54f4673e95_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/container_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
", "@Framework/Form/container_attributes.html.php", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/container_attributes.html.php");
    }
}
