<?php

/* @WebProfiler/Profiler/info.html.twig */
class __TwigTemplate_c508ea06cb5467e85c3eed762cf64842c9d74eaf8387ef91f60f3b6adf2f17a4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Profiler/info.html.twig", 1);
        $this->blocks = array(
            'summary' => array($this, 'block_summary'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9738ca04eb33fe611555bdc4e62af0f758e1d2dce392a8135eb414cd129ba726 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9738ca04eb33fe611555bdc4e62af0f758e1d2dce392a8135eb414cd129ba726->enter($__internal_9738ca04eb33fe611555bdc4e62af0f758e1d2dce392a8135eb414cd129ba726_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/info.html.twig"));

        $__internal_541a9ee58fd0fa6dfbab5cdbd5d2331753c4edf27791b3e2a8e783d5e09f1cde = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_541a9ee58fd0fa6dfbab5cdbd5d2331753c4edf27791b3e2a8e783d5e09f1cde->enter($__internal_541a9ee58fd0fa6dfbab5cdbd5d2331753c4edf27791b3e2a8e783d5e09f1cde_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/info.html.twig"));

        // line 3
        $context["messages"] = array("purge" => array("status" => "success", "title" => "The profiler database was purged successfully", "message" => "Now you need to browse some pages with the Symfony Profiler enabled to collect data."), "no_token" => array("status" => "error", "title" => (((((        // line 11
array_key_exists("token", $context)) ? (_twig_default_filter(($context["token"] ?? $this->getContext($context, "token")), "")) : ("")) == "latest")) ? ("There are no profiles") : ("Token not found")), "message" => (((((        // line 12
array_key_exists("token", $context)) ? (_twig_default_filter(($context["token"] ?? $this->getContext($context, "token")), "")) : ("")) == "latest")) ? ("No profiles found in the database.") : ((("Token \"" . ((array_key_exists("token", $context)) ? (_twig_default_filter(($context["token"] ?? $this->getContext($context, "token")), "")) : (""))) . "\" was not found in the database.")))), "upload_error" => array("status" => "error", "title" => "A problem occurred when uploading the data", "message" => "No file given or the file was not uploaded successfully."), "already_exists" => array("status" => "error", "title" => "A problem occurred when uploading the data", "message" => "The token already exists in the database."));
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9738ca04eb33fe611555bdc4e62af0f758e1d2dce392a8135eb414cd129ba726->leave($__internal_9738ca04eb33fe611555bdc4e62af0f758e1d2dce392a8135eb414cd129ba726_prof);

        
        $__internal_541a9ee58fd0fa6dfbab5cdbd5d2331753c4edf27791b3e2a8e783d5e09f1cde->leave($__internal_541a9ee58fd0fa6dfbab5cdbd5d2331753c4edf27791b3e2a8e783d5e09f1cde_prof);

    }

    // line 26
    public function block_summary($context, array $blocks = array())
    {
        $__internal_0f8d3f4deccdf7f29cc18f4c42f554a2f386a48f77fdcd647e4e35b136658107 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0f8d3f4deccdf7f29cc18f4c42f554a2f386a48f77fdcd647e4e35b136658107->enter($__internal_0f8d3f4deccdf7f29cc18f4c42f554a2f386a48f77fdcd647e4e35b136658107_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "summary"));

        $__internal_c09ecd6ca5be47abae7e7f188d09d7bd0d5421f939c5f991514dfdb8ba37f1e3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c09ecd6ca5be47abae7e7f188d09d7bd0d5421f939c5f991514dfdb8ba37f1e3->enter($__internal_c09ecd6ca5be47abae7e7f188d09d7bd0d5421f939c5f991514dfdb8ba37f1e3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "summary"));

        // line 27
        echo "    <div class=\"status status-";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["messages"] ?? $this->getContext($context, "messages")), ($context["about"] ?? $this->getContext($context, "about")), array(), "array"), "status", array()), "html", null, true);
        echo "\">
        <div class=\"container\">
            <h2>";
        // line 29
        echo twig_escape_filter($this->env, twig_title_string_filter($this->env, $this->getAttribute($this->getAttribute(($context["messages"] ?? $this->getContext($context, "messages")), ($context["about"] ?? $this->getContext($context, "about")), array(), "array"), "status", array())), "html", null, true);
        echo "</h2>
        </div>
    </div>
";
        
        $__internal_c09ecd6ca5be47abae7e7f188d09d7bd0d5421f939c5f991514dfdb8ba37f1e3->leave($__internal_c09ecd6ca5be47abae7e7f188d09d7bd0d5421f939c5f991514dfdb8ba37f1e3_prof);

        
        $__internal_0f8d3f4deccdf7f29cc18f4c42f554a2f386a48f77fdcd647e4e35b136658107->leave($__internal_0f8d3f4deccdf7f29cc18f4c42f554a2f386a48f77fdcd647e4e35b136658107_prof);

    }

    // line 34
    public function block_panel($context, array $blocks = array())
    {
        $__internal_fd4d1cc7da180179e27931d52cc712ad7251e2c02867b649cc70816e25f53e8c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fd4d1cc7da180179e27931d52cc712ad7251e2c02867b649cc70816e25f53e8c->enter($__internal_fd4d1cc7da180179e27931d52cc712ad7251e2c02867b649cc70816e25f53e8c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_1c9899b858faded1a219b9fbcde00a908e4f6f02626b9b1bbf3be1450295d9d7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1c9899b858faded1a219b9fbcde00a908e4f6f02626b9b1bbf3be1450295d9d7->enter($__internal_1c9899b858faded1a219b9fbcde00a908e4f6f02626b9b1bbf3be1450295d9d7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 35
        echo "    <h2>";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["messages"] ?? $this->getContext($context, "messages")), ($context["about"] ?? $this->getContext($context, "about")), array(), "array"), "title", array()), "html", null, true);
        echo "</h2>
    <p>";
        // line 36
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["messages"] ?? $this->getContext($context, "messages")), ($context["about"] ?? $this->getContext($context, "about")), array(), "array"), "message", array()), "html", null, true);
        echo "</p>
";
        
        $__internal_1c9899b858faded1a219b9fbcde00a908e4f6f02626b9b1bbf3be1450295d9d7->leave($__internal_1c9899b858faded1a219b9fbcde00a908e4f6f02626b9b1bbf3be1450295d9d7_prof);

        
        $__internal_fd4d1cc7da180179e27931d52cc712ad7251e2c02867b649cc70816e25f53e8c->leave($__internal_fd4d1cc7da180179e27931d52cc712ad7251e2c02867b649cc70816e25f53e8c_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/info.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 36,  84 => 35,  75 => 34,  61 => 29,  55 => 27,  46 => 26,  36 => 1,  34 => 12,  33 => 11,  32 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% set messages = {
    'purge' : {
        status:  'success',
        title:   'The profiler database was purged successfully',
        message: 'Now you need to browse some pages with the Symfony Profiler enabled to collect data.'
    },
    'no_token' : {
        status:  'error',
        title:   (token|default('') == 'latest') ? 'There are no profiles' : 'Token not found',
        message: (token|default('') == 'latest') ? 'No profiles found in the database.' : 'Token \"' ~ token|default('') ~ '\" was not found in the database.'
    },
    'upload_error' : {
        status:  'error',
        title:   'A problem occurred when uploading the data',
        message: 'No file given or the file was not uploaded successfully.'
    },
    'already_exists' : {
        status:  'error',
        title:   'A problem occurred when uploading the data',
        message: 'The token already exists in the database.'
    }
} %}

{% block summary %}
    <div class=\"status status-{{ messages[about].status }}\">
        <div class=\"container\">
            <h2>{{ messages[about].status|title }}</h2>
        </div>
    </div>
{% endblock %}

{% block panel %}
    <h2>{{ messages[about].title }}</h2>
    <p>{{ messages[about].message }}</p>
{% endblock %}
", "@WebProfiler/Profiler/info.html.twig", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/info.html.twig");
    }
}
