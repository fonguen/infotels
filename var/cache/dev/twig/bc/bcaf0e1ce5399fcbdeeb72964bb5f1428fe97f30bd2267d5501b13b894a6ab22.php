<?php

/* departement/index.html.twig */
class __TwigTemplate_e766bb5de2b2604437a13e62e53d1402cf58fb96a3e555cc54eee7644ccad09c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "departement/index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_09ab20753ca66b014675ad26283ce32d0c775b28332bf413da27bc1e4675f49d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_09ab20753ca66b014675ad26283ce32d0c775b28332bf413da27bc1e4675f49d->enter($__internal_09ab20753ca66b014675ad26283ce32d0c775b28332bf413da27bc1e4675f49d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "departement/index.html.twig"));

        $__internal_86a9a9927dbfc7bc53cb8b09ace89b7388b2c0afb30414951d50ff0fb553237a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_86a9a9927dbfc7bc53cb8b09ace89b7388b2c0afb30414951d50ff0fb553237a->enter($__internal_86a9a9927dbfc7bc53cb8b09ace89b7388b2c0afb30414951d50ff0fb553237a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "departement/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_09ab20753ca66b014675ad26283ce32d0c775b28332bf413da27bc1e4675f49d->leave($__internal_09ab20753ca66b014675ad26283ce32d0c775b28332bf413da27bc1e4675f49d_prof);

        
        $__internal_86a9a9927dbfc7bc53cb8b09ace89b7388b2c0afb30414951d50ff0fb553237a->leave($__internal_86a9a9927dbfc7bc53cb8b09ace89b7388b2c0afb30414951d50ff0fb553237a_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_f2fe2de2859449c9df55efe784c837290496f0cc82c82f72cf4ad18e7de979fd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f2fe2de2859449c9df55efe784c837290496f0cc82c82f72cf4ad18e7de979fd->enter($__internal_f2fe2de2859449c9df55efe784c837290496f0cc82c82f72cf4ad18e7de979fd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_11f9399a0e8ac6b22ef3986a6e18f8bf28a47857b3a427d055341b964e8208fc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_11f9399a0e8ac6b22ef3986a6e18f8bf28a47857b3a427d055341b964e8208fc->enter($__internal_11f9399a0e8ac6b22ef3986a6e18f8bf28a47857b3a427d055341b964e8208fc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<br><br>
<div class=\"container\">


    <h4>Departements list</h4>
    <a href=\"";
        // line 9
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_departement_new");
        echo "\" class=\"btn btn-large btn-info\">Create a new departement</a>
    <table class=\"table\">
        <thead>
            <tr>
                <th>Id</th>
                <th>Nom</th>
                <th>Code</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        ";
        // line 20
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["departements"] ?? $this->getContext($context, "departements")));
        foreach ($context['_seq'] as $context["_key"] => $context["departement"]) {
            // line 21
            echo "            <tr>
                <td><a href=\"";
            // line 22
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_departement_show", array("id" => $this->getAttribute($context["departement"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["departement"], "id", array()), "html", null, true);
            echo "</a></td>
                <td>";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute($context["departement"], "nom", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute($context["departement"], "code", array()), "html", null, true);
            echo "</td>
                <td>

                      <a href=\"";
            // line 27
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_departement_show", array("id" => $this->getAttribute($context["departement"], "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-warning\">show</a>

                      <a href=\"";
            // line 29
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_departement_edit", array("id" => $this->getAttribute($context["departement"], "id", array()))), "html", null, true);
            echo "\"class=\"btn btn-danger\">edit</a>

                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['departement'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 34
        echo "        </tbody>
    </table>

</div>
";
        
        $__internal_11f9399a0e8ac6b22ef3986a6e18f8bf28a47857b3a427d055341b964e8208fc->leave($__internal_11f9399a0e8ac6b22ef3986a6e18f8bf28a47857b3a427d055341b964e8208fc_prof);

        
        $__internal_f2fe2de2859449c9df55efe784c837290496f0cc82c82f72cf4ad18e7de979fd->leave($__internal_f2fe2de2859449c9df55efe784c837290496f0cc82c82f72cf4ad18e7de979fd_prof);

    }

    public function getTemplateName()
    {
        return "departement/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  109 => 34,  98 => 29,  93 => 27,  87 => 24,  83 => 23,  77 => 22,  74 => 21,  70 => 20,  56 => 9,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
<br><br>
<div class=\"container\">


    <h4>Departements list</h4>
    <a href=\"{{ path('admin_departement_new') }}\" class=\"btn btn-large btn-info\">Create a new departement</a>
    <table class=\"table\">
        <thead>
            <tr>
                <th>Id</th>
                <th>Nom</th>
                <th>Code</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        {% for departement in departements %}
            <tr>
                <td><a href=\"{{ path('admin_departement_show', { 'id': departement.id }) }}\">{{ departement.id }}</a></td>
                <td>{{ departement.nom }}</td>
                <td>{{ departement.code }}</td>
                <td>

                      <a href=\"{{ path('admin_departement_show', { 'id': departement.id }) }}\" class=\"btn btn-warning\">show</a>

                      <a href=\"{{ path('admin_departement_edit', { 'id': departement.id }) }}\"class=\"btn btn-danger\">edit</a>

                </td>
            </tr>
        {% endfor %}
        </tbody>
    </table>

</div>
{% endblock %}
", "departement/index.html.twig", "/home/fonguen/symfony projet/infotels/app/Resources/views/departement/index.html.twig");
    }
}
