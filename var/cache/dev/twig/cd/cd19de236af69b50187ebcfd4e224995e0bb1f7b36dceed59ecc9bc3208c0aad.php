<?php

/* @Twig/Exception/exception.json.twig */
class __TwigTemplate_dff714291d772c1d2eb33fb782988853acf4be5b6090dc0dc8afe1112931c71f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_53577361522d3cada0e87660f7fc3816c1403a72a348660e53e91e48df96dbf5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_53577361522d3cada0e87660f7fc3816c1403a72a348660e53e91e48df96dbf5->enter($__internal_53577361522d3cada0e87660f7fc3816c1403a72a348660e53e91e48df96dbf5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.json.twig"));

        $__internal_37362ded3b38df7e7a73ccbada4c5e60afc4caa2745494e67ba2f0a639434ec7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_37362ded3b38df7e7a73ccbada4c5e60afc4caa2745494e67ba2f0a639434ec7->enter($__internal_37362ded3b38df7e7a73ccbada4c5e60afc4caa2745494e67ba2f0a639434ec7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => ($context["status_code"] ?? $this->getContext($context, "status_code")), "message" => ($context["status_text"] ?? $this->getContext($context, "status_text")), "exception" => $this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "toarray", array()))));
        echo "
";
        
        $__internal_53577361522d3cada0e87660f7fc3816c1403a72a348660e53e91e48df96dbf5->leave($__internal_53577361522d3cada0e87660f7fc3816c1403a72a348660e53e91e48df96dbf5_prof);

        
        $__internal_37362ded3b38df7e7a73ccbada4c5e60afc4caa2745494e67ba2f0a639434ec7->leave($__internal_37362ded3b38df7e7a73ccbada4c5e60afc4caa2745494e67ba2f0a639434ec7_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ { 'error': { 'code': status_code, 'message': status_text, 'exception': exception.toarray } }|json_encode|raw }}
", "@Twig/Exception/exception.json.twig", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.json.twig");
    }
}
