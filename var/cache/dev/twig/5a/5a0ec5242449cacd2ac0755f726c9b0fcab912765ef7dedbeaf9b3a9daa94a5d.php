<?php

/* user/new.html.twig */
class __TwigTemplate_1d89f33b4dd9e542972aa3aeccd2f3ca338d7f2513273c2742ed0e0bacaac395 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "user/new.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_aee3c7eaadb5624937914491c18816fed7104e6d08755913539501639757f7e4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_aee3c7eaadb5624937914491c18816fed7104e6d08755913539501639757f7e4->enter($__internal_aee3c7eaadb5624937914491c18816fed7104e6d08755913539501639757f7e4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "user/new.html.twig"));

        $__internal_9985f4c9f1cd89b9b72f275c67d726848e81cbdad2dff9b6b291544d3d65f37f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9985f4c9f1cd89b9b72f275c67d726848e81cbdad2dff9b6b291544d3d65f37f->enter($__internal_9985f4c9f1cd89b9b72f275c67d726848e81cbdad2dff9b6b291544d3d65f37f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "user/new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_aee3c7eaadb5624937914491c18816fed7104e6d08755913539501639757f7e4->leave($__internal_aee3c7eaadb5624937914491c18816fed7104e6d08755913539501639757f7e4_prof);

        
        $__internal_9985f4c9f1cd89b9b72f275c67d726848e81cbdad2dff9b6b291544d3d65f37f->leave($__internal_9985f4c9f1cd89b9b72f275c67d726848e81cbdad2dff9b6b291544d3d65f37f_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_ff1d6d00962e2cb23dc385b26f5a6293cda27937daf269a7872c79e29837ec00 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ff1d6d00962e2cb23dc385b26f5a6293cda27937daf269a7872c79e29837ec00->enter($__internal_ff1d6d00962e2cb23dc385b26f5a6293cda27937daf269a7872c79e29837ec00_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_98d4e12d049aafbdf3c858358d72da2f46f204a7eee8ba8077176177290f5937 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_98d4e12d049aafbdf3c858358d72da2f46f204a7eee8ba8077176177290f5937->enter($__internal_98d4e12d049aafbdf3c858358d72da2f46f204a7eee8ba8077176177290f5937_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<div class=\"container\">


    <h1>User creation</h1>

    ";
        // line 9
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
        ";
        // line 10
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo "
        <input type=\"submit\" value=\"Create\" class=\"btn btn-large btn-info\"/>
    ";
        // line 12
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "

    <ul>
        <li>
            <a href=\"";
        // line 16
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_user_index");
        echo "\" class=\"btn btn-success\">Back to the list</a>
        </li>
    </ul>
</div>
";
        
        $__internal_98d4e12d049aafbdf3c858358d72da2f46f204a7eee8ba8077176177290f5937->leave($__internal_98d4e12d049aafbdf3c858358d72da2f46f204a7eee8ba8077176177290f5937_prof);

        
        $__internal_ff1d6d00962e2cb23dc385b26f5a6293cda27937daf269a7872c79e29837ec00->leave($__internal_ff1d6d00962e2cb23dc385b26f5a6293cda27937daf269a7872c79e29837ec00_prof);

    }

    public function getTemplateName()
    {
        return "user/new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 16,  65 => 12,  60 => 10,  56 => 9,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
<div class=\"container\">


    <h1>User creation</h1>

    {{ form_start(form) }}
        {{ form_widget(form) }}
        <input type=\"submit\" value=\"Create\" class=\"btn btn-large btn-info\"/>
    {{ form_end(form) }}

    <ul>
        <li>
            <a href=\"{{ path('admin_user_index') }}\" class=\"btn btn-success\">Back to the list</a>
        </li>
    </ul>
</div>
{% endblock %}
", "user/new.html.twig", "/home/fonguen/symfony projet/infotels/app/Resources/views/user/new.html.twig");
    }
}
