<?php

/* @Framework/Form/reset_widget.html.php */
class __TwigTemplate_82553e742c8a8181a2ea4872a2246df9b3b5242a7e959195cd467a521b6dfccc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_86d67ab94b4dab600b574d912d988b63b52cea42743df9ec27af536ebc18c0cc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_86d67ab94b4dab600b574d912d988b63b52cea42743df9ec27af536ebc18c0cc->enter($__internal_86d67ab94b4dab600b574d912d988b63b52cea42743df9ec27af536ebc18c0cc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        $__internal_73a2bd89edc60b9b10d5b91280f4ca8e9221ada7e43a4b9143a98788994416b6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_73a2bd89edc60b9b10d5b91280f4ca8e9221ada7e43a4b9143a98788994416b6->enter($__internal_73a2bd89edc60b9b10d5b91280f4ca8e9221ada7e43a4b9143a98788994416b6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'reset')) ?>
";
        
        $__internal_86d67ab94b4dab600b574d912d988b63b52cea42743df9ec27af536ebc18c0cc->leave($__internal_86d67ab94b4dab600b574d912d988b63b52cea42743df9ec27af536ebc18c0cc_prof);

        
        $__internal_73a2bd89edc60b9b10d5b91280f4ca8e9221ada7e43a4b9143a98788994416b6->leave($__internal_73a2bd89edc60b9b10d5b91280f4ca8e9221ada7e43a4b9143a98788994416b6_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/reset_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'reset')) ?>
", "@Framework/Form/reset_widget.html.php", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/reset_widget.html.php");
    }
}
