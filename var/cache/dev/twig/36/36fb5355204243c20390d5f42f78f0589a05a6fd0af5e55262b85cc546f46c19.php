<?php

/* @WebProfiler/Icon/no.svg */
class __TwigTemplate_55b92f2281f4af3b539454648ea13af269225fb0bc26c9a07334bea2c0be8ee0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c712e8597d678b91fd1db271b801ca2d099ca76bcf21bcd995c261cfffade2f3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c712e8597d678b91fd1db271b801ca2d099ca76bcf21bcd995c261cfffade2f3->enter($__internal_c712e8597d678b91fd1db271b801ca2d099ca76bcf21bcd995c261cfffade2f3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Icon/no.svg"));

        $__internal_1d7e2a059246c35c1a71d1b7799f812c6dc7c821b588cea48debc9551e7192ee = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1d7e2a059246c35c1a71d1b7799f812c6dc7c821b588cea48debc9551e7192ee->enter($__internal_1d7e2a059246c35c1a71d1b7799f812c6dc7c821b588cea48debc9551e7192ee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Icon/no.svg"));

        // line 1
        echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"28\" height=\"28\" viewBox=\"0 0 12 12\" enable-background=\"new 0 0 12 12\" xml:space=\"preserve\">
    <path fill=\"#B0413E\" d=\"M10.4,8.4L8,6l2.4-2.4c0.8-0.8,0.7-1.6,0.2-2.2C10,0.9,9.2,0.8,8.4,1.6L6,4L3.6,1.6C2.8,0.8,2,0.9,1.4,1.4
    C0.9,2,0.8,2.8,1.6,3.6L4,6L1.6,8.4C0.8,9.2,0.9,10,1.4,10.6c0.6,0.6,1.4,0.6,2.2-0.2L6,8l2.4,2.4c0.8,0.8,1.6,0.7,2.2,0.2
    C11.1,10,11.2,9.2,10.4,8.4z\"/>
</svg>
";
        
        $__internal_c712e8597d678b91fd1db271b801ca2d099ca76bcf21bcd995c261cfffade2f3->leave($__internal_c712e8597d678b91fd1db271b801ca2d099ca76bcf21bcd995c261cfffade2f3_prof);

        
        $__internal_1d7e2a059246c35c1a71d1b7799f812c6dc7c821b588cea48debc9551e7192ee->leave($__internal_1d7e2a059246c35c1a71d1b7799f812c6dc7c821b588cea48debc9551e7192ee_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Icon/no.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"28\" height=\"28\" viewBox=\"0 0 12 12\" enable-background=\"new 0 0 12 12\" xml:space=\"preserve\">
    <path fill=\"#B0413E\" d=\"M10.4,8.4L8,6l2.4-2.4c0.8-0.8,0.7-1.6,0.2-2.2C10,0.9,9.2,0.8,8.4,1.6L6,4L3.6,1.6C2.8,0.8,2,0.9,1.4,1.4
    C0.9,2,0.8,2.8,1.6,3.6L4,6L1.6,8.4C0.8,9.2,0.9,10,1.4,10.6c0.6,0.6,1.4,0.6,2.2-0.2L6,8l2.4,2.4c0.8,0.8,1.6,0.7,2.2,0.2
    C11.1,10,11.2,9.2,10.4,8.4z\"/>
</svg>
", "@WebProfiler/Icon/no.svg", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Icon/no.svg");
    }
}
