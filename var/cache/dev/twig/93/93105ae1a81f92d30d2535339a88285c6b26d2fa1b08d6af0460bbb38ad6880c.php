<?php

/* default/index.html.twig */
class __TwigTemplate_cfc6776f2bb1afbf96bb805286813771d005003a7e6ae1841e7ecaada70646e6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "default/index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'stylesheets' => array($this, 'block_stylesheets'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4aa6af53dfa7b2fecb9521cba83586a62e31d84e03d48c42968fb70931a06bbb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4aa6af53dfa7b2fecb9521cba83586a62e31d84e03d48c42968fb70931a06bbb->enter($__internal_4aa6af53dfa7b2fecb9521cba83586a62e31d84e03d48c42968fb70931a06bbb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/index.html.twig"));

        $__internal_a319c70f64c31b477f02436b07eaf148291ab561b4497d2c1da9efff862cd11a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a319c70f64c31b477f02436b07eaf148291ab561b4497d2c1da9efff862cd11a->enter($__internal_a319c70f64c31b477f02436b07eaf148291ab561b4497d2c1da9efff862cd11a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_4aa6af53dfa7b2fecb9521cba83586a62e31d84e03d48c42968fb70931a06bbb->leave($__internal_4aa6af53dfa7b2fecb9521cba83586a62e31d84e03d48c42968fb70931a06bbb_prof);

        
        $__internal_a319c70f64c31b477f02436b07eaf148291ab561b4497d2c1da9efff862cd11a->leave($__internal_a319c70f64c31b477f02436b07eaf148291ab561b4497d2c1da9efff862cd11a_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_03391477923df0da83515a91c3969ca9635de42478157f238cf13d92984cee62 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_03391477923df0da83515a91c3969ca9635de42478157f238cf13d92984cee62->enter($__internal_03391477923df0da83515a91c3969ca9635de42478157f238cf13d92984cee62_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_6bda5fc8fcb1704160b108512fb95e1a5eb2353c2f080d04294f45b6893f49c9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6bda5fc8fcb1704160b108512fb95e1a5eb2353c2f080d04294f45b6893f49c9->enter($__internal_6bda5fc8fcb1704160b108512fb95e1a5eb2353c2f080d04294f45b6893f49c9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<div class=\"container\">
  <table width=\"100%\">
    <tr>
      <td>
          <img src=\"uma.jpeg\" >
      </td>
      <td>
        <p>
          <center>

              <h2 id=\"toto\">UNIVERSITE DE MAROUA</h2>
            <br>
            <h2><i> ECOLE NATIONALE SUPERIEURE POLYTECHNIQUE DE MAROUA</i></h2><br>
            <h2><b>DEPARTEMENT D'INFORMATIQUE ET DES TELECMMUNICATIONS</b>   </h2>
          </center>

        </p>

      </td>
      <td>
            <img src=\"uma.jpeg\" >
      </td>
    </tr>

  </table>
  <br><br>
  <table class=\"table\" >

    ";
        // line 32
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["formation"]);
        foreach ($context['_seq'] as $context["_key"] => $context["formation"]) {
            // line 33
            echo "
    <tr>
      <td><p> <h4> <b>";
            // line 35
            echo twig_escape_filter($this->env, $this->getAttribute($context["formation"], "nom", array()), "html", null, true);
            echo "</b> </h4><br>
        ";
            // line 36
            echo twig_escape_filter($this->env, $this->getAttribute($context["formation"], "description", array()), "html", null, true);
            echo "</p> </td>
      <td>  <img src=\"";
            // line 37
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl(("uploads/formation/" . $this->getAttribute($context["formation"], "path", array()))), "html", null, true);
            echo "\" alt=\"\" width=\"550\" height=\"400\"> </td>

    </tr>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['formation'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 41
        echo "
  </table>
</div>
<div class=\"navigation\">
";
        // line 45
        echo $this->env->getExtension('Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension')->render($this->env, ($context["formation"] ?? $this->getContext($context, "formation")));
        echo "
</div>
  <table width=\"100%\">
    <tr>
      <td><h4><b>FILIERE TELECOMMUNICATIONS</b> </h4> </td>
      <td><h4><b>FILIERE INFORMATIQUE</b> </h4> </td>
    </tr>
    <tr>
      <td>
        <p>
          <h5>
            <br><br>
            L'objectif de la formation est de donner aux future <br>ingénieurs
            l'ensemble des connaissances de bases <br>relatives aux quatre doomaines
            qui constituent <br>le socle des Telecommunications, Services et<br> Usages
            à savoir l'Informatique le Reseaux ,les Telecommunications et les
            Humanités. Les etudiants<br>  doivent etre capables de de comprendre
            le <br> fonctionnement et l'utilisation  des technologies et<br>  les supports
            pour les télécommunications et de l'informatique,consevoir des
            architectures de <br> systemes de communication,des applictions et<br>  des
            servises ,developper les dommines de recherche appliquée adaptée aux
            besoins du pays et de la region du sahel,integrer les Télécoms dans la
             strategie de l'entreprise et situer les enjeux techniques,ecoconomiques
             et organisationnels
          </h5>
        </p>
      </td>
      <td>
        <p>
          <h5>
            La filiere Informatique forment les ingenieurs de haut niveau specialistes
            de l'informatique et de la communication. Les objectifs des programmes de formations
            sont cohérents avec les besoins exprimés par l'environnement économique sahélien.
            Ils permettent de donner les aptitudes a cosevoir et mettre en oeuvre des solutions
            économiquement  et technologiegiquement pertinentes dans un contexte technologiegiquement
            en constante évolution, donner une grande adaptabilité et un fort potentiel à enrichir
            leurs compétences.

          </h5>
        </p>
      </td>
    </tr>

  </table>
  <div class=\"slider\">
    <div class=\"slides\">
        <div class=\"slide\"><img src=\"uma.jpeg\" ></div>
        <div class=\"slide\"><img src=\"uma2.jpg\" ></div>
        <div class=\"slide\"><img src=\"2.jpg\" ></div>
    </div>
</div>

</div>







";
        
        $__internal_6bda5fc8fcb1704160b108512fb95e1a5eb2353c2f080d04294f45b6893f49c9->leave($__internal_6bda5fc8fcb1704160b108512fb95e1a5eb2353c2f080d04294f45b6893f49c9_prof);

        
        $__internal_03391477923df0da83515a91c3969ca9635de42478157f238cf13d92984cee62->leave($__internal_03391477923df0da83515a91c3969ca9635de42478157f238cf13d92984cee62_prof);

    }

    // line 107
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_5e6ed0631eaa4f1f45364f45c74b9526fb9e84dc0fc2eb2725e03c0edb777b24 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5e6ed0631eaa4f1f45364f45c74b9526fb9e84dc0fc2eb2725e03c0edb777b24->enter($__internal_5e6ed0631eaa4f1f45364f45c74b9526fb9e84dc0fc2eb2725e03c0edb777b24_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_c64941ff27405d457bd4c91e143defcec519299d8b6c5a0fba94d338370e5fe3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c64941ff27405d457bd4c91e143defcec519299d8b6c5a0fba94d338370e5fe3->enter($__internal_c64941ff27405d457bd4c91e143defcec519299d8b6c5a0fba94d338370e5fe3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 108
        echo "<style>
    body { background: #F5F5F5; font: 18px/1.5 sans-serif; }
    h1, h2 { line-height: 1.2; margin: 0 0 .5em; }
    h1 { font-size: 36px; }
    h2 { font-size: 21px; margin-bottom: 1em; }
    p { margin: 0 0 1em 0; }
    a { color: #0000F0; }
    a:hover { text-decoration: none; }
    code { background: #F5F5F5; max-width: 100px; padding: 2px 6px; word-wrap: break-word; }
    #wrapper { background: #FFF; margin: 1em auto; max-width: 800px; width: 95%; }
    #container { padding: 2em; }
    #welcome, #status { margin-bottom: 2em; }
    #welcome h1 span { display: block; font-size: 75%; }
    #icon-status, #icon-book { float: left; height: 64px; margin-right: 1em; margin-top: -4px; width: 64px; }
    #icon-book { display: none; }

    @media (min-width: 768px) {
        #wrapper { width: 80%; margin: 2em auto; }
        #icon-book { display: inline-block; }
        #status a, #next a { display: block; }

        @-webkit-keyframes fade-in { 0% { opacity: 0; } 100% { opacity: 1; } }
        @keyframes fade-in { 0% { opacity: 0; } 100% { opacity: 1; } }
        .sf-toolbar { opacity: 0; -webkit-animation: fade-in 1s .2s forwards; animation: fade-in 1s .2s forwards;}
    }
</style>
";
        
        $__internal_c64941ff27405d457bd4c91e143defcec519299d8b6c5a0fba94d338370e5fe3->leave($__internal_c64941ff27405d457bd4c91e143defcec519299d8b6c5a0fba94d338370e5fe3_prof);

        
        $__internal_5e6ed0631eaa4f1f45364f45c74b9526fb9e84dc0fc2eb2725e03c0edb777b24->leave($__internal_5e6ed0631eaa4f1f45364f45c74b9526fb9e84dc0fc2eb2725e03c0edb777b24_prof);

    }

    public function getTemplateName()
    {
        return "default/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  192 => 108,  183 => 107,  112 => 45,  106 => 41,  96 => 37,  92 => 36,  88 => 35,  84 => 33,  80 => 32,  50 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
<div class=\"container\">
  <table width=\"100%\">
    <tr>
      <td>
          <img src=\"uma.jpeg\" >
      </td>
      <td>
        <p>
          <center>

              <h2 id=\"toto\">UNIVERSITE DE MAROUA</h2>
            <br>
            <h2><i> ECOLE NATIONALE SUPERIEURE POLYTECHNIQUE DE MAROUA</i></h2><br>
            <h2><b>DEPARTEMENT D'INFORMATIQUE ET DES TELECMMUNICATIONS</b>   </h2>
          </center>

        </p>

      </td>
      <td>
            <img src=\"uma.jpeg\" >
      </td>
    </tr>

  </table>
  <br><br>
  <table class=\"table\" >

    {% for formation in formation %}

    <tr>
      <td><p> <h4> <b>{{ formation.nom }}</b> </h4><br>
        {{ formation.description }}</p> </td>
      <td>  <img src=\"{{ asset('uploads/formation/' ~ formation.path) }}\" alt=\"\" width=\"550\" height=\"400\"> </td>

    </tr>
    {% endfor %}

  </table>
</div>
<div class=\"navigation\">
{{ knp_pagination_render(formation) }}
</div>
  <table width=\"100%\">
    <tr>
      <td><h4><b>FILIERE TELECOMMUNICATIONS</b> </h4> </td>
      <td><h4><b>FILIERE INFORMATIQUE</b> </h4> </td>
    </tr>
    <tr>
      <td>
        <p>
          <h5>
            <br><br>
            L'objectif de la formation est de donner aux future <br>ingénieurs
            l'ensemble des connaissances de bases <br>relatives aux quatre doomaines
            qui constituent <br>le socle des Telecommunications, Services et<br> Usages
            à savoir l'Informatique le Reseaux ,les Telecommunications et les
            Humanités. Les etudiants<br>  doivent etre capables de de comprendre
            le <br> fonctionnement et l'utilisation  des technologies et<br>  les supports
            pour les télécommunications et de l'informatique,consevoir des
            architectures de <br> systemes de communication,des applictions et<br>  des
            servises ,developper les dommines de recherche appliquée adaptée aux
            besoins du pays et de la region du sahel,integrer les Télécoms dans la
             strategie de l'entreprise et situer les enjeux techniques,ecoconomiques
             et organisationnels
          </h5>
        </p>
      </td>
      <td>
        <p>
          <h5>
            La filiere Informatique forment les ingenieurs de haut niveau specialistes
            de l'informatique et de la communication. Les objectifs des programmes de formations
            sont cohérents avec les besoins exprimés par l'environnement économique sahélien.
            Ils permettent de donner les aptitudes a cosevoir et mettre en oeuvre des solutions
            économiquement  et technologiegiquement pertinentes dans un contexte technologiegiquement
            en constante évolution, donner une grande adaptabilité et un fort potentiel à enrichir
            leurs compétences.

          </h5>
        </p>
      </td>
    </tr>

  </table>
  <div class=\"slider\">
    <div class=\"slides\">
        <div class=\"slide\"><img src=\"uma.jpeg\" ></div>
        <div class=\"slide\"><img src=\"uma2.jpg\" ></div>
        <div class=\"slide\"><img src=\"2.jpg\" ></div>
    </div>
</div>

</div>







{% endblock %}

{% block stylesheets %}
<style>
    body { background: #F5F5F5; font: 18px/1.5 sans-serif; }
    h1, h2 { line-height: 1.2; margin: 0 0 .5em; }
    h1 { font-size: 36px; }
    h2 { font-size: 21px; margin-bottom: 1em; }
    p { margin: 0 0 1em 0; }
    a { color: #0000F0; }
    a:hover { text-decoration: none; }
    code { background: #F5F5F5; max-width: 100px; padding: 2px 6px; word-wrap: break-word; }
    #wrapper { background: #FFF; margin: 1em auto; max-width: 800px; width: 95%; }
    #container { padding: 2em; }
    #welcome, #status { margin-bottom: 2em; }
    #welcome h1 span { display: block; font-size: 75%; }
    #icon-status, #icon-book { float: left; height: 64px; margin-right: 1em; margin-top: -4px; width: 64px; }
    #icon-book { display: none; }

    @media (min-width: 768px) {
        #wrapper { width: 80%; margin: 2em auto; }
        #icon-book { display: inline-block; }
        #status a, #next a { display: block; }

        @-webkit-keyframes fade-in { 0% { opacity: 0; } 100% { opacity: 1; } }
        @keyframes fade-in { 0% { opacity: 0; } 100% { opacity: 1; } }
        .sf-toolbar { opacity: 0; -webkit-animation: fade-in 1s .2s forwards; animation: fade-in 1s .2s forwards;}
    }
</style>
{% endblock %}
", "default/index.html.twig", "/home/fonguen/symfony projet/infotels/app/Resources/views/default/index.html.twig");
    }
}
