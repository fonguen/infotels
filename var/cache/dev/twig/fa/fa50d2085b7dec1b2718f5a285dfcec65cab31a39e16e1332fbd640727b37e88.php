<?php

/* @App/Blog/menuadmin.html.twig */
class __TwigTemplate_d1a6668cf486ab9d1d73bcf5030b22579d5d2cf5d5659b0c4449635296e97f7c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "@App/Blog/menuadmin.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a2588bb931c6655fcee1938c82bcc5d2f20e4f27fd88e758541d230409d57ac1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a2588bb931c6655fcee1938c82bcc5d2f20e4f27fd88e758541d230409d57ac1->enter($__internal_a2588bb931c6655fcee1938c82bcc5d2f20e4f27fd88e758541d230409d57ac1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@App/Blog/menuadmin.html.twig"));

        $__internal_5a93c80ced34f6e06c4732fd365f17e53b598d3ce7faaee88c04203e2cb771a3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5a93c80ced34f6e06c4732fd365f17e53b598d3ce7faaee88c04203e2cb771a3->enter($__internal_5a93c80ced34f6e06c4732fd365f17e53b598d3ce7faaee88c04203e2cb771a3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@App/Blog/menuadmin.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a2588bb931c6655fcee1938c82bcc5d2f20e4f27fd88e758541d230409d57ac1->leave($__internal_a2588bb931c6655fcee1938c82bcc5d2f20e4f27fd88e758541d230409d57ac1_prof);

        
        $__internal_5a93c80ced34f6e06c4732fd365f17e53b598d3ce7faaee88c04203e2cb771a3->leave($__internal_5a93c80ced34f6e06c4732fd365f17e53b598d3ce7faaee88c04203e2cb771a3_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_9b93435cdeb6fb7c23ce49d76e51b88883696873bc30e006299f6039168c5d0d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9b93435cdeb6fb7c23ce49d76e51b88883696873bc30e006299f6039168c5d0d->enter($__internal_9b93435cdeb6fb7c23ce49d76e51b88883696873bc30e006299f6039168c5d0d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_271b7b537e928ac36a95819aeb2f981a25fa8a5ce2b24bdc80dd8bc73084c632 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_271b7b537e928ac36a95819aeb2f981a25fa8a5ce2b24bdc80dd8bc73084c632->enter($__internal_271b7b537e928ac36a95819aeb2f981a25fa8a5ce2b24bdc80dd8bc73084c632_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "polo
";
        
        $__internal_271b7b537e928ac36a95819aeb2f981a25fa8a5ce2b24bdc80dd8bc73084c632->leave($__internal_271b7b537e928ac36a95819aeb2f981a25fa8a5ce2b24bdc80dd8bc73084c632_prof);

        
        $__internal_9b93435cdeb6fb7c23ce49d76e51b88883696873bc30e006299f6039168c5d0d->leave($__internal_9b93435cdeb6fb7c23ce49d76e51b88883696873bc30e006299f6039168c5d0d_prof);

    }

    public function getTemplateName()
    {
        return "@App/Blog/menuadmin.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
polo
{% endblock %}
", "@App/Blog/menuadmin.html.twig", "/home/fonguen/symfony projet/infotels/src/AppBundle/Resources/views/Blog/menuadmin.html.twig");
    }
}
