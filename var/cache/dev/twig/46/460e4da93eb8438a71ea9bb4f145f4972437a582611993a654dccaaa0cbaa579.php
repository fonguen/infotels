<?php

/* @Framework/FormTable/hidden_row.html.php */
class __TwigTemplate_cd9021d6b2d8f07e826d035db7047e0e93d025abfaef5c7a775a0377888759bc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7c08b19453a95b1aec42f19967ac78434b3f99db56ca3d0728afa300e38cb112 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7c08b19453a95b1aec42f19967ac78434b3f99db56ca3d0728afa300e38cb112->enter($__internal_7c08b19453a95b1aec42f19967ac78434b3f99db56ca3d0728afa300e38cb112_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/hidden_row.html.php"));

        $__internal_005aa04739356329d3ae2fc3f055fc5c0b1541d583deb0acd9e0ddf9d6a10756 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_005aa04739356329d3ae2fc3f055fc5c0b1541d583deb0acd9e0ddf9d6a10756->enter($__internal_005aa04739356329d3ae2fc3f055fc5c0b1541d583deb0acd9e0ddf9d6a10756_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/hidden_row.html.php"));

        // line 1
        echo "<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_7c08b19453a95b1aec42f19967ac78434b3f99db56ca3d0728afa300e38cb112->leave($__internal_7c08b19453a95b1aec42f19967ac78434b3f99db56ca3d0728afa300e38cb112_prof);

        
        $__internal_005aa04739356329d3ae2fc3f055fc5c0b1541d583deb0acd9e0ddf9d6a10756->leave($__internal_005aa04739356329d3ae2fc3f055fc5c0b1541d583deb0acd9e0ddf9d6a10756_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
", "@Framework/FormTable/hidden_row.html.php", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/hidden_row.html.php");
    }
}
