<?php

/* @Twig/Exception/exception.js.twig */
class __TwigTemplate_abe98ee102754dec7bcaa588af017b887b174cdeeb0b98c3b21d6654aab6459b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c03df8e6284c1399da4eb9922741f1dc56c24240ae899dad50781f55140df492 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c03df8e6284c1399da4eb9922741f1dc56c24240ae899dad50781f55140df492->enter($__internal_c03df8e6284c1399da4eb9922741f1dc56c24240ae899dad50781f55140df492_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.js.twig"));

        $__internal_5e08db2f01cff8554c60e6df4fe6c9801b20034114d2db6b2bc83dd2bc151bc4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5e08db2f01cff8554c60e6df4fe6c9801b20034114d2db6b2bc83dd2bc151bc4->enter($__internal_5e08db2f01cff8554c60e6df4fe6c9801b20034114d2db6b2bc83dd2bc151bc4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        $this->loadTemplate("@Twig/Exception/exception.txt.twig", "@Twig/Exception/exception.js.twig", 2)->display(array_merge($context, array("exception" => ($context["exception"] ?? $this->getContext($context, "exception")))));
        // line 3
        echo "*/
";
        
        $__internal_c03df8e6284c1399da4eb9922741f1dc56c24240ae899dad50781f55140df492->leave($__internal_c03df8e6284c1399da4eb9922741f1dc56c24240ae899dad50781f55140df492_prof);

        
        $__internal_5e08db2f01cff8554c60e6df4fe6c9801b20034114d2db6b2bc83dd2bc151bc4->leave($__internal_5e08db2f01cff8554c60e6df4fe6c9801b20034114d2db6b2bc83dd2bc151bc4_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 3,  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{% include '@Twig/Exception/exception.txt.twig' with { 'exception': exception } %}
*/
", "@Twig/Exception/exception.js.twig", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.js.twig");
    }
}
