<?php

/* AppBundle:Blog:resultat.html.twig */
class __TwigTemplate_636c18228c613632e23ca81dadef6a42a940d60cdd856cbe493838f4388c52ed extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "AppBundle:Blog:resultat.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e1c63c87bfce2b4e03f239969e06f8537e7a201d3dfbe8233dcb5cdb2f2b7a65 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e1c63c87bfce2b4e03f239969e06f8537e7a201d3dfbe8233dcb5cdb2f2b7a65->enter($__internal_e1c63c87bfce2b4e03f239969e06f8537e7a201d3dfbe8233dcb5cdb2f2b7a65_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:Blog:resultat.html.twig"));

        $__internal_81e22b1d09bb3865f670fcb2fbafd85a490c1d71439913e1fb8cc2f983d2378b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_81e22b1d09bb3865f670fcb2fbafd85a490c1d71439913e1fb8cc2f983d2378b->enter($__internal_81e22b1d09bb3865f670fcb2fbafd85a490c1d71439913e1fb8cc2f983d2378b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:Blog:resultat.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e1c63c87bfce2b4e03f239969e06f8537e7a201d3dfbe8233dcb5cdb2f2b7a65->leave($__internal_e1c63c87bfce2b4e03f239969e06f8537e7a201d3dfbe8233dcb5cdb2f2b7a65_prof);

        
        $__internal_81e22b1d09bb3865f670fcb2fbafd85a490c1d71439913e1fb8cc2f983d2378b->leave($__internal_81e22b1d09bb3865f670fcb2fbafd85a490c1d71439913e1fb8cc2f983d2378b_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_7ff577ade3e1fcc849c8c98cd1da5b9725afeedf30b1d29e122362068122c1ae = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7ff577ade3e1fcc849c8c98cd1da5b9725afeedf30b1d29e122362068122c1ae->enter($__internal_7ff577ade3e1fcc849c8c98cd1da5b9725afeedf30b1d29e122362068122c1ae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_db550237c921d1d98791ede93c99670cc5c77fde379531e2da5e3dccc0634517 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_db550237c921d1d98791ede93c99670cc5c77fde379531e2da5e3dccc0634517->enter($__internal_db550237c921d1d98791ede93c99670cc5c77fde379531e2da5e3dccc0634517_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "AppBundle:Blog:affichecour";
        
        $__internal_db550237c921d1d98791ede93c99670cc5c77fde379531e2da5e3dccc0634517->leave($__internal_db550237c921d1d98791ede93c99670cc5c77fde379531e2da5e3dccc0634517_prof);

        
        $__internal_7ff577ade3e1fcc849c8c98cd1da5b9725afeedf30b1d29e122362068122c1ae->leave($__internal_7ff577ade3e1fcc849c8c98cd1da5b9725afeedf30b1d29e122362068122c1ae_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_c747d3d84eacb0203c41a73019c1c97dd28a603dd22cea7fffe38c1c9fcedd3b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c747d3d84eacb0203c41a73019c1c97dd28a603dd22cea7fffe38c1c9fcedd3b->enter($__internal_c747d3d84eacb0203c41a73019c1c97dd28a603dd22cea7fffe38c1c9fcedd3b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_55c0a338d37fa83c1fcf737b2d9882b2cda08f7a5fe1abdf19c0794e7227334e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_55c0a338d37fa83c1fcf737b2d9882b2cda08f7a5fe1abdf19c0794e7227334e->enter($__internal_55c0a338d37fa83c1fcf737b2d9882b2cda08f7a5fe1abdf19c0794e7227334e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "<div class=\"container\">
  <table class=\"menuuser\">
    <tr>
      <td><a href=\"user_affichecour\" class=\"btn btn-large btn-info\">doccumment</a></td>
      <td><a href=\"user_affichepragramme\" class=\"btn btn-large btn-success\">programmes</a></td>
      <td><a href=\"user_afficheresultat\" class=\"btn btn-primary\">resultat</a></td>
      <td><a href=\"user_affichvideo\" class=\"btn btn-warning\">videos</a></td>
      <td><a href=\"\" class=\"btn btn-large btn-info\">déconnexion</a></td>
    </tr>

  </table><br><br>
  <a href=\"\" class=\"btn btn-large btn-info\">resultats publiés</a>


    \t\t<table class=\"table\" >
    \t\t\t<tr>
    \t\t\t\t<td> <b>intitulé du resultat</b></td><td> <b>date de publication</b></td><td> <b>action</b></td>
    \t\t\t</tr>

    \t\t\t";
        // line 25
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["document"]);
        foreach ($context['_seq'] as $context["_key"] => $context["document"]) {
            // line 26
            echo "
    \t\t\t<tr>
    \t\t\t\t<td> ";
            // line 28
            echo twig_escape_filter($this->env, $this->getAttribute($context["document"], "nom", array()), "html", null, true);
            echo "</td>
            <td> ";
            // line 29
            echo twig_escape_filter($this->env, $this->getAttribute($context["document"], "datePublication", array()), "html", null, true);
            echo "</td>
    \t\t\t\t<td><a href=\"";
            // line 30
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl(("uploads/document/" . $this->getAttribute($context["document"], "path", array()))), "html", null, true);
            echo "\" >Telecharger</a> </td>
    \t\t\t</tr>
    \t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['document'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 33
        echo "
    \t\t</table>

    \t<div class=\"navigation\">
    ";
        // line 37
        echo $this->env->getExtension('Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension')->render($this->env, ($context["document"] ?? $this->getContext($context, "document")));
        echo "
</div>
</div>
";
        
        $__internal_55c0a338d37fa83c1fcf737b2d9882b2cda08f7a5fe1abdf19c0794e7227334e->leave($__internal_55c0a338d37fa83c1fcf737b2d9882b2cda08f7a5fe1abdf19c0794e7227334e_prof);

        
        $__internal_c747d3d84eacb0203c41a73019c1c97dd28a603dd22cea7fffe38c1c9fcedd3b->leave($__internal_c747d3d84eacb0203c41a73019c1c97dd28a603dd22cea7fffe38c1c9fcedd3b_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle:Blog:resultat.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  120 => 37,  114 => 33,  105 => 30,  101 => 29,  97 => 28,  93 => 26,  89 => 25,  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"::base.html.twig\" %}

{% block title %}AppBundle:Blog:affichecour{% endblock %}

{% block body %}
<div class=\"container\">
  <table class=\"menuuser\">
    <tr>
      <td><a href=\"user_affichecour\" class=\"btn btn-large btn-info\">doccumment</a></td>
      <td><a href=\"user_affichepragramme\" class=\"btn btn-large btn-success\">programmes</a></td>
      <td><a href=\"user_afficheresultat\" class=\"btn btn-primary\">resultat</a></td>
      <td><a href=\"user_affichvideo\" class=\"btn btn-warning\">videos</a></td>
      <td><a href=\"\" class=\"btn btn-large btn-info\">déconnexion</a></td>
    </tr>

  </table><br><br>
  <a href=\"\" class=\"btn btn-large btn-info\">resultats publiés</a>


    \t\t<table class=\"table\" >
    \t\t\t<tr>
    \t\t\t\t<td> <b>intitulé du resultat</b></td><td> <b>date de publication</b></td><td> <b>action</b></td>
    \t\t\t</tr>

    \t\t\t{% for document in document %}

    \t\t\t<tr>
    \t\t\t\t<td> {{ document.nom }}</td>
            <td> {{ document.datePublication }}</td>
    \t\t\t\t<td><a href=\"{{ asset('uploads/document/' ~ document.path) }}\" >Telecharger</a> </td>
    \t\t\t</tr>
    \t\t\t{% endfor %}

    \t\t</table>

    \t<div class=\"navigation\">
    {{ knp_pagination_render(document) }}
</div>
</div>
{% endblock %}
", "AppBundle:Blog:resultat.html.twig", "/home/fonguen/symfony projet/infotels/var/cache/dev/../../../src/AppBundle/Resources/views/Blog/resultat.html.twig");
    }
}
