<?php

/* evenement/show.html.twig */
class __TwigTemplate_2c4289d7a7093fdd22131929cef80e6d67c60121e7f7f4a06237f1e197945e62 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "evenement/show.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ef8ddd0ec608e735144c8ae0076afd2341209eabdb61af1641b9b30964dd48cb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ef8ddd0ec608e735144c8ae0076afd2341209eabdb61af1641b9b30964dd48cb->enter($__internal_ef8ddd0ec608e735144c8ae0076afd2341209eabdb61af1641b9b30964dd48cb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "evenement/show.html.twig"));

        $__internal_268cbbdfc506175e29396b0a1ca68eaf15e61bd0b9682736a6d4c3b0dfa4696e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_268cbbdfc506175e29396b0a1ca68eaf15e61bd0b9682736a6d4c3b0dfa4696e->enter($__internal_268cbbdfc506175e29396b0a1ca68eaf15e61bd0b9682736a6d4c3b0dfa4696e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "evenement/show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ef8ddd0ec608e735144c8ae0076afd2341209eabdb61af1641b9b30964dd48cb->leave($__internal_ef8ddd0ec608e735144c8ae0076afd2341209eabdb61af1641b9b30964dd48cb_prof);

        
        $__internal_268cbbdfc506175e29396b0a1ca68eaf15e61bd0b9682736a6d4c3b0dfa4696e->leave($__internal_268cbbdfc506175e29396b0a1ca68eaf15e61bd0b9682736a6d4c3b0dfa4696e_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_40ce5a697c1ce2da043e8d8611358156b70ffd08b772719c35dff189a0a90523 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_40ce5a697c1ce2da043e8d8611358156b70ffd08b772719c35dff189a0a90523->enter($__internal_40ce5a697c1ce2da043e8d8611358156b70ffd08b772719c35dff189a0a90523_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_20ea23d4c8f045b9d766133f332a0c24ee1a332afc9a352b2749a1f5e61244e0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_20ea23d4c8f045b9d766133f332a0c24ee1a332afc9a352b2749a1f5e61244e0->enter($__internal_20ea23d4c8f045b9d766133f332a0c24ee1a332afc9a352b2749a1f5e61244e0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Evenement</h1>

    <table>
        <tbody>
            <tr>
                <th>Id</th>
                <td>";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute(($context["evenement"] ?? $this->getContext($context, "evenement")), "id", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Intituleevenement</th>
                <td>";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute(($context["evenement"] ?? $this->getContext($context, "evenement")), "intituleEvenement", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Dateevenement</th>
                <td>";
        // line 18
        if ($this->getAttribute(($context["evenement"] ?? $this->getContext($context, "evenement")), "dateEvenement", array())) {
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute(($context["evenement"] ?? $this->getContext($context, "evenement")), "dateEvenement", array()), "Y-m-d H:i:s"), "html", null, true);
        }
        echo "</td>
            </tr>
            <tr>
                <th>Description</th>
                <td>";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute(($context["evenement"] ?? $this->getContext($context, "evenement")), "description", array()), "html", null, true);
        echo "</td>
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"";
        // line 29
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_evenement_index");
        echo "\" class=\"btn btn-success\">Back to the list</a>
        </li>
        <li>
            <a href=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_evenement_edit", array("id" => $this->getAttribute(($context["evenement"] ?? $this->getContext($context, "evenement")), "id", array()))), "html", null, true);
        echo "\" class=\"btn btn-warning\" >Edit</a>
        </li>
        <li>
            ";
        // line 35
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_start');
        echo "
                <input type=\"submit\" value=\"Delete\" class=\"btn btn-danger\">
            ";
        // line 37
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_end');
        echo "
        </li>
    </ul>
";
        
        $__internal_20ea23d4c8f045b9d766133f332a0c24ee1a332afc9a352b2749a1f5e61244e0->leave($__internal_20ea23d4c8f045b9d766133f332a0c24ee1a332afc9a352b2749a1f5e61244e0_prof);

        
        $__internal_40ce5a697c1ce2da043e8d8611358156b70ffd08b772719c35dff189a0a90523->leave($__internal_40ce5a697c1ce2da043e8d8611358156b70ffd08b772719c35dff189a0a90523_prof);

    }

    public function getTemplateName()
    {
        return "evenement/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  107 => 37,  102 => 35,  96 => 32,  90 => 29,  80 => 22,  71 => 18,  64 => 14,  57 => 10,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Evenement</h1>

    <table>
        <tbody>
            <tr>
                <th>Id</th>
                <td>{{ evenement.id }}</td>
            </tr>
            <tr>
                <th>Intituleevenement</th>
                <td>{{ evenement.intituleEvenement }}</td>
            </tr>
            <tr>
                <th>Dateevenement</th>
                <td>{% if evenement.dateEvenement %}{{ evenement.dateEvenement|date('Y-m-d H:i:s') }}{% endif %}</td>
            </tr>
            <tr>
                <th>Description</th>
                <td>{{ evenement.description }}</td>
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"{{ path('admin_evenement_index') }}\" class=\"btn btn-success\">Back to the list</a>
        </li>
        <li>
            <a href=\"{{ path('admin_evenement_edit', { 'id': evenement.id }) }}\" class=\"btn btn-warning\" >Edit</a>
        </li>
        <li>
            {{ form_start(delete_form) }}
                <input type=\"submit\" value=\"Delete\" class=\"btn btn-danger\">
            {{ form_end(delete_form) }}
        </li>
    </ul>
{% endblock %}
", "evenement/show.html.twig", "/home/fonguen/symfony projet/infotels/app/Resources/views/evenement/show.html.twig");
    }
}
