<?php

/* @Framework/Form/form_widget.html.php */
class __TwigTemplate_d1453fb99422597676fb00f3453b286feb3bba4448a395940c4b209de298d742 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c4e2db1199c5e884a3f803a4ff1b6999f85f784b843c1fcec0253433bbc7dc0a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c4e2db1199c5e884a3f803a4ff1b6999f85f784b843c1fcec0253433bbc7dc0a->enter($__internal_c4e2db1199c5e884a3f803a4ff1b6999f85f784b843c1fcec0253433bbc7dc0a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget.html.php"));

        $__internal_4f48cfaf21ade329bcc09842e40077707d1791df0eccf63579d9fc89bd520f70 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4f48cfaf21ade329bcc09842e40077707d1791df0eccf63579d9fc89bd520f70->enter($__internal_4f48cfaf21ade329bcc09842e40077707d1791df0eccf63579d9fc89bd520f70_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget.html.php"));

        // line 1
        echo "<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
";
        
        $__internal_c4e2db1199c5e884a3f803a4ff1b6999f85f784b843c1fcec0253433bbc7dc0a->leave($__internal_c4e2db1199c5e884a3f803a4ff1b6999f85f784b843c1fcec0253433bbc7dc0a_prof);

        
        $__internal_4f48cfaf21ade329bcc09842e40077707d1791df0eccf63579d9fc89bd520f70->leave($__internal_4f48cfaf21ade329bcc09842e40077707d1791df0eccf63579d9fc89bd520f70_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
", "@Framework/Form/form_widget.html.php", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_widget.html.php");
    }
}
