<?php

/* niveau/edit.html.twig */
class __TwigTemplate_a0adb36cc8d32da99e94db9f037a6e9fd450e173f839dbdcab6033be1d3a9c3b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "niveau/edit.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_78128c8b13ab786a17203094f3b8ad89836ba51227f1ab6ea79d77c7ee9685a3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_78128c8b13ab786a17203094f3b8ad89836ba51227f1ab6ea79d77c7ee9685a3->enter($__internal_78128c8b13ab786a17203094f3b8ad89836ba51227f1ab6ea79d77c7ee9685a3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "niveau/edit.html.twig"));

        $__internal_d33cec8e9e731535ce0d4cbe629ec33eb1bf909a911411dedc08e5ef1301c775 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d33cec8e9e731535ce0d4cbe629ec33eb1bf909a911411dedc08e5ef1301c775->enter($__internal_d33cec8e9e731535ce0d4cbe629ec33eb1bf909a911411dedc08e5ef1301c775_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "niveau/edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_78128c8b13ab786a17203094f3b8ad89836ba51227f1ab6ea79d77c7ee9685a3->leave($__internal_78128c8b13ab786a17203094f3b8ad89836ba51227f1ab6ea79d77c7ee9685a3_prof);

        
        $__internal_d33cec8e9e731535ce0d4cbe629ec33eb1bf909a911411dedc08e5ef1301c775->leave($__internal_d33cec8e9e731535ce0d4cbe629ec33eb1bf909a911411dedc08e5ef1301c775_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_76198c23201bda747a5cad5fed5efad8cce625a861ea929323a8404c77fafc9a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_76198c23201bda747a5cad5fed5efad8cce625a861ea929323a8404c77fafc9a->enter($__internal_76198c23201bda747a5cad5fed5efad8cce625a861ea929323a8404c77fafc9a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_0061f89a2da8b07cba94762bad1e2559a6b1785b285c57a0e103d74f81d05e0c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0061f89a2da8b07cba94762bad1e2559a6b1785b285c57a0e103d74f81d05e0c->enter($__internal_0061f89a2da8b07cba94762bad1e2559a6b1785b285c57a0e103d74f81d05e0c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Niveau edit</h1>

    ";
        // line 6
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'form_start');
        echo "
        ";
        // line 7
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'widget');
        echo "
        <input type=\"submit\" value=\"Edit\" />
    ";
        // line 9
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'form_end');
        echo "

    <ul>
        <li>
            <a href=\"";
        // line 13
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_niveau_index");
        echo "\">Back to the list</a>
        </li>
        <li>
            ";
        // line 16
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_start');
        echo "
                <input type=\"submit\" value=\"Delete\">
            ";
        // line 18
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_end');
        echo "
        </li>
    </ul>
";
        
        $__internal_0061f89a2da8b07cba94762bad1e2559a6b1785b285c57a0e103d74f81d05e0c->leave($__internal_0061f89a2da8b07cba94762bad1e2559a6b1785b285c57a0e103d74f81d05e0c_prof);

        
        $__internal_76198c23201bda747a5cad5fed5efad8cce625a861ea929323a8404c77fafc9a->leave($__internal_76198c23201bda747a5cad5fed5efad8cce625a861ea929323a8404c77fafc9a_prof);

    }

    public function getTemplateName()
    {
        return "niveau/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 18,  75 => 16,  69 => 13,  62 => 9,  57 => 7,  53 => 6,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Niveau edit</h1>

    {{ form_start(edit_form) }}
        {{ form_widget(edit_form) }}
        <input type=\"submit\" value=\"Edit\" />
    {{ form_end(edit_form) }}

    <ul>
        <li>
            <a href=\"{{ path('admin_niveau_index') }}\">Back to the list</a>
        </li>
        <li>
            {{ form_start(delete_form) }}
                <input type=\"submit\" value=\"Delete\">
            {{ form_end(delete_form) }}
        </li>
    </ul>
{% endblock %}
", "niveau/edit.html.twig", "/home/fonguen/symfony projet/infotels/app/Resources/views/niveau/edit.html.twig");
    }
}
