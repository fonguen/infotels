<?php

/* @FOSUser/Resetting/request.html.twig */
class __TwigTemplate_3ec66d055d337906392e07c48fc1b6eb51eb3b5cae299328d89a2f66e731435f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Resetting/request.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9d5dbccebf6e7e2de14bbd79411284519aa3c396c065d58db4c87423184f9abc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9d5dbccebf6e7e2de14bbd79411284519aa3c396c065d58db4c87423184f9abc->enter($__internal_9d5dbccebf6e7e2de14bbd79411284519aa3c396c065d58db4c87423184f9abc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Resetting/request.html.twig"));

        $__internal_31244358c03e0bad0df8b168cc8dfa19c12bed4543ee696c59cc3890fe0e71f6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_31244358c03e0bad0df8b168cc8dfa19c12bed4543ee696c59cc3890fe0e71f6->enter($__internal_31244358c03e0bad0df8b168cc8dfa19c12bed4543ee696c59cc3890fe0e71f6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Resetting/request.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9d5dbccebf6e7e2de14bbd79411284519aa3c396c065d58db4c87423184f9abc->leave($__internal_9d5dbccebf6e7e2de14bbd79411284519aa3c396c065d58db4c87423184f9abc_prof);

        
        $__internal_31244358c03e0bad0df8b168cc8dfa19c12bed4543ee696c59cc3890fe0e71f6->leave($__internal_31244358c03e0bad0df8b168cc8dfa19c12bed4543ee696c59cc3890fe0e71f6_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_59e150695ba7b83157e52cad5187bc813583fa8e13f4269c4501828e7745d978 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_59e150695ba7b83157e52cad5187bc813583fa8e13f4269c4501828e7745d978->enter($__internal_59e150695ba7b83157e52cad5187bc813583fa8e13f4269c4501828e7745d978_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_865b9d9a5082fb9424aa07c16377f5eee13261e3716767daeeba21f991725ad3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_865b9d9a5082fb9424aa07c16377f5eee13261e3716767daeeba21f991725ad3->enter($__internal_865b9d9a5082fb9424aa07c16377f5eee13261e3716767daeeba21f991725ad3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Resetting/request_content.html.twig", "@FOSUser/Resetting/request.html.twig", 4)->display($context);
        
        $__internal_865b9d9a5082fb9424aa07c16377f5eee13261e3716767daeeba21f991725ad3->leave($__internal_865b9d9a5082fb9424aa07c16377f5eee13261e3716767daeeba21f991725ad3_prof);

        
        $__internal_59e150695ba7b83157e52cad5187bc813583fa8e13f4269c4501828e7745d978->leave($__internal_59e150695ba7b83157e52cad5187bc813583fa8e13f4269c4501828e7745d978_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Resetting/request.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Resetting/request_content.html.twig\" %}
{% endblock fos_user_content %}
", "@FOSUser/Resetting/request.html.twig", "/home/fonguen/symfony projet/infotels/vendor/friendsofsymfony/user-bundle/Resources/views/Resetting/request.html.twig");
    }
}
