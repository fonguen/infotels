<?php

/* @WebProfiler/Collector/exception.html.twig */
class __TwigTemplate_a0558bfcd5b4b8bf2c553e9b37390d5919a604b988b8aac4a40c72e2d1e7579f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8e828e3feb34c00b789ce02804a4baa02c9f5ff93523ffac45351a15195f3f9d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8e828e3feb34c00b789ce02804a4baa02c9f5ff93523ffac45351a15195f3f9d->enter($__internal_8e828e3feb34c00b789ce02804a4baa02c9f5ff93523ffac45351a15195f3f9d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $__internal_61ae5b3536aa667a3cecaff2cf7f1bea8b0b34a3b6e879a847658fc0f8a25cd9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_61ae5b3536aa667a3cecaff2cf7f1bea8b0b34a3b6e879a847658fc0f8a25cd9->enter($__internal_61ae5b3536aa667a3cecaff2cf7f1bea8b0b34a3b6e879a847658fc0f8a25cd9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8e828e3feb34c00b789ce02804a4baa02c9f5ff93523ffac45351a15195f3f9d->leave($__internal_8e828e3feb34c00b789ce02804a4baa02c9f5ff93523ffac45351a15195f3f9d_prof);

        
        $__internal_61ae5b3536aa667a3cecaff2cf7f1bea8b0b34a3b6e879a847658fc0f8a25cd9->leave($__internal_61ae5b3536aa667a3cecaff2cf7f1bea8b0b34a3b6e879a847658fc0f8a25cd9_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_a47219ea8545eba42f3f078fa3385700a0a678a34379f9195896b9edee399001 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a47219ea8545eba42f3f078fa3385700a0a678a34379f9195896b9edee399001->enter($__internal_a47219ea8545eba42f3f078fa3385700a0a678a34379f9195896b9edee399001_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_97b452bf2ed244563fe17601e78d26db35babfe44dfc1c9848a042b26c697f7b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_97b452bf2ed244563fe17601e78d26db35babfe44dfc1c9848a042b26c697f7b->enter($__internal_97b452bf2ed244563fe17601e78d26db35babfe44dfc1c9848a042b26c697f7b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_97b452bf2ed244563fe17601e78d26db35babfe44dfc1c9848a042b26c697f7b->leave($__internal_97b452bf2ed244563fe17601e78d26db35babfe44dfc1c9848a042b26c697f7b_prof);

        
        $__internal_a47219ea8545eba42f3f078fa3385700a0a678a34379f9195896b9edee399001->leave($__internal_a47219ea8545eba42f3f078fa3385700a0a678a34379f9195896b9edee399001_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_6291c17b1b2af9722d4abaa4b01b018e2791fcb8bd629a3d920d7e1d7d17f55f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6291c17b1b2af9722d4abaa4b01b018e2791fcb8bd629a3d920d7e1d7d17f55f->enter($__internal_6291c17b1b2af9722d4abaa4b01b018e2791fcb8bd629a3d920d7e1d7d17f55f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_86dda3912380487cad94a30225458a4ed3b8f401740df16cb17c22616439dce2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_86dda3912380487cad94a30225458a4ed3b8f401740df16cb17c22616439dce2->enter($__internal_86dda3912380487cad94a30225458a4ed3b8f401740df16cb17c22616439dce2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo (($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_86dda3912380487cad94a30225458a4ed3b8f401740df16cb17c22616439dce2->leave($__internal_86dda3912380487cad94a30225458a4ed3b8f401740df16cb17c22616439dce2_prof);

        
        $__internal_6291c17b1b2af9722d4abaa4b01b018e2791fcb8bd629a3d920d7e1d7d17f55f->leave($__internal_6291c17b1b2af9722d4abaa4b01b018e2791fcb8bd629a3d920d7e1d7d17f55f_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_afc16cd861462f01109a7ee43f27d2009239d8769bf784247ccd1ec151cc8c84 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_afc16cd861462f01109a7ee43f27d2009239d8769bf784247ccd1ec151cc8c84->enter($__internal_afc16cd861462f01109a7ee43f27d2009239d8769bf784247ccd1ec151cc8c84_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_d5d49e115819caeea849ae811d9e046ae640d69579b79b4c7e478800ea5685d4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d5d49e115819caeea849ae811d9e046ae640d69579b79b4c7e478800ea5685d4->enter($__internal_d5d49e115819caeea849ae811d9e046ae640d69579b79b4c7e478800ea5685d4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !$this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </div>
    ";
        }
        
        $__internal_d5d49e115819caeea849ae811d9e046ae640d69579b79b4c7e478800ea5685d4->leave($__internal_d5d49e115819caeea849ae811d9e046ae640d69579b79b4c7e478800ea5685d4_prof);

        
        $__internal_afc16cd861462f01109a7ee43f27d2009239d8769bf784247ccd1ec151cc8c84->leave($__internal_afc16cd861462f01109a7ee43f27d2009239d8769bf784247ccd1ec151cc8c84_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "@WebProfiler/Collector/exception.html.twig", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/exception.html.twig");
    }
}
