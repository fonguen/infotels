<?php

/* document/edit.html.twig */
class __TwigTemplate_81b947acf12a5563f9acfe6cd8a11ccfaa8690571bde6e29a2240ed488d39934 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "document/edit.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_72fb0e1f4065da902063f276c014488e3d5351b44f00a377b3d94aa7c8042aab = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_72fb0e1f4065da902063f276c014488e3d5351b44f00a377b3d94aa7c8042aab->enter($__internal_72fb0e1f4065da902063f276c014488e3d5351b44f00a377b3d94aa7c8042aab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "document/edit.html.twig"));

        $__internal_b46e775c74c0825c6fd0b19c8ed71cc36dcbbca8cdd95cbcda34881f206008cf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b46e775c74c0825c6fd0b19c8ed71cc36dcbbca8cdd95cbcda34881f206008cf->enter($__internal_b46e775c74c0825c6fd0b19c8ed71cc36dcbbca8cdd95cbcda34881f206008cf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "document/edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_72fb0e1f4065da902063f276c014488e3d5351b44f00a377b3d94aa7c8042aab->leave($__internal_72fb0e1f4065da902063f276c014488e3d5351b44f00a377b3d94aa7c8042aab_prof);

        
        $__internal_b46e775c74c0825c6fd0b19c8ed71cc36dcbbca8cdd95cbcda34881f206008cf->leave($__internal_b46e775c74c0825c6fd0b19c8ed71cc36dcbbca8cdd95cbcda34881f206008cf_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_9d9b9de2211e0454792932e1cbf65f40ef4e8daaa29a64c4ffb5799ade2d0b5b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9d9b9de2211e0454792932e1cbf65f40ef4e8daaa29a64c4ffb5799ade2d0b5b->enter($__internal_9d9b9de2211e0454792932e1cbf65f40ef4e8daaa29a64c4ffb5799ade2d0b5b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_42eb40b0f71251d9a4a897ebf5c530f84e91b612887404f0744ab6a48d7aab10 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_42eb40b0f71251d9a4a897ebf5c530f84e91b612887404f0744ab6a48d7aab10->enter($__internal_42eb40b0f71251d9a4a897ebf5c530f84e91b612887404f0744ab6a48d7aab10_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Document edit</h1>

    ";
        // line 6
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'form_start');
        echo "
        ";
        // line 7
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'widget');
        echo "
        <input type=\"submit\" value=\"Edit\" />
    ";
        // line 9
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'form_end');
        echo "

    <ul>
        <li>
            <a href=\"";
        // line 13
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_document_index");
        echo "\">Back to the list</a>
        </li>
        <li>
            ";
        // line 16
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_start');
        echo "
                <input type=\"submit\" value=\"Delete\">
            ";
        // line 18
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_end');
        echo "
        </li>
    </ul>
";
        
        $__internal_42eb40b0f71251d9a4a897ebf5c530f84e91b612887404f0744ab6a48d7aab10->leave($__internal_42eb40b0f71251d9a4a897ebf5c530f84e91b612887404f0744ab6a48d7aab10_prof);

        
        $__internal_9d9b9de2211e0454792932e1cbf65f40ef4e8daaa29a64c4ffb5799ade2d0b5b->leave($__internal_9d9b9de2211e0454792932e1cbf65f40ef4e8daaa29a64c4ffb5799ade2d0b5b_prof);

    }

    public function getTemplateName()
    {
        return "document/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 18,  75 => 16,  69 => 13,  62 => 9,  57 => 7,  53 => 6,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Document edit</h1>

    {{ form_start(edit_form) }}
        {{ form_widget(edit_form) }}
        <input type=\"submit\" value=\"Edit\" />
    {{ form_end(edit_form) }}

    <ul>
        <li>
            <a href=\"{{ path('admin_document_index') }}\">Back to the list</a>
        </li>
        <li>
            {{ form_start(delete_form) }}
                <input type=\"submit\" value=\"Delete\">
            {{ form_end(delete_form) }}
        </li>
    </ul>
{% endblock %}
", "document/edit.html.twig", "/home/fonguen/symfony projet/infotels/app/Resources/views/document/edit.html.twig");
    }
}
