<?php

/* @KnpPaginator/Pagination/sortable_link.html.twig */
class __TwigTemplate_ff7c71e74a20e4c61f5cc7d241cb0ba390fa33ccf5c667155c0cc4c25829da7a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ea97f4e567c14b4c51bc6d3bf31a957c8c6f7dc7cd15d30783e694d06a816281 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ea97f4e567c14b4c51bc6d3bf31a957c8c6f7dc7cd15d30783e694d06a816281->enter($__internal_ea97f4e567c14b4c51bc6d3bf31a957c8c6f7dc7cd15d30783e694d06a816281_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@KnpPaginator/Pagination/sortable_link.html.twig"));

        $__internal_5efab48a50d8889228d7944814e8ff4f7a6c8bb19eea1ef4ec0e9ef4926a2cbf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5efab48a50d8889228d7944814e8ff4f7a6c8bb19eea1ef4ec0e9ef4926a2cbf->enter($__internal_5efab48a50d8889228d7944814e8ff4f7a6c8bb19eea1ef4ec0e9ef4926a2cbf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@KnpPaginator/Pagination/sortable_link.html.twig"));

        // line 1
        echo "<a";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["options"] ?? $this->getContext($context, "options")));
        foreach ($context['_seq'] as $context["attr"] => $context["value"]) {
            echo " ";
            echo twig_escape_filter($this->env, $context["attr"], "html", null, true);
            echo "=\"";
            echo twig_escape_filter($this->env, $context["value"], "html", null, true);
            echo "\"";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['attr'], $context['value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo ">";
        echo twig_escape_filter($this->env, ($context["title"] ?? $this->getContext($context, "title")), "html", null, true);
        echo "</a>
";
        
        $__internal_ea97f4e567c14b4c51bc6d3bf31a957c8c6f7dc7cd15d30783e694d06a816281->leave($__internal_ea97f4e567c14b4c51bc6d3bf31a957c8c6f7dc7cd15d30783e694d06a816281_prof);

        
        $__internal_5efab48a50d8889228d7944814e8ff4f7a6c8bb19eea1ef4ec0e9ef4926a2cbf->leave($__internal_5efab48a50d8889228d7944814e8ff4f7a6c8bb19eea1ef4ec0e9ef4926a2cbf_prof);

    }

    public function getTemplateName()
    {
        return "@KnpPaginator/Pagination/sortable_link.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<a{% for attr, value in options %} {{ attr }}=\"{{ value }}\"{% endfor %}>{{ title }}</a>
", "@KnpPaginator/Pagination/sortable_link.html.twig", "/home/fonguen/symfony projet/infotels/vendor/knplabs/knp-paginator-bundle/Resources/views/Pagination/sortable_link.html.twig");
    }
}
