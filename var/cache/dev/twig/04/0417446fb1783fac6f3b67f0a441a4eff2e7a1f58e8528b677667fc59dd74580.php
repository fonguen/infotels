<?php

/* formation/edit.html.twig */
class __TwigTemplate_f065cb70b46930231d661e88542052e3b20d103b2a1dc624293bdfbc32425560 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "formation/edit.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2f7bc000c82cf808835c6837f90b7ca8ba6c5e6e0303e341a6376451f2d8b5ba = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2f7bc000c82cf808835c6837f90b7ca8ba6c5e6e0303e341a6376451f2d8b5ba->enter($__internal_2f7bc000c82cf808835c6837f90b7ca8ba6c5e6e0303e341a6376451f2d8b5ba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "formation/edit.html.twig"));

        $__internal_8e4a0a5b17e64ff315929937cffe6bf8aef54ec3baeb2f3f1b39a1dbec0164e9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8e4a0a5b17e64ff315929937cffe6bf8aef54ec3baeb2f3f1b39a1dbec0164e9->enter($__internal_8e4a0a5b17e64ff315929937cffe6bf8aef54ec3baeb2f3f1b39a1dbec0164e9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "formation/edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2f7bc000c82cf808835c6837f90b7ca8ba6c5e6e0303e341a6376451f2d8b5ba->leave($__internal_2f7bc000c82cf808835c6837f90b7ca8ba6c5e6e0303e341a6376451f2d8b5ba_prof);

        
        $__internal_8e4a0a5b17e64ff315929937cffe6bf8aef54ec3baeb2f3f1b39a1dbec0164e9->leave($__internal_8e4a0a5b17e64ff315929937cffe6bf8aef54ec3baeb2f3f1b39a1dbec0164e9_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_2b35321c21c788e868227291767b5cf9d40b9e943d12394437851d23e9095bcd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2b35321c21c788e868227291767b5cf9d40b9e943d12394437851d23e9095bcd->enter($__internal_2b35321c21c788e868227291767b5cf9d40b9e943d12394437851d23e9095bcd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_9c71cbf7930dc4fb7e3826cb3e7995ef813eb6c0a23f4c608a134fe474e2ccfb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9c71cbf7930dc4fb7e3826cb3e7995ef813eb6c0a23f4c608a134fe474e2ccfb->enter($__internal_9c71cbf7930dc4fb7e3826cb3e7995ef813eb6c0a23f4c608a134fe474e2ccfb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<br><br><br>
    <h1>Formation edit</h1>

    ";
        // line 7
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'form_start');
        echo "
        ";
        // line 8
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'widget');
        echo "
        <input type=\"submit\" value=\"Edit\" />
    ";
        // line 10
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'form_end');
        echo "

    <ul>
        <li>
            <a href=\"";
        // line 14
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_formation_index");
        echo "\">Back to the list</a>
        </li>
        <li>
            ";
        // line 17
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_start');
        echo "
                <input type=\"submit\" value=\"Delete\">
            ";
        // line 19
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_end');
        echo "
        </li>
    </ul>
";
        
        $__internal_9c71cbf7930dc4fb7e3826cb3e7995ef813eb6c0a23f4c608a134fe474e2ccfb->leave($__internal_9c71cbf7930dc4fb7e3826cb3e7995ef813eb6c0a23f4c608a134fe474e2ccfb_prof);

        
        $__internal_2b35321c21c788e868227291767b5cf9d40b9e943d12394437851d23e9095bcd->leave($__internal_2b35321c21c788e868227291767b5cf9d40b9e943d12394437851d23e9095bcd_prof);

    }

    public function getTemplateName()
    {
        return "formation/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  81 => 19,  76 => 17,  70 => 14,  63 => 10,  58 => 8,  54 => 7,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
<br><br><br>
    <h1>Formation edit</h1>

    {{ form_start(edit_form) }}
        {{ form_widget(edit_form) }}
        <input type=\"submit\" value=\"Edit\" />
    {{ form_end(edit_form) }}

    <ul>
        <li>
            <a href=\"{{ path('admin_formation_index') }}\">Back to the list</a>
        </li>
        <li>
            {{ form_start(delete_form) }}
                <input type=\"submit\" value=\"Delete\">
            {{ form_end(delete_form) }}
        </li>
    </ul>
{% endblock %}
", "formation/edit.html.twig", "/home/fonguen/symfony projet/infotels/app/Resources/views/formation/edit.html.twig");
    }
}
