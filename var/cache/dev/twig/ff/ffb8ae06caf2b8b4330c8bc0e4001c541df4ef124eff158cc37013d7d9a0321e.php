<?php

/* @FOSUser/Registration/check_email.html.twig */
class __TwigTemplate_17b9c5d45bda97348a9883deba3d882e6cafa40fd501b547ff0ba582d576373c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Registration/check_email.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_edcabadaa16952d863364e94699be267cb1991a4d034ceb3d9f23b1a2b73166b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_edcabadaa16952d863364e94699be267cb1991a4d034ceb3d9f23b1a2b73166b->enter($__internal_edcabadaa16952d863364e94699be267cb1991a4d034ceb3d9f23b1a2b73166b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/check_email.html.twig"));

        $__internal_24acc1eef0174d08a2266fc9fadbc04053c1f810a40a69a5a241ff855cfe47e9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_24acc1eef0174d08a2266fc9fadbc04053c1f810a40a69a5a241ff855cfe47e9->enter($__internal_24acc1eef0174d08a2266fc9fadbc04053c1f810a40a69a5a241ff855cfe47e9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/check_email.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_edcabadaa16952d863364e94699be267cb1991a4d034ceb3d9f23b1a2b73166b->leave($__internal_edcabadaa16952d863364e94699be267cb1991a4d034ceb3d9f23b1a2b73166b_prof);

        
        $__internal_24acc1eef0174d08a2266fc9fadbc04053c1f810a40a69a5a241ff855cfe47e9->leave($__internal_24acc1eef0174d08a2266fc9fadbc04053c1f810a40a69a5a241ff855cfe47e9_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_eec57606adab45bd5b0e8073605d8f5356304ce53dbd305137269558545eb278 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_eec57606adab45bd5b0e8073605d8f5356304ce53dbd305137269558545eb278->enter($__internal_eec57606adab45bd5b0e8073605d8f5356304ce53dbd305137269558545eb278_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_6915fccc0721367ae70760e936a4b5d936c05962865616b7bc97682082be970d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6915fccc0721367ae70760e936a4b5d936c05962865616b7bc97682082be970d->enter($__internal_6915fccc0721367ae70760e936a4b5d936c05962865616b7bc97682082be970d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "    <p>";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.check_email", array("%email%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "email", array())), "FOSUserBundle"), "html", null, true);
        echo "</p>
";
        
        $__internal_6915fccc0721367ae70760e936a4b5d936c05962865616b7bc97682082be970d->leave($__internal_6915fccc0721367ae70760e936a4b5d936c05962865616b7bc97682082be970d_prof);

        
        $__internal_eec57606adab45bd5b0e8073605d8f5356304ce53dbd305137269558545eb278->leave($__internal_eec57606adab45bd5b0e8073605d8f5356304ce53dbd305137269558545eb278_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Registration/check_email.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 6,  40 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% trans_default_domain 'FOSUserBundle' %}

{% block fos_user_content %}
    <p>{{ 'registration.check_email'|trans({'%email%': user.email}) }}</p>
{% endblock fos_user_content %}
", "@FOSUser/Registration/check_email.html.twig", "/home/fonguen/symfony projet/infotels/vendor/friendsofsymfony/user-bundle/Resources/views/Registration/check_email.html.twig");
    }
}
