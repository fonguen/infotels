<?php

/* AppBundle:Blog:affichvideo.html.twig */
class __TwigTemplate_49f7873baaf9069ddd7593da70d7f5029e74898546f4a2520278ff60f08978c5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "AppBundle:Blog:affichvideo.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_90de178b4943bc80da15912abf989fc8a66a31c76fbc3c9c2d21459511bdb83e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_90de178b4943bc80da15912abf989fc8a66a31c76fbc3c9c2d21459511bdb83e->enter($__internal_90de178b4943bc80da15912abf989fc8a66a31c76fbc3c9c2d21459511bdb83e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:Blog:affichvideo.html.twig"));

        $__internal_8a9205e460217e7a31bc3286b1cd59f0c7bafd13ce6acca034b31be516f55557 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8a9205e460217e7a31bc3286b1cd59f0c7bafd13ce6acca034b31be516f55557->enter($__internal_8a9205e460217e7a31bc3286b1cd59f0c7bafd13ce6acca034b31be516f55557_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:Blog:affichvideo.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_90de178b4943bc80da15912abf989fc8a66a31c76fbc3c9c2d21459511bdb83e->leave($__internal_90de178b4943bc80da15912abf989fc8a66a31c76fbc3c9c2d21459511bdb83e_prof);

        
        $__internal_8a9205e460217e7a31bc3286b1cd59f0c7bafd13ce6acca034b31be516f55557->leave($__internal_8a9205e460217e7a31bc3286b1cd59f0c7bafd13ce6acca034b31be516f55557_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_6dac0dd5c789500ed05761543d15d1379ae328470ee34d1ed93dfa02caf92c9f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6dac0dd5c789500ed05761543d15d1379ae328470ee34d1ed93dfa02caf92c9f->enter($__internal_6dac0dd5c789500ed05761543d15d1379ae328470ee34d1ed93dfa02caf92c9f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_cbb1deb7029e33a2238805e4d6488f69d574a92f9d90702d6e74435cc9e90597 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cbb1deb7029e33a2238805e4d6488f69d574a92f9d90702d6e74435cc9e90597->enter($__internal_cbb1deb7029e33a2238805e4d6488f69d574a92f9d90702d6e74435cc9e90597_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "AppBundle:Blog:affichvideo";
        
        $__internal_cbb1deb7029e33a2238805e4d6488f69d574a92f9d90702d6e74435cc9e90597->leave($__internal_cbb1deb7029e33a2238805e4d6488f69d574a92f9d90702d6e74435cc9e90597_prof);

        
        $__internal_6dac0dd5c789500ed05761543d15d1379ae328470ee34d1ed93dfa02caf92c9f->leave($__internal_6dac0dd5c789500ed05761543d15d1379ae328470ee34d1ed93dfa02caf92c9f_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_5c89eaf13976b54e281d83884a4430092b4a41f4b4b1f7d7bcde93612e1d6c08 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5c89eaf13976b54e281d83884a4430092b4a41f4b4b1f7d7bcde93612e1d6c08->enter($__internal_5c89eaf13976b54e281d83884a4430092b4a41f4b4b1f7d7bcde93612e1d6c08_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_fb2d5b434ab247783f297a8d1e110888cacbe5c6b9d5d19febb28ff5a4df4a36 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fb2d5b434ab247783f297a8d1e110888cacbe5c6b9d5d19febb28ff5a4df4a36->enter($__internal_fb2d5b434ab247783f297a8d1e110888cacbe5c6b9d5d19febb28ff5a4df4a36_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "<br><br>
<div class=\"container\">
  <table class=\"menuuser\">
    <tr>
      <td><a href=\"user_affichecour\" class=\"btn btn-large btn-info\"><i class=\"fa fa-book\" aria-hidden=\"true\"></i>
        doccumment</a></td>
      <td><a href=\"user_affichepragramme\" class=\"btn btn-large btn-success\"><i class=\"fa fa-edit\" ></i>programmes</a></td>
      <td><a href=\"user_afficheresultat\" class=\"btn btn-primary\"><i class=\"fa fa-paper-plane\" aria-hidden=\"true\"></i>
        resultat</a></td>
      <td><a href=\"user_affichvideo\" class=\"btn btn-warning\"><i class=\"fa fa-fast-forward\" aria-hidden=\"true\"></i>
        videos</a></td>
      <td><a href=\"\" class=\"btn btn-large btn-info\"><i class=\"fa fa-wrench\" aria-hidden=\"true\"></i>déconnexion</a></td>
    </tr>

  </table><br><br>



<div class=\"row\">
    ";
        // line 25
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["document"]);
        foreach ($context['_seq'] as $context["_key"] => $context["document"]) {
            // line 26
            echo "    <div class=\"col-sm-4 col-lg-4 col-md-4\">
        <div class=\"thumbnails\"></div>
        <video controls alt=\"\" height=\"450\"   width=\"300\">
                            <source  src=\"";
            // line 29
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl(("uploads/document/" . $this->getAttribute($context["document"], "path", array()))), "html", null, true);
            echo "\" />

                        </video>
                        <div class=\"caption\">
                            <p> ";
            // line 33
            echo twig_escape_filter($this->env, $this->getAttribute($context["document"], "description", array()), "html", null, true);
            echo "</p>

                        </div>


    </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['document'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 40
        echo "

</div>
<div class=\"navigation\">
    ";
        // line 44
        echo $this->env->getExtension('Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension')->render($this->env, ($context["document"] ?? $this->getContext($context, "document")));
        echo "
  </div>
</div>

";
        
        $__internal_fb2d5b434ab247783f297a8d1e110888cacbe5c6b9d5d19febb28ff5a4df4a36->leave($__internal_fb2d5b434ab247783f297a8d1e110888cacbe5c6b9d5d19febb28ff5a4df4a36_prof);

        
        $__internal_5c89eaf13976b54e281d83884a4430092b4a41f4b4b1f7d7bcde93612e1d6c08->leave($__internal_5c89eaf13976b54e281d83884a4430092b4a41f4b4b1f7d7bcde93612e1d6c08_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle:Blog:affichvideo.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  124 => 44,  118 => 40,  105 => 33,  98 => 29,  93 => 26,  89 => 25,  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"::base.html.twig\" %}

{% block title %}AppBundle:Blog:affichvideo{% endblock %}

{% block body %}
<br><br>
<div class=\"container\">
  <table class=\"menuuser\">
    <tr>
      <td><a href=\"user_affichecour\" class=\"btn btn-large btn-info\"><i class=\"fa fa-book\" aria-hidden=\"true\"></i>
        doccumment</a></td>
      <td><a href=\"user_affichepragramme\" class=\"btn btn-large btn-success\"><i class=\"fa fa-edit\" ></i>programmes</a></td>
      <td><a href=\"user_afficheresultat\" class=\"btn btn-primary\"><i class=\"fa fa-paper-plane\" aria-hidden=\"true\"></i>
        resultat</a></td>
      <td><a href=\"user_affichvideo\" class=\"btn btn-warning\"><i class=\"fa fa-fast-forward\" aria-hidden=\"true\"></i>
        videos</a></td>
      <td><a href=\"\" class=\"btn btn-large btn-info\"><i class=\"fa fa-wrench\" aria-hidden=\"true\"></i>déconnexion</a></td>
    </tr>

  </table><br><br>



<div class=\"row\">
    {% for document in document %}
    <div class=\"col-sm-4 col-lg-4 col-md-4\">
        <div class=\"thumbnails\"></div>
        <video controls alt=\"\" height=\"450\"   width=\"300\">
                            <source  src=\"{{ asset('uploads/document/' ~ document.path) }}\" />

                        </video>
                        <div class=\"caption\">
                            <p> {{ document.description }}</p>

                        </div>


    </div>
    {% endfor %}


</div>
<div class=\"navigation\">
    {{ knp_pagination_render(document) }}
  </div>
</div>

{% endblock %}
", "AppBundle:Blog:affichvideo.html.twig", "/home/fonguen/symfony projet/infotels/var/cache/dev/../../../src/AppBundle/Resources/views/Blog/affichvideo.html.twig");
    }
}
