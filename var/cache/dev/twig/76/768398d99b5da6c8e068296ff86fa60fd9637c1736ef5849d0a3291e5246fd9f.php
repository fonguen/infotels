<?php

/* @Framework/Form/form_end.html.php */
class __TwigTemplate_2dffe6c0574d22e0ec9b7f48b6f57fff1d133e63e7c5f67564bb283cfb0d6675 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a5e74fc414c7a7398523845377bcf9cd2edad7970c1ef8da08aabeda7c75139d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a5e74fc414c7a7398523845377bcf9cd2edad7970c1ef8da08aabeda7c75139d->enter($__internal_a5e74fc414c7a7398523845377bcf9cd2edad7970c1ef8da08aabeda7c75139d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_end.html.php"));

        $__internal_a42b3c2f2f4d8770710874ffd4ebdc317cff74ef42d772449620af942e6b7868 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a42b3c2f2f4d8770710874ffd4ebdc317cff74ef42d772449620af942e6b7868->enter($__internal_a42b3c2f2f4d8770710874ffd4ebdc317cff74ef42d772449620af942e6b7868_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_end.html.php"));

        // line 1
        echo "<?php if (!isset(\$render_rest) || \$render_rest): ?>
<?php echo \$view['form']->rest(\$form) ?>
<?php endif ?>
</form>
";
        
        $__internal_a5e74fc414c7a7398523845377bcf9cd2edad7970c1ef8da08aabeda7c75139d->leave($__internal_a5e74fc414c7a7398523845377bcf9cd2edad7970c1ef8da08aabeda7c75139d_prof);

        
        $__internal_a42b3c2f2f4d8770710874ffd4ebdc317cff74ef42d772449620af942e6b7868->leave($__internal_a42b3c2f2f4d8770710874ffd4ebdc317cff74ef42d772449620af942e6b7868_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_end.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (!isset(\$render_rest) || \$render_rest): ?>
<?php echo \$view['form']->rest(\$form) ?>
<?php endif ?>
</form>
", "@Framework/Form/form_end.html.php", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_end.html.php");
    }
}
