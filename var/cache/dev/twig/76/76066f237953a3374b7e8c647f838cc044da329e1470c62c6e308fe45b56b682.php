<?php

/* departement/edit.html.twig */
class __TwigTemplate_9281e1ae8e63ce2d7e03adb751f4b0a7e014fdfa7c03009ab983d65e73691a71 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "departement/edit.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_56af92d71cb6acee0efa6b49e4fc1f866757f60c8bf00c0d8534fac84314ec58 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_56af92d71cb6acee0efa6b49e4fc1f866757f60c8bf00c0d8534fac84314ec58->enter($__internal_56af92d71cb6acee0efa6b49e4fc1f866757f60c8bf00c0d8534fac84314ec58_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "departement/edit.html.twig"));

        $__internal_ca1f102d1652d34d56946d20b66f73d35dae80767dd4b1adbe025343fe806b54 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ca1f102d1652d34d56946d20b66f73d35dae80767dd4b1adbe025343fe806b54->enter($__internal_ca1f102d1652d34d56946d20b66f73d35dae80767dd4b1adbe025343fe806b54_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "departement/edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_56af92d71cb6acee0efa6b49e4fc1f866757f60c8bf00c0d8534fac84314ec58->leave($__internal_56af92d71cb6acee0efa6b49e4fc1f866757f60c8bf00c0d8534fac84314ec58_prof);

        
        $__internal_ca1f102d1652d34d56946d20b66f73d35dae80767dd4b1adbe025343fe806b54->leave($__internal_ca1f102d1652d34d56946d20b66f73d35dae80767dd4b1adbe025343fe806b54_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_124cb34a4d9023b7893933402d0faee6b9d0c26098e17954fcd322dbb35f811a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_124cb34a4d9023b7893933402d0faee6b9d0c26098e17954fcd322dbb35f811a->enter($__internal_124cb34a4d9023b7893933402d0faee6b9d0c26098e17954fcd322dbb35f811a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_09884502814dfa713f466809d2368a50cfda3cf58cb2023a09b72e4dc769d449 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_09884502814dfa713f466809d2368a50cfda3cf58cb2023a09b72e4dc769d449->enter($__internal_09884502814dfa713f466809d2368a50cfda3cf58cb2023a09b72e4dc769d449_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<br><br>
<div class=\"container\">


    <h1>Departement edit</h1>


    ";
        // line 11
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'form_start');
        echo "
        ";
        // line 12
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'widget');
        echo "
        <input type=\"submit\" value=\"Edit\"class=\"btn btn-warning\" />
    ";
        // line 14
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'form_end');
        echo "

    <ul>
        <li>
            <a href=\"";
        // line 18
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_departement_index");
        echo "\" class=\"btn btn-success\">Back to the list</a>
        </li>
        <li>
            ";
        // line 21
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_start');
        echo "
                <input type=\"submit\" value=\"Delete\" class=\"btn btn-danger\">
            ";
        // line 23
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_end');
        echo "
        </li>
    </ul>

</div>
";
        
        $__internal_09884502814dfa713f466809d2368a50cfda3cf58cb2023a09b72e4dc769d449->leave($__internal_09884502814dfa713f466809d2368a50cfda3cf58cb2023a09b72e4dc769d449_prof);

        
        $__internal_124cb34a4d9023b7893933402d0faee6b9d0c26098e17954fcd322dbb35f811a->leave($__internal_124cb34a4d9023b7893933402d0faee6b9d0c26098e17954fcd322dbb35f811a_prof);

    }

    public function getTemplateName()
    {
        return "departement/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 23,  80 => 21,  74 => 18,  67 => 14,  62 => 12,  58 => 11,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
<br><br>
<div class=\"container\">


    <h1>Departement edit</h1>


    {{ form_start(edit_form) }}
        {{ form_widget(edit_form) }}
        <input type=\"submit\" value=\"Edit\"class=\"btn btn-warning\" />
    {{ form_end(edit_form) }}

    <ul>
        <li>
            <a href=\"{{ path('admin_departement_index') }}\" class=\"btn btn-success\">Back to the list</a>
        </li>
        <li>
            {{ form_start(delete_form) }}
                <input type=\"submit\" value=\"Delete\" class=\"btn btn-danger\">
            {{ form_end(delete_form) }}
        </li>
    </ul>

</div>
{% endblock %}
", "departement/edit.html.twig", "/home/fonguen/symfony projet/infotels/app/Resources/views/departement/edit.html.twig");
    }
}
