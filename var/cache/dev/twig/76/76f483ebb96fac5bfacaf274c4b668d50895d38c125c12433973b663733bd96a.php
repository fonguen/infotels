<?php

/* @Framework/Form/form_row.html.php */
class __TwigTemplate_c4de1f19dfbaf4dc0e971af7971e1950e5a7d75b4c542eb58dc9afcdaf029590 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c60a4d77cadfd3571ca553c452400bc48e28970b7b92d94a71b7f95522b75fb4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c60a4d77cadfd3571ca553c452400bc48e28970b7b92d94a71b7f95522b75fb4->enter($__internal_c60a4d77cadfd3571ca553c452400bc48e28970b7b92d94a71b7f95522b75fb4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_row.html.php"));

        $__internal_13dbcd65c32b0836146b72b5defbdafa9476ffb143601b812d36f7d175417178 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_13dbcd65c32b0836146b72b5defbdafa9476ffb143601b812d36f7d175417178->enter($__internal_13dbcd65c32b0836146b72b5defbdafa9476ffb143601b812d36f7d175417178_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->label(\$form) ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_c60a4d77cadfd3571ca553c452400bc48e28970b7b92d94a71b7f95522b75fb4->leave($__internal_c60a4d77cadfd3571ca553c452400bc48e28970b7b92d94a71b7f95522b75fb4_prof);

        
        $__internal_13dbcd65c32b0836146b72b5defbdafa9476ffb143601b812d36f7d175417178->leave($__internal_13dbcd65c32b0836146b72b5defbdafa9476ffb143601b812d36f7d175417178_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div>
    <?php echo \$view['form']->label(\$form) ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
", "@Framework/Form/form_row.html.php", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_row.html.php");
    }
}
