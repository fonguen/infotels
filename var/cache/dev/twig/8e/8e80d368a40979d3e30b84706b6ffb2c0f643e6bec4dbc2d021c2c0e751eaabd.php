<?php

/* evenement/edit.html.twig */
class __TwigTemplate_90f4625ff81e5e0d99196d4e6c414230c93d1672680cafe2446ec010b4f812f4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "evenement/edit.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_caabcc3277b368c029ba67252ec8312d2ae7e00193889cf4175f1763428cf10d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_caabcc3277b368c029ba67252ec8312d2ae7e00193889cf4175f1763428cf10d->enter($__internal_caabcc3277b368c029ba67252ec8312d2ae7e00193889cf4175f1763428cf10d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "evenement/edit.html.twig"));

        $__internal_e41464498df9de39cc9330837518fc9455d8529f41a0f014a4cb8b4fe7de8d78 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e41464498df9de39cc9330837518fc9455d8529f41a0f014a4cb8b4fe7de8d78->enter($__internal_e41464498df9de39cc9330837518fc9455d8529f41a0f014a4cb8b4fe7de8d78_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "evenement/edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_caabcc3277b368c029ba67252ec8312d2ae7e00193889cf4175f1763428cf10d->leave($__internal_caabcc3277b368c029ba67252ec8312d2ae7e00193889cf4175f1763428cf10d_prof);

        
        $__internal_e41464498df9de39cc9330837518fc9455d8529f41a0f014a4cb8b4fe7de8d78->leave($__internal_e41464498df9de39cc9330837518fc9455d8529f41a0f014a4cb8b4fe7de8d78_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_1c3d01a27f37e9d220e63d12db91827d38a54f38f79947e5a08f14be0d907ee9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1c3d01a27f37e9d220e63d12db91827d38a54f38f79947e5a08f14be0d907ee9->enter($__internal_1c3d01a27f37e9d220e63d12db91827d38a54f38f79947e5a08f14be0d907ee9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_831bb34de0e474d0c3ee2751c6a769f2a4ada7daf8f9ece9f3964aac8c1be9c5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_831bb34de0e474d0c3ee2751c6a769f2a4ada7daf8f9ece9f3964aac8c1be9c5->enter($__internal_831bb34de0e474d0c3ee2751c6a769f2a4ada7daf8f9ece9f3964aac8c1be9c5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<div class=\"container\">


    <h1>Evenement edit</h1>

    ";
        // line 9
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'form_start');
        echo "
        ";
        // line 10
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'widget');
        echo "
        <input type=\"submit\" value=\"Edit\" class=\"btn btn-warning\"/>
    ";
        // line 12
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'form_end');
        echo "

    <ul>
        <li>
            <a href=\"";
        // line 16
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_evenement_index");
        echo "\" class=\"btn btn-success\">Back to the list</a>
        </li>
        <li>
            ";
        // line 19
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_start');
        echo "
                <input type=\"submit\" value=\"Delete\" class=\"btn btn-danger\">
            ";
        // line 21
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_end');
        echo "
        </li>
    </ul>
</div>
";
        
        $__internal_831bb34de0e474d0c3ee2751c6a769f2a4ada7daf8f9ece9f3964aac8c1be9c5->leave($__internal_831bb34de0e474d0c3ee2751c6a769f2a4ada7daf8f9ece9f3964aac8c1be9c5_prof);

        
        $__internal_1c3d01a27f37e9d220e63d12db91827d38a54f38f79947e5a08f14be0d907ee9->leave($__internal_1c3d01a27f37e9d220e63d12db91827d38a54f38f79947e5a08f14be0d907ee9_prof);

    }

    public function getTemplateName()
    {
        return "evenement/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  83 => 21,  78 => 19,  72 => 16,  65 => 12,  60 => 10,  56 => 9,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
<div class=\"container\">


    <h1>Evenement edit</h1>

    {{ form_start(edit_form) }}
        {{ form_widget(edit_form) }}
        <input type=\"submit\" value=\"Edit\" class=\"btn btn-warning\"/>
    {{ form_end(edit_form) }}

    <ul>
        <li>
            <a href=\"{{ path('admin_evenement_index') }}\" class=\"btn btn-success\">Back to the list</a>
        </li>
        <li>
            {{ form_start(delete_form) }}
                <input type=\"submit\" value=\"Delete\" class=\"btn btn-danger\">
            {{ form_end(delete_form) }}
        </li>
    </ul>
</div>
{% endblock %}
", "evenement/edit.html.twig", "/home/fonguen/symfony projet/infotels/app/Resources/views/evenement/edit.html.twig");
    }
}
