<?php

/* @Framework/Form/money_widget.html.php */
class __TwigTemplate_dac929b59d0e95e1ae27f3058132f438833c43c33a650be82795e1903090aa7d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8f9b5e0eae0fa44465ba57e5242e04a5db5df44d1a2a5ac247dfbe22f34abdfe = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8f9b5e0eae0fa44465ba57e5242e04a5db5df44d1a2a5ac247dfbe22f34abdfe->enter($__internal_8f9b5e0eae0fa44465ba57e5242e04a5db5df44d1a2a5ac247dfbe22f34abdfe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/money_widget.html.php"));

        $__internal_19c74465e41a4fdd025f18f6f2acf8621f1166fdea6f583685473b1a78887d8d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_19c74465e41a4fdd025f18f6f2acf8621f1166fdea6f583685473b1a78887d8d->enter($__internal_19c74465e41a4fdd025f18f6f2acf8621f1166fdea6f583685473b1a78887d8d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/money_widget.html.php"));

        // line 1
        echo "<?php echo str_replace('";
        echo twig_escape_filter($this->env, ($context["widget"] ?? $this->getContext($context, "widget")), "html", null, true);
        echo "', \$view['form']->block(\$form, 'form_widget_simple'), \$money_pattern) ?>
";
        
        $__internal_8f9b5e0eae0fa44465ba57e5242e04a5db5df44d1a2a5ac247dfbe22f34abdfe->leave($__internal_8f9b5e0eae0fa44465ba57e5242e04a5db5df44d1a2a5ac247dfbe22f34abdfe_prof);

        
        $__internal_19c74465e41a4fdd025f18f6f2acf8621f1166fdea6f583685473b1a78887d8d->leave($__internal_19c74465e41a4fdd025f18f6f2acf8621f1166fdea6f583685473b1a78887d8d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/money_widget.html.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo str_replace('{{ widget }}', \$view['form']->block(\$form, 'form_widget_simple'), \$money_pattern) ?>
", "@Framework/Form/money_widget.html.php", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/money_widget.html.php");
    }
}
