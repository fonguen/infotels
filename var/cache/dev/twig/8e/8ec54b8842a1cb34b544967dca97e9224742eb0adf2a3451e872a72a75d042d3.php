<?php

/* @WebProfiler/Profiler/ajax_layout.html.twig */
class __TwigTemplate_7da1fca18a2be6627e2993d6d23dcdf31c4dd48e9998b718cab257fdf8d84b20 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3eaeea6bf5bbfbdc209312df9823cf3ed9c3403006a797a01a1282d57dabbfc2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3eaeea6bf5bbfbdc209312df9823cf3ed9c3403006a797a01a1282d57dabbfc2->enter($__internal_3eaeea6bf5bbfbdc209312df9823cf3ed9c3403006a797a01a1282d57dabbfc2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/ajax_layout.html.twig"));

        $__internal_a060df9e07573919cfa348d216fb702230f53922e24ad8228584da22ee4ab892 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a060df9e07573919cfa348d216fb702230f53922e24ad8228584da22ee4ab892->enter($__internal_a060df9e07573919cfa348d216fb702230f53922e24ad8228584da22ee4ab892_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_3eaeea6bf5bbfbdc209312df9823cf3ed9c3403006a797a01a1282d57dabbfc2->leave($__internal_3eaeea6bf5bbfbdc209312df9823cf3ed9c3403006a797a01a1282d57dabbfc2_prof);

        
        $__internal_a060df9e07573919cfa348d216fb702230f53922e24ad8228584da22ee4ab892->leave($__internal_a060df9e07573919cfa348d216fb702230f53922e24ad8228584da22ee4ab892_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_391d5a87ab152378ccee65397526479eb577375eb51d3c30777af173d75e15a7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_391d5a87ab152378ccee65397526479eb577375eb51d3c30777af173d75e15a7->enter($__internal_391d5a87ab152378ccee65397526479eb577375eb51d3c30777af173d75e15a7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_7d9cae6c5800b251eefece48f783a3faaa55bd87d5bb13a4f16a754b1f4a81f4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7d9cae6c5800b251eefece48f783a3faaa55bd87d5bb13a4f16a754b1f4a81f4->enter($__internal_7d9cae6c5800b251eefece48f783a3faaa55bd87d5bb13a4f16a754b1f4a81f4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_7d9cae6c5800b251eefece48f783a3faaa55bd87d5bb13a4f16a754b1f4a81f4->leave($__internal_7d9cae6c5800b251eefece48f783a3faaa55bd87d5bb13a4f16a754b1f4a81f4_prof);

        
        $__internal_391d5a87ab152378ccee65397526479eb577375eb51d3c30777af173d75e15a7->leave($__internal_391d5a87ab152378ccee65397526479eb577375eb51d3c30777af173d75e15a7_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% block panel '' %}
", "@WebProfiler/Profiler/ajax_layout.html.twig", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/ajax_layout.html.twig");
    }
}
