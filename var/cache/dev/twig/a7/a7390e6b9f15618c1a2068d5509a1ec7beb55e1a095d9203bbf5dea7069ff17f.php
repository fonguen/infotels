<?php

/* @Framework/Form/choice_widget.html.php */
class __TwigTemplate_3ba4e61335b20e9397d557887cdd6a2666ca2a2ca9b511485b0ce16476d4406d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e6f015458eef511fbb007afd73183bd0458bddaf7c1cb810c53b56deb796548b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e6f015458eef511fbb007afd73183bd0458bddaf7c1cb810c53b56deb796548b->enter($__internal_e6f015458eef511fbb007afd73183bd0458bddaf7c1cb810c53b56deb796548b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget.html.php"));

        $__internal_77720891fdd7a47ac1996da84e5a64153fcb947155a45b2382e42fd1916976eb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_77720891fdd7a47ac1996da84e5a64153fcb947155a45b2382e42fd1916976eb->enter($__internal_77720891fdd7a47ac1996da84e5a64153fcb947155a45b2382e42fd1916976eb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget.html.php"));

        // line 1
        echo "<?php if (\$expanded): ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_expanded') ?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_collapsed') ?>
<?php endif ?>
";
        
        $__internal_e6f015458eef511fbb007afd73183bd0458bddaf7c1cb810c53b56deb796548b->leave($__internal_e6f015458eef511fbb007afd73183bd0458bddaf7c1cb810c53b56deb796548b_prof);

        
        $__internal_77720891fdd7a47ac1996da84e5a64153fcb947155a45b2382e42fd1916976eb->leave($__internal_77720891fdd7a47ac1996da84e5a64153fcb947155a45b2382e42fd1916976eb_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$expanded): ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_expanded') ?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_collapsed') ?>
<?php endif ?>
", "@Framework/Form/choice_widget.html.php", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choice_widget.html.php");
    }
}
