<?php

/* ::base.html.twig */
class __TwigTemplate_db4e1817a781d955a1dd9f5fdec16d23770865471c516940753b822ba2f4fecb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7bf4818515e39cdec8527498c7d2590514de66b47ffd32c288c0f074c7ed7b99 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7bf4818515e39cdec8527498c7d2590514de66b47ffd32c288c0f074c7ed7b99->enter($__internal_7bf4818515e39cdec8527498c7d2590514de66b47ffd32c288c0f074c7ed7b99_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        $__internal_97c5f1db60bd28aef757c21d730025fc125e20efe61bc95c55e890293ed5f938 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_97c5f1db60bd28aef757c21d730025fc125e20efe61bc95c55e890293ed5f938->enter($__internal_97c5f1db60bd28aef757c21d730025fc125e20efe61bc95c55e890293ed5f938_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
       <link  href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bootstrap/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" media=\"screen\" />
       <link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.8.2/css/all.css\" integrity=\"sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay\" crossorigin=\"anonymous\">
      <script src=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bootstrap/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
    </head>
    <style>
    body{
      padding-top: 10px;
    }
    #navbare{
      width: 90%;
      height: 70px;
      color: white;

    }
    #toto{
      text-color: white;
    }
    #nav{


      width: 90%;
    }
    #linkref{
      color: white;
      text-decoration: blue;
      padding: 15px;
      top: 0;
    }
    .slider{
        width: 500px;
        height: 300px;
        overflow: hidden;
        margin: 130px auto;
    }
    .slides{
    width: calc(500px * 3);
    animation: glisse 12s infinite;
     }
    .slide{
        float: left;
     }
     @keyframes glisse{
        0%{
            transform: translateX(0);
        }
        10%{
            transform: translateX(0);
        }
         33%{
            transform: translateX(-5000);
        }
        43%{
           transform: translateX(-5000);
       }
         66%{
            transform: translateX(-1000px);
        }
        76%{
           transform: translateX(-1000px);
       }
         100%{
            transform: translateX(0);
        }

     }
.def {                                                   /** gros titre */
  font-family: serif;                                  /* On se permet un changement de police : pas plus de deux par page, et à utiliser avec parcimonie (c'est le cas) */
  background-color: blue;                              /* Fond du titre bleu (et sans image de fond) */
  color: white;                                          /*  Donc on utilise une police blanche pour que le texte soit visible */
  border: 3px dotted #9999FF;                            /* On encadre le titre d'un bleu clair de 3 pixels de largeur */
  padding: 0.3em;                                       /* Espacement intérieur non nul pour que le texte ne colle pas à la bordure du cadre */
   text-align: center;                                     /* Le titre doit être centré ! */
  letter-spacing: 0.7em;
    border-radius: 80px ;                                 /* On espace les caractères pour que ce soit joli :-) */
}
        .def {                                                   /** gros titre */
  font-family: serif;                                  /* On se permet un changement de police : pas plus de deux par page, et à utiliser avec parcimonie (c'est le cas) */
  background-color: blue;                              /* Fond du titre bleu (et sans image de fond) */
  color: white;                                          /*  Donc on utilise une police blanche pour que le texte soit visible */
  border: 3px dotted #9999FF;                            /* On encadre le titre d'un bleu clair de 3 pixels de largeur */
  padding: 0.3em;                                       /* Espacement intérieur non nul pour que le texte ne colle pas à la bordure du cadre */
   text-align: center;                                     /* Le titre doit être centré ! */
  letter-spacing: 0.7em;
    border-radius: 80px ;                                 /* On espace les caractères pour que ce soit joli :-) */
}
.droit {
  align: height;
  text-color: #000000;
  text-decoration: blue;
}
.entete {
  align: height;
  text-color: red;
  text-decoration: blue;
}
*{
  padding: 0;
  margin: 0;
  box-sizing: border-box;
}
.menu-bar{
  background: rgba(0,100,0);
  text-align: center;
}
.menu-bar ul{
  display: inline-flex;
  list-style: none;
  color: #fff;
}
.menu-bar ul li{
  width: 150px;
  margin: 5px;
  padding: 5px;
}
.menu-bar ul li a{
  text-decoration: none;
  color: #fff;
}
.active,.menu-bar ul li:hover{
  background: #2bab0d;
  border-radius: 3px;


}
.sub-menu-1{
  display: none;
}
.menu-bar ul li:hover .sub-menu-1{
  display: block;
  position: absolute;
  background: rgb(0, 100, 0);
  margin-top: 15px;
  margin-left: 15px;
}
.menu-bar ul li:hover .sub-menu-1 ul{
  display: block;
  margin: 10px;
}
.menu-bar ul li:hover .sub-menu-1 ul li{
  width: 150px;
  padding: 10px;
  border-bottom: 1px dotted #fff;
  background: transparent;
  border-radius: 0;
  text-align: left;
}
.menu-bar ul li:hover .sub-menu-1 ul li:last-child{
  border-bottom: none;
}
.menu-bar ul li:hover .sub-menu-1 ul li a:hover{
  color: #b2ff00;
}
.fa fa-angle-right{
  float: right;
}
.sub-menu-2{
  display: none;
}
.hover-me:hover .sub-menu-2{
  position: absolute;
  display: block;
  margin-top: -40px;
  margin-left: 140px;
  background: rgb(0, 100, 0);

}
.menu-bar .fa{

  margin-right: 8px;
}
.mino{
  text-color: green;
}
.menuuser{
  align: right;
}
</style>
<div class=\"container\">

<div id=\"navbare\">




<div class=\"container\">
  <table width=\"80%\"  >
    <tr>
      <td><a href=\"evenentaffich\" id=\"droit\"><i class=\"fa fa-archive\" aria-hidden=\"true\"></i>
       evenement</a> </td>

      <td><a href=\"\"><i class=\"fa fa-phone\" ></i>contacts</a> </td>

      <td><a href=\"inscription\"><i class=\"fa fa-graduation-cap\" aria-hidden=\"true\"></i>
        espace etudiants</a> </td>
        <td><a href=\"\"><i class=\"fa fa-wrench\" aria-hidden=\"true\"></i>
          connection</a> </td>
    </tr>

  </table>
  <br><br>


    <marquee direction=\"\" class=\"def\"><h2 > Departement d'Informatique et Telecommunication de l'Ecole Nationale Superieure Polytechnique de Maroua </h2></marquee>

</div>
</div>
<br><br><br><br><br><br>
<div class=\"menu-bar\">
  <ul>
      <li class=\"active\"><a href=\"/\"><i class=\"fa fa-home\" ></i>
            HOME</a></li>
      <li><a href=\"\"><i class=\"fa fa-edit\" ></i>FORMATION</a>
        <div class=\"sub-menu-1\">
          <ul>
            <li><a href=\"informatique\">INFORMATIQUE  <i class=\"fa fa-angle-right\"></i></a></li>
            <li><a href=\"telecom\">TELECOMMICATION<i class=\"fa fa-angle-right\"></i> </a></li>
          </ul>

        </div>
      </li>
      <li><a href=\"\"><i class=\"fa fa-clone\" ></i>L'ECOLE</a>
        <div class=\"sub-menu-1\">
          <ul>
            <li><a href=\"\"><i class=\"fa fa-angle-right\"></i>PRESENTATION</a> </li>
            <li><a href=\"\"> <i class=\"fa fa-angle-right\"></i>PROJETS ETUDIANTS</a></li>
            <li><a href=\"\"><i class=\"fa fa-angle-right\"></i> AVANTAGES ETUDIANTS</a></li>
          </ul>

        </div>
      </li>
      <li><a href=\"\"><i class=\"fa fa-folder-open\" aria-hidden=\"true\"></i>
          ADMISSION</a></li>
      <li><a href=\"evenentaffich\"><i class=\"fa fa-angellist\" ></i>EVENEMENT</a></li>
      <li><a href=\"\"><i class=\"fa fa-phone\" ></i>CONTACTS</a></li>
        <li><a href=\"\"><i class=\"fa fa-user\" ></i>CONNECTION</a>
          <div class=\"sub-menu-1\">
            <ul>
              <li class=\"hover-me\"><a href=\"inscription\"><i class=\"fa fa-angle-right\"></i>INSCRIPTION</a>

              </li>
              <li class=\"hover-me\"><a href=\"\"><i class=\"fa fa-angle-right\"></i>LOGIN</a>
                <div class=\"sub-menu-2\">
                  <ul>
                    <li>log</li>
                  </ul>

                </div>
              </li>
            </ul>

          </div>
        </li>
  </ul>
</div>


<br><br><br>



    <body>
        ";
        // line 270
        $this->displayBlock('body', $context, $blocks);
        // line 271
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 272
        echo "    </body>
</html>
";
        
        $__internal_7bf4818515e39cdec8527498c7d2590514de66b47ffd32c288c0f074c7ed7b99->leave($__internal_7bf4818515e39cdec8527498c7d2590514de66b47ffd32c288c0f074c7ed7b99_prof);

        
        $__internal_97c5f1db60bd28aef757c21d730025fc125e20efe61bc95c55e890293ed5f938->leave($__internal_97c5f1db60bd28aef757c21d730025fc125e20efe61bc95c55e890293ed5f938_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_4f26a704456ca39f2f928eaa83741ae8a28ada8da335ff12b79e120c1ececc2e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4f26a704456ca39f2f928eaa83741ae8a28ada8da335ff12b79e120c1ececc2e->enter($__internal_4f26a704456ca39f2f928eaa83741ae8a28ada8da335ff12b79e120c1ececc2e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_971352a24a489bd8043beffa491309b1516b78d1dbd4eb9cd121f9e3441aa39c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_971352a24a489bd8043beffa491309b1516b78d1dbd4eb9cd121f9e3441aa39c->enter($__internal_971352a24a489bd8043beffa491309b1516b78d1dbd4eb9cd121f9e3441aa39c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_971352a24a489bd8043beffa491309b1516b78d1dbd4eb9cd121f9e3441aa39c->leave($__internal_971352a24a489bd8043beffa491309b1516b78d1dbd4eb9cd121f9e3441aa39c_prof);

        
        $__internal_4f26a704456ca39f2f928eaa83741ae8a28ada8da335ff12b79e120c1ececc2e->leave($__internal_4f26a704456ca39f2f928eaa83741ae8a28ada8da335ff12b79e120c1ececc2e_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_e2a2b67be3b3e72de1296876eaa6b2af0718295d28e5f3c170b42685e72aed12 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e2a2b67be3b3e72de1296876eaa6b2af0718295d28e5f3c170b42685e72aed12->enter($__internal_e2a2b67be3b3e72de1296876eaa6b2af0718295d28e5f3c170b42685e72aed12_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_aa95e628e4cfa71b26bbb010e0ac33027920d867eca9f7a6522a0c1fa0386b70 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_aa95e628e4cfa71b26bbb010e0ac33027920d867eca9f7a6522a0c1fa0386b70->enter($__internal_aa95e628e4cfa71b26bbb010e0ac33027920d867eca9f7a6522a0c1fa0386b70_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_aa95e628e4cfa71b26bbb010e0ac33027920d867eca9f7a6522a0c1fa0386b70->leave($__internal_aa95e628e4cfa71b26bbb010e0ac33027920d867eca9f7a6522a0c1fa0386b70_prof);

        
        $__internal_e2a2b67be3b3e72de1296876eaa6b2af0718295d28e5f3c170b42685e72aed12->leave($__internal_e2a2b67be3b3e72de1296876eaa6b2af0718295d28e5f3c170b42685e72aed12_prof);

    }

    // line 270
    public function block_body($context, array $blocks = array())
    {
        $__internal_d7250aa6324538d182861ea331bc9f7e6e82f3e87d04a43cdb618ef35ff87bee = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d7250aa6324538d182861ea331bc9f7e6e82f3e87d04a43cdb618ef35ff87bee->enter($__internal_d7250aa6324538d182861ea331bc9f7e6e82f3e87d04a43cdb618ef35ff87bee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_839e23312b8cd7d772f1e2deb522a162e447ce851357afed03ce79b19cdd05a7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_839e23312b8cd7d772f1e2deb522a162e447ce851357afed03ce79b19cdd05a7->enter($__internal_839e23312b8cd7d772f1e2deb522a162e447ce851357afed03ce79b19cdd05a7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_839e23312b8cd7d772f1e2deb522a162e447ce851357afed03ce79b19cdd05a7->leave($__internal_839e23312b8cd7d772f1e2deb522a162e447ce851357afed03ce79b19cdd05a7_prof);

        
        $__internal_d7250aa6324538d182861ea331bc9f7e6e82f3e87d04a43cdb618ef35ff87bee->leave($__internal_d7250aa6324538d182861ea331bc9f7e6e82f3e87d04a43cdb618ef35ff87bee_prof);

    }

    // line 271
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_ae878d47ecebed7c63668e669a273222fda49f07c232a0f002e8d9e64778e6ce = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ae878d47ecebed7c63668e669a273222fda49f07c232a0f002e8d9e64778e6ce->enter($__internal_ae878d47ecebed7c63668e669a273222fda49f07c232a0f002e8d9e64778e6ce_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_75e4cf8e72737b52bb3e48a03fedbfc3e115546f106e377e5b2b392371d702af = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_75e4cf8e72737b52bb3e48a03fedbfc3e115546f106e377e5b2b392371d702af->enter($__internal_75e4cf8e72737b52bb3e48a03fedbfc3e115546f106e377e5b2b392371d702af_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_75e4cf8e72737b52bb3e48a03fedbfc3e115546f106e377e5b2b392371d702af->leave($__internal_75e4cf8e72737b52bb3e48a03fedbfc3e115546f106e377e5b2b392371d702af_prof);

        
        $__internal_ae878d47ecebed7c63668e669a273222fda49f07c232a0f002e8d9e64778e6ce->leave($__internal_ae878d47ecebed7c63668e669a273222fda49f07c232a0f002e8d9e64778e6ce_prof);

    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  386 => 271,  369 => 270,  352 => 6,  334 => 5,  322 => 272,  319 => 271,  317 => 270,  55 => 11,  50 => 9,  46 => 8,  41 => 7,  39 => 6,  35 => 5,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
       <link  href=\"{{ asset('bootstrap/css/bootstrap.min.css') }}\" rel=\"stylesheet\" media=\"screen\" />
       <link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.8.2/css/all.css\" integrity=\"sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay\" crossorigin=\"anonymous\">
      <script src=\"{{ asset('bootstrap/js/bootstrap.min.js') }}\"></script>
    </head>
    <style>
    body{
      padding-top: 10px;
    }
    #navbare{
      width: 90%;
      height: 70px;
      color: white;

    }
    #toto{
      text-color: white;
    }
    #nav{


      width: 90%;
    }
    #linkref{
      color: white;
      text-decoration: blue;
      padding: 15px;
      top: 0;
    }
    .slider{
        width: 500px;
        height: 300px;
        overflow: hidden;
        margin: 130px auto;
    }
    .slides{
    width: calc(500px * 3);
    animation: glisse 12s infinite;
     }
    .slide{
        float: left;
     }
     @keyframes glisse{
        0%{
            transform: translateX(0);
        }
        10%{
            transform: translateX(0);
        }
         33%{
            transform: translateX(-5000);
        }
        43%{
           transform: translateX(-5000);
       }
         66%{
            transform: translateX(-1000px);
        }
        76%{
           transform: translateX(-1000px);
       }
         100%{
            transform: translateX(0);
        }

     }
.def {                                                   /** gros titre */
  font-family: serif;                                  /* On se permet un changement de police : pas plus de deux par page, et à utiliser avec parcimonie (c'est le cas) */
  background-color: blue;                              /* Fond du titre bleu (et sans image de fond) */
  color: white;                                          /*  Donc on utilise une police blanche pour que le texte soit visible */
  border: 3px dotted #9999FF;                            /* On encadre le titre d'un bleu clair de 3 pixels de largeur */
  padding: 0.3em;                                       /* Espacement intérieur non nul pour que le texte ne colle pas à la bordure du cadre */
   text-align: center;                                     /* Le titre doit être centré ! */
  letter-spacing: 0.7em;
    border-radius: 80px ;                                 /* On espace les caractères pour que ce soit joli :-) */
}
        .def {                                                   /** gros titre */
  font-family: serif;                                  /* On se permet un changement de police : pas plus de deux par page, et à utiliser avec parcimonie (c'est le cas) */
  background-color: blue;                              /* Fond du titre bleu (et sans image de fond) */
  color: white;                                          /*  Donc on utilise une police blanche pour que le texte soit visible */
  border: 3px dotted #9999FF;                            /* On encadre le titre d'un bleu clair de 3 pixels de largeur */
  padding: 0.3em;                                       /* Espacement intérieur non nul pour que le texte ne colle pas à la bordure du cadre */
   text-align: center;                                     /* Le titre doit être centré ! */
  letter-spacing: 0.7em;
    border-radius: 80px ;                                 /* On espace les caractères pour que ce soit joli :-) */
}
.droit {
  align: height;
  text-color: #000000;
  text-decoration: blue;
}
.entete {
  align: height;
  text-color: red;
  text-decoration: blue;
}
*{
  padding: 0;
  margin: 0;
  box-sizing: border-box;
}
.menu-bar{
  background: rgba(0,100,0);
  text-align: center;
}
.menu-bar ul{
  display: inline-flex;
  list-style: none;
  color: #fff;
}
.menu-bar ul li{
  width: 150px;
  margin: 5px;
  padding: 5px;
}
.menu-bar ul li a{
  text-decoration: none;
  color: #fff;
}
.active,.menu-bar ul li:hover{
  background: #2bab0d;
  border-radius: 3px;


}
.sub-menu-1{
  display: none;
}
.menu-bar ul li:hover .sub-menu-1{
  display: block;
  position: absolute;
  background: rgb(0, 100, 0);
  margin-top: 15px;
  margin-left: 15px;
}
.menu-bar ul li:hover .sub-menu-1 ul{
  display: block;
  margin: 10px;
}
.menu-bar ul li:hover .sub-menu-1 ul li{
  width: 150px;
  padding: 10px;
  border-bottom: 1px dotted #fff;
  background: transparent;
  border-radius: 0;
  text-align: left;
}
.menu-bar ul li:hover .sub-menu-1 ul li:last-child{
  border-bottom: none;
}
.menu-bar ul li:hover .sub-menu-1 ul li a:hover{
  color: #b2ff00;
}
.fa fa-angle-right{
  float: right;
}
.sub-menu-2{
  display: none;
}
.hover-me:hover .sub-menu-2{
  position: absolute;
  display: block;
  margin-top: -40px;
  margin-left: 140px;
  background: rgb(0, 100, 0);

}
.menu-bar .fa{

  margin-right: 8px;
}
.mino{
  text-color: green;
}
.menuuser{
  align: right;
}
</style>
<div class=\"container\">

<div id=\"navbare\">




<div class=\"container\">
  <table width=\"80%\"  >
    <tr>
      <td><a href=\"evenentaffich\" id=\"droit\"><i class=\"fa fa-archive\" aria-hidden=\"true\"></i>
       evenement</a> </td>

      <td><a href=\"\"><i class=\"fa fa-phone\" ></i>contacts</a> </td>

      <td><a href=\"inscription\"><i class=\"fa fa-graduation-cap\" aria-hidden=\"true\"></i>
        espace etudiants</a> </td>
        <td><a href=\"\"><i class=\"fa fa-wrench\" aria-hidden=\"true\"></i>
          connection</a> </td>
    </tr>

  </table>
  <br><br>


    <marquee direction=\"\" class=\"def\"><h2 > Departement d'Informatique et Telecommunication de l'Ecole Nationale Superieure Polytechnique de Maroua </h2></marquee>

</div>
</div>
<br><br><br><br><br><br>
<div class=\"menu-bar\">
  <ul>
      <li class=\"active\"><a href=\"/\"><i class=\"fa fa-home\" ></i>
            HOME</a></li>
      <li><a href=\"\"><i class=\"fa fa-edit\" ></i>FORMATION</a>
        <div class=\"sub-menu-1\">
          <ul>
            <li><a href=\"informatique\">INFORMATIQUE  <i class=\"fa fa-angle-right\"></i></a></li>
            <li><a href=\"telecom\">TELECOMMICATION<i class=\"fa fa-angle-right\"></i> </a></li>
          </ul>

        </div>
      </li>
      <li><a href=\"\"><i class=\"fa fa-clone\" ></i>L'ECOLE</a>
        <div class=\"sub-menu-1\">
          <ul>
            <li><a href=\"\"><i class=\"fa fa-angle-right\"></i>PRESENTATION</a> </li>
            <li><a href=\"\"> <i class=\"fa fa-angle-right\"></i>PROJETS ETUDIANTS</a></li>
            <li><a href=\"\"><i class=\"fa fa-angle-right\"></i> AVANTAGES ETUDIANTS</a></li>
          </ul>

        </div>
      </li>
      <li><a href=\"\"><i class=\"fa fa-folder-open\" aria-hidden=\"true\"></i>
          ADMISSION</a></li>
      <li><a href=\"evenentaffich\"><i class=\"fa fa-angellist\" ></i>EVENEMENT</a></li>
      <li><a href=\"\"><i class=\"fa fa-phone\" ></i>CONTACTS</a></li>
        <li><a href=\"\"><i class=\"fa fa-user\" ></i>CONNECTION</a>
          <div class=\"sub-menu-1\">
            <ul>
              <li class=\"hover-me\"><a href=\"inscription\"><i class=\"fa fa-angle-right\"></i>INSCRIPTION</a>

              </li>
              <li class=\"hover-me\"><a href=\"\"><i class=\"fa fa-angle-right\"></i>LOGIN</a>
                <div class=\"sub-menu-2\">
                  <ul>
                    <li>log</li>
                  </ul>

                </div>
              </li>
            </ul>

          </div>
        </li>
  </ul>
</div>


<br><br><br>



    <body>
        {% block body %}{% endblock %}
        {% block javascripts %}{% endblock %}
    </body>
</html>
", "::base.html.twig", "/home/fonguen/symfony projet/infotels/var/cache/dev/../../../app/Resources/views/base.html.twig");
    }
}
