<?php

/* @Framework/FormTable/form_widget_compound.html.php */
class __TwigTemplate_53a7505adf73737c1af06cceb190ba1181006457cd7332daec19c3e4a29f2d7f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a7a345a4295a4b60b960c1362dc67d7420c4223ad09b49d3b8061a4d759c2463 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a7a345a4295a4b60b960c1362dc67d7420c4223ad09b49d3b8061a4d759c2463->enter($__internal_a7a345a4295a4b60b960c1362dc67d7420c4223ad09b49d3b8061a4d759c2463_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_widget_compound.html.php"));

        $__internal_0db53cda225bdadf0c72ed84f8464146eec6ba24ee19db63b943d8b75d2c92da = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0db53cda225bdadf0c72ed84f8464146eec6ba24ee19db63b943d8b75d2c92da->enter($__internal_0db53cda225bdadf0c72ed84f8464146eec6ba24ee19db63b943d8b75d2c92da_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_widget_compound.html.php"));

        // line 1
        echo "<table <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <tr>
        <td colspan=\"2\">
            <?php echo \$view['form']->errors(\$form) ?>
        </td>
    </tr>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</table>
";
        
        $__internal_a7a345a4295a4b60b960c1362dc67d7420c4223ad09b49d3b8061a4d759c2463->leave($__internal_a7a345a4295a4b60b960c1362dc67d7420c4223ad09b49d3b8061a4d759c2463_prof);

        
        $__internal_0db53cda225bdadf0c72ed84f8464146eec6ba24ee19db63b943d8b75d2c92da->leave($__internal_0db53cda225bdadf0c72ed84f8464146eec6ba24ee19db63b943d8b75d2c92da_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/form_widget_compound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<table <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <tr>
        <td colspan=\"2\">
            <?php echo \$view['form']->errors(\$form) ?>
        </td>
    </tr>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</table>
", "@Framework/FormTable/form_widget_compound.html.php", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/form_widget_compound.html.php");
    }
}
