<?php

/* user/index.html.twig */
class __TwigTemplate_049128159137099661979d770f25ba90f04d6f83c3082a0c0de5fc47cdc0d98b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "user/index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_aef14cb3124c020417e2fec07ef994f0697b931abe04c999be7418263fb74a21 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_aef14cb3124c020417e2fec07ef994f0697b931abe04c999be7418263fb74a21->enter($__internal_aef14cb3124c020417e2fec07ef994f0697b931abe04c999be7418263fb74a21_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "user/index.html.twig"));

        $__internal_3514a619f57cdf57617b433704499ff2ec8e848e79d5227b687f2dbc2a598c30 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3514a619f57cdf57617b433704499ff2ec8e848e79d5227b687f2dbc2a598c30->enter($__internal_3514a619f57cdf57617b433704499ff2ec8e848e79d5227b687f2dbc2a598c30_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "user/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_aef14cb3124c020417e2fec07ef994f0697b931abe04c999be7418263fb74a21->leave($__internal_aef14cb3124c020417e2fec07ef994f0697b931abe04c999be7418263fb74a21_prof);

        
        $__internal_3514a619f57cdf57617b433704499ff2ec8e848e79d5227b687f2dbc2a598c30->leave($__internal_3514a619f57cdf57617b433704499ff2ec8e848e79d5227b687f2dbc2a598c30_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_476c9e1fc1a68e34d7f861099943822ad24a75a9cda31b48fd74cf0d858f1d45 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_476c9e1fc1a68e34d7f861099943822ad24a75a9cda31b48fd74cf0d858f1d45->enter($__internal_476c9e1fc1a68e34d7f861099943822ad24a75a9cda31b48fd74cf0d858f1d45_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_b935e72fa437bb1f6efa2f0049c259ef43f350c3d3075825e429c36bfff0f01b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b935e72fa437bb1f6efa2f0049c259ef43f350c3d3075825e429c36bfff0f01b->enter($__internal_b935e72fa437bb1f6efa2f0049c259ef43f350c3d3075825e429c36bfff0f01b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<div class=\"container\">


    <h1>Users list</h1>
    <a href=\"";
        // line 8
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_user_new");
        echo "\" class=\"btn btn-large btn-info\">Create a new user</a>

    <table class=\"table\">
        <thead>
            <tr>
                <th>Id</th>
                <th>Nom</th>
                  <th>Matricule</th>
                <th>Email</th>
                <th>Password</th>
                <th>Role</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        ";
        // line 23
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["users"] ?? $this->getContext($context, "users")));
        foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
            // line 24
            echo "            <tr>
                <td><a href=\"";
            // line 25
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_user_show", array("id" => $this->getAttribute($context["user"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "id", array()), "html", null, true);
            echo "</a></td>
                <td>";
            // line 26
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "username", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 27
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "matricule", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 28
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "email", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 29
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "password", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 30
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "role", array()), "html", null, true);
            echo "</td>
                <td>
                    <ul>
                        <li>
                            <a href=\"";
            // line 34
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_user_show", array("id" => $this->getAttribute($context["user"], "id", array()))), "html", null, true);
            echo "\"class=\"btn btn-warning\">show</a>
                        </li>
                        <li>
                            <a href=\"";
            // line 37
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_user_edit", array("id" => $this->getAttribute($context["user"], "id", array()))), "html", null, true);
            echo "\"class=\"btn btn-danger\">edit</a>
                        </li>
                    </ul>
                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 43
        echo "        </tbody>
    </table>

</div>
";
        
        $__internal_b935e72fa437bb1f6efa2f0049c259ef43f350c3d3075825e429c36bfff0f01b->leave($__internal_b935e72fa437bb1f6efa2f0049c259ef43f350c3d3075825e429c36bfff0f01b_prof);

        
        $__internal_476c9e1fc1a68e34d7f861099943822ad24a75a9cda31b48fd74cf0d858f1d45->leave($__internal_476c9e1fc1a68e34d7f861099943822ad24a75a9cda31b48fd74cf0d858f1d45_prof);

    }

    public function getTemplateName()
    {
        return "user/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  127 => 43,  115 => 37,  109 => 34,  102 => 30,  98 => 29,  94 => 28,  90 => 27,  86 => 26,  80 => 25,  77 => 24,  73 => 23,  55 => 8,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
<div class=\"container\">


    <h1>Users list</h1>
    <a href=\"{{ path('admin_user_new') }}\" class=\"btn btn-large btn-info\">Create a new user</a>

    <table class=\"table\">
        <thead>
            <tr>
                <th>Id</th>
                <th>Nom</th>
                  <th>Matricule</th>
                <th>Email</th>
                <th>Password</th>
                <th>Role</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        {% for user in users %}
            <tr>
                <td><a href=\"{{ path('admin_user_show', { 'id': user.id }) }}\">{{ user.id }}</a></td>
                <td>{{ user.username }}</td>
                <td>{{ user.matricule }}</td>
                <td>{{ user.email }}</td>
                <td>{{ user.password }}</td>
                <td>{{ user.role }}</td>
                <td>
                    <ul>
                        <li>
                            <a href=\"{{ path('admin_user_show', { 'id': user.id }) }}\"class=\"btn btn-warning\">show</a>
                        </li>
                        <li>
                            <a href=\"{{ path('admin_user_edit', { 'id': user.id }) }}\"class=\"btn btn-danger\">edit</a>
                        </li>
                    </ul>
                </td>
            </tr>
        {% endfor %}
        </tbody>
    </table>

</div>
{% endblock %}
", "user/index.html.twig", "/home/fonguen/symfony projet/infotels/app/Resources/views/user/index.html.twig");
    }
}
