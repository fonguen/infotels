<?php

/* formation/new.html.twig */
class __TwigTemplate_4288dc766de1110e186f4c124f3e6f373d0200408c948bc8ae8902e3407ad9c4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "formation/new.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_08bc6a7ea058b587183587027b1c267b0a7deae2881dbc66c78c870acb8c65fe = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_08bc6a7ea058b587183587027b1c267b0a7deae2881dbc66c78c870acb8c65fe->enter($__internal_08bc6a7ea058b587183587027b1c267b0a7deae2881dbc66c78c870acb8c65fe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "formation/new.html.twig"));

        $__internal_a749422bd1c77434b63bd6ae0cbe01e4be264950eb2ec795bb31b64dfc08debd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a749422bd1c77434b63bd6ae0cbe01e4be264950eb2ec795bb31b64dfc08debd->enter($__internal_a749422bd1c77434b63bd6ae0cbe01e4be264950eb2ec795bb31b64dfc08debd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "formation/new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_08bc6a7ea058b587183587027b1c267b0a7deae2881dbc66c78c870acb8c65fe->leave($__internal_08bc6a7ea058b587183587027b1c267b0a7deae2881dbc66c78c870acb8c65fe_prof);

        
        $__internal_a749422bd1c77434b63bd6ae0cbe01e4be264950eb2ec795bb31b64dfc08debd->leave($__internal_a749422bd1c77434b63bd6ae0cbe01e4be264950eb2ec795bb31b64dfc08debd_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_e376e230f8bb59f523dcb73cd0480fbbb35b6827f41d503da0f1a7d64c0e131c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e376e230f8bb59f523dcb73cd0480fbbb35b6827f41d503da0f1a7d64c0e131c->enter($__internal_e376e230f8bb59f523dcb73cd0480fbbb35b6827f41d503da0f1a7d64c0e131c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_375e50a584e4cb3ebb4477064fecd351c9640c0593be0b8502e5fc83570e4461 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_375e50a584e4cb3ebb4477064fecd351c9640c0593be0b8502e5fc83570e4461->enter($__internal_375e50a584e4cb3ebb4477064fecd351c9640c0593be0b8502e5fc83570e4461_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<br><br><br>
<div class=\"container\">


    <h2>Formation creation</h2>

    ";
        // line 10
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
        ";
        // line 11
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo "
        <input type=\"submit\" value=\"Create\" />
    ";
        // line 13
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "


            <a href=\"";
        // line 16
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_formation_index");
        echo "\" class=\"btn btn-large btn-info\">Back to the list</a>
</div>
";
        
        $__internal_375e50a584e4cb3ebb4477064fecd351c9640c0593be0b8502e5fc83570e4461->leave($__internal_375e50a584e4cb3ebb4477064fecd351c9640c0593be0b8502e5fc83570e4461_prof);

        
        $__internal_e376e230f8bb59f523dcb73cd0480fbbb35b6827f41d503da0f1a7d64c0e131c->leave($__internal_e376e230f8bb59f523dcb73cd0480fbbb35b6827f41d503da0f1a7d64c0e131c_prof);

    }

    public function getTemplateName()
    {
        return "formation/new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 16,  66 => 13,  61 => 11,  57 => 10,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
<br><br><br>
<div class=\"container\">


    <h2>Formation creation</h2>

    {{ form_start(form) }}
        {{ form_widget(form) }}
        <input type=\"submit\" value=\"Create\" />
    {{ form_end(form) }}


            <a href=\"{{ path('admin_formation_index') }}\" class=\"btn btn-large btn-info\">Back to the list</a>
</div>
{% endblock %}
", "formation/new.html.twig", "/home/fonguen/symfony projet/infotels/app/Resources/views/formation/new.html.twig");
    }
}
