<?php

/* @Framework/Form/form_widget_compound.html.php */
class __TwigTemplate_9cfcf207453171f65b0a8dd1ff14aa59d3a5f31b50cb003639af3b6616fc0c2d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5ae3c14b4e6d33cdf18982bf1ae779a45c2d11b41c7a6a9cb2c2af4b2e7422c6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5ae3c14b4e6d33cdf18982bf1ae779a45c2d11b41c7a6a9cb2c2af4b2e7422c6->enter($__internal_5ae3c14b4e6d33cdf18982bf1ae779a45c2d11b41c7a6a9cb2c2af4b2e7422c6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_compound.html.php"));

        $__internal_08d8264ed72911da307fe9a90f0101aa32c33cdbc01bb634d91f4f0a5c7e7046 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_08d8264ed72911da307fe9a90f0101aa32c33cdbc01bb634d91f4f0a5c7e7046->enter($__internal_08d8264ed72911da307fe9a90f0101aa32c33cdbc01bb634d91f4f0a5c7e7046_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_compound.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</div>
";
        
        $__internal_5ae3c14b4e6d33cdf18982bf1ae779a45c2d11b41c7a6a9cb2c2af4b2e7422c6->leave($__internal_5ae3c14b4e6d33cdf18982bf1ae779a45c2d11b41c7a6a9cb2c2af4b2e7422c6_prof);

        
        $__internal_08d8264ed72911da307fe9a90f0101aa32c33cdbc01bb634d91f4f0a5c7e7046->leave($__internal_08d8264ed72911da307fe9a90f0101aa32c33cdbc01bb634d91f4f0a5c7e7046_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget_compound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</div>
", "@Framework/Form/form_widget_compound.html.php", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_widget_compound.html.php");
    }
}
