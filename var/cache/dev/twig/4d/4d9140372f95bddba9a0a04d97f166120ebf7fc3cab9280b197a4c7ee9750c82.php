<?php

/* @Framework/Form/choice_options.html.php */
class __TwigTemplate_1cbca7752c2a406f2a346aa86bdc7e83413fb570c25fca16ead424188a031b0c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7a8d4b6d454a9d06c6bdd53478bbb1067030e08ee5902c3628f89df11f3933ba = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7a8d4b6d454a9d06c6bdd53478bbb1067030e08ee5902c3628f89df11f3933ba->enter($__internal_7a8d4b6d454a9d06c6bdd53478bbb1067030e08ee5902c3628f89df11f3933ba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_options.html.php"));

        $__internal_82ebbffee95e8a9514a941e97083c5a5234eeab6e5dc2d98b2290edac37e2158 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_82ebbffee95e8a9514a941e97083c5a5234eeab6e5dc2d98b2290edac37e2158->enter($__internal_82ebbffee95e8a9514a941e97083c5a5234eeab6e5dc2d98b2290edac37e2158_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_options.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'choice_widget_options') ?>
";
        
        $__internal_7a8d4b6d454a9d06c6bdd53478bbb1067030e08ee5902c3628f89df11f3933ba->leave($__internal_7a8d4b6d454a9d06c6bdd53478bbb1067030e08ee5902c3628f89df11f3933ba_prof);

        
        $__internal_82ebbffee95e8a9514a941e97083c5a5234eeab6e5dc2d98b2290edac37e2158->leave($__internal_82ebbffee95e8a9514a941e97083c5a5234eeab6e5dc2d98b2290edac37e2158_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_options.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'choice_widget_options') ?>
", "@Framework/Form/choice_options.html.php", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choice_options.html.php");
    }
}
