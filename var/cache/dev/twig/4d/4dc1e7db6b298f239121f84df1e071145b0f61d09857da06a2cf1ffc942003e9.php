<?php

/* @FOSUser/Group/edit.html.twig */
class __TwigTemplate_53e038043477b4c8e9517b645fa7945ab76b4cf8866b4721010397bddac71aaf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Group/edit.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bcb466cf229cfa75897dc401c6a26f23f5b88af340fbbc82eb389e072188557b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bcb466cf229cfa75897dc401c6a26f23f5b88af340fbbc82eb389e072188557b->enter($__internal_bcb466cf229cfa75897dc401c6a26f23f5b88af340fbbc82eb389e072188557b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Group/edit.html.twig"));

        $__internal_bbeb3675e3e59d4ac98c1a0faac12517af02538d17a9f1bf6edaae998bec8b3e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bbeb3675e3e59d4ac98c1a0faac12517af02538d17a9f1bf6edaae998bec8b3e->enter($__internal_bbeb3675e3e59d4ac98c1a0faac12517af02538d17a9f1bf6edaae998bec8b3e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Group/edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_bcb466cf229cfa75897dc401c6a26f23f5b88af340fbbc82eb389e072188557b->leave($__internal_bcb466cf229cfa75897dc401c6a26f23f5b88af340fbbc82eb389e072188557b_prof);

        
        $__internal_bbeb3675e3e59d4ac98c1a0faac12517af02538d17a9f1bf6edaae998bec8b3e->leave($__internal_bbeb3675e3e59d4ac98c1a0faac12517af02538d17a9f1bf6edaae998bec8b3e_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_0cf250e1b8ea8bf360064e27e59be940ff57c2dfd697d503f0ec25820221495a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0cf250e1b8ea8bf360064e27e59be940ff57c2dfd697d503f0ec25820221495a->enter($__internal_0cf250e1b8ea8bf360064e27e59be940ff57c2dfd697d503f0ec25820221495a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_10f2a0f6d57e05fd3aae8b93e02d3e20bb90ac997f2866cd138430ec1f913820 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_10f2a0f6d57e05fd3aae8b93e02d3e20bb90ac997f2866cd138430ec1f913820->enter($__internal_10f2a0f6d57e05fd3aae8b93e02d3e20bb90ac997f2866cd138430ec1f913820_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/edit_content.html.twig", "@FOSUser/Group/edit.html.twig", 4)->display($context);
        
        $__internal_10f2a0f6d57e05fd3aae8b93e02d3e20bb90ac997f2866cd138430ec1f913820->leave($__internal_10f2a0f6d57e05fd3aae8b93e02d3e20bb90ac997f2866cd138430ec1f913820_prof);

        
        $__internal_0cf250e1b8ea8bf360064e27e59be940ff57c2dfd697d503f0ec25820221495a->leave($__internal_0cf250e1b8ea8bf360064e27e59be940ff57c2dfd697d503f0ec25820221495a_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Group/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/edit_content.html.twig\" %}
{% endblock fos_user_content %}
", "@FOSUser/Group/edit.html.twig", "/home/fonguen/symfony projet/infotels/vendor/friendsofsymfony/user-bundle/Resources/views/Group/edit.html.twig");
    }
}
