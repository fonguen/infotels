<?php

/* @FOSUser/Registration/email.txt.twig */
class __TwigTemplate_ff5752e13bd8577ca5f0cb4843b8c2363815b9096df5af4785a8d64003622bee extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5e3a59c3494ae3cd8c4172619d075198b82cea3b02719ab6ac54dd95ef20e99c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5e3a59c3494ae3cd8c4172619d075198b82cea3b02719ab6ac54dd95ef20e99c->enter($__internal_5e3a59c3494ae3cd8c4172619d075198b82cea3b02719ab6ac54dd95ef20e99c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/email.txt.twig"));

        $__internal_4f1700f9ac098d132dbe3453c2ee032cc702c6f1a318cf0b1f677c6200624e10 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4f1700f9ac098d132dbe3453c2ee032cc702c6f1a318cf0b1f677c6200624e10->enter($__internal_4f1700f9ac098d132dbe3453c2ee032cc702c6f1a318cf0b1f677c6200624e10_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        echo "
";
        // line 8
        $this->displayBlock('body_text', $context, $blocks);
        // line 13
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_5e3a59c3494ae3cd8c4172619d075198b82cea3b02719ab6ac54dd95ef20e99c->leave($__internal_5e3a59c3494ae3cd8c4172619d075198b82cea3b02719ab6ac54dd95ef20e99c_prof);

        
        $__internal_4f1700f9ac098d132dbe3453c2ee032cc702c6f1a318cf0b1f677c6200624e10->leave($__internal_4f1700f9ac098d132dbe3453c2ee032cc702c6f1a318cf0b1f677c6200624e10_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_1e1918cb0773dc53619cee182a4583c866cb3c69a638a38deb9b474373d40ce0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1e1918cb0773dc53619cee182a4583c866cb3c69a638a38deb9b474373d40ce0->enter($__internal_1e1918cb0773dc53619cee182a4583c866cb3c69a638a38deb9b474373d40ce0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        $__internal_d2238368067cef394b04d609315adfb3b62eec2b2e4105f3116033cfa41e4165 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d2238368067cef394b04d609315adfb3b62eec2b2e4105f3116033cfa41e4165->enter($__internal_d2238368067cef394b04d609315adfb3b62eec2b2e4105f3116033cfa41e4165_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.email.subject", array("%username%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => ($context["confirmationUrl"] ?? $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        
        $__internal_d2238368067cef394b04d609315adfb3b62eec2b2e4105f3116033cfa41e4165->leave($__internal_d2238368067cef394b04d609315adfb3b62eec2b2e4105f3116033cfa41e4165_prof);

        
        $__internal_1e1918cb0773dc53619cee182a4583c866cb3c69a638a38deb9b474373d40ce0->leave($__internal_1e1918cb0773dc53619cee182a4583c866cb3c69a638a38deb9b474373d40ce0_prof);

    }

    // line 8
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_b95eea5208fa6b6dc58ec007cd821c540abaa491e9d552663a6dc4fc171ae9a4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b95eea5208fa6b6dc58ec007cd821c540abaa491e9d552663a6dc4fc171ae9a4->enter($__internal_b95eea5208fa6b6dc58ec007cd821c540abaa491e9d552663a6dc4fc171ae9a4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        $__internal_0c58d14748eecd961e6b29cb28fc59f8f225e074accd5f4745e169286fd9e844 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0c58d14748eecd961e6b29cb28fc59f8f225e074accd5f4745e169286fd9e844->enter($__internal_0c58d14748eecd961e6b29cb28fc59f8f225e074accd5f4745e169286fd9e844_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.email.message", array("%username%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => ($context["confirmationUrl"] ?? $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        echo "
";
        
        $__internal_0c58d14748eecd961e6b29cb28fc59f8f225e074accd5f4745e169286fd9e844->leave($__internal_0c58d14748eecd961e6b29cb28fc59f8f225e074accd5f4745e169286fd9e844_prof);

        
        $__internal_b95eea5208fa6b6dc58ec007cd821c540abaa491e9d552663a6dc4fc171ae9a4->leave($__internal_b95eea5208fa6b6dc58ec007cd821c540abaa491e9d552663a6dc4fc171ae9a4_prof);

    }

    // line 13
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_78fd1e59347ef0389d809187a5d7f49c889877020a5fc986da8fa0ae176e0f9a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_78fd1e59347ef0389d809187a5d7f49c889877020a5fc986da8fa0ae176e0f9a->enter($__internal_78fd1e59347ef0389d809187a5d7f49c889877020a5fc986da8fa0ae176e0f9a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        $__internal_d3ae9fc359e2a2a4c76cc9bc8a9f5116afab0cf88d5eebffe242023ee0b842e0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d3ae9fc359e2a2a4c76cc9bc8a9f5116afab0cf88d5eebffe242023ee0b842e0->enter($__internal_d3ae9fc359e2a2a4c76cc9bc8a9f5116afab0cf88d5eebffe242023ee0b842e0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_d3ae9fc359e2a2a4c76cc9bc8a9f5116afab0cf88d5eebffe242023ee0b842e0->leave($__internal_d3ae9fc359e2a2a4c76cc9bc8a9f5116afab0cf88d5eebffe242023ee0b842e0_prof);

        
        $__internal_78fd1e59347ef0389d809187a5d7f49c889877020a5fc986da8fa0ae176e0f9a->leave($__internal_78fd1e59347ef0389d809187a5d7f49c889877020a5fc986da8fa0ae176e0f9a_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Registration/email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  85 => 13,  73 => 10,  64 => 8,  54 => 4,  45 => 2,  35 => 13,  33 => 8,  30 => 7,  28 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}
{% block subject %}
{%- autoescape false -%}
{{ 'registration.email.subject'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}
{%- endautoescape -%}
{% endblock %}

{% block body_text %}
{% autoescape false %}
{{ 'registration.email.message'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}
{% endautoescape %}
{% endblock %}
{% block body_html %}{% endblock %}
", "@FOSUser/Registration/email.txt.twig", "/home/fonguen/symfony projet/infotels/vendor/friendsofsymfony/user-bundle/Resources/views/Registration/email.txt.twig");
    }
}
