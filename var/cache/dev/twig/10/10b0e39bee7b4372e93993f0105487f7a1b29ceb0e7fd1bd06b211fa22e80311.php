<?php

/* @Framework/Form/choice_attributes.html.php */
class __TwigTemplate_e5662eae3cac4345293995fad79d18fb1211bf31ace8c3c68c6db22a93695258 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_381790b31456e95cb6e04fac145a2c17b8b027368afdb98b273d42df06937c49 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_381790b31456e95cb6e04fac145a2c17b8b027368afdb98b273d42df06937c49->enter($__internal_381790b31456e95cb6e04fac145a2c17b8b027368afdb98b273d42df06937c49_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_attributes.html.php"));

        $__internal_001e7fccfd1554c46a132ce7a7d9f78383b1834afbf0f3de218e7ea064367e06 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_001e7fccfd1554c46a132ce7a7d9f78383b1834afbf0f3de218e7ea064367e06->enter($__internal_001e7fccfd1554c46a132ce7a7d9f78383b1834afbf0f3de218e7ea064367e06_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_attributes.html.php"));

        // line 1
        echo "<?php if (\$disabled): ?>disabled=\"disabled\" <?php endif ?>
<?php foreach (\$choice_attr as \$k => \$v): ?>
<?php if (\$v === true): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$k)) ?>
<?php elseif (\$v !== false): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$v)) ?>
<?php endif ?>
<?php endforeach ?>
";
        
        $__internal_381790b31456e95cb6e04fac145a2c17b8b027368afdb98b273d42df06937c49->leave($__internal_381790b31456e95cb6e04fac145a2c17b8b027368afdb98b273d42df06937c49_prof);

        
        $__internal_001e7fccfd1554c46a132ce7a7d9f78383b1834afbf0f3de218e7ea064367e06->leave($__internal_001e7fccfd1554c46a132ce7a7d9f78383b1834afbf0f3de218e7ea064367e06_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$disabled): ?>disabled=\"disabled\" <?php endif ?>
<?php foreach (\$choice_attr as \$k => \$v): ?>
<?php if (\$v === true): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$k)) ?>
<?php elseif (\$v !== false): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$v)) ?>
<?php endif ?>
<?php endforeach ?>
", "@Framework/Form/choice_attributes.html.php", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choice_attributes.html.php");
    }
}
