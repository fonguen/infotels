<?php

/* @Twig/Exception/error.js.twig */
class __TwigTemplate_d1a70514037b2e20d95916e35d621adbb12107767aa40c6d675f1ed28ae8157c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_503792501a2d13557e19153c42c48de30aa2c5eb57f1fc1efeb0f2058a1b546d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_503792501a2d13557e19153c42c48de30aa2c5eb57f1fc1efeb0f2058a1b546d->enter($__internal_503792501a2d13557e19153c42c48de30aa2c5eb57f1fc1efeb0f2058a1b546d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.js.twig"));

        $__internal_7d6f28deb9046a874daa7b4ff0967592f2952d5747655cbd509d57afe141f65f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7d6f28deb9046a874daa7b4ff0967592f2952d5747655cbd509d57afe141f65f->enter($__internal_7d6f28deb9046a874daa7b4ff0967592f2952d5747655cbd509d57afe141f65f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, ($context["status_code"] ?? $this->getContext($context, "status_code")), "js", null, true);
        echo " ";
        echo twig_escape_filter($this->env, ($context["status_text"] ?? $this->getContext($context, "status_text")), "js", null, true);
        echo "

*/
";
        
        $__internal_503792501a2d13557e19153c42c48de30aa2c5eb57f1fc1efeb0f2058a1b546d->leave($__internal_503792501a2d13557e19153c42c48de30aa2c5eb57f1fc1efeb0f2058a1b546d_prof);

        
        $__internal_7d6f28deb9046a874daa7b4ff0967592f2952d5747655cbd509d57afe141f65f->leave($__internal_7d6f28deb9046a874daa7b4ff0967592f2952d5747655cbd509d57afe141f65f_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{{ status_code }} {{ status_text }}

*/
", "@Twig/Exception/error.js.twig", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.js.twig");
    }
}
