<?php

/* @Twig/Exception/traces.txt.twig */
class __TwigTemplate_c5816d7b10451e29264fe754cadee69848c0758847a9869a106d59445fc8eb6c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_13f5028b7b2d202ca4d1369de585093caac220a2b97ba99e4285f6adf5134fa6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_13f5028b7b2d202ca4d1369de585093caac220a2b97ba99e4285f6adf5134fa6->enter($__internal_13f5028b7b2d202ca4d1369de585093caac220a2b97ba99e4285f6adf5134fa6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/traces.txt.twig"));

        $__internal_8e6025fc6e492e9035ae54560b69b1af8af656868c3dff2c18007a6a9b2e2571 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8e6025fc6e492e9035ae54560b69b1af8af656868c3dff2c18007a6a9b2e2571->enter($__internal_8e6025fc6e492e9035ae54560b69b1af8af656868c3dff2c18007a6a9b2e2571_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/traces.txt.twig"));

        // line 1
        if (twig_length_filter($this->env, $this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "trace", array()))) {
            // line 2
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "trace", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["trace"]) {
                // line 3
                $this->loadTemplate("@Twig/Exception/trace.txt.twig", "@Twig/Exception/traces.txt.twig", 3)->display(array("trace" => $context["trace"]));
                // line 4
                echo "
";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['trace'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        
        $__internal_13f5028b7b2d202ca4d1369de585093caac220a2b97ba99e4285f6adf5134fa6->leave($__internal_13f5028b7b2d202ca4d1369de585093caac220a2b97ba99e4285f6adf5134fa6_prof);

        
        $__internal_8e6025fc6e492e9035ae54560b69b1af8af656868c3dff2c18007a6a9b2e2571->leave($__internal_8e6025fc6e492e9035ae54560b69b1af8af656868c3dff2c18007a6a9b2e2571_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/traces.txt.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  33 => 4,  31 => 3,  27 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% if exception.trace|length %}
{% for trace in exception.trace %}
{% include '@Twig/Exception/trace.txt.twig' with { 'trace': trace } only %}

{% endfor %}
{% endif %}
", "@Twig/Exception/traces.txt.twig", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/traces.txt.twig");
    }
}
