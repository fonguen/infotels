<?php

/* @FOSUser/Resetting/email.txt.twig */
class __TwigTemplate_cbf13aebbe7a8238c1c987de216a9cc1c5312dafc645fa886cdb4d85bd8ecd67 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_27fa52dd825f49b2c5d9679483709a34b3432a682b8c61b708229a9ce4c3eff6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_27fa52dd825f49b2c5d9679483709a34b3432a682b8c61b708229a9ce4c3eff6->enter($__internal_27fa52dd825f49b2c5d9679483709a34b3432a682b8c61b708229a9ce4c3eff6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Resetting/email.txt.twig"));

        $__internal_46848e08d763fcced77bed004335df823eee961a03a8fd10847fae9b5d239a65 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_46848e08d763fcced77bed004335df823eee961a03a8fd10847fae9b5d239a65->enter($__internal_46848e08d763fcced77bed004335df823eee961a03a8fd10847fae9b5d239a65_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Resetting/email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        echo "
";
        // line 8
        $this->displayBlock('body_text', $context, $blocks);
        // line 13
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_27fa52dd825f49b2c5d9679483709a34b3432a682b8c61b708229a9ce4c3eff6->leave($__internal_27fa52dd825f49b2c5d9679483709a34b3432a682b8c61b708229a9ce4c3eff6_prof);

        
        $__internal_46848e08d763fcced77bed004335df823eee961a03a8fd10847fae9b5d239a65->leave($__internal_46848e08d763fcced77bed004335df823eee961a03a8fd10847fae9b5d239a65_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_6174b1883cde22caeec254ad6da5ca72fab388d68313bf04e6ee20f74017f763 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6174b1883cde22caeec254ad6da5ca72fab388d68313bf04e6ee20f74017f763->enter($__internal_6174b1883cde22caeec254ad6da5ca72fab388d68313bf04e6ee20f74017f763_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        $__internal_8e7578e5d7fb6f33a51891d7808d5ccbe7539a6332dc2a0affe0bdeac9b6e62c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8e7578e5d7fb6f33a51891d7808d5ccbe7539a6332dc2a0affe0bdeac9b6e62c->enter($__internal_8e7578e5d7fb6f33a51891d7808d5ccbe7539a6332dc2a0affe0bdeac9b6e62c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.email.subject", array("%username%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array())), "FOSUserBundle");
        
        $__internal_8e7578e5d7fb6f33a51891d7808d5ccbe7539a6332dc2a0affe0bdeac9b6e62c->leave($__internal_8e7578e5d7fb6f33a51891d7808d5ccbe7539a6332dc2a0affe0bdeac9b6e62c_prof);

        
        $__internal_6174b1883cde22caeec254ad6da5ca72fab388d68313bf04e6ee20f74017f763->leave($__internal_6174b1883cde22caeec254ad6da5ca72fab388d68313bf04e6ee20f74017f763_prof);

    }

    // line 8
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_c61c703501a4c954c6935274e61f486b0ac6402273199e5098c12dcd16ff775c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c61c703501a4c954c6935274e61f486b0ac6402273199e5098c12dcd16ff775c->enter($__internal_c61c703501a4c954c6935274e61f486b0ac6402273199e5098c12dcd16ff775c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        $__internal_dee60441b05cb3bd4627a224351eed0b56b88ebedae597df239a6b0f7d5fe02a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dee60441b05cb3bd4627a224351eed0b56b88ebedae597df239a6b0f7d5fe02a->enter($__internal_dee60441b05cb3bd4627a224351eed0b56b88ebedae597df239a6b0f7d5fe02a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.email.message", array("%username%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => ($context["confirmationUrl"] ?? $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        echo "
";
        
        $__internal_dee60441b05cb3bd4627a224351eed0b56b88ebedae597df239a6b0f7d5fe02a->leave($__internal_dee60441b05cb3bd4627a224351eed0b56b88ebedae597df239a6b0f7d5fe02a_prof);

        
        $__internal_c61c703501a4c954c6935274e61f486b0ac6402273199e5098c12dcd16ff775c->leave($__internal_c61c703501a4c954c6935274e61f486b0ac6402273199e5098c12dcd16ff775c_prof);

    }

    // line 13
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_5f488036d86630e701612b48ab86118dce93a1ed45272136fd5ecf36afdb920b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5f488036d86630e701612b48ab86118dce93a1ed45272136fd5ecf36afdb920b->enter($__internal_5f488036d86630e701612b48ab86118dce93a1ed45272136fd5ecf36afdb920b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        $__internal_9ecb29e9538fcbac2f169a9e19c3281e257c94639df09b52f7aef1221520f28b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9ecb29e9538fcbac2f169a9e19c3281e257c94639df09b52f7aef1221520f28b->enter($__internal_9ecb29e9538fcbac2f169a9e19c3281e257c94639df09b52f7aef1221520f28b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_9ecb29e9538fcbac2f169a9e19c3281e257c94639df09b52f7aef1221520f28b->leave($__internal_9ecb29e9538fcbac2f169a9e19c3281e257c94639df09b52f7aef1221520f28b_prof);

        
        $__internal_5f488036d86630e701612b48ab86118dce93a1ed45272136fd5ecf36afdb920b->leave($__internal_5f488036d86630e701612b48ab86118dce93a1ed45272136fd5ecf36afdb920b_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Resetting/email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  85 => 13,  73 => 10,  64 => 8,  54 => 4,  45 => 2,  35 => 13,  33 => 8,  30 => 7,  28 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}
{% block subject %}
{%- autoescape false -%}
{{ 'resetting.email.subject'|trans({'%username%': user.username}) }}
{%- endautoescape -%}
{% endblock %}

{% block body_text %}
{% autoescape false %}
{{ 'resetting.email.message'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}
{% endautoescape %}
{% endblock %}
{% block body_html %}{% endblock %}
", "@FOSUser/Resetting/email.txt.twig", "/home/fonguen/symfony projet/infotels/vendor/friendsofsymfony/user-bundle/Resources/views/Resetting/email.txt.twig");
    }
}
