<?php

/* @Framework/Form/number_widget.html.php */
class __TwigTemplate_f39812fc728959521da7ca3f14ed274d5b8ef37e98f5f1d991d2c5e0f0355416 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6f35c129d1ae77f38a71471e33ca73d7391017624e7c56d54dc9ba28ce3a999c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6f35c129d1ae77f38a71471e33ca73d7391017624e7c56d54dc9ba28ce3a999c->enter($__internal_6f35c129d1ae77f38a71471e33ca73d7391017624e7c56d54dc9ba28ce3a999c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/number_widget.html.php"));

        $__internal_e92af25fa8e2cd1e7318cd153e91f0e49aa0c1ada872c1011577691726c71041 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e92af25fa8e2cd1e7318cd153e91f0e49aa0c1ada872c1011577691726c71041->enter($__internal_e92af25fa8e2cd1e7318cd153e91f0e49aa0c1ada872c1011577691726c71041_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/number_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?>
";
        
        $__internal_6f35c129d1ae77f38a71471e33ca73d7391017624e7c56d54dc9ba28ce3a999c->leave($__internal_6f35c129d1ae77f38a71471e33ca73d7391017624e7c56d54dc9ba28ce3a999c_prof);

        
        $__internal_e92af25fa8e2cd1e7318cd153e91f0e49aa0c1ada872c1011577691726c71041->leave($__internal_e92af25fa8e2cd1e7318cd153e91f0e49aa0c1ada872c1011577691726c71041_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/number_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?>
", "@Framework/Form/number_widget.html.php", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/number_widget.html.php");
    }
}
