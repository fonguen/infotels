<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_f0e345d091aed46f9d2e4e1da86eca7d9c5ad3f6c3b492afdb30ad740cf99b46 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_49fddf42638667ab10ba0d8366518fa78cc4423a3e4a8ac4dac32564f8be8bed = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_49fddf42638667ab10ba0d8366518fa78cc4423a3e4a8ac4dac32564f8be8bed->enter($__internal_49fddf42638667ab10ba0d8366518fa78cc4423a3e4a8ac4dac32564f8be8bed_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $__internal_2e915a1619a86c5daefab82fa61e792a15e774e6bebea05538b148ca187a70df = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2e915a1619a86c5daefab82fa61e792a15e774e6bebea05538b148ca187a70df->enter($__internal_2e915a1619a86c5daefab82fa61e792a15e774e6bebea05538b148ca187a70df_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_49fddf42638667ab10ba0d8366518fa78cc4423a3e4a8ac4dac32564f8be8bed->leave($__internal_49fddf42638667ab10ba0d8366518fa78cc4423a3e4a8ac4dac32564f8be8bed_prof);

        
        $__internal_2e915a1619a86c5daefab82fa61e792a15e774e6bebea05538b148ca187a70df->leave($__internal_2e915a1619a86c5daefab82fa61e792a15e774e6bebea05538b148ca187a70df_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_6f63cc1783c0536f85f54124048b75037a7cbf399419e2cfcab8c7724668bb11 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6f63cc1783c0536f85f54124048b75037a7cbf399419e2cfcab8c7724668bb11->enter($__internal_6f63cc1783c0536f85f54124048b75037a7cbf399419e2cfcab8c7724668bb11_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_44af033de2ee011d8ee5d7bcb6209bf8ab6d4b3a0c61108988098cf47460ad5c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_44af033de2ee011d8ee5d7bcb6209bf8ab6d4b3a0c61108988098cf47460ad5c->enter($__internal_44af033de2ee011d8ee5d7bcb6209bf8ab6d4b3a0c61108988098cf47460ad5c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
";
        
        $__internal_44af033de2ee011d8ee5d7bcb6209bf8ab6d4b3a0c61108988098cf47460ad5c->leave($__internal_44af033de2ee011d8ee5d7bcb6209bf8ab6d4b3a0c61108988098cf47460ad5c_prof);

        
        $__internal_6f63cc1783c0536f85f54124048b75037a7cbf399419e2cfcab8c7724668bb11->leave($__internal_6f63cc1783c0536f85f54124048b75037a7cbf399419e2cfcab8c7724668bb11_prof);

    }

    // line 136
    public function block_title($context, array $blocks = array())
    {
        $__internal_c7efb2f60fc02e9cdc96946cc070d9a4bbc3f94b07526e83d4eb8384b8df8778 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c7efb2f60fc02e9cdc96946cc070d9a4bbc3f94b07526e83d4eb8384b8df8778->enter($__internal_c7efb2f60fc02e9cdc96946cc070d9a4bbc3f94b07526e83d4eb8384b8df8778_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_ce3e73d11982b7a6ca1b8fe8584d1acfdd74c7bb62323059ec3a5fd9c3e1ca4c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ce3e73d11982b7a6ca1b8fe8584d1acfdd74c7bb62323059ec3a5fd9c3e1ca4c->enter($__internal_ce3e73d11982b7a6ca1b8fe8584d1acfdd74c7bb62323059ec3a5fd9c3e1ca4c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 137
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, ($context["status_code"] ?? $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, ($context["status_text"] ?? $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_ce3e73d11982b7a6ca1b8fe8584d1acfdd74c7bb62323059ec3a5fd9c3e1ca4c->leave($__internal_ce3e73d11982b7a6ca1b8fe8584d1acfdd74c7bb62323059ec3a5fd9c3e1ca4c_prof);

        
        $__internal_c7efb2f60fc02e9cdc96946cc070d9a4bbc3f94b07526e83d4eb8384b8df8778->leave($__internal_c7efb2f60fc02e9cdc96946cc070d9a4bbc3f94b07526e83d4eb8384b8df8778_prof);

    }

    // line 140
    public function block_body($context, array $blocks = array())
    {
        $__internal_237f6c8de15f6e0120f6a59809940808072d754528ef05e618ba17e6091c4c14 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_237f6c8de15f6e0120f6a59809940808072d754528ef05e618ba17e6091c4c14->enter($__internal_237f6c8de15f6e0120f6a59809940808072d754528ef05e618ba17e6091c4c14_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_92f10c02cb2bf07e0c88ad02e31fb971bc2f4d56f5a6e9cbed63cef9d3fab7b8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_92f10c02cb2bf07e0c88ad02e31fb971bc2f4d56f5a6e9cbed63cef9d3fab7b8->enter($__internal_92f10c02cb2bf07e0c88ad02e31fb971bc2f4d56f5a6e9cbed63cef9d3fab7b8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 141
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 141)->display($context);
        
        $__internal_92f10c02cb2bf07e0c88ad02e31fb971bc2f4d56f5a6e9cbed63cef9d3fab7b8->leave($__internal_92f10c02cb2bf07e0c88ad02e31fb971bc2f4d56f5a6e9cbed63cef9d3fab7b8_prof);

        
        $__internal_237f6c8de15f6e0120f6a59809940808072d754528ef05e618ba17e6091c4c14->leave($__internal_237f6c8de15f6e0120f6a59809940808072d754528ef05e618ba17e6091c4c14_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  226 => 141,  217 => 140,  200 => 137,  191 => 136,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "@Twig/Exception/exception_full.html.twig", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception_full.html.twig");
    }
}
