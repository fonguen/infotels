<?php

/* cours/edit.html.twig */
class __TwigTemplate_af68f0dd4deec224d4437c6fc0f73cf9d0ac66069096c20100a3698b55fb17f6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "cours/edit.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_62ab0c9323df2154fff533c3b936a441bfe78a09f52ba5d7b844a8985916bc10 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_62ab0c9323df2154fff533c3b936a441bfe78a09f52ba5d7b844a8985916bc10->enter($__internal_62ab0c9323df2154fff533c3b936a441bfe78a09f52ba5d7b844a8985916bc10_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "cours/edit.html.twig"));

        $__internal_203cd9733a715548c172478b117fac0405adf6bd23e8e1a972fb36e2c936e836 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_203cd9733a715548c172478b117fac0405adf6bd23e8e1a972fb36e2c936e836->enter($__internal_203cd9733a715548c172478b117fac0405adf6bd23e8e1a972fb36e2c936e836_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "cours/edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_62ab0c9323df2154fff533c3b936a441bfe78a09f52ba5d7b844a8985916bc10->leave($__internal_62ab0c9323df2154fff533c3b936a441bfe78a09f52ba5d7b844a8985916bc10_prof);

        
        $__internal_203cd9733a715548c172478b117fac0405adf6bd23e8e1a972fb36e2c936e836->leave($__internal_203cd9733a715548c172478b117fac0405adf6bd23e8e1a972fb36e2c936e836_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_07ccbc724d74efc091b693adceb174a64b76d3f34caff4717985b8fa369394e4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_07ccbc724d74efc091b693adceb174a64b76d3f34caff4717985b8fa369394e4->enter($__internal_07ccbc724d74efc091b693adceb174a64b76d3f34caff4717985b8fa369394e4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_55548274e45075ca0368a9c3fde38ef4c96d2cb56cbd76453ac97fdeb7e9457b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_55548274e45075ca0368a9c3fde38ef4c96d2cb56cbd76453ac97fdeb7e9457b->enter($__internal_55548274e45075ca0368a9c3fde38ef4c96d2cb56cbd76453ac97fdeb7e9457b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Cour edit</h1>

    ";
        // line 6
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'form_start');
        echo "
        ";
        // line 7
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'widget');
        echo "
        <input type=\"submit\" value=\"Edit\" />
    ";
        // line 9
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'form_end');
        echo "

    <ul>
        <li>
            <a href=\"";
        // line 13
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_cours_index");
        echo "\">Back to the list</a>
        </li>
        <li>
            ";
        // line 16
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_start');
        echo "
                <input type=\"submit\" value=\"Delete\">
            ";
        // line 18
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_end');
        echo "
        </li>
    </ul>
";
        
        $__internal_55548274e45075ca0368a9c3fde38ef4c96d2cb56cbd76453ac97fdeb7e9457b->leave($__internal_55548274e45075ca0368a9c3fde38ef4c96d2cb56cbd76453ac97fdeb7e9457b_prof);

        
        $__internal_07ccbc724d74efc091b693adceb174a64b76d3f34caff4717985b8fa369394e4->leave($__internal_07ccbc724d74efc091b693adceb174a64b76d3f34caff4717985b8fa369394e4_prof);

    }

    public function getTemplateName()
    {
        return "cours/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 18,  75 => 16,  69 => 13,  62 => 9,  57 => 7,  53 => 6,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Cour edit</h1>

    {{ form_start(edit_form) }}
        {{ form_widget(edit_form) }}
        <input type=\"submit\" value=\"Edit\" />
    {{ form_end(edit_form) }}

    <ul>
        <li>
            <a href=\"{{ path('admin_cours_index') }}\">Back to the list</a>
        </li>
        <li>
            {{ form_start(delete_form) }}
                <input type=\"submit\" value=\"Delete\">
            {{ form_end(delete_form) }}
        </li>
    </ul>
{% endblock %}
", "cours/edit.html.twig", "/home/fonguen/symfony projet/infotels/app/Resources/views/cours/edit.html.twig");
    }
}
