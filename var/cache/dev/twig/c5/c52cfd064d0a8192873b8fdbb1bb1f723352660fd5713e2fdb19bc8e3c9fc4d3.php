<?php

/* @App/Blog/even.html.twig */
class __TwigTemplate_b7ceabe39d26d2373562c8cac8474265251450ed99f29a1c5bc705656e3292e7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "@App/Blog/even.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_047e181d7c2fc0ec0823ba50458084273608a570bcf5244ab4f3666bd166c5aa = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_047e181d7c2fc0ec0823ba50458084273608a570bcf5244ab4f3666bd166c5aa->enter($__internal_047e181d7c2fc0ec0823ba50458084273608a570bcf5244ab4f3666bd166c5aa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@App/Blog/even.html.twig"));

        $__internal_ca93221ba159b5471b24e056d2feaeeaa7f1a06b0c571f69505b94275eff5da7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ca93221ba159b5471b24e056d2feaeeaa7f1a06b0c571f69505b94275eff5da7->enter($__internal_ca93221ba159b5471b24e056d2feaeeaa7f1a06b0c571f69505b94275eff5da7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@App/Blog/even.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_047e181d7c2fc0ec0823ba50458084273608a570bcf5244ab4f3666bd166c5aa->leave($__internal_047e181d7c2fc0ec0823ba50458084273608a570bcf5244ab4f3666bd166c5aa_prof);

        
        $__internal_ca93221ba159b5471b24e056d2feaeeaa7f1a06b0c571f69505b94275eff5da7->leave($__internal_ca93221ba159b5471b24e056d2feaeeaa7f1a06b0c571f69505b94275eff5da7_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_08e5616d92e6626bca8c80b131f2387c2afe48710020690c7af657249b1a7381 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_08e5616d92e6626bca8c80b131f2387c2afe48710020690c7af657249b1a7381->enter($__internal_08e5616d92e6626bca8c80b131f2387c2afe48710020690c7af657249b1a7381_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_737515ff9deea495ff51bd31cd3b0b59bede79ee5b7434e245f5a44880dc92da = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_737515ff9deea495ff51bd31cd3b0b59bede79ee5b7434e245f5a44880dc92da->enter($__internal_737515ff9deea495ff51bd31cd3b0b59bede79ee5b7434e245f5a44880dc92da_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<br><br>
<div class=\"container\">



<form class=\"col-lg-6\" method=\"post\" action=\"rechercherevenment\">

<div class=\"form-group\">
    <table>
        <tr>
            <td width=\"100%\">
                <input id=\"text\" type=\"text\" class=\"form-control\" name=\"motcle\" >
            </td>
            <td>
                <button type=\"submit\" class=\"btn btn-info\" ><i class=\"fas fa-search\"></i></button>

            </td>
        </tr>
    </table>



</div>
</form>


    \t\t<table class=\"table\" >
    \t\t\t<tr>
    \t\t\t\t<td> <b>INTITULE DE L'EVENEMENT</b></td><td> <b>DATE</b></td><td> <b>DEPARTEMENT</b></td><td> <b>DESCRPTION</b></td>
    \t\t\t</tr>

    \t\t\t";
        // line 35
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["even"]);
        foreach ($context['_seq'] as $context["_key"] => $context["even"]) {
            // line 36
            echo "
    \t\t\t<tr>
    \t\t\t\t<td> ";
            // line 38
            echo twig_escape_filter($this->env, $this->getAttribute($context["even"], "intituleEvenement", array()), "html", null, true);
            echo "</td>
    \t\t\t\t<td>";
            // line 39
            echo twig_escape_filter($this->env, $this->getAttribute($context["even"], "dateEvenement", array()), "html", null, true);
            echo " </td>
            <td>";
            // line 40
            echo twig_escape_filter($this->env, $this->getAttribute($context["even"], "Departement", array()), "html", null, true);
            echo " </td>
            <td><p> ";
            // line 41
            echo twig_escape_filter($this->env, $this->getAttribute($context["even"], "description", array()), "html", null, true);
            echo "</p> </td>
    \t\t\t</tr>
    \t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['even'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 44
        echo "
    \t\t</table>
    \t</div>
    \t<div class=\"navigation\">
    ";
        // line 48
        echo $this->env->getExtension('Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension')->render($this->env, ($context["even"] ?? $this->getContext($context, "even")));
        echo "
</div>

</div>
";
        
        $__internal_737515ff9deea495ff51bd31cd3b0b59bede79ee5b7434e245f5a44880dc92da->leave($__internal_737515ff9deea495ff51bd31cd3b0b59bede79ee5b7434e245f5a44880dc92da_prof);

        
        $__internal_08e5616d92e6626bca8c80b131f2387c2afe48710020690c7af657249b1a7381->leave($__internal_08e5616d92e6626bca8c80b131f2387c2afe48710020690c7af657249b1a7381_prof);

    }

    public function getTemplateName()
    {
        return "@App/Blog/even.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 48,  111 => 44,  102 => 41,  98 => 40,  94 => 39,  90 => 38,  86 => 36,  82 => 35,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
<br><br>
<div class=\"container\">



<form class=\"col-lg-6\" method=\"post\" action=\"rechercherevenment\">

<div class=\"form-group\">
    <table>
        <tr>
            <td width=\"100%\">
                <input id=\"text\" type=\"text\" class=\"form-control\" name=\"motcle\" >
            </td>
            <td>
                <button type=\"submit\" class=\"btn btn-info\" ><i class=\"fas fa-search\"></i></button>

            </td>
        </tr>
    </table>



</div>
</form>


    \t\t<table class=\"table\" >
    \t\t\t<tr>
    \t\t\t\t<td> <b>INTITULE DE L'EVENEMENT</b></td><td> <b>DATE</b></td><td> <b>DEPARTEMENT</b></td><td> <b>DESCRPTION</b></td>
    \t\t\t</tr>

    \t\t\t{% for even in even %}

    \t\t\t<tr>
    \t\t\t\t<td> {{ even.intituleEvenement }}</td>
    \t\t\t\t<td>{{ even.dateEvenement}} </td>
            <td>{{ even.Departement }} </td>
            <td><p> {{ even.description}}</p> </td>
    \t\t\t</tr>
    \t\t\t{% endfor %}

    \t\t</table>
    \t</div>
    \t<div class=\"navigation\">
    {{ knp_pagination_render(even) }}
</div>

</div>
{% endblock %}
", "@App/Blog/even.html.twig", "/home/fonguen/symfony projet/infotels/src/AppBundle/Resources/views/Blog/even.html.twig");
    }
}
