<?php

/* @FOSUser/Registration/register.html.twig */
class __TwigTemplate_d726fdd6e157b17fe76611fed363b944b344493269bc61ae4b4787ab030e21f6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Registration/register.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0696624357d46caaadb2c6ee98bf8259cfb185852c1b6da63c48a22b5bcb92a8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0696624357d46caaadb2c6ee98bf8259cfb185852c1b6da63c48a22b5bcb92a8->enter($__internal_0696624357d46caaadb2c6ee98bf8259cfb185852c1b6da63c48a22b5bcb92a8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/register.html.twig"));

        $__internal_83a2e939045c0ae65530760ff0d77da8ee584a6be83642060bdaed2a12785d3f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_83a2e939045c0ae65530760ff0d77da8ee584a6be83642060bdaed2a12785d3f->enter($__internal_83a2e939045c0ae65530760ff0d77da8ee584a6be83642060bdaed2a12785d3f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/register.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0696624357d46caaadb2c6ee98bf8259cfb185852c1b6da63c48a22b5bcb92a8->leave($__internal_0696624357d46caaadb2c6ee98bf8259cfb185852c1b6da63c48a22b5bcb92a8_prof);

        
        $__internal_83a2e939045c0ae65530760ff0d77da8ee584a6be83642060bdaed2a12785d3f->leave($__internal_83a2e939045c0ae65530760ff0d77da8ee584a6be83642060bdaed2a12785d3f_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_9bce6dde9c7495e41fcd8702a06275cdca975f0c9ee435e681e1e00bd07c6778 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9bce6dde9c7495e41fcd8702a06275cdca975f0c9ee435e681e1e00bd07c6778->enter($__internal_9bce6dde9c7495e41fcd8702a06275cdca975f0c9ee435e681e1e00bd07c6778_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_b756e2ea7e4de1fee10fe876f961facf97be0cf0c9abbe04b938efda00359b11 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b756e2ea7e4de1fee10fe876f961facf97be0cf0c9abbe04b938efda00359b11->enter($__internal_b756e2ea7e4de1fee10fe876f961facf97be0cf0c9abbe04b938efda00359b11_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Registration/register_content.html.twig", "@FOSUser/Registration/register.html.twig", 4)->display($context);
        
        $__internal_b756e2ea7e4de1fee10fe876f961facf97be0cf0c9abbe04b938efda00359b11->leave($__internal_b756e2ea7e4de1fee10fe876f961facf97be0cf0c9abbe04b938efda00359b11_prof);

        
        $__internal_9bce6dde9c7495e41fcd8702a06275cdca975f0c9ee435e681e1e00bd07c6778->leave($__internal_9bce6dde9c7495e41fcd8702a06275cdca975f0c9ee435e681e1e00bd07c6778_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Registration/register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Registration/register_content.html.twig\" %}
{% endblock fos_user_content %}
", "@FOSUser/Registration/register.html.twig", "/home/fonguen/symfony projet/infotels/vendor/friendsofsymfony/user-bundle/Resources/views/Registration/register.html.twig");
    }
}
