<?php

/* base.html.twig */
class __TwigTemplate_75adbe21aed52fc8612046d2bf7cc08b5302319fe393045cb066ae52f0bf12f4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d7f8c53bcd4b6fe71670e484d407f27fa3140cf015b0c61b990c19b9cbda4415 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d7f8c53bcd4b6fe71670e484d407f27fa3140cf015b0c61b990c19b9cbda4415->enter($__internal_d7f8c53bcd4b6fe71670e484d407f27fa3140cf015b0c61b990c19b9cbda4415_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_199c4bfe6ca2fc3a6f870b258369f4c4aba99becfd0c67d6c8cab7ece4a01530 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_199c4bfe6ca2fc3a6f870b258369f4c4aba99becfd0c67d6c8cab7ece4a01530->enter($__internal_199c4bfe6ca2fc3a6f870b258369f4c4aba99becfd0c67d6c8cab7ece4a01530_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
       <link  href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bootstrap/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" media=\"screen\" />
       <link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.8.2/css/all.css\" integrity=\"sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay\" crossorigin=\"anonymous\">
      <script src=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bootstrap/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
    </head>
    <style>
    body{
      padding-top: 10px;
    }
    #navbare{
      width: 90%;
      height: 70px;
      color: white;

    }
    #toto{
      text-color: white;
    }
    #nav{


      width: 90%;
    }
    #linkref{
      color: white;
      text-decoration: blue;
      padding: 15px;
      top: 0;
    }
    .slider{
        width: 500px;
        height: 300px;
        overflow: hidden;
        margin: 130px auto;
    }
    .slides{
    width: calc(500px * 3);
    animation: glisse 12s infinite;
     }
    .slide{
        float: left;
     }
     @keyframes glisse{
        0%{
            transform: translateX(0);
        }
        10%{
            transform: translateX(0);
        }
         33%{
            transform: translateX(-5000);
        }
        43%{
           transform: translateX(-5000);
       }
         66%{
            transform: translateX(-1000px);
        }
        76%{
           transform: translateX(-1000px);
       }
         100%{
            transform: translateX(0);
        }

     }
.def {                                                   /** gros titre */
  font-family: serif;                                  /* On se permet un changement de police : pas plus de deux par page, et à utiliser avec parcimonie (c'est le cas) */
  background-color: blue;                              /* Fond du titre bleu (et sans image de fond) */
  color: white;                                          /*  Donc on utilise une police blanche pour que le texte soit visible */
  border: 3px dotted #9999FF;                            /* On encadre le titre d'un bleu clair de 3 pixels de largeur */
  padding: 0.3em;                                       /* Espacement intérieur non nul pour que le texte ne colle pas à la bordure du cadre */
   text-align: center;                                     /* Le titre doit être centré ! */
  letter-spacing: 0.7em;
    border-radius: 80px ;                                 /* On espace les caractères pour que ce soit joli :-) */
}
        .def {                                                   /** gros titre */
  font-family: serif;                                  /* On se permet un changement de police : pas plus de deux par page, et à utiliser avec parcimonie (c'est le cas) */
  background-color: blue;                              /* Fond du titre bleu (et sans image de fond) */
  color: white;                                          /*  Donc on utilise une police blanche pour que le texte soit visible */
  border: 3px dotted #9999FF;                            /* On encadre le titre d'un bleu clair de 3 pixels de largeur */
  padding: 0.3em;                                       /* Espacement intérieur non nul pour que le texte ne colle pas à la bordure du cadre */
   text-align: center;                                     /* Le titre doit être centré ! */
  letter-spacing: 0.7em;
    border-radius: 80px ;                                 /* On espace les caractères pour que ce soit joli :-) */
}
.droit {
  align: height;
  text-color: #000000;
  text-decoration: blue;
}
.entete {
  align: height;
  text-color: red;
  text-decoration: blue;
}
*{
  padding: 0;
  margin: 0;
  box-sizing: border-box;
}
.menu-bar{
  background: rgba(0,100,0);
  text-align: center;
}
.menu-bar ul{
  display: inline-flex;
  list-style: none;
  color: #fff;
}
.menu-bar ul li{
  width: 150px;
  margin: 5px;
  padding: 5px;
}
.menu-bar ul li a{
  text-decoration: none;
  color: #fff;
}
.active,.menu-bar ul li:hover{
  background: #2bab0d;
  border-radius: 3px;


}
.sub-menu-1{
  display: none;
}
.menu-bar ul li:hover .sub-menu-1{
  display: block;
  position: absolute;
  background: rgb(0, 100, 0);
  margin-top: 15px;
  margin-left: 15px;
}
.menu-bar ul li:hover .sub-menu-1 ul{
  display: block;
  margin: 10px;
}
.menu-bar ul li:hover .sub-menu-1 ul li{
  width: 150px;
  padding: 10px;
  border-bottom: 1px dotted #fff;
  background: transparent;
  border-radius: 0;
  text-align: left;
}
.menu-bar ul li:hover .sub-menu-1 ul li:last-child{
  border-bottom: none;
}
.menu-bar ul li:hover .sub-menu-1 ul li a:hover{
  color: #b2ff00;
}
.fa fa-angle-right{
  float: right;
}
.sub-menu-2{
  display: none;
}
.hover-me:hover .sub-menu-2{
  position: absolute;
  display: block;
  margin-top: -40px;
  margin-left: 140px;
  background: rgb(0, 100, 0);

}
.menu-bar .fa{

  margin-right: 8px;
}
.mino{
  text-color: green;
}
.menuuser{
  align: right;
}
</style>
<div class=\"container\">

<div id=\"navbare\">




<div class=\"container\">
  <table width=\"80%\"  >
    <tr>
      <td><a href=\"evenentaffich\" id=\"droit\"><i class=\"fa fa-archive\" aria-hidden=\"true\"></i>
       evenement</a> </td>

      <td><a href=\"\"><i class=\"fa fa-phone\" ></i>contacts</a> </td>

      <td><a href=\"inscription\"><i class=\"fa fa-graduation-cap\" aria-hidden=\"true\"></i>
        espace etudiants</a> </td>
        <td><a href=\"\"><i class=\"fa fa-wrench\" aria-hidden=\"true\"></i>
          connection</a> </td>
    </tr>

  </table>
  <br><br>


    <marquee direction=\"\" class=\"def\"><h2 > Departement d'Informatique et Telecommunication de l'Ecole Nationale Superieure Polytechnique de Maroua </h2></marquee>

</div>
</div>
<br><br><br><br><br><br>
<div class=\"menu-bar\">
  <ul>
      <li class=\"active\"><a href=\"/\"><i class=\"fa fa-home\" ></i>
            HOME</a></li>
      <li><a href=\"\"><i class=\"fa fa-edit\" ></i>FORMATION</a>
        <div class=\"sub-menu-1\">
          <ul>
            <li><a href=\"informatique\">INFORMATIQUE  <i class=\"fa fa-angle-right\"></i></a></li>
            <li><a href=\"telecom\">TELECOMMICATION<i class=\"fa fa-angle-right\"></i> </a></li>
          </ul>

        </div>
      </li>
      <li><a href=\"\"><i class=\"fa fa-clone\" ></i>L'ECOLE</a>
        <div class=\"sub-menu-1\">
          <ul>
            <li><a href=\"\"><i class=\"fa fa-angle-right\"></i>PRESENTATION</a> </li>
            <li><a href=\"\"> <i class=\"fa fa-angle-right\"></i>PROJETS ETUDIANTS</a></li>
            <li><a href=\"\"><i class=\"fa fa-angle-right\"></i> AVANTAGES ETUDIANTS</a></li>
          </ul>

        </div>
      </li>
      <li><a href=\"\"><i class=\"fa fa-folder-open\" aria-hidden=\"true\"></i>
          ADMISSION</a></li>
      <li><a href=\"evenentaffich\"><i class=\"fa fa-angellist\" ></i>EVENEMENT</a></li>
      <li><a href=\"\"><i class=\"fa fa-phone\" ></i>CONTACTS</a></li>
        <li><a href=\"\"><i class=\"fa fa-user\" ></i>CONNECTION</a>
          <div class=\"sub-menu-1\">
            <ul>
              <li class=\"hover-me\"><a href=\"inscription\"><i class=\"fa fa-angle-right\"></i>INSCRIPTION</a>

              </li>
              <li class=\"hover-me\"><a href=\"\"><i class=\"fa fa-angle-right\"></i>LOGIN</a>
                <div class=\"sub-menu-2\">
                  <ul>
                    <li>log</li>
                  </ul>

                </div>
              </li>
            </ul>

          </div>
        </li>
  </ul>
</div>


<br><br><br>



    <body>
        ";
        // line 270
        $this->displayBlock('body', $context, $blocks);
        // line 271
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 272
        echo "    </body>
</html>
";
        
        $__internal_d7f8c53bcd4b6fe71670e484d407f27fa3140cf015b0c61b990c19b9cbda4415->leave($__internal_d7f8c53bcd4b6fe71670e484d407f27fa3140cf015b0c61b990c19b9cbda4415_prof);

        
        $__internal_199c4bfe6ca2fc3a6f870b258369f4c4aba99becfd0c67d6c8cab7ece4a01530->leave($__internal_199c4bfe6ca2fc3a6f870b258369f4c4aba99becfd0c67d6c8cab7ece4a01530_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_177354bcb840ee8a76d219e5602778900c732e0430cc9b15fb90c3a29d562042 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_177354bcb840ee8a76d219e5602778900c732e0430cc9b15fb90c3a29d562042->enter($__internal_177354bcb840ee8a76d219e5602778900c732e0430cc9b15fb90c3a29d562042_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_a18c00b0a4dd30b4d3031ab6d2de0f728ba8d44cd023e831a3f5529c51ecf8c6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a18c00b0a4dd30b4d3031ab6d2de0f728ba8d44cd023e831a3f5529c51ecf8c6->enter($__internal_a18c00b0a4dd30b4d3031ab6d2de0f728ba8d44cd023e831a3f5529c51ecf8c6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_a18c00b0a4dd30b4d3031ab6d2de0f728ba8d44cd023e831a3f5529c51ecf8c6->leave($__internal_a18c00b0a4dd30b4d3031ab6d2de0f728ba8d44cd023e831a3f5529c51ecf8c6_prof);

        
        $__internal_177354bcb840ee8a76d219e5602778900c732e0430cc9b15fb90c3a29d562042->leave($__internal_177354bcb840ee8a76d219e5602778900c732e0430cc9b15fb90c3a29d562042_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_ca888c7e0c6d9fdac491e02cdfb44b85ba06680d3733cd6c07e87c1adb652ee6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ca888c7e0c6d9fdac491e02cdfb44b85ba06680d3733cd6c07e87c1adb652ee6->enter($__internal_ca888c7e0c6d9fdac491e02cdfb44b85ba06680d3733cd6c07e87c1adb652ee6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_9e4824a57adcf3d5b80da7f488bf3bc060e58973d647f15407e8b1cf79bfc5e6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9e4824a57adcf3d5b80da7f488bf3bc060e58973d647f15407e8b1cf79bfc5e6->enter($__internal_9e4824a57adcf3d5b80da7f488bf3bc060e58973d647f15407e8b1cf79bfc5e6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_9e4824a57adcf3d5b80da7f488bf3bc060e58973d647f15407e8b1cf79bfc5e6->leave($__internal_9e4824a57adcf3d5b80da7f488bf3bc060e58973d647f15407e8b1cf79bfc5e6_prof);

        
        $__internal_ca888c7e0c6d9fdac491e02cdfb44b85ba06680d3733cd6c07e87c1adb652ee6->leave($__internal_ca888c7e0c6d9fdac491e02cdfb44b85ba06680d3733cd6c07e87c1adb652ee6_prof);

    }

    // line 270
    public function block_body($context, array $blocks = array())
    {
        $__internal_f828402590a3bdf6b87d0b3b6bc454a91d9cff72a76102b1f52b6830d4675aee = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f828402590a3bdf6b87d0b3b6bc454a91d9cff72a76102b1f52b6830d4675aee->enter($__internal_f828402590a3bdf6b87d0b3b6bc454a91d9cff72a76102b1f52b6830d4675aee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_23489891543fd9d7ca9ff75535c96fe79a4face6a87531aa74c4f770ac965ff3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_23489891543fd9d7ca9ff75535c96fe79a4face6a87531aa74c4f770ac965ff3->enter($__internal_23489891543fd9d7ca9ff75535c96fe79a4face6a87531aa74c4f770ac965ff3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_23489891543fd9d7ca9ff75535c96fe79a4face6a87531aa74c4f770ac965ff3->leave($__internal_23489891543fd9d7ca9ff75535c96fe79a4face6a87531aa74c4f770ac965ff3_prof);

        
        $__internal_f828402590a3bdf6b87d0b3b6bc454a91d9cff72a76102b1f52b6830d4675aee->leave($__internal_f828402590a3bdf6b87d0b3b6bc454a91d9cff72a76102b1f52b6830d4675aee_prof);

    }

    // line 271
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_e27b9f9506180177b92ef086de75f24e6859fbc2c0b1ba7a6f858b682e2d37ba = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e27b9f9506180177b92ef086de75f24e6859fbc2c0b1ba7a6f858b682e2d37ba->enter($__internal_e27b9f9506180177b92ef086de75f24e6859fbc2c0b1ba7a6f858b682e2d37ba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_43a5a21f717af16b7ec7500e69eff398aaa43a45ae60191bbf1d969a4a0d799b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_43a5a21f717af16b7ec7500e69eff398aaa43a45ae60191bbf1d969a4a0d799b->enter($__internal_43a5a21f717af16b7ec7500e69eff398aaa43a45ae60191bbf1d969a4a0d799b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_43a5a21f717af16b7ec7500e69eff398aaa43a45ae60191bbf1d969a4a0d799b->leave($__internal_43a5a21f717af16b7ec7500e69eff398aaa43a45ae60191bbf1d969a4a0d799b_prof);

        
        $__internal_e27b9f9506180177b92ef086de75f24e6859fbc2c0b1ba7a6f858b682e2d37ba->leave($__internal_e27b9f9506180177b92ef086de75f24e6859fbc2c0b1ba7a6f858b682e2d37ba_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  386 => 271,  369 => 270,  352 => 6,  334 => 5,  322 => 272,  319 => 271,  317 => 270,  55 => 11,  50 => 9,  46 => 8,  41 => 7,  39 => 6,  35 => 5,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
       <link  href=\"{{ asset('bootstrap/css/bootstrap.min.css') }}\" rel=\"stylesheet\" media=\"screen\" />
       <link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.8.2/css/all.css\" integrity=\"sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay\" crossorigin=\"anonymous\">
      <script src=\"{{ asset('bootstrap/js/bootstrap.min.js') }}\"></script>
    </head>
    <style>
    body{
      padding-top: 10px;
    }
    #navbare{
      width: 90%;
      height: 70px;
      color: white;

    }
    #toto{
      text-color: white;
    }
    #nav{


      width: 90%;
    }
    #linkref{
      color: white;
      text-decoration: blue;
      padding: 15px;
      top: 0;
    }
    .slider{
        width: 500px;
        height: 300px;
        overflow: hidden;
        margin: 130px auto;
    }
    .slides{
    width: calc(500px * 3);
    animation: glisse 12s infinite;
     }
    .slide{
        float: left;
     }
     @keyframes glisse{
        0%{
            transform: translateX(0);
        }
        10%{
            transform: translateX(0);
        }
         33%{
            transform: translateX(-5000);
        }
        43%{
           transform: translateX(-5000);
       }
         66%{
            transform: translateX(-1000px);
        }
        76%{
           transform: translateX(-1000px);
       }
         100%{
            transform: translateX(0);
        }

     }
.def {                                                   /** gros titre */
  font-family: serif;                                  /* On se permet un changement de police : pas plus de deux par page, et à utiliser avec parcimonie (c'est le cas) */
  background-color: blue;                              /* Fond du titre bleu (et sans image de fond) */
  color: white;                                          /*  Donc on utilise une police blanche pour que le texte soit visible */
  border: 3px dotted #9999FF;                            /* On encadre le titre d'un bleu clair de 3 pixels de largeur */
  padding: 0.3em;                                       /* Espacement intérieur non nul pour que le texte ne colle pas à la bordure du cadre */
   text-align: center;                                     /* Le titre doit être centré ! */
  letter-spacing: 0.7em;
    border-radius: 80px ;                                 /* On espace les caractères pour que ce soit joli :-) */
}
        .def {                                                   /** gros titre */
  font-family: serif;                                  /* On se permet un changement de police : pas plus de deux par page, et à utiliser avec parcimonie (c'est le cas) */
  background-color: blue;                              /* Fond du titre bleu (et sans image de fond) */
  color: white;                                          /*  Donc on utilise une police blanche pour que le texte soit visible */
  border: 3px dotted #9999FF;                            /* On encadre le titre d'un bleu clair de 3 pixels de largeur */
  padding: 0.3em;                                       /* Espacement intérieur non nul pour que le texte ne colle pas à la bordure du cadre */
   text-align: center;                                     /* Le titre doit être centré ! */
  letter-spacing: 0.7em;
    border-radius: 80px ;                                 /* On espace les caractères pour que ce soit joli :-) */
}
.droit {
  align: height;
  text-color: #000000;
  text-decoration: blue;
}
.entete {
  align: height;
  text-color: red;
  text-decoration: blue;
}
*{
  padding: 0;
  margin: 0;
  box-sizing: border-box;
}
.menu-bar{
  background: rgba(0,100,0);
  text-align: center;
}
.menu-bar ul{
  display: inline-flex;
  list-style: none;
  color: #fff;
}
.menu-bar ul li{
  width: 150px;
  margin: 5px;
  padding: 5px;
}
.menu-bar ul li a{
  text-decoration: none;
  color: #fff;
}
.active,.menu-bar ul li:hover{
  background: #2bab0d;
  border-radius: 3px;


}
.sub-menu-1{
  display: none;
}
.menu-bar ul li:hover .sub-menu-1{
  display: block;
  position: absolute;
  background: rgb(0, 100, 0);
  margin-top: 15px;
  margin-left: 15px;
}
.menu-bar ul li:hover .sub-menu-1 ul{
  display: block;
  margin: 10px;
}
.menu-bar ul li:hover .sub-menu-1 ul li{
  width: 150px;
  padding: 10px;
  border-bottom: 1px dotted #fff;
  background: transparent;
  border-radius: 0;
  text-align: left;
}
.menu-bar ul li:hover .sub-menu-1 ul li:last-child{
  border-bottom: none;
}
.menu-bar ul li:hover .sub-menu-1 ul li a:hover{
  color: #b2ff00;
}
.fa fa-angle-right{
  float: right;
}
.sub-menu-2{
  display: none;
}
.hover-me:hover .sub-menu-2{
  position: absolute;
  display: block;
  margin-top: -40px;
  margin-left: 140px;
  background: rgb(0, 100, 0);

}
.menu-bar .fa{

  margin-right: 8px;
}
.mino{
  text-color: green;
}
.menuuser{
  align: right;
}
</style>
<div class=\"container\">

<div id=\"navbare\">




<div class=\"container\">
  <table width=\"80%\"  >
    <tr>
      <td><a href=\"evenentaffich\" id=\"droit\"><i class=\"fa fa-archive\" aria-hidden=\"true\"></i>
       evenement</a> </td>

      <td><a href=\"\"><i class=\"fa fa-phone\" ></i>contacts</a> </td>

      <td><a href=\"inscription\"><i class=\"fa fa-graduation-cap\" aria-hidden=\"true\"></i>
        espace etudiants</a> </td>
        <td><a href=\"\"><i class=\"fa fa-wrench\" aria-hidden=\"true\"></i>
          connection</a> </td>
    </tr>

  </table>
  <br><br>


    <marquee direction=\"\" class=\"def\"><h2 > Departement d'Informatique et Telecommunication de l'Ecole Nationale Superieure Polytechnique de Maroua </h2></marquee>

</div>
</div>
<br><br><br><br><br><br>
<div class=\"menu-bar\">
  <ul>
      <li class=\"active\"><a href=\"/\"><i class=\"fa fa-home\" ></i>
            HOME</a></li>
      <li><a href=\"\"><i class=\"fa fa-edit\" ></i>FORMATION</a>
        <div class=\"sub-menu-1\">
          <ul>
            <li><a href=\"informatique\">INFORMATIQUE  <i class=\"fa fa-angle-right\"></i></a></li>
            <li><a href=\"telecom\">TELECOMMICATION<i class=\"fa fa-angle-right\"></i> </a></li>
          </ul>

        </div>
      </li>
      <li><a href=\"\"><i class=\"fa fa-clone\" ></i>L'ECOLE</a>
        <div class=\"sub-menu-1\">
          <ul>
            <li><a href=\"\"><i class=\"fa fa-angle-right\"></i>PRESENTATION</a> </li>
            <li><a href=\"\"> <i class=\"fa fa-angle-right\"></i>PROJETS ETUDIANTS</a></li>
            <li><a href=\"\"><i class=\"fa fa-angle-right\"></i> AVANTAGES ETUDIANTS</a></li>
          </ul>

        </div>
      </li>
      <li><a href=\"\"><i class=\"fa fa-folder-open\" aria-hidden=\"true\"></i>
          ADMISSION</a></li>
      <li><a href=\"evenentaffich\"><i class=\"fa fa-angellist\" ></i>EVENEMENT</a></li>
      <li><a href=\"\"><i class=\"fa fa-phone\" ></i>CONTACTS</a></li>
        <li><a href=\"\"><i class=\"fa fa-user\" ></i>CONNECTION</a>
          <div class=\"sub-menu-1\">
            <ul>
              <li class=\"hover-me\"><a href=\"inscription\"><i class=\"fa fa-angle-right\"></i>INSCRIPTION</a>

              </li>
              <li class=\"hover-me\"><a href=\"\"><i class=\"fa fa-angle-right\"></i>LOGIN</a>
                <div class=\"sub-menu-2\">
                  <ul>
                    <li>log</li>
                  </ul>

                </div>
              </li>
            </ul>

          </div>
        </li>
  </ul>
</div>


<br><br><br>



    <body>
        {% block body %}{% endblock %}
        {% block javascripts %}{% endblock %}
    </body>
</html>
", "base.html.twig", "/home/fonguen/symfony projet/infotels/app/Resources/views/base.html.twig");
    }
}
