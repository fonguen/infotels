<?php

/* @Twig/Exception/error.txt.twig */
class __TwigTemplate_91050d0e7a0b38026821d8954a4f1b1bc4d27eebd1509f94dcb02f90e6f3ae3e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ab80b82c1bb8b5b179c9e4255c334ca761e58a5b72b6bba917c5609c6dc0756c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ab80b82c1bb8b5b179c9e4255c334ca761e58a5b72b6bba917c5609c6dc0756c->enter($__internal_ab80b82c1bb8b5b179c9e4255c334ca761e58a5b72b6bba917c5609c6dc0756c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.txt.twig"));

        $__internal_bb775749e58b1f1d151a2ffdacd1ece53eee7119b4a304d6fd1c729023b4eddf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bb775749e58b1f1d151a2ffdacd1ece53eee7119b4a304d6fd1c729023b4eddf->enter($__internal_bb775749e58b1f1d151a2ffdacd1ece53eee7119b4a304d6fd1c729023b4eddf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.txt.twig"));

        // line 1
        echo "Oops! An Error Occurred
=======================

The server returned a \"";
        // line 4
        echo ($context["status_code"] ?? $this->getContext($context, "status_code"));
        echo " ";
        echo ($context["status_text"] ?? $this->getContext($context, "status_text"));
        echo "\".

Something is broken. Please let us know what you were doing when this error occurred.
We will fix it as soon as possible. Sorry for any inconvenience caused.
";
        
        $__internal_ab80b82c1bb8b5b179c9e4255c334ca761e58a5b72b6bba917c5609c6dc0756c->leave($__internal_ab80b82c1bb8b5b179c9e4255c334ca761e58a5b72b6bba917c5609c6dc0756c_prof);

        
        $__internal_bb775749e58b1f1d151a2ffdacd1ece53eee7119b4a304d6fd1c729023b4eddf->leave($__internal_bb775749e58b1f1d151a2ffdacd1ece53eee7119b4a304d6fd1c729023b4eddf_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.txt.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 4,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("Oops! An Error Occurred
=======================

The server returned a \"{{ status_code }} {{ status_text }}\".

Something is broken. Please let us know what you were doing when this error occurred.
We will fix it as soon as possible. Sorry for any inconvenience caused.
", "@Twig/Exception/error.txt.twig", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.txt.twig");
    }
}
