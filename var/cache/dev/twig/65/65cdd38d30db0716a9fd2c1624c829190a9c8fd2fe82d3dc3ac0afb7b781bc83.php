<?php

/* @Twig/Exception/error.css.twig */
class __TwigTemplate_9ce689715c31a5ac1cdcd79ce34e43eee6647123941038ecaa433f60d2f04b4e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_553886c6188dd33dd5cc8693de4ad92fb421c9f22965668e826675614aa2cbc4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_553886c6188dd33dd5cc8693de4ad92fb421c9f22965668e826675614aa2cbc4->enter($__internal_553886c6188dd33dd5cc8693de4ad92fb421c9f22965668e826675614aa2cbc4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.css.twig"));

        $__internal_6302c8b3fe185f5092addc83347e173b93bed6ec443ed6eaa5ba7d66e562928c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6302c8b3fe185f5092addc83347e173b93bed6ec443ed6eaa5ba7d66e562928c->enter($__internal_6302c8b3fe185f5092addc83347e173b93bed6ec443ed6eaa5ba7d66e562928c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, ($context["status_code"] ?? $this->getContext($context, "status_code")), "css", null, true);
        echo " ";
        echo twig_escape_filter($this->env, ($context["status_text"] ?? $this->getContext($context, "status_text")), "css", null, true);
        echo "

*/
";
        
        $__internal_553886c6188dd33dd5cc8693de4ad92fb421c9f22965668e826675614aa2cbc4->leave($__internal_553886c6188dd33dd5cc8693de4ad92fb421c9f22965668e826675614aa2cbc4_prof);

        
        $__internal_6302c8b3fe185f5092addc83347e173b93bed6ec443ed6eaa5ba7d66e562928c->leave($__internal_6302c8b3fe185f5092addc83347e173b93bed6ec443ed6eaa5ba7d66e562928c_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{{ status_code }} {{ status_text }}

*/
", "@Twig/Exception/error.css.twig", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.css.twig");
    }
}
