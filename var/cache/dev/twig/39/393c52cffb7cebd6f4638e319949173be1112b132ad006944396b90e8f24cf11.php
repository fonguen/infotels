<?php

/* @Framework/Form/url_widget.html.php */
class __TwigTemplate_d0965048a9a1618097e1af952a0f850a5ce7f8fe1c9861407c22bc630efaee1c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_26e28e75c78453f9d6c58124e7213ff4fb5e3a8d87ec5727f038a29c0cce6d9c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_26e28e75c78453f9d6c58124e7213ff4fb5e3a8d87ec5727f038a29c0cce6d9c->enter($__internal_26e28e75c78453f9d6c58124e7213ff4fb5e3a8d87ec5727f038a29c0cce6d9c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/url_widget.html.php"));

        $__internal_b4e2d41d40598cac2eb7ffc045966ed09a49db7d0640478234204b17c218950a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b4e2d41d40598cac2eb7ffc045966ed09a49db7d0640478234204b17c218950a->enter($__internal_b4e2d41d40598cac2eb7ffc045966ed09a49db7d0640478234204b17c218950a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/url_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'url')) ?>
";
        
        $__internal_26e28e75c78453f9d6c58124e7213ff4fb5e3a8d87ec5727f038a29c0cce6d9c->leave($__internal_26e28e75c78453f9d6c58124e7213ff4fb5e3a8d87ec5727f038a29c0cce6d9c_prof);

        
        $__internal_b4e2d41d40598cac2eb7ffc045966ed09a49db7d0640478234204b17c218950a->leave($__internal_b4e2d41d40598cac2eb7ffc045966ed09a49db7d0640478234204b17c218950a_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/url_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'url')) ?>
", "@Framework/Form/url_widget.html.php", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/url_widget.html.php");
    }
}
