<?php

/* @Framework/Form/form_rows.html.php */
class __TwigTemplate_183b2e1bcb785f8d5720f456108b90cdbb54ed4b38d3f13a32c1b47a4a606540 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6faff95f463d9e56bbd53cd2c4f2a735b41714d84ebe5e62134292a6d02f8dfe = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6faff95f463d9e56bbd53cd2c4f2a735b41714d84ebe5e62134292a6d02f8dfe->enter($__internal_6faff95f463d9e56bbd53cd2c4f2a735b41714d84ebe5e62134292a6d02f8dfe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rows.html.php"));

        $__internal_4ee5a2dc7567b378a7e352944f5ffeb2fc444b237868fc588fbf74684ee29663 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4ee5a2dc7567b378a7e352944f5ffeb2fc444b237868fc588fbf74684ee29663->enter($__internal_4ee5a2dc7567b378a7e352944f5ffeb2fc444b237868fc588fbf74684ee29663_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rows.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child) : ?>
    <?php echo \$view['form']->row(\$child) ?>
<?php endforeach; ?>
";
        
        $__internal_6faff95f463d9e56bbd53cd2c4f2a735b41714d84ebe5e62134292a6d02f8dfe->leave($__internal_6faff95f463d9e56bbd53cd2c4f2a735b41714d84ebe5e62134292a6d02f8dfe_prof);

        
        $__internal_4ee5a2dc7567b378a7e352944f5ffeb2fc444b237868fc588fbf74684ee29663->leave($__internal_4ee5a2dc7567b378a7e352944f5ffeb2fc444b237868fc588fbf74684ee29663_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rows.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php foreach (\$form as \$child) : ?>
    <?php echo \$view['form']->row(\$child) ?>
<?php endforeach; ?>
", "@Framework/Form/form_rows.html.php", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_rows.html.php");
    }
}
