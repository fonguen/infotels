<?php

/* user/show.html.twig */
class __TwigTemplate_62eeabb57fca7689a5881e8cf10ff9be7124e6184bbeec2a30cacf61b4fa8dc1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "user/show.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2697d920994ea778f184f2116ab62b4256734cbe248d8015a411cd7ee680a6c9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2697d920994ea778f184f2116ab62b4256734cbe248d8015a411cd7ee680a6c9->enter($__internal_2697d920994ea778f184f2116ab62b4256734cbe248d8015a411cd7ee680a6c9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "user/show.html.twig"));

        $__internal_c631b3de7f2e0164be25659b2590b6ab38047fa45b99b88c4c53e58e033f457d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c631b3de7f2e0164be25659b2590b6ab38047fa45b99b88c4c53e58e033f457d->enter($__internal_c631b3de7f2e0164be25659b2590b6ab38047fa45b99b88c4c53e58e033f457d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "user/show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2697d920994ea778f184f2116ab62b4256734cbe248d8015a411cd7ee680a6c9->leave($__internal_2697d920994ea778f184f2116ab62b4256734cbe248d8015a411cd7ee680a6c9_prof);

        
        $__internal_c631b3de7f2e0164be25659b2590b6ab38047fa45b99b88c4c53e58e033f457d->leave($__internal_c631b3de7f2e0164be25659b2590b6ab38047fa45b99b88c4c53e58e033f457d_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_dead2041065fb657e934a2485adc7597c77baa26ae10d984439fc917c4f872cf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dead2041065fb657e934a2485adc7597c77baa26ae10d984439fc917c4f872cf->enter($__internal_dead2041065fb657e934a2485adc7597c77baa26ae10d984439fc917c4f872cf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_61fdb1ff3497df5447a1f00971a0c1da3d4b1c02366171d2d2ddc5397e2875d3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_61fdb1ff3497df5447a1f00971a0c1da3d4b1c02366171d2d2ddc5397e2875d3->enter($__internal_61fdb1ff3497df5447a1f00971a0c1da3d4b1c02366171d2d2ddc5397e2875d3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<div class=\"container\">


    <h1>User</h1>

    <table class=\"table\">
        <tbody>
            <tr>
                <th>Id</th>
                <td>";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "id", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Nom</th>
                <td>";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array()), "html", null, true);
        echo "</td>
            </tr>

            <tr>
                <th>Email</th>
                <td>";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "email", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Password</th>
                <td>";
        // line 26
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "password", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Role</th>
                <td>";
        // line 30
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "role", array()), "html", null, true);
        echo "</td>
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"";
        // line 37
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_user_index");
        echo "\" class=\"btn btn-success\">Back to the list</a>
        </li>
        <li>
            <a href=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_user_edit", array("id" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "id", array()))), "html", null, true);
        echo "\" class=\"btn btn-warning\" >Edit</a>
        </li>
        <li>
            ";
        // line 43
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_start');
        echo "
                <input type=\"submit\" value=\"Delete\" class=\"btn btn-danger\">
            ";
        // line 45
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_end');
        echo "
        </li>
    </ul>
</div>
";
        
        $__internal_61fdb1ff3497df5447a1f00971a0c1da3d4b1c02366171d2d2ddc5397e2875d3->leave($__internal_61fdb1ff3497df5447a1f00971a0c1da3d4b1c02366171d2d2ddc5397e2875d3_prof);

        
        $__internal_dead2041065fb657e934a2485adc7597c77baa26ae10d984439fc917c4f872cf->leave($__internal_dead2041065fb657e934a2485adc7597c77baa26ae10d984439fc917c4f872cf_prof);

    }

    public function getTemplateName()
    {
        return "user/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  116 => 45,  111 => 43,  105 => 40,  99 => 37,  89 => 30,  82 => 26,  75 => 22,  67 => 17,  60 => 13,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
<div class=\"container\">


    <h1>User</h1>

    <table class=\"table\">
        <tbody>
            <tr>
                <th>Id</th>
                <td>{{ user.id }}</td>
            </tr>
            <tr>
                <th>Nom</th>
                <td>{{ user.username }}</td>
            </tr>

            <tr>
                <th>Email</th>
                <td>{{ user.email }}</td>
            </tr>
            <tr>
                <th>Password</th>
                <td>{{ user.password }}</td>
            </tr>
            <tr>
                <th>Role</th>
                <td>{{ user.role }}</td>
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"{{ path('admin_user_index') }}\" class=\"btn btn-success\">Back to the list</a>
        </li>
        <li>
            <a href=\"{{ path('admin_user_edit', { 'id': user.id }) }}\" class=\"btn btn-warning\" >Edit</a>
        </li>
        <li>
            {{ form_start(delete_form) }}
                <input type=\"submit\" value=\"Delete\" class=\"btn btn-danger\">
            {{ form_end(delete_form) }}
        </li>
    </ul>
</div>
{% endblock %}
", "user/show.html.twig", "/home/fonguen/symfony projet/infotels/app/Resources/views/user/show.html.twig");
    }
}
