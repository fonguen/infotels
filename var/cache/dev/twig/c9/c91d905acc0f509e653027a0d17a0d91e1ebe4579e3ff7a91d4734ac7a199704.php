<?php

/* @Security/Collector/icon.svg */
class __TwigTemplate_65cca1ebc97b79e3614d71e36933505b4b7f9cd6c7ea19b12b47e3972de38f2a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c47c47f28aa7b89d451882e773e567e8c2517a2a6ccd3fc3c59fd1f77287f8db = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c47c47f28aa7b89d451882e773e567e8c2517a2a6ccd3fc3c59fd1f77287f8db->enter($__internal_c47c47f28aa7b89d451882e773e567e8c2517a2a6ccd3fc3c59fd1f77287f8db_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Security/Collector/icon.svg"));

        $__internal_4ec614179c3d61d35219ed529b12afb27d9c333a58bd3600437c63adce23b44e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4ec614179c3d61d35219ed529b12afb27d9c333a58bd3600437c63adce23b44e->enter($__internal_4ec614179c3d61d35219ed529b12afb27d9c333a58bd3600437c63adce23b44e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Security/Collector/icon.svg"));

        // line 1
        echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
    <path fill=\"#AAAAAA\" d=\"M21,20.4V22H3v-1.6c0-3.7,2.4-6.9,5.8-8c-1.7-1.1-2.9-3-2.9-5.2c0-3.4,2.7-6.1,6.1-6.1s6.1,2.7,6.1,6.1c0,2.2-1.2,4.1-2.9,5.2C18.6,13.5,21,16.7,21,20.4z\"/>
</svg>
";
        
        $__internal_c47c47f28aa7b89d451882e773e567e8c2517a2a6ccd3fc3c59fd1f77287f8db->leave($__internal_c47c47f28aa7b89d451882e773e567e8c2517a2a6ccd3fc3c59fd1f77287f8db_prof);

        
        $__internal_4ec614179c3d61d35219ed529b12afb27d9c333a58bd3600437c63adce23b44e->leave($__internal_4ec614179c3d61d35219ed529b12afb27d9c333a58bd3600437c63adce23b44e_prof);

    }

    public function getTemplateName()
    {
        return "@Security/Collector/icon.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
    <path fill=\"#AAAAAA\" d=\"M21,20.4V22H3v-1.6c0-3.7,2.4-6.9,5.8-8c-1.7-1.1-2.9-3-2.9-5.2c0-3.4,2.7-6.1,6.1-6.1s6.1,2.7,6.1,6.1c0,2.2-1.2,4.1-2.9,5.2C18.6,13.5,21,16.7,21,20.4z\"/>
</svg>
", "@Security/Collector/icon.svg", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/SecurityBundle/Resources/views/Collector/icon.svg");
    }
}
