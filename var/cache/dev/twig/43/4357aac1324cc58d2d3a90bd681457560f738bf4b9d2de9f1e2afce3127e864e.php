<?php

/* @Framework/FormTable/form_row.html.php */
class __TwigTemplate_a7e96a497b272c1bd7461823bbcac4e5c27d0a3c5a0b37da00aa4d4ce5bed34c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d90874685b35c40c515fc6796b68cb58f7d4d8157e6f57e88d91777de4b29af0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d90874685b35c40c515fc6796b68cb58f7d4d8157e6f57e88d91777de4b29af0->enter($__internal_d90874685b35c40c515fc6796b68cb58f7d4d8157e6f57e88d91777de4b29af0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_row.html.php"));

        $__internal_adf8d4b2656745aac4654394c1d6a374d8d2e1b65064eff0e82770508e2c7e0a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_adf8d4b2656745aac4654394c1d6a374d8d2e1b65064eff0e82770508e2c7e0a->enter($__internal_adf8d4b2656745aac4654394c1d6a374d8d2e1b65064eff0e82770508e2c7e0a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_row.html.php"));

        // line 1
        echo "<tr>
    <td>
        <?php echo \$view['form']->label(\$form) ?>
    </td>
    <td>
        <?php echo \$view['form']->errors(\$form) ?>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_d90874685b35c40c515fc6796b68cb58f7d4d8157e6f57e88d91777de4b29af0->leave($__internal_d90874685b35c40c515fc6796b68cb58f7d4d8157e6f57e88d91777de4b29af0_prof);

        
        $__internal_adf8d4b2656745aac4654394c1d6a374d8d2e1b65064eff0e82770508e2c7e0a->leave($__internal_adf8d4b2656745aac4654394c1d6a374d8d2e1b65064eff0e82770508e2c7e0a_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<tr>
    <td>
        <?php echo \$view['form']->label(\$form) ?>
    </td>
    <td>
        <?php echo \$view['form']->errors(\$form) ?>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
", "@Framework/FormTable/form_row.html.php", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/form_row.html.php");
    }
}
