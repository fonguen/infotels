<?php

/* @Twig/Exception/error.json.twig */
class __TwigTemplate_9d8f9d27a2ac3f5940670e8667ab586eeacf1fa33fabcfab3a6b0de46e613a01 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e4dcf90f523f6c803e461d99a23263e4ba9fc8f98429ed297307354c47839ec8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e4dcf90f523f6c803e461d99a23263e4ba9fc8f98429ed297307354c47839ec8->enter($__internal_e4dcf90f523f6c803e461d99a23263e4ba9fc8f98429ed297307354c47839ec8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.json.twig"));

        $__internal_158355b0f69066fe54716412e731adcc18002a94171c0dbe2c4acc8e92fe5056 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_158355b0f69066fe54716412e731adcc18002a94171c0dbe2c4acc8e92fe5056->enter($__internal_158355b0f69066fe54716412e731adcc18002a94171c0dbe2c4acc8e92fe5056_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => ($context["status_code"] ?? $this->getContext($context, "status_code")), "message" => ($context["status_text"] ?? $this->getContext($context, "status_text")))));
        echo "
";
        
        $__internal_e4dcf90f523f6c803e461d99a23263e4ba9fc8f98429ed297307354c47839ec8->leave($__internal_e4dcf90f523f6c803e461d99a23263e4ba9fc8f98429ed297307354c47839ec8_prof);

        
        $__internal_158355b0f69066fe54716412e731adcc18002a94171c0dbe2c4acc8e92fe5056->leave($__internal_158355b0f69066fe54716412e731adcc18002a94171c0dbe2c4acc8e92fe5056_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ { 'error': { 'code': status_code, 'message': status_text } }|json_encode|raw }}
", "@Twig/Exception/error.json.twig", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.json.twig");
    }
}
