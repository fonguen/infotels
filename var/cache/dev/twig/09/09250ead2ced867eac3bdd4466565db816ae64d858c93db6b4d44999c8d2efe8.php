<?php

/* formation/show.html.twig */
class __TwigTemplate_8921d7ad8de31478c0dfb4239fb1790245cedf630e31d1e9a88f4769806313fe extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "formation/show.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fe2514b592d91c44dc5328b9bfc2d431608ef7684b29dca834926d30ead94a0a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fe2514b592d91c44dc5328b9bfc2d431608ef7684b29dca834926d30ead94a0a->enter($__internal_fe2514b592d91c44dc5328b9bfc2d431608ef7684b29dca834926d30ead94a0a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "formation/show.html.twig"));

        $__internal_187b46fd954b9dfafb080378098a181077d8a91b8b889015fc10d9f27ce3a68f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_187b46fd954b9dfafb080378098a181077d8a91b8b889015fc10d9f27ce3a68f->enter($__internal_187b46fd954b9dfafb080378098a181077d8a91b8b889015fc10d9f27ce3a68f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "formation/show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_fe2514b592d91c44dc5328b9bfc2d431608ef7684b29dca834926d30ead94a0a->leave($__internal_fe2514b592d91c44dc5328b9bfc2d431608ef7684b29dca834926d30ead94a0a_prof);

        
        $__internal_187b46fd954b9dfafb080378098a181077d8a91b8b889015fc10d9f27ce3a68f->leave($__internal_187b46fd954b9dfafb080378098a181077d8a91b8b889015fc10d9f27ce3a68f_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_c915c173e4699a4abb6f84395f0a356a0b315a3842e1edfa1f51597b9b39a7d1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c915c173e4699a4abb6f84395f0a356a0b315a3842e1edfa1f51597b9b39a7d1->enter($__internal_c915c173e4699a4abb6f84395f0a356a0b315a3842e1edfa1f51597b9b39a7d1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_1908e5238911b68d5471192eb1167ececa4a530b5bf77e00207ec398fb101e50 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1908e5238911b68d5471192eb1167ececa4a530b5bf77e00207ec398fb101e50->enter($__internal_1908e5238911b68d5471192eb1167ececa4a530b5bf77e00207ec398fb101e50_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<br><br><br>
    <h1>Formation</h1>

    <table>
        <tbody>
            <tr>
                <th>Id</th>
                <td>";
        // line 11
        echo twig_escape_filter($this->env, $this->getAttribute(($context["formation"] ?? $this->getContext($context, "formation")), "id", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Nom</th>
                <td>";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute(($context["formation"] ?? $this->getContext($context, "formation")), "nom", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Description</th>
                <td>";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute(($context["formation"] ?? $this->getContext($context, "formation")), "description", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Path</th>
                <td>";
        // line 23
        echo twig_escape_filter($this->env, $this->getAttribute(($context["formation"] ?? $this->getContext($context, "formation")), "path", array()), "html", null, true);
        echo "</td>
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"";
        // line 30
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_formation_index");
        echo "\">Back to the list</a>
        </li>
        <li>
            <a href=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_formation_edit", array("id" => $this->getAttribute(($context["formation"] ?? $this->getContext($context, "formation")), "id", array()))), "html", null, true);
        echo "\">Edit</a>
        </li>
        <li>
            ";
        // line 36
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_start');
        echo "
                <input type=\"submit\" value=\"Delete\">
            ";
        // line 38
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_end');
        echo "
        </li>
    </ul>
";
        
        $__internal_1908e5238911b68d5471192eb1167ececa4a530b5bf77e00207ec398fb101e50->leave($__internal_1908e5238911b68d5471192eb1167ececa4a530b5bf77e00207ec398fb101e50_prof);

        
        $__internal_c915c173e4699a4abb6f84395f0a356a0b315a3842e1edfa1f51597b9b39a7d1->leave($__internal_c915c173e4699a4abb6f84395f0a356a0b315a3842e1edfa1f51597b9b39a7d1_prof);

    }

    public function getTemplateName()
    {
        return "formation/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  106 => 38,  101 => 36,  95 => 33,  89 => 30,  79 => 23,  72 => 19,  65 => 15,  58 => 11,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
<br><br><br>
    <h1>Formation</h1>

    <table>
        <tbody>
            <tr>
                <th>Id</th>
                <td>{{ formation.id }}</td>
            </tr>
            <tr>
                <th>Nom</th>
                <td>{{ formation.nom }}</td>
            </tr>
            <tr>
                <th>Description</th>
                <td>{{ formation.description }}</td>
            </tr>
            <tr>
                <th>Path</th>
                <td>{{ formation.path }}</td>
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"{{ path('admin_formation_index') }}\">Back to the list</a>
        </li>
        <li>
            <a href=\"{{ path('admin_formation_edit', { 'id': formation.id }) }}\">Edit</a>
        </li>
        <li>
            {{ form_start(delete_form) }}
                <input type=\"submit\" value=\"Delete\">
            {{ form_end(delete_form) }}
        </li>
    </ul>
{% endblock %}
", "formation/show.html.twig", "/home/fonguen/symfony projet/infotels/app/Resources/views/formation/show.html.twig");
    }
}
