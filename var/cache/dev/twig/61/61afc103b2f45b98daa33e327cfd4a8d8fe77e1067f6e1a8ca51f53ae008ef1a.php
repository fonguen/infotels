<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_1e3201730528ee6c71c43dfd73b6cb8f7fe77508eff4dc89f5ac51b0000ff837 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9a82a40e96f069623f2142a3f93150d548e757811671513775a4ecddd614f06e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9a82a40e96f069623f2142a3f93150d548e757811671513775a4ecddd614f06e->enter($__internal_9a82a40e96f069623f2142a3f93150d548e757811671513775a4ecddd614f06e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $__internal_e75f2cb43b03d2406f2a49c920ba058ed9a05021af9c4d14d7eeab2c68f3ffb0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e75f2cb43b03d2406f2a49c920ba058ed9a05021af9c4d14d7eeab2c68f3ffb0->enter($__internal_e75f2cb43b03d2406f2a49c920ba058ed9a05021af9c4d14d7eeab2c68f3ffb0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9a82a40e96f069623f2142a3f93150d548e757811671513775a4ecddd614f06e->leave($__internal_9a82a40e96f069623f2142a3f93150d548e757811671513775a4ecddd614f06e_prof);

        
        $__internal_e75f2cb43b03d2406f2a49c920ba058ed9a05021af9c4d14d7eeab2c68f3ffb0->leave($__internal_e75f2cb43b03d2406f2a49c920ba058ed9a05021af9c4d14d7eeab2c68f3ffb0_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_f70430353c168ad0f7c82b07c45766abe03604f48b3d1f89da1674c3c438099c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f70430353c168ad0f7c82b07c45766abe03604f48b3d1f89da1674c3c438099c->enter($__internal_f70430353c168ad0f7c82b07c45766abe03604f48b3d1f89da1674c3c438099c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_d329566dfcf273a6b65f3140f91adcfbdca040f107ef8c122909427976629ecb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d329566dfcf273a6b65f3140f91adcfbdca040f107ef8c122909427976629ecb->enter($__internal_d329566dfcf273a6b65f3140f91adcfbdca040f107ef8c122909427976629ecb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
";
        
        $__internal_d329566dfcf273a6b65f3140f91adcfbdca040f107ef8c122909427976629ecb->leave($__internal_d329566dfcf273a6b65f3140f91adcfbdca040f107ef8c122909427976629ecb_prof);

        
        $__internal_f70430353c168ad0f7c82b07c45766abe03604f48b3d1f89da1674c3c438099c->leave($__internal_f70430353c168ad0f7c82b07c45766abe03604f48b3d1f89da1674c3c438099c_prof);

    }

    // line 136
    public function block_title($context, array $blocks = array())
    {
        $__internal_76b4e7875165d0bf991956f730717f14e3f209f51440cb61c7732bcc4e6bc145 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_76b4e7875165d0bf991956f730717f14e3f209f51440cb61c7732bcc4e6bc145->enter($__internal_76b4e7875165d0bf991956f730717f14e3f209f51440cb61c7732bcc4e6bc145_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_9704b1533be8dcb87cd72966ca1adde29c9f28ad7deccd3eedc31977d48679df = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9704b1533be8dcb87cd72966ca1adde29c9f28ad7deccd3eedc31977d48679df->enter($__internal_9704b1533be8dcb87cd72966ca1adde29c9f28ad7deccd3eedc31977d48679df_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 137
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, ($context["status_code"] ?? $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, ($context["status_text"] ?? $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_9704b1533be8dcb87cd72966ca1adde29c9f28ad7deccd3eedc31977d48679df->leave($__internal_9704b1533be8dcb87cd72966ca1adde29c9f28ad7deccd3eedc31977d48679df_prof);

        
        $__internal_76b4e7875165d0bf991956f730717f14e3f209f51440cb61c7732bcc4e6bc145->leave($__internal_76b4e7875165d0bf991956f730717f14e3f209f51440cb61c7732bcc4e6bc145_prof);

    }

    // line 140
    public function block_body($context, array $blocks = array())
    {
        $__internal_a3c0c037ecfea9c3d67eaac75fa8d55aa620c14ba5298ba817cfccb2c9046ba5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a3c0c037ecfea9c3d67eaac75fa8d55aa620c14ba5298ba817cfccb2c9046ba5->enter($__internal_a3c0c037ecfea9c3d67eaac75fa8d55aa620c14ba5298ba817cfccb2c9046ba5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_a44698f6344716543f35f77ad7642131cd9d7689724d9c516238a397e0e5f0a7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a44698f6344716543f35f77ad7642131cd9d7689724d9c516238a397e0e5f0a7->enter($__internal_a44698f6344716543f35f77ad7642131cd9d7689724d9c516238a397e0e5f0a7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 141
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 141)->display($context);
        
        $__internal_a44698f6344716543f35f77ad7642131cd9d7689724d9c516238a397e0e5f0a7->leave($__internal_a44698f6344716543f35f77ad7642131cd9d7689724d9c516238a397e0e5f0a7_prof);

        
        $__internal_a3c0c037ecfea9c3d67eaac75fa8d55aa620c14ba5298ba817cfccb2c9046ba5->leave($__internal_a3c0c037ecfea9c3d67eaac75fa8d55aa620c14ba5298ba817cfccb2c9046ba5_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  226 => 141,  217 => 140,  200 => 137,  191 => 136,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "@Twig/Exception/exception_full.html.twig", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception_full.html.twig");
    }
}
