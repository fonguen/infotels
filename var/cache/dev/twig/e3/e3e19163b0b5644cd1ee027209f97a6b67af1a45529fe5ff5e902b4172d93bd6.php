<?php

/* @App/Blog/affichepragramme.html.twig */
class __TwigTemplate_796c3566da3e8062c13927f6bb25af741b14619bf2e3c1bfce5379bc286f698e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "@App/Blog/affichepragramme.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_15f6f921be14ef308488265afcef93a383e5fc6f44ef8c7a574f1a85e6c248dc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_15f6f921be14ef308488265afcef93a383e5fc6f44ef8c7a574f1a85e6c248dc->enter($__internal_15f6f921be14ef308488265afcef93a383e5fc6f44ef8c7a574f1a85e6c248dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@App/Blog/affichepragramme.html.twig"));

        $__internal_28ccea1566587c119dc2e606ea3f197be53bbd1401b900dd7f99b265017ee18b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_28ccea1566587c119dc2e606ea3f197be53bbd1401b900dd7f99b265017ee18b->enter($__internal_28ccea1566587c119dc2e606ea3f197be53bbd1401b900dd7f99b265017ee18b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@App/Blog/affichepragramme.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_15f6f921be14ef308488265afcef93a383e5fc6f44ef8c7a574f1a85e6c248dc->leave($__internal_15f6f921be14ef308488265afcef93a383e5fc6f44ef8c7a574f1a85e6c248dc_prof);

        
        $__internal_28ccea1566587c119dc2e606ea3f197be53bbd1401b900dd7f99b265017ee18b->leave($__internal_28ccea1566587c119dc2e606ea3f197be53bbd1401b900dd7f99b265017ee18b_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_ea1f06a295bfad4b98994e80f297fd0366fd6b631da1ebf6de1aa1b81c741e96 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ea1f06a295bfad4b98994e80f297fd0366fd6b631da1ebf6de1aa1b81c741e96->enter($__internal_ea1f06a295bfad4b98994e80f297fd0366fd6b631da1ebf6de1aa1b81c741e96_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_4a59e7149c428adace8d861da248ed3069ee4a8c61ede21627748fc040595c59 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4a59e7149c428adace8d861da248ed3069ee4a8c61ede21627748fc040595c59->enter($__internal_4a59e7149c428adace8d861da248ed3069ee4a8c61ede21627748fc040595c59_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "AppBundle:Blog:affichecour";
        
        $__internal_4a59e7149c428adace8d861da248ed3069ee4a8c61ede21627748fc040595c59->leave($__internal_4a59e7149c428adace8d861da248ed3069ee4a8c61ede21627748fc040595c59_prof);

        
        $__internal_ea1f06a295bfad4b98994e80f297fd0366fd6b631da1ebf6de1aa1b81c741e96->leave($__internal_ea1f06a295bfad4b98994e80f297fd0366fd6b631da1ebf6de1aa1b81c741e96_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_f861170773a4eae8328a964de01d910c22f4d7bcab92a80b118072d26f6c26e9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f861170773a4eae8328a964de01d910c22f4d7bcab92a80b118072d26f6c26e9->enter($__internal_f861170773a4eae8328a964de01d910c22f4d7bcab92a80b118072d26f6c26e9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_a213850776c13ea5cfa37bd9186899382b192419be7cdc7ec791b5f4dfc16414 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a213850776c13ea5cfa37bd9186899382b192419be7cdc7ec791b5f4dfc16414->enter($__internal_a213850776c13ea5cfa37bd9186899382b192419be7cdc7ec791b5f4dfc16414_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "<div class=\"container\">
  <table class=\"menuuser\">
    <tr>
      <td><a href=\"user/affichecour\" class=\"btn btn-large btn-info\">doccumment</a></td>
      <td><a href=\"user/affichepragramme\" class=\"btn btn-large btn-success\">programmes</a></td>
      <td><a href=\"\" class=\"btn btn-primary\">resultat</a></td>
      <td><a href=\"user/affichvideo\" class=\"btn btn-warning\">videos</a></td>
      <td><a href=\"\" class=\"btn btn-large btn-info\">déconnexion</a></td>
    </tr>

  </table><br><br>
  <a href=\"\" class=\"btn btn-large btn-info\">resultats publiés</a>


    \t\t<table class=\"table\" >
    \t\t\t<tr>
    \t\t\t\t<td> <b>intitulé du resultat</b></td><td> <b>date de publication</b></td><td> <b>action</b></td>
    \t\t\t</tr>

    \t\t\t";
        // line 25
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["document"]);
        foreach ($context['_seq'] as $context["_key"] => $context["document"]) {
            // line 26
            echo "
    \t\t\t<tr>
    \t\t\t\t<td> ";
            // line 28
            echo twig_escape_filter($this->env, $this->getAttribute($context["document"], "nom", array()), "html", null, true);
            echo "</td>
            <td> ";
            // line 29
            echo twig_escape_filter($this->env, $this->getAttribute($context["document"], "datePublication", array()), "html", null, true);
            echo "</td>
    \t\t\t\t<td><a href=\"";
            // line 30
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl(("uploads/document/" . $this->getAttribute($context["document"], "path", array()))), "html", null, true);
            echo "\" >Telecharger</a> </td>
    \t\t\t</tr>
    \t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['document'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 33
        echo "
    \t\t</table>

    \t<div class=\"navigation\">
    ";
        // line 37
        echo $this->env->getExtension('Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension')->render($this->env, ($context["document"] ?? $this->getContext($context, "document")));
        echo "
</div>
</div>
";
        
        $__internal_a213850776c13ea5cfa37bd9186899382b192419be7cdc7ec791b5f4dfc16414->leave($__internal_a213850776c13ea5cfa37bd9186899382b192419be7cdc7ec791b5f4dfc16414_prof);

        
        $__internal_f861170773a4eae8328a964de01d910c22f4d7bcab92a80b118072d26f6c26e9->leave($__internal_f861170773a4eae8328a964de01d910c22f4d7bcab92a80b118072d26f6c26e9_prof);

    }

    public function getTemplateName()
    {
        return "@App/Blog/affichepragramme.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  120 => 37,  114 => 33,  105 => 30,  101 => 29,  97 => 28,  93 => 26,  89 => 25,  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"::base.html.twig\" %}

{% block title %}AppBundle:Blog:affichecour{% endblock %}

{% block body %}
<div class=\"container\">
  <table class=\"menuuser\">
    <tr>
      <td><a href=\"user/affichecour\" class=\"btn btn-large btn-info\">doccumment</a></td>
      <td><a href=\"user/affichepragramme\" class=\"btn btn-large btn-success\">programmes</a></td>
      <td><a href=\"\" class=\"btn btn-primary\">resultat</a></td>
      <td><a href=\"user/affichvideo\" class=\"btn btn-warning\">videos</a></td>
      <td><a href=\"\" class=\"btn btn-large btn-info\">déconnexion</a></td>
    </tr>

  </table><br><br>
  <a href=\"\" class=\"btn btn-large btn-info\">resultats publiés</a>


    \t\t<table class=\"table\" >
    \t\t\t<tr>
    \t\t\t\t<td> <b>intitulé du resultat</b></td><td> <b>date de publication</b></td><td> <b>action</b></td>
    \t\t\t</tr>

    \t\t\t{% for document in document %}

    \t\t\t<tr>
    \t\t\t\t<td> {{ document.nom }}</td>
            <td> {{ document.datePublication }}</td>
    \t\t\t\t<td><a href=\"{{ asset('uploads/document/' ~ document.path) }}\" >Telecharger</a> </td>
    \t\t\t</tr>
    \t\t\t{% endfor %}

    \t\t</table>

    \t<div class=\"navigation\">
    {{ knp_pagination_render(document) }}
</div>
</div>
{% endblock %}
", "@App/Blog/affichepragramme.html.twig", "/home/fonguen/symfony projet/infotels/src/AppBundle/Resources/views/Blog/affichepragramme.html.twig");
    }
}
