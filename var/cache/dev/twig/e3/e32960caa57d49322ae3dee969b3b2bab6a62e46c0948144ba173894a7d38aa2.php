<?php

/* @Twig/Exception/exception.atom.twig */
class __TwigTemplate_b6665da1ba3da1406e1376757560b9acec54aa6274c37a833ff41a6332ef8d3d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_45897d68fd8e460837b83aa7b4c7bd730174bb57fdfd054bd4f72d74e41d2890 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_45897d68fd8e460837b83aa7b4c7bd730174bb57fdfd054bd4f72d74e41d2890->enter($__internal_45897d68fd8e460837b83aa7b4c7bd730174bb57fdfd054bd4f72d74e41d2890_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.atom.twig"));

        $__internal_2552bdd431a502991cbcba1309dca85f1a9250d88c2b435ee2a22650382cf781 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2552bdd431a502991cbcba1309dca85f1a9250d88c2b435ee2a22650382cf781->enter($__internal_2552bdd431a502991cbcba1309dca85f1a9250d88c2b435ee2a22650382cf781_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.atom.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/exception.xml.twig", "@Twig/Exception/exception.atom.twig", 1)->display(array_merge($context, array("exception" => ($context["exception"] ?? $this->getContext($context, "exception")))));
        
        $__internal_45897d68fd8e460837b83aa7b4c7bd730174bb57fdfd054bd4f72d74e41d2890->leave($__internal_45897d68fd8e460837b83aa7b4c7bd730174bb57fdfd054bd4f72d74e41d2890_prof);

        
        $__internal_2552bdd431a502991cbcba1309dca85f1a9250d88c2b435ee2a22650382cf781->leave($__internal_2552bdd431a502991cbcba1309dca85f1a9250d88c2b435ee2a22650382cf781_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% include '@Twig/Exception/exception.xml.twig' with { 'exception': exception } %}
", "@Twig/Exception/exception.atom.twig", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.atom.twig");
    }
}
