<?php

/* formation/index.html.twig */
class __TwigTemplate_9ae37d0d67fc929a0c2a855e61d97aa7a3a0a45de704c41e758efa5d1ed429d6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "formation/index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_aac6bf97939c4d4a4471f0937ccd2eb1ce0612d74bd4ecf050d7fb87e7feaf2c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_aac6bf97939c4d4a4471f0937ccd2eb1ce0612d74bd4ecf050d7fb87e7feaf2c->enter($__internal_aac6bf97939c4d4a4471f0937ccd2eb1ce0612d74bd4ecf050d7fb87e7feaf2c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "formation/index.html.twig"));

        $__internal_df001eb07f1e5ca5e0e24cfa2875df595cfe7e0187d7285fbbfee4175fe0e54d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_df001eb07f1e5ca5e0e24cfa2875df595cfe7e0187d7285fbbfee4175fe0e54d->enter($__internal_df001eb07f1e5ca5e0e24cfa2875df595cfe7e0187d7285fbbfee4175fe0e54d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "formation/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_aac6bf97939c4d4a4471f0937ccd2eb1ce0612d74bd4ecf050d7fb87e7feaf2c->leave($__internal_aac6bf97939c4d4a4471f0937ccd2eb1ce0612d74bd4ecf050d7fb87e7feaf2c_prof);

        
        $__internal_df001eb07f1e5ca5e0e24cfa2875df595cfe7e0187d7285fbbfee4175fe0e54d->leave($__internal_df001eb07f1e5ca5e0e24cfa2875df595cfe7e0187d7285fbbfee4175fe0e54d_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_b59f3b82c53050db108d90493cb009d6a0b31a78a993542354bf13fae482fe58 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b59f3b82c53050db108d90493cb009d6a0b31a78a993542354bf13fae482fe58->enter($__internal_b59f3b82c53050db108d90493cb009d6a0b31a78a993542354bf13fae482fe58_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_63a107a948b5b25cff8ba45ab311377f4e628747009ce0de9be18bea17137a97 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_63a107a948b5b25cff8ba45ab311377f4e628747009ce0de9be18bea17137a97->enter($__internal_63a107a948b5b25cff8ba45ab311377f4e628747009ce0de9be18bea17137a97_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<br><br><br>
<div class=\"container\">


    <h2>Formations list</h2>
    <a href=\"";
        // line 9
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_formation_new");
        echo "\" class=\"btn btn-large btn-info\">Create a new formation</a>

    <table class=\"table\">
        <thead>
            <tr>
                <th>Id</th>
                <th>Nom</th>
                <th>Description</th>
                <th>Path</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        ";
        // line 22
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["formations"] ?? $this->getContext($context, "formations")));
        foreach ($context['_seq'] as $context["_key"] => $context["formation"]) {
            // line 23
            echo "            <tr>
                <td><a href=\"";
            // line 24
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_formation_show", array("id" => $this->getAttribute($context["formation"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["formation"], "id", array()), "html", null, true);
            echo "</a></td>
                <td>";
            // line 25
            echo twig_escape_filter($this->env, $this->getAttribute($context["formation"], "nom", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 26
            echo twig_escape_filter($this->env, $this->getAttribute($context["formation"], "description", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 27
            echo twig_escape_filter($this->env, $this->getAttribute($context["formation"], "path", array()), "html", null, true);
            echo "</td>
                <td>

                      <a href=\"";
            // line 30
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_formation_show", array("id" => $this->getAttribute($context["formation"], "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-warning\">show</a>

                      <a href=\"";
            // line 32
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_formation_edit", array("id" => $this->getAttribute($context["formation"], "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-danger\">edit</a>

                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['formation'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 37
        echo "        </tbody>
    </table>

</div>
";
        
        $__internal_63a107a948b5b25cff8ba45ab311377f4e628747009ce0de9be18bea17137a97->leave($__internal_63a107a948b5b25cff8ba45ab311377f4e628747009ce0de9be18bea17137a97_prof);

        
        $__internal_b59f3b82c53050db108d90493cb009d6a0b31a78a993542354bf13fae482fe58->leave($__internal_b59f3b82c53050db108d90493cb009d6a0b31a78a993542354bf13fae482fe58_prof);

    }

    public function getTemplateName()
    {
        return "formation/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  115 => 37,  104 => 32,  99 => 30,  93 => 27,  89 => 26,  85 => 25,  79 => 24,  76 => 23,  72 => 22,  56 => 9,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
<br><br><br>
<div class=\"container\">


    <h2>Formations list</h2>
    <a href=\"{{ path('admin_formation_new') }}\" class=\"btn btn-large btn-info\">Create a new formation</a>

    <table class=\"table\">
        <thead>
            <tr>
                <th>Id</th>
                <th>Nom</th>
                <th>Description</th>
                <th>Path</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        {% for formation in formations %}
            <tr>
                <td><a href=\"{{ path('admin_formation_show', { 'id': formation.id }) }}\">{{ formation.id }}</a></td>
                <td>{{ formation.nom }}</td>
                <td>{{ formation.description }}</td>
                <td>{{ formation.path }}</td>
                <td>

                      <a href=\"{{ path('admin_formation_show', { 'id': formation.id }) }}\" class=\"btn btn-warning\">show</a>

                      <a href=\"{{ path('admin_formation_edit', { 'id': formation.id }) }}\" class=\"btn btn-danger\">edit</a>

                </td>
            </tr>
        {% endfor %}
        </tbody>
    </table>

</div>
{% endblock %}
", "formation/index.html.twig", "/home/fonguen/symfony projet/infotels/app/Resources/views/formation/index.html.twig");
    }
}
