<?php

/* @Framework/Form/datetime_widget.html.php */
class __TwigTemplate_78271360f9e846432e68ebdd59ab21d4bdf616c85f8c7157e77e29643418c514 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_658077a4f3da60407e2591dcf43cb6c6e7556b4661211bbc6b8df4f840ecd13c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_658077a4f3da60407e2591dcf43cb6c6e7556b4661211bbc6b8df4f840ecd13c->enter($__internal_658077a4f3da60407e2591dcf43cb6c6e7556b4661211bbc6b8df4f840ecd13c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/datetime_widget.html.php"));

        $__internal_9214b6310b431210e8eae6ad962b77d627dce547895d0d8a121497242f7fa605 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9214b6310b431210e8eae6ad962b77d627dce547895d0d8a121497242f7fa605->enter($__internal_9214b6310b431210e8eae6ad962b77d627dce547895d0d8a121497242f7fa605_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/datetime_widget.html.php"));

        // line 1
        echo "<?php if (\$widget == 'single_text'): ?>
    <?php echo \$view['form']->block(\$form, 'form_widget_simple'); ?>
<?php else: ?>
    <div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
        <?php echo \$view['form']->widget(\$form['date']).' '.\$view['form']->widget(\$form['time']) ?>
    </div>
<?php endif ?>
";
        
        $__internal_658077a4f3da60407e2591dcf43cb6c6e7556b4661211bbc6b8df4f840ecd13c->leave($__internal_658077a4f3da60407e2591dcf43cb6c6e7556b4661211bbc6b8df4f840ecd13c_prof);

        
        $__internal_9214b6310b431210e8eae6ad962b77d627dce547895d0d8a121497242f7fa605->leave($__internal_9214b6310b431210e8eae6ad962b77d627dce547895d0d8a121497242f7fa605_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/datetime_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$widget == 'single_text'): ?>
    <?php echo \$view['form']->block(\$form, 'form_widget_simple'); ?>
<?php else: ?>
    <div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
        <?php echo \$view['form']->widget(\$form['date']).' '.\$view['form']->widget(\$form['time']) ?>
    </div>
<?php endif ?>
", "@Framework/Form/datetime_widget.html.php", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/datetime_widget.html.php");
    }
}
