<?php

/* @FOSUser/Group/show_content.html.twig */
class __TwigTemplate_6fff3bc3f3925aad51979b597c59edfe77148ea48822421e52a3341d02e55df1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f32bcfef33ce3a63211fc1b40630270b08cb33e71f0f4c278fb6c3e0075ea735 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f32bcfef33ce3a63211fc1b40630270b08cb33e71f0f4c278fb6c3e0075ea735->enter($__internal_f32bcfef33ce3a63211fc1b40630270b08cb33e71f0f4c278fb6c3e0075ea735_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Group/show_content.html.twig"));

        $__internal_c3d7ed06627e14bf39ff5e7980d66176c05aa151e425998ea7c64f34cc3eed67 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c3d7ed06627e14bf39ff5e7980d66176c05aa151e425998ea7c64f34cc3eed67->enter($__internal_c3d7ed06627e14bf39ff5e7980d66176c05aa151e425998ea7c64f34cc3eed67_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Group/show_content.html.twig"));

        // line 2
        echo "
<div class=\"fos_user_group_show\">
    <p>";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("group.show.name", array(), "FOSUserBundle"), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["group"] ?? $this->getContext($context, "group")), "getName", array(), "method"), "html", null, true);
        echo "</p>
</div>
";
        
        $__internal_f32bcfef33ce3a63211fc1b40630270b08cb33e71f0f4c278fb6c3e0075ea735->leave($__internal_f32bcfef33ce3a63211fc1b40630270b08cb33e71f0f4c278fb6c3e0075ea735_prof);

        
        $__internal_c3d7ed06627e14bf39ff5e7980d66176c05aa151e425998ea7c64f34cc3eed67->leave($__internal_c3d7ed06627e14bf39ff5e7980d66176c05aa151e425998ea7c64f34cc3eed67_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Group/show_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  29 => 4,  25 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}

<div class=\"fos_user_group_show\">
    <p>{{ 'group.show.name'|trans }}: {{ group.getName() }}</p>
</div>
", "@FOSUser/Group/show_content.html.twig", "/home/fonguen/symfony projet/infotels/vendor/friendsofsymfony/user-bundle/Resources/views/Group/show_content.html.twig");
    }
}
