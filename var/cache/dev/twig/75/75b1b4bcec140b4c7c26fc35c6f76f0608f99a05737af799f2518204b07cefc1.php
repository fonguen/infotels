<?php

/* @App/Blog/insciption.html.twig */
class __TwigTemplate_6135b81161f1e84d107b597c85bdad2a9bdc5683201f4679c97ef453faf9e04f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "@App/Blog/insciption.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2d26275866b65a33f808b819159ac02a50dc8f64a4e08255d7dde06f9def8049 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2d26275866b65a33f808b819159ac02a50dc8f64a4e08255d7dde06f9def8049->enter($__internal_2d26275866b65a33f808b819159ac02a50dc8f64a4e08255d7dde06f9def8049_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@App/Blog/insciption.html.twig"));

        $__internal_a5f79229c0cd64f316ecc024ead9153f0085fcd539c826529a8d42a1ec1cbcca = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a5f79229c0cd64f316ecc024ead9153f0085fcd539c826529a8d42a1ec1cbcca->enter($__internal_a5f79229c0cd64f316ecc024ead9153f0085fcd539c826529a8d42a1ec1cbcca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@App/Blog/insciption.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2d26275866b65a33f808b819159ac02a50dc8f64a4e08255d7dde06f9def8049->leave($__internal_2d26275866b65a33f808b819159ac02a50dc8f64a4e08255d7dde06f9def8049_prof);

        
        $__internal_a5f79229c0cd64f316ecc024ead9153f0085fcd539c826529a8d42a1ec1cbcca->leave($__internal_a5f79229c0cd64f316ecc024ead9153f0085fcd539c826529a8d42a1ec1cbcca_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_ea82e9138cf7d7647675164a273c8b58859db8f06a7dc0ecda3bbd616d2ad540 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ea82e9138cf7d7647675164a273c8b58859db8f06a7dc0ecda3bbd616d2ad540->enter($__internal_ea82e9138cf7d7647675164a273c8b58859db8f06a7dc0ecda3bbd616d2ad540_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_72962aaabb5423546ebe2718d4863ccbcbb91406633757f0c42d6e989397845e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_72962aaabb5423546ebe2718d4863ccbcbb91406633757f0c42d6e989397845e->enter($__internal_72962aaabb5423546ebe2718d4863ccbcbb91406633757f0c42d6e989397845e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
<div class=\"container\">
  <a href=\"user/affichecour\" class=\"btn btn-primary\"><i class=\"fa fa-wrench\" aria-hidden=\"true\"></i>
    Log</a>
  <center><h3>Inscription</h3></center><br><br>

    ";
        // line 10
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
        ";
        // line 11
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo "
        <input type=\"submit\" value=\"Create\" class=\"btn btn-large btn-info\"/>
    ";
        // line 13
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "

";
        
        $__internal_72962aaabb5423546ebe2718d4863ccbcbb91406633757f0c42d6e989397845e->leave($__internal_72962aaabb5423546ebe2718d4863ccbcbb91406633757f0c42d6e989397845e_prof);

        
        $__internal_ea82e9138cf7d7647675164a273c8b58859db8f06a7dc0ecda3bbd616d2ad540->leave($__internal_ea82e9138cf7d7647675164a273c8b58859db8f06a7dc0ecda3bbd616d2ad540_prof);

    }

    public function getTemplateName()
    {
        return "@App/Blog/insciption.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  66 => 13,  61 => 11,  57 => 10,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}

<div class=\"container\">
  <a href=\"user/affichecour\" class=\"btn btn-primary\"><i class=\"fa fa-wrench\" aria-hidden=\"true\"></i>
    Log</a>
  <center><h3>Inscription</h3></center><br><br>

    {{ form_start(form) }}
        {{ form_widget(form) }}
        <input type=\"submit\" value=\"Create\" class=\"btn btn-large btn-info\"/>
    {{ form_end(form) }}

{% endblock %}
", "@App/Blog/insciption.html.twig", "/home/fonguen/symfony projet/infotels/src/AppBundle/Resources/views/Blog/insciption.html.twig");
    }
}
