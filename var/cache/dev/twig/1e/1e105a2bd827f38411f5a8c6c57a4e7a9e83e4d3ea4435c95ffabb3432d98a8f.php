<?php

/* @Framework/Form/button_widget.html.php */
class __TwigTemplate_24225820d4d8d784573dd9ef829d6bbbaf34d3599d2a3b3718b6970800500493 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0aa49b7833b961764213514551aaa2ef91c1e6adc0e2a88836c1e149ee424410 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0aa49b7833b961764213514551aaa2ef91c1e6adc0e2a88836c1e149ee424410->enter($__internal_0aa49b7833b961764213514551aaa2ef91c1e6adc0e2a88836c1e149ee424410_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_widget.html.php"));

        $__internal_f75cb87c8048481fc5afce1567269d21ffe69d44c4e423d4260b5f05c3bde8c3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f75cb87c8048481fc5afce1567269d21ffe69d44c4e423d4260b5f05c3bde8c3->enter($__internal_f75cb87c8048481fc5afce1567269d21ffe69d44c4e423d4260b5f05c3bde8c3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_widget.html.php"));

        // line 1
        echo "<?php if (!\$label) { \$label = isset(\$label_format)
    ? strtr(\$label_format, array('%name%' => \$name, '%id%' => \$id))
    : \$view['form']->humanize(\$name); } ?>
<button type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'button' ?>\" <?php echo \$view['form']->block(\$form, 'button_attributes') ?>><?php echo \$view->escape(false !== \$translation_domain ? \$view['translator']->trans(\$label, array(), \$translation_domain) : \$label) ?></button>
";
        
        $__internal_0aa49b7833b961764213514551aaa2ef91c1e6adc0e2a88836c1e149ee424410->leave($__internal_0aa49b7833b961764213514551aaa2ef91c1e6adc0e2a88836c1e149ee424410_prof);

        
        $__internal_f75cb87c8048481fc5afce1567269d21ffe69d44c4e423d4260b5f05c3bde8c3->leave($__internal_f75cb87c8048481fc5afce1567269d21ffe69d44c4e423d4260b5f05c3bde8c3_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (!\$label) { \$label = isset(\$label_format)
    ? strtr(\$label_format, array('%name%' => \$name, '%id%' => \$id))
    : \$view['form']->humanize(\$name); } ?>
<button type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'button' ?>\" <?php echo \$view['form']->block(\$form, 'button_attributes') ?>><?php echo \$view->escape(false !== \$translation_domain ? \$view['translator']->trans(\$label, array(), \$translation_domain) : \$label) ?></button>
", "@Framework/Form/button_widget.html.php", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/button_widget.html.php");
    }
}
