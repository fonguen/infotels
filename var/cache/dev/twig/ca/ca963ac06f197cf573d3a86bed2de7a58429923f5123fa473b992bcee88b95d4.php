<?php

/* @App/Blog/resultat.html.twig */
class __TwigTemplate_86a4590398dfde7ad52a07e7b883fd7f49fe05d8d80c5e7cafbac0c3e1458651 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "@App/Blog/resultat.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e7e6268e9603e1a1bc1313dee499986e1165f05eece859c80a66b781ba78e27f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e7e6268e9603e1a1bc1313dee499986e1165f05eece859c80a66b781ba78e27f->enter($__internal_e7e6268e9603e1a1bc1313dee499986e1165f05eece859c80a66b781ba78e27f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@App/Blog/resultat.html.twig"));

        $__internal_aef26e2e968663542e2411e68332670f878cccd3b5b9d4db120fe913bd111654 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_aef26e2e968663542e2411e68332670f878cccd3b5b9d4db120fe913bd111654->enter($__internal_aef26e2e968663542e2411e68332670f878cccd3b5b9d4db120fe913bd111654_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@App/Blog/resultat.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e7e6268e9603e1a1bc1313dee499986e1165f05eece859c80a66b781ba78e27f->leave($__internal_e7e6268e9603e1a1bc1313dee499986e1165f05eece859c80a66b781ba78e27f_prof);

        
        $__internal_aef26e2e968663542e2411e68332670f878cccd3b5b9d4db120fe913bd111654->leave($__internal_aef26e2e968663542e2411e68332670f878cccd3b5b9d4db120fe913bd111654_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_54252a3e8c8efef107b3dcf087f80ee45bb16a4683206fc6a2174d474848fadb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_54252a3e8c8efef107b3dcf087f80ee45bb16a4683206fc6a2174d474848fadb->enter($__internal_54252a3e8c8efef107b3dcf087f80ee45bb16a4683206fc6a2174d474848fadb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_85b16f67c7a77318ed7e7d11bc29ce56cb84748c970fcf13a9cc3c53465b5565 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_85b16f67c7a77318ed7e7d11bc29ce56cb84748c970fcf13a9cc3c53465b5565->enter($__internal_85b16f67c7a77318ed7e7d11bc29ce56cb84748c970fcf13a9cc3c53465b5565_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "AppBundle:Blog:affichecour";
        
        $__internal_85b16f67c7a77318ed7e7d11bc29ce56cb84748c970fcf13a9cc3c53465b5565->leave($__internal_85b16f67c7a77318ed7e7d11bc29ce56cb84748c970fcf13a9cc3c53465b5565_prof);

        
        $__internal_54252a3e8c8efef107b3dcf087f80ee45bb16a4683206fc6a2174d474848fadb->leave($__internal_54252a3e8c8efef107b3dcf087f80ee45bb16a4683206fc6a2174d474848fadb_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_1818be96f02725c0981cab6ee48b469b2129c136c7bc2c2dd7df326663b6b60f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1818be96f02725c0981cab6ee48b469b2129c136c7bc2c2dd7df326663b6b60f->enter($__internal_1818be96f02725c0981cab6ee48b469b2129c136c7bc2c2dd7df326663b6b60f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_1305f14df26a1486538c5024cdc5cb295731e8df5f6ab7c1c08ffe8d05c8de12 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1305f14df26a1486538c5024cdc5cb295731e8df5f6ab7c1c08ffe8d05c8de12->enter($__internal_1305f14df26a1486538c5024cdc5cb295731e8df5f6ab7c1c08ffe8d05c8de12_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "<div class=\"container\">
  <table class=\"menuuser\">
    <tr>
      <td><a href=\"user/affichecour\" class=\"btn btn-large btn-info\">doccumment</a></td>
      <td><a href=\"user/affichepragramme\" class=\"btn btn-large btn-success\">programmes</a></td>
      <td><a href=\"\" class=\"btn btn-primary\">resultat</a></td>
      <td><a href=\"user/affichvideo\" class=\"btn btn-warning\">videos</a></td>
      <td><a href=\"\" class=\"btn btn-large btn-info\">déconnexion</a></td>
    </tr>

  </table><br><br>
  <a href=\"\" class=\"btn btn-large btn-info\">resultats publiés</a>


    \t\t<table class=\"table\" >
    \t\t\t<tr>
    \t\t\t\t<td> <b>intitulé du resultat</b></td><td> <b>date de publication</b></td><td> <b>action</b></td>
    \t\t\t</tr>

    \t\t\t";
        // line 25
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["document"]);
        foreach ($context['_seq'] as $context["_key"] => $context["document"]) {
            // line 26
            echo "
    \t\t\t<tr>
    \t\t\t\t<td> ";
            // line 28
            echo twig_escape_filter($this->env, $this->getAttribute($context["document"], "nom", array()), "html", null, true);
            echo "</td>
            <td> ";
            // line 29
            echo twig_escape_filter($this->env, $this->getAttribute($context["document"], "datePublication", array()), "html", null, true);
            echo "</td>
    \t\t\t\t<td><a href=\"";
            // line 30
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl(("uploads/document/" . $this->getAttribute($context["document"], "path", array()))), "html", null, true);
            echo "\" >Telecharger</a> </td>
    \t\t\t</tr>
    \t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['document'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 33
        echo "
    \t\t</table>

    \t<div class=\"navigation\">
    ";
        // line 37
        echo $this->env->getExtension('Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension')->render($this->env, ($context["document"] ?? $this->getContext($context, "document")));
        echo "
</div>
</div>
";
        
        $__internal_1305f14df26a1486538c5024cdc5cb295731e8df5f6ab7c1c08ffe8d05c8de12->leave($__internal_1305f14df26a1486538c5024cdc5cb295731e8df5f6ab7c1c08ffe8d05c8de12_prof);

        
        $__internal_1818be96f02725c0981cab6ee48b469b2129c136c7bc2c2dd7df326663b6b60f->leave($__internal_1818be96f02725c0981cab6ee48b469b2129c136c7bc2c2dd7df326663b6b60f_prof);

    }

    public function getTemplateName()
    {
        return "@App/Blog/resultat.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  120 => 37,  114 => 33,  105 => 30,  101 => 29,  97 => 28,  93 => 26,  89 => 25,  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"::base.html.twig\" %}

{% block title %}AppBundle:Blog:affichecour{% endblock %}

{% block body %}
<div class=\"container\">
  <table class=\"menuuser\">
    <tr>
      <td><a href=\"user/affichecour\" class=\"btn btn-large btn-info\">doccumment</a></td>
      <td><a href=\"user/affichepragramme\" class=\"btn btn-large btn-success\">programmes</a></td>
      <td><a href=\"\" class=\"btn btn-primary\">resultat</a></td>
      <td><a href=\"user/affichvideo\" class=\"btn btn-warning\">videos</a></td>
      <td><a href=\"\" class=\"btn btn-large btn-info\">déconnexion</a></td>
    </tr>

  </table><br><br>
  <a href=\"\" class=\"btn btn-large btn-info\">resultats publiés</a>


    \t\t<table class=\"table\" >
    \t\t\t<tr>
    \t\t\t\t<td> <b>intitulé du resultat</b></td><td> <b>date de publication</b></td><td> <b>action</b></td>
    \t\t\t</tr>

    \t\t\t{% for document in document %}

    \t\t\t<tr>
    \t\t\t\t<td> {{ document.nom }}</td>
            <td> {{ document.datePublication }}</td>
    \t\t\t\t<td><a href=\"{{ asset('uploads/document/' ~ document.path) }}\" >Telecharger</a> </td>
    \t\t\t</tr>
    \t\t\t{% endfor %}

    \t\t</table>

    \t<div class=\"navigation\">
    {{ knp_pagination_render(document) }}
</div>
</div>
{% endblock %}
", "@App/Blog/resultat.html.twig", "/home/fonguen/symfony projet/infotels/src/AppBundle/Resources/views/Blog/resultat.html.twig");
    }
}
