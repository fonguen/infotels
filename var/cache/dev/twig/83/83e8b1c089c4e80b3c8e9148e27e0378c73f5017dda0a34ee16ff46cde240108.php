<?php

/* @App/Blog/telecom.html.twig */
class __TwigTemplate_1a71ad6e0307af18c883523a3164caeb03b337c228b0336ea910520599de611d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "@App/Blog/telecom.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_954f59020ff9db706fc277ba1a059098b180b690af7670c2fd2195c6ffcd5f9e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_954f59020ff9db706fc277ba1a059098b180b690af7670c2fd2195c6ffcd5f9e->enter($__internal_954f59020ff9db706fc277ba1a059098b180b690af7670c2fd2195c6ffcd5f9e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@App/Blog/telecom.html.twig"));

        $__internal_4f43e5e617e441ff9990f8922bfbb784183188d60350201600d7e115760b5597 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4f43e5e617e441ff9990f8922bfbb784183188d60350201600d7e115760b5597->enter($__internal_4f43e5e617e441ff9990f8922bfbb784183188d60350201600d7e115760b5597_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@App/Blog/telecom.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_954f59020ff9db706fc277ba1a059098b180b690af7670c2fd2195c6ffcd5f9e->leave($__internal_954f59020ff9db706fc277ba1a059098b180b690af7670c2fd2195c6ffcd5f9e_prof);

        
        $__internal_4f43e5e617e441ff9990f8922bfbb784183188d60350201600d7e115760b5597->leave($__internal_4f43e5e617e441ff9990f8922bfbb784183188d60350201600d7e115760b5597_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_948db6d4b3587d362de3ba8320ec6bace9d711a5b0815341cf707fd10c30c13a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_948db6d4b3587d362de3ba8320ec6bace9d711a5b0815341cf707fd10c30c13a->enter($__internal_948db6d4b3587d362de3ba8320ec6bace9d711a5b0815341cf707fd10c30c13a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_20a18cdfbe0fa09c2091da1335496b24d662ff03e3211fabafe9cc186593fb05 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_20a18cdfbe0fa09c2091da1335496b24d662ff03e3211fabafe9cc186593fb05->enter($__internal_20a18cdfbe0fa09c2091da1335496b24d662ff03e3211fabafe9cc186593fb05_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "</form>


    \t\t<table class=\"table\" >

    \t\t\t";
        // line 9
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["formation"]);
        foreach ($context['_seq'] as $context["_key"] => $context["formation"]) {
            // line 10
            echo "
    \t\t\t<tr>
    \t\t\t\t<td><p> <h4> <b>";
            // line 12
            echo twig_escape_filter($this->env, $this->getAttribute($context["formation"], "nom", array()), "html", null, true);
            echo "</b> </h4><br>
              ";
            // line 13
            echo twig_escape_filter($this->env, $this->getAttribute($context["formation"], "description", array()), "html", null, true);
            echo "</p> </td>
            <td>  <img src=\"";
            // line 14
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl(("uploads/formation/" . $this->getAttribute($context["formation"], "path", array()))), "html", null, true);
            echo "\" alt=\"\"> </td>

    \t\t\t</tr>
    \t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['formation'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 18
        echo "
    \t\t</table>
    \t</div>
    \t<div class=\"navigation\">
    ";
        // line 22
        echo $this->env->getExtension('Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension')->render($this->env, ($context["formation"] ?? $this->getContext($context, "formation")));
        echo "
</div>


";
        
        $__internal_20a18cdfbe0fa09c2091da1335496b24d662ff03e3211fabafe9cc186593fb05->leave($__internal_20a18cdfbe0fa09c2091da1335496b24d662ff03e3211fabafe9cc186593fb05_prof);

        
        $__internal_948db6d4b3587d362de3ba8320ec6bace9d711a5b0815341cf707fd10c30c13a->leave($__internal_948db6d4b3587d362de3ba8320ec6bace9d711a5b0815341cf707fd10c30c13a_prof);

    }

    public function getTemplateName()
    {
        return "@App/Blog/telecom.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  88 => 22,  82 => 18,  72 => 14,  68 => 13,  64 => 12,  60 => 10,  56 => 9,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
</form>


    \t\t<table class=\"table\" >

    \t\t\t{% for formation in formation %}

    \t\t\t<tr>
    \t\t\t\t<td><p> <h4> <b>{{ formation.nom }}</b> </h4><br>
              {{ formation.description }}</p> </td>
            <td>  <img src=\"{{ asset('uploads/formation/' ~ formation.path) }}\" alt=\"\"> </td>

    \t\t\t</tr>
    \t\t\t{% endfor %}

    \t\t</table>
    \t</div>
    \t<div class=\"navigation\">
    {{ knp_pagination_render(formation) }}
</div>


{% endblock %}
", "@App/Blog/telecom.html.twig", "/home/fonguen/symfony projet/infotels/src/AppBundle/Resources/views/Blog/telecom.html.twig");
    }
}
