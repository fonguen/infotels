<?php

/* niveau/show.html.twig */
class __TwigTemplate_668c5eee9ab1a8a243ee4161bcfa42f7c2b974e69e31769c19478389d2cb80fb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "niveau/show.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f79930dadfc033a85ca1ec9f8e9026bb9d255484faa3c568a2e76fb55658c9af = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f79930dadfc033a85ca1ec9f8e9026bb9d255484faa3c568a2e76fb55658c9af->enter($__internal_f79930dadfc033a85ca1ec9f8e9026bb9d255484faa3c568a2e76fb55658c9af_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "niveau/show.html.twig"));

        $__internal_4171274dcdbb54351c2d5bac214983ef76ce53f1e176f0fbbb5e4f5120ca0336 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4171274dcdbb54351c2d5bac214983ef76ce53f1e176f0fbbb5e4f5120ca0336->enter($__internal_4171274dcdbb54351c2d5bac214983ef76ce53f1e176f0fbbb5e4f5120ca0336_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "niveau/show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f79930dadfc033a85ca1ec9f8e9026bb9d255484faa3c568a2e76fb55658c9af->leave($__internal_f79930dadfc033a85ca1ec9f8e9026bb9d255484faa3c568a2e76fb55658c9af_prof);

        
        $__internal_4171274dcdbb54351c2d5bac214983ef76ce53f1e176f0fbbb5e4f5120ca0336->leave($__internal_4171274dcdbb54351c2d5bac214983ef76ce53f1e176f0fbbb5e4f5120ca0336_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_42ba1cc52a043779192917cc1bdb443ff26f0cc0c1c4bbda1cfc7b01cd1c92f9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_42ba1cc52a043779192917cc1bdb443ff26f0cc0c1c4bbda1cfc7b01cd1c92f9->enter($__internal_42ba1cc52a043779192917cc1bdb443ff26f0cc0c1c4bbda1cfc7b01cd1c92f9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_1ce2bce0493fb3b62263bd003641762ef12611d4c39b93d818516e4b205e97be = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1ce2bce0493fb3b62263bd003641762ef12611d4c39b93d818516e4b205e97be->enter($__internal_1ce2bce0493fb3b62263bd003641762ef12611d4c39b93d818516e4b205e97be_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Niveau</h1>

    <table>
        <tbody>
            <tr>
                <th>Id</th>
                <td>";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute(($context["niveau"] ?? $this->getContext($context, "niveau")), "id", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Nom</th>
                <td>";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute(($context["niveau"] ?? $this->getContext($context, "niveau")), "nom", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Options</th>
                <td>";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute(($context["niveau"] ?? $this->getContext($context, "niveau")), "options", array()), "html", null, true);
        echo "</td>
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"";
        // line 25
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_niveau_index");
        echo "\">Back to the list</a>
        </li>
        <li>
            <a href=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_niveau_edit", array("id" => $this->getAttribute(($context["niveau"] ?? $this->getContext($context, "niveau")), "id", array()))), "html", null, true);
        echo "\">Edit</a>
        </li>
        <li>
            ";
        // line 31
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_start');
        echo "
                <input type=\"submit\" value=\"Delete\">
            ";
        // line 33
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_end');
        echo "
        </li>
    </ul>
";
        
        $__internal_1ce2bce0493fb3b62263bd003641762ef12611d4c39b93d818516e4b205e97be->leave($__internal_1ce2bce0493fb3b62263bd003641762ef12611d4c39b93d818516e4b205e97be_prof);

        
        $__internal_42ba1cc52a043779192917cc1bdb443ff26f0cc0c1c4bbda1cfc7b01cd1c92f9->leave($__internal_42ba1cc52a043779192917cc1bdb443ff26f0cc0c1c4bbda1cfc7b01cd1c92f9_prof);

    }

    public function getTemplateName()
    {
        return "niveau/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  98 => 33,  93 => 31,  87 => 28,  81 => 25,  71 => 18,  64 => 14,  57 => 10,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Niveau</h1>

    <table>
        <tbody>
            <tr>
                <th>Id</th>
                <td>{{ niveau.id }}</td>
            </tr>
            <tr>
                <th>Nom</th>
                <td>{{ niveau.nom }}</td>
            </tr>
            <tr>
                <th>Options</th>
                <td>{{ niveau.options }}</td>
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"{{ path('admin_niveau_index') }}\">Back to the list</a>
        </li>
        <li>
            <a href=\"{{ path('admin_niveau_edit', { 'id': niveau.id }) }}\">Edit</a>
        </li>
        <li>
            {{ form_start(delete_form) }}
                <input type=\"submit\" value=\"Delete\">
            {{ form_end(delete_form) }}
        </li>
    </ul>
{% endblock %}
", "niveau/show.html.twig", "/home/fonguen/symfony projet/infotels/app/Resources/views/niveau/show.html.twig");
    }
}
