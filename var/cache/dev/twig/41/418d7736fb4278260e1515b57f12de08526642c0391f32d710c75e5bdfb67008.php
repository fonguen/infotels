<?php

/* @App/Blog/menu.html.twig */
class __TwigTemplate_56f41545633aba192b9500061be9db935c9529c808bc3dbcd465ee1a4b330069 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "@App/Blog/menu.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fe29dc7b58ac4bdc892f8fdc69ae8aca7249d820312503cd8c778f65540529e3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fe29dc7b58ac4bdc892f8fdc69ae8aca7249d820312503cd8c778f65540529e3->enter($__internal_fe29dc7b58ac4bdc892f8fdc69ae8aca7249d820312503cd8c778f65540529e3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@App/Blog/menu.html.twig"));

        $__internal_a5beac934a6da53d73fdb759f6639d83f1121cf29d9dfa5589fc5ae45810bfaf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a5beac934a6da53d73fdb759f6639d83f1121cf29d9dfa5589fc5ae45810bfaf->enter($__internal_a5beac934a6da53d73fdb759f6639d83f1121cf29d9dfa5589fc5ae45810bfaf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@App/Blog/menu.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_fe29dc7b58ac4bdc892f8fdc69ae8aca7249d820312503cd8c778f65540529e3->leave($__internal_fe29dc7b58ac4bdc892f8fdc69ae8aca7249d820312503cd8c778f65540529e3_prof);

        
        $__internal_a5beac934a6da53d73fdb759f6639d83f1121cf29d9dfa5589fc5ae45810bfaf->leave($__internal_a5beac934a6da53d73fdb759f6639d83f1121cf29d9dfa5589fc5ae45810bfaf_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_3593d5a2708439369bdb382fc6af1f63abd9080fe1abf941f0f396fe07e13fab = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3593d5a2708439369bdb382fc6af1f63abd9080fe1abf941f0f396fe07e13fab->enter($__internal_3593d5a2708439369bdb382fc6af1f63abd9080fe1abf941f0f396fe07e13fab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_ec5a8be3f8f24e542ed70300fcae818917ff7b57f57368c0ae166f0ba8f9632a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ec5a8be3f8f24e542ed70300fcae818917ff7b57f57368c0ae166f0ba8f9632a->enter($__internal_ec5a8be3f8f24e542ed70300fcae818917ff7b57f57368c0ae166f0ba8f9632a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "</form>


    \t\t<table class=\"table\" >

    \t\t\t";
        // line 9
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["formation"]);
        foreach ($context['_seq'] as $context["_key"] => $context["formation"]) {
            // line 10
            echo "
    \t\t\t<tr>
    \t\t\t\t<td><p> <h4> <b>";
            // line 12
            echo twig_escape_filter($this->env, $this->getAttribute($context["formation"], "nom", array()), "html", null, true);
            echo "</b> </h4><br>
              ";
            // line 13
            echo twig_escape_filter($this->env, $this->getAttribute($context["formation"], "description", array()), "html", null, true);
            echo "</p> </td>
            <td>  <img src=\"";
            // line 14
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl(("uploads/formation/" . $this->getAttribute($context["formation"], "path", array()))), "html", null, true);
            echo "\" alt=\"\"> </td>

    \t\t\t</tr>
    \t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['formation'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 18
        echo "
    \t\t</table>
    \t</div>
    \t<div class=\"navigation\">
    ";
        // line 22
        echo $this->env->getExtension('Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension')->render($this->env, ($context["formation"] ?? $this->getContext($context, "formation")));
        echo "
</div>


";
        
        $__internal_ec5a8be3f8f24e542ed70300fcae818917ff7b57f57368c0ae166f0ba8f9632a->leave($__internal_ec5a8be3f8f24e542ed70300fcae818917ff7b57f57368c0ae166f0ba8f9632a_prof);

        
        $__internal_3593d5a2708439369bdb382fc6af1f63abd9080fe1abf941f0f396fe07e13fab->leave($__internal_3593d5a2708439369bdb382fc6af1f63abd9080fe1abf941f0f396fe07e13fab_prof);

    }

    public function getTemplateName()
    {
        return "@App/Blog/menu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  88 => 22,  82 => 18,  72 => 14,  68 => 13,  64 => 12,  60 => 10,  56 => 9,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
</form>


    \t\t<table class=\"table\" >

    \t\t\t{% for formation in formation %}

    \t\t\t<tr>
    \t\t\t\t<td><p> <h4> <b>{{ formation.nom }}</b> </h4><br>
              {{ formation.description }}</p> </td>
            <td>  <img src=\"{{ asset('uploads/formation/' ~ formation.path) }}\" alt=\"\"> </td>

    \t\t\t</tr>
    \t\t\t{% endfor %}

    \t\t</table>
    \t</div>
    \t<div class=\"navigation\">
    {{ knp_pagination_render(formation) }}
</div>


{% endblock %}
", "@App/Blog/menu.html.twig", "/home/fonguen/symfony projet/infotels/src/AppBundle/Resources/views/Blog/menu.html.twig");
    }
}
