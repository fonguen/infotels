<?php

/* cours/index.html.twig */
class __TwigTemplate_8c653b1450037d659b3178fc41241f21380bc1223f93a9c84f709d4354f9d6bd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "cours/index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a17117675a1427692be077eb8f62347b31767f14b7c9489ca01bc2120a80b81d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a17117675a1427692be077eb8f62347b31767f14b7c9489ca01bc2120a80b81d->enter($__internal_a17117675a1427692be077eb8f62347b31767f14b7c9489ca01bc2120a80b81d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "cours/index.html.twig"));

        $__internal_e2e4f30c79eb93cb2a16db3c4d4c57d743f488679f0da095cc6debe9c90e540a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e2e4f30c79eb93cb2a16db3c4d4c57d743f488679f0da095cc6debe9c90e540a->enter($__internal_e2e4f30c79eb93cb2a16db3c4d4c57d743f488679f0da095cc6debe9c90e540a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "cours/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a17117675a1427692be077eb8f62347b31767f14b7c9489ca01bc2120a80b81d->leave($__internal_a17117675a1427692be077eb8f62347b31767f14b7c9489ca01bc2120a80b81d_prof);

        
        $__internal_e2e4f30c79eb93cb2a16db3c4d4c57d743f488679f0da095cc6debe9c90e540a->leave($__internal_e2e4f30c79eb93cb2a16db3c4d4c57d743f488679f0da095cc6debe9c90e540a_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_c86dbbf376d01ff85ff40e95c9bfb1d56db3d4b2856cea1a7053e6874402f5cb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c86dbbf376d01ff85ff40e95c9bfb1d56db3d4b2856cea1a7053e6874402f5cb->enter($__internal_c86dbbf376d01ff85ff40e95c9bfb1d56db3d4b2856cea1a7053e6874402f5cb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_d912552b4a0c53ad845ec84258c52b245054292f3b813087516c28966ff3635e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d912552b4a0c53ad845ec84258c52b245054292f3b813087516c28966ff3635e->enter($__internal_d912552b4a0c53ad845ec84258c52b245054292f3b813087516c28966ff3635e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<div class=\"container\">


    <h4 >Cours list</h4>
    <a href=\"";
        // line 8
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_cours_new");
        echo "\" class=\"btn btn-large btn-info\">Create a new cour</a>

    <table class=\"table\">
        <thead>
            <tr>
                <th>Id</th>
                <th>Intitulecours</th>
                <th>Code</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        ";
        // line 20
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["cours"] ?? $this->getContext($context, "cours")));
        foreach ($context['_seq'] as $context["_key"] => $context["cour"]) {
            // line 21
            echo "            <tr>
                <td><a href=\"";
            // line 22
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_cours_show", array("id" => $this->getAttribute($context["cour"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["cour"], "id", array()), "html", null, true);
            echo "</a></td>
                <td>";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute($context["cour"], "intituleCours", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute($context["cour"], "code", array()), "html", null, true);
            echo "</td>
                <td>

                            <a href=\"";
            // line 27
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_cours_show", array("id" => $this->getAttribute($context["cour"], "id", array()))), "html", null, true);
            echo "\" class=\"label label-warning\">show</a>

                            <a href=\"";
            // line 29
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_cours_edit", array("id" => $this->getAttribute($context["cour"], "id", array()))), "html", null, true);
            echo "\" class=\"label label-danger\">edit</a>
                        </li>
                    </ul>
                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['cour'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 35
        echo "        </tbody>
    </table>




</div>
";
        
        $__internal_d912552b4a0c53ad845ec84258c52b245054292f3b813087516c28966ff3635e->leave($__internal_d912552b4a0c53ad845ec84258c52b245054292f3b813087516c28966ff3635e_prof);

        
        $__internal_c86dbbf376d01ff85ff40e95c9bfb1d56db3d4b2856cea1a7053e6874402f5cb->leave($__internal_c86dbbf376d01ff85ff40e95c9bfb1d56db3d4b2856cea1a7053e6874402f5cb_prof);

    }

    public function getTemplateName()
    {
        return "cours/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  110 => 35,  98 => 29,  93 => 27,  87 => 24,  83 => 23,  77 => 22,  74 => 21,  70 => 20,  55 => 8,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
<div class=\"container\">


    <h4 >Cours list</h4>
    <a href=\"{{ path('admin_cours_new') }}\" class=\"btn btn-large btn-info\">Create a new cour</a>

    <table class=\"table\">
        <thead>
            <tr>
                <th>Id</th>
                <th>Intitulecours</th>
                <th>Code</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        {% for cour in cours %}
            <tr>
                <td><a href=\"{{ path('admin_cours_show', { 'id': cour.id }) }}\">{{ cour.id }}</a></td>
                <td>{{ cour.intituleCours }}</td>
                <td>{{ cour.code }}</td>
                <td>

                            <a href=\"{{ path('admin_cours_show', { 'id': cour.id }) }}\" class=\"label label-warning\">show</a>

                            <a href=\"{{ path('admin_cours_edit', { 'id': cour.id }) }}\" class=\"label label-danger\">edit</a>
                        </li>
                    </ul>
                </td>
            </tr>
        {% endfor %}
        </tbody>
    </table>




</div>
{% endblock %}
", "cours/index.html.twig", "/home/fonguen/symfony projet/infotels/app/Resources/views/cours/index.html.twig");
    }
}
