<?php

/* @Framework/Form/form_widget_simple.html.php */
class __TwigTemplate_96abf6c26e17dd17372dedc9e775cd7bdb86d564c9b1212671ecdd8e01eb21f1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_37714defd83a968c7e388244714b8146c75a7e7bc99a949c623db8d92fdc030c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_37714defd83a968c7e388244714b8146c75a7e7bc99a949c623db8d92fdc030c->enter($__internal_37714defd83a968c7e388244714b8146c75a7e7bc99a949c623db8d92fdc030c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_simple.html.php"));

        $__internal_cb11e498be78d1c237f52b0686dce38b2b8a89438da29d36cb0f0c6edeac51b1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cb11e498be78d1c237f52b0686dce38b2b8a89438da29d36cb0f0c6edeac51b1->enter($__internal_cb11e498be78d1c237f52b0686dce38b2b8a89438da29d36cb0f0c6edeac51b1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_simple.html.php"));

        // line 1
        echo "<input type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'text' ?>\" <?php echo \$view['form']->block(\$form, 'widget_attributes') ?><?php if (!empty(\$value) || is_numeric(\$value)): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?> />
";
        
        $__internal_37714defd83a968c7e388244714b8146c75a7e7bc99a949c623db8d92fdc030c->leave($__internal_37714defd83a968c7e388244714b8146c75a7e7bc99a949c623db8d92fdc030c_prof);

        
        $__internal_cb11e498be78d1c237f52b0686dce38b2b8a89438da29d36cb0f0c6edeac51b1->leave($__internal_cb11e498be78d1c237f52b0686dce38b2b8a89438da29d36cb0f0c6edeac51b1_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget_simple.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<input type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'text' ?>\" <?php echo \$view['form']->block(\$form, 'widget_attributes') ?><?php if (!empty(\$value) || is_numeric(\$value)): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?> />
", "@Framework/Form/form_widget_simple.html.php", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_widget_simple.html.php");
    }
}
