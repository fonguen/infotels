<?php

/* @Framework/Form/form.html.php */
class __TwigTemplate_48304aa1898d05517c704e9283a33e91e009db260f1f16db507f6c568ede68ec extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0fa8636a633f7887b10bec528d4dce45a3faad562599f21979b831f8122505e5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0fa8636a633f7887b10bec528d4dce45a3faad562599f21979b831f8122505e5->enter($__internal_0fa8636a633f7887b10bec528d4dce45a3faad562599f21979b831f8122505e5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form.html.php"));

        $__internal_dfb4b5a1461272426e6a0b8a66d751fc5b77ffbaa216d35783067877565d562b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dfb4b5a1461272426e6a0b8a66d751fc5b77ffbaa216d35783067877565d562b->enter($__internal_dfb4b5a1461272426e6a0b8a66d751fc5b77ffbaa216d35783067877565d562b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form.html.php"));

        // line 1
        echo "<?php echo \$view['form']->start(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
<?php echo \$view['form']->end(\$form) ?>
";
        
        $__internal_0fa8636a633f7887b10bec528d4dce45a3faad562599f21979b831f8122505e5->leave($__internal_0fa8636a633f7887b10bec528d4dce45a3faad562599f21979b831f8122505e5_prof);

        
        $__internal_dfb4b5a1461272426e6a0b8a66d751fc5b77ffbaa216d35783067877565d562b->leave($__internal_dfb4b5a1461272426e6a0b8a66d751fc5b77ffbaa216d35783067877565d562b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->start(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
<?php echo \$view['form']->end(\$form) ?>
", "@Framework/Form/form.html.php", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form.html.php");
    }
}
