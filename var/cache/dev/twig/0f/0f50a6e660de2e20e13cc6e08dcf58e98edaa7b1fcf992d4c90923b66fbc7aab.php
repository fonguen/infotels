<?php

/* AppBundle:Blog:affichepragramme.html.twig */
class __TwigTemplate_f0317d3067aae69a98d69292cd1e8f3df0f24391b3596e40fcece658b090efbe extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "AppBundle:Blog:affichepragramme.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b17a58292577d88b9acb1157c52fa2caa32da808d89dada131a13c725181f200 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b17a58292577d88b9acb1157c52fa2caa32da808d89dada131a13c725181f200->enter($__internal_b17a58292577d88b9acb1157c52fa2caa32da808d89dada131a13c725181f200_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:Blog:affichepragramme.html.twig"));

        $__internal_5b5ecba7f6d2fc25cc4972cb10ea83291570e88e22bc57ffe201f3d1281c1ccc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5b5ecba7f6d2fc25cc4972cb10ea83291570e88e22bc57ffe201f3d1281c1ccc->enter($__internal_5b5ecba7f6d2fc25cc4972cb10ea83291570e88e22bc57ffe201f3d1281c1ccc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:Blog:affichepragramme.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b17a58292577d88b9acb1157c52fa2caa32da808d89dada131a13c725181f200->leave($__internal_b17a58292577d88b9acb1157c52fa2caa32da808d89dada131a13c725181f200_prof);

        
        $__internal_5b5ecba7f6d2fc25cc4972cb10ea83291570e88e22bc57ffe201f3d1281c1ccc->leave($__internal_5b5ecba7f6d2fc25cc4972cb10ea83291570e88e22bc57ffe201f3d1281c1ccc_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_c8cd178bb167858e08429dce1a655d8ce9b0b3ed000c1a66f630e64a3ae7b98c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c8cd178bb167858e08429dce1a655d8ce9b0b3ed000c1a66f630e64a3ae7b98c->enter($__internal_c8cd178bb167858e08429dce1a655d8ce9b0b3ed000c1a66f630e64a3ae7b98c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_9600be8fa15a1dc69e8487d0a1e8983db1a594e1ffa7fcc81b49431ea0fdc47f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9600be8fa15a1dc69e8487d0a1e8983db1a594e1ffa7fcc81b49431ea0fdc47f->enter($__internal_9600be8fa15a1dc69e8487d0a1e8983db1a594e1ffa7fcc81b49431ea0fdc47f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "AppBundle:Blog:affichecour";
        
        $__internal_9600be8fa15a1dc69e8487d0a1e8983db1a594e1ffa7fcc81b49431ea0fdc47f->leave($__internal_9600be8fa15a1dc69e8487d0a1e8983db1a594e1ffa7fcc81b49431ea0fdc47f_prof);

        
        $__internal_c8cd178bb167858e08429dce1a655d8ce9b0b3ed000c1a66f630e64a3ae7b98c->leave($__internal_c8cd178bb167858e08429dce1a655d8ce9b0b3ed000c1a66f630e64a3ae7b98c_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_590b241a747b1d1b8fc0bcb4f36aec6027b6b3893b506f94c2cc813008430d78 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_590b241a747b1d1b8fc0bcb4f36aec6027b6b3893b506f94c2cc813008430d78->enter($__internal_590b241a747b1d1b8fc0bcb4f36aec6027b6b3893b506f94c2cc813008430d78_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_638022981137f562761151073ce2d9263af60c969ffa453200ef060c12653e4f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_638022981137f562761151073ce2d9263af60c969ffa453200ef060c12653e4f->enter($__internal_638022981137f562761151073ce2d9263af60c969ffa453200ef060c12653e4f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "<div class=\"container\">
  <table class=\"menuuser\">
    <tr>
      <td><a href=\"user_affichecour\" class=\"btn btn-large btn-info\">doccumment</a></td>
      <td><a href=\"user_affichepragramme\" class=\"btn btn-large btn-success\">programmes</a></td>
      <td><a href=\"user_afficheresultat\" class=\"btn btn-primary\">resultat</a></td>
      <td><a href=\"user_affichvideo\" class=\"btn btn-warning\">videos</a></td>
      <td><a href=\"\" class=\"btn btn-large btn-info\">déconnexion</a></td>
    </tr>

  </table><br><br>
  <a href=\"\" class=\"btn btn-large btn-info\">resultats publiés</a>


    \t\t<table class=\"table\" >
    \t\t\t<tr>
    \t\t\t\t<td> <b>intitulé du resultat</b></td><td> <b>date de publication</b></td><td> <b>action</b></td>
    \t\t\t</tr>

    \t\t\t";
        // line 25
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["document"]);
        foreach ($context['_seq'] as $context["_key"] => $context["document"]) {
            // line 26
            echo "
    \t\t\t<tr>
    \t\t\t\t<td> ";
            // line 28
            echo twig_escape_filter($this->env, $this->getAttribute($context["document"], "nom", array()), "html", null, true);
            echo "</td>
            <td> ";
            // line 29
            echo twig_escape_filter($this->env, $this->getAttribute($context["document"], "datePublication", array()), "html", null, true);
            echo "</td>
    \t\t\t\t<td><a href=\"";
            // line 30
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl(("uploads/document/" . $this->getAttribute($context["document"], "path", array()))), "html", null, true);
            echo "\" >Telecharger</a> </td>
    \t\t\t</tr>
    \t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['document'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 33
        echo "
    \t\t</table>

    \t<div class=\"navigation\">
    ";
        // line 37
        echo $this->env->getExtension('Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension')->render($this->env, ($context["document"] ?? $this->getContext($context, "document")));
        echo "
</div>
</div>
";
        
        $__internal_638022981137f562761151073ce2d9263af60c969ffa453200ef060c12653e4f->leave($__internal_638022981137f562761151073ce2d9263af60c969ffa453200ef060c12653e4f_prof);

        
        $__internal_590b241a747b1d1b8fc0bcb4f36aec6027b6b3893b506f94c2cc813008430d78->leave($__internal_590b241a747b1d1b8fc0bcb4f36aec6027b6b3893b506f94c2cc813008430d78_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle:Blog:affichepragramme.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  120 => 37,  114 => 33,  105 => 30,  101 => 29,  97 => 28,  93 => 26,  89 => 25,  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"::base.html.twig\" %}

{% block title %}AppBundle:Blog:affichecour{% endblock %}

{% block body %}
<div class=\"container\">
  <table class=\"menuuser\">
    <tr>
      <td><a href=\"user_affichecour\" class=\"btn btn-large btn-info\">doccumment</a></td>
      <td><a href=\"user_affichepragramme\" class=\"btn btn-large btn-success\">programmes</a></td>
      <td><a href=\"user_afficheresultat\" class=\"btn btn-primary\">resultat</a></td>
      <td><a href=\"user_affichvideo\" class=\"btn btn-warning\">videos</a></td>
      <td><a href=\"\" class=\"btn btn-large btn-info\">déconnexion</a></td>
    </tr>

  </table><br><br>
  <a href=\"\" class=\"btn btn-large btn-info\">resultats publiés</a>


    \t\t<table class=\"table\" >
    \t\t\t<tr>
    \t\t\t\t<td> <b>intitulé du resultat</b></td><td> <b>date de publication</b></td><td> <b>action</b></td>
    \t\t\t</tr>

    \t\t\t{% for document in document %}

    \t\t\t<tr>
    \t\t\t\t<td> {{ document.nom }}</td>
            <td> {{ document.datePublication }}</td>
    \t\t\t\t<td><a href=\"{{ asset('uploads/document/' ~ document.path) }}\" >Telecharger</a> </td>
    \t\t\t</tr>
    \t\t\t{% endfor %}

    \t\t</table>

    \t<div class=\"navigation\">
    {{ knp_pagination_render(document) }}
</div>
</div>
{% endblock %}
", "AppBundle:Blog:affichepragramme.html.twig", "/home/fonguen/symfony projet/infotels/var/cache/dev/../../../src/AppBundle/Resources/views/Blog/affichepragramme.html.twig");
    }
}
