<?php

/* user/edit.html.twig */
class __TwigTemplate_a8426f2c4a6fe87e61b1e1d9bcef587aba5deec9a88789c7453362fd573483ba extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "user/edit.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0a3d69f28eec2ecff09f402ade81e21ef7c6f0bf99c359a7d1f6bbb2efd0a987 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0a3d69f28eec2ecff09f402ade81e21ef7c6f0bf99c359a7d1f6bbb2efd0a987->enter($__internal_0a3d69f28eec2ecff09f402ade81e21ef7c6f0bf99c359a7d1f6bbb2efd0a987_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "user/edit.html.twig"));

        $__internal_89fbbb66a2c950c5ef28e510fd5800a0195231d741d9d780f4d3f9c5886cb5dc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_89fbbb66a2c950c5ef28e510fd5800a0195231d741d9d780f4d3f9c5886cb5dc->enter($__internal_89fbbb66a2c950c5ef28e510fd5800a0195231d741d9d780f4d3f9c5886cb5dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "user/edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0a3d69f28eec2ecff09f402ade81e21ef7c6f0bf99c359a7d1f6bbb2efd0a987->leave($__internal_0a3d69f28eec2ecff09f402ade81e21ef7c6f0bf99c359a7d1f6bbb2efd0a987_prof);

        
        $__internal_89fbbb66a2c950c5ef28e510fd5800a0195231d741d9d780f4d3f9c5886cb5dc->leave($__internal_89fbbb66a2c950c5ef28e510fd5800a0195231d741d9d780f4d3f9c5886cb5dc_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_1a6f6c25ce464d5eab4ddb0fe902307e940f2318178a1da5ab77763705baca3a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1a6f6c25ce464d5eab4ddb0fe902307e940f2318178a1da5ab77763705baca3a->enter($__internal_1a6f6c25ce464d5eab4ddb0fe902307e940f2318178a1da5ab77763705baca3a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_11d4be7a3a385e466b83ba71a517bb11492b48619bebe0008e884ec32feb2309 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_11d4be7a3a385e466b83ba71a517bb11492b48619bebe0008e884ec32feb2309->enter($__internal_11d4be7a3a385e466b83ba71a517bb11492b48619bebe0008e884ec32feb2309_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<div class=\"contacts\">

</div>
    <h1>User edit</h1>

    ";
        // line 9
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'form_start');
        echo "
        ";
        // line 10
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'widget');
        echo "
        <input type=\"submit\" value=\"Edit\" class=\"btn btn-warning\"/>
    ";
        // line 12
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'form_end');
        echo "

    <ul>
        <li>
            <a href=\"";
        // line 16
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_user_index");
        echo "\" class=\"btn btn-success\">Back to the list</a>
        </li>
        <li>
            ";
        // line 19
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_start');
        echo "
                <input type=\"submit\" value=\"Delete\" class=\"btn btn-danger\">
            ";
        // line 21
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_end');
        echo "
        </li>
    </ul>
</div>
";
        
        $__internal_11d4be7a3a385e466b83ba71a517bb11492b48619bebe0008e884ec32feb2309->leave($__internal_11d4be7a3a385e466b83ba71a517bb11492b48619bebe0008e884ec32feb2309_prof);

        
        $__internal_1a6f6c25ce464d5eab4ddb0fe902307e940f2318178a1da5ab77763705baca3a->leave($__internal_1a6f6c25ce464d5eab4ddb0fe902307e940f2318178a1da5ab77763705baca3a_prof);

    }

    public function getTemplateName()
    {
        return "user/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  83 => 21,  78 => 19,  72 => 16,  65 => 12,  60 => 10,  56 => 9,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
<div class=\"contacts\">

</div>
    <h1>User edit</h1>

    {{ form_start(edit_form) }}
        {{ form_widget(edit_form) }}
        <input type=\"submit\" value=\"Edit\" class=\"btn btn-warning\"/>
    {{ form_end(edit_form) }}

    <ul>
        <li>
            <a href=\"{{ path('admin_user_index') }}\" class=\"btn btn-success\">Back to the list</a>
        </li>
        <li>
            {{ form_start(delete_form) }}
                <input type=\"submit\" value=\"Delete\" class=\"btn btn-danger\">
            {{ form_end(delete_form) }}
        </li>
    </ul>
</div>
{% endblock %}
", "user/edit.html.twig", "/home/fonguen/symfony projet/infotels/app/Resources/views/user/edit.html.twig");
    }
}
