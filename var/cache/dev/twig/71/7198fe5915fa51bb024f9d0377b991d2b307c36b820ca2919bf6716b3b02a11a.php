<?php

/* @Framework/Form/radio_widget.html.php */
class __TwigTemplate_d072d4ce978747138becec710f49644f1363e362e968aac6e3b2a7ea2236be55 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_856fcccade931120c2e08441ab97d1fcfafa5d2a3d8c718a94d8d879b2d09e64 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_856fcccade931120c2e08441ab97d1fcfafa5d2a3d8c718a94d8d879b2d09e64->enter($__internal_856fcccade931120c2e08441ab97d1fcfafa5d2a3d8c718a94d8d879b2d09e64_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/radio_widget.html.php"));

        $__internal_c1a6ab00fcae3eeac67e95d07328e061109b1054c57e867fe04b82495033f225 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c1a6ab00fcae3eeac67e95d07328e061109b1054c57e867fe04b82495033f225->enter($__internal_c1a6ab00fcae3eeac67e95d07328e061109b1054c57e867fe04b82495033f225_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/radio_widget.html.php"));

        // line 1
        echo "<input type=\"radio\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    value=\"<?php echo \$view->escape(\$value) ?>\"
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $__internal_856fcccade931120c2e08441ab97d1fcfafa5d2a3d8c718a94d8d879b2d09e64->leave($__internal_856fcccade931120c2e08441ab97d1fcfafa5d2a3d8c718a94d8d879b2d09e64_prof);

        
        $__internal_c1a6ab00fcae3eeac67e95d07328e061109b1054c57e867fe04b82495033f225->leave($__internal_c1a6ab00fcae3eeac67e95d07328e061109b1054c57e867fe04b82495033f225_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/radio_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<input type=\"radio\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    value=\"<?php echo \$view->escape(\$value) ?>\"
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
", "@Framework/Form/radio_widget.html.php", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/radio_widget.html.php");
    }
}
