<?php

/* departement/new.html.twig */
class __TwigTemplate_d4515bfa58e53f17f9318d63e159d51dd17851840727245f1160164bcacf98ea extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "departement/new.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_05fb6e1e79c4c756af2125aed8ae7f5943fef081a37e66a5a710ff753e328195 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_05fb6e1e79c4c756af2125aed8ae7f5943fef081a37e66a5a710ff753e328195->enter($__internal_05fb6e1e79c4c756af2125aed8ae7f5943fef081a37e66a5a710ff753e328195_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "departement/new.html.twig"));

        $__internal_59a27b98ca2212f8507c44a8ef665c444ca800e2f7d131029c52e741a17bb2b8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_59a27b98ca2212f8507c44a8ef665c444ca800e2f7d131029c52e741a17bb2b8->enter($__internal_59a27b98ca2212f8507c44a8ef665c444ca800e2f7d131029c52e741a17bb2b8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "departement/new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_05fb6e1e79c4c756af2125aed8ae7f5943fef081a37e66a5a710ff753e328195->leave($__internal_05fb6e1e79c4c756af2125aed8ae7f5943fef081a37e66a5a710ff753e328195_prof);

        
        $__internal_59a27b98ca2212f8507c44a8ef665c444ca800e2f7d131029c52e741a17bb2b8->leave($__internal_59a27b98ca2212f8507c44a8ef665c444ca800e2f7d131029c52e741a17bb2b8_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_3cfd18741d8b5dc546e97d7dc10c6ac82a0186e19a07eb93f084c6bf3db4f27d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3cfd18741d8b5dc546e97d7dc10c6ac82a0186e19a07eb93f084c6bf3db4f27d->enter($__internal_3cfd18741d8b5dc546e97d7dc10c6ac82a0186e19a07eb93f084c6bf3db4f27d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_c043db90c2c5bdb96b64535362182c664b4b5315a22c5d86c3c9e7a33dd667a2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c043db90c2c5bdb96b64535362182c664b4b5315a22c5d86c3c9e7a33dd667a2->enter($__internal_c043db90c2c5bdb96b64535362182c664b4b5315a22c5d86c3c9e7a33dd667a2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<br><br>
<div class=\"container\">


    <h4>Departement creation</h4>

    ";
        // line 10
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
        ";
        // line 11
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo "
        <input type=\"submit\" value=\"Create\"  class=\"btn btn-large btn-info\"/>
    ";
        // line 13
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
    <br>

            <a href=\"";
        // line 16
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_departement_index");
        echo "\" class=\"btn btn-success\">Back to the list</a>

</div>
";
        
        $__internal_c043db90c2c5bdb96b64535362182c664b4b5315a22c5d86c3c9e7a33dd667a2->leave($__internal_c043db90c2c5bdb96b64535362182c664b4b5315a22c5d86c3c9e7a33dd667a2_prof);

        
        $__internal_3cfd18741d8b5dc546e97d7dc10c6ac82a0186e19a07eb93f084c6bf3db4f27d->leave($__internal_3cfd18741d8b5dc546e97d7dc10c6ac82a0186e19a07eb93f084c6bf3db4f27d_prof);

    }

    public function getTemplateName()
    {
        return "departement/new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 16,  66 => 13,  61 => 11,  57 => 10,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
<br><br>
<div class=\"container\">


    <h4>Departement creation</h4>

    {{ form_start(form) }}
        {{ form_widget(form) }}
        <input type=\"submit\" value=\"Create\"  class=\"btn btn-large btn-info\"/>
    {{ form_end(form) }}
    <br>

            <a href=\"{{ path('admin_departement_index') }}\" class=\"btn btn-success\">Back to the list</a>

</div>
{% endblock %}
", "departement/new.html.twig", "/home/fonguen/symfony projet/infotels/app/Resources/views/departement/new.html.twig");
    }
}
