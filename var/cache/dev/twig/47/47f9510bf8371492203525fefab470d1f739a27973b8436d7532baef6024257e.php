<?php

/* @WebProfiler/Profiler/open.html.twig */
class __TwigTemplate_969af9f897d5552a9f5808c730a4393c647d23a299ba3242052c68485f83c2e2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/base.html.twig", "@WebProfiler/Profiler/open.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3960b3f1a68dceb3a7304c264e1e01a31461abdf84397dcb4fa9c83e818cdbda = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3960b3f1a68dceb3a7304c264e1e01a31461abdf84397dcb4fa9c83e818cdbda->enter($__internal_3960b3f1a68dceb3a7304c264e1e01a31461abdf84397dcb4fa9c83e818cdbda_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/open.html.twig"));

        $__internal_49b8494bf2c8a6b2ea857083050a31f7fe646a6d54ff9195b1e42d12f529e233 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_49b8494bf2c8a6b2ea857083050a31f7fe646a6d54ff9195b1e42d12f529e233->enter($__internal_49b8494bf2c8a6b2ea857083050a31f7fe646a6d54ff9195b1e42d12f529e233_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/open.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3960b3f1a68dceb3a7304c264e1e01a31461abdf84397dcb4fa9c83e818cdbda->leave($__internal_3960b3f1a68dceb3a7304c264e1e01a31461abdf84397dcb4fa9c83e818cdbda_prof);

        
        $__internal_49b8494bf2c8a6b2ea857083050a31f7fe646a6d54ff9195b1e42d12f529e233->leave($__internal_49b8494bf2c8a6b2ea857083050a31f7fe646a6d54ff9195b1e42d12f529e233_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_01ec2031724a450e83437323613622ffec37ae147f3c4de78b5c576bd4154644 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_01ec2031724a450e83437323613622ffec37ae147f3c4de78b5c576bd4154644->enter($__internal_01ec2031724a450e83437323613622ffec37ae147f3c4de78b5c576bd4154644_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_ab561194802fea4e2a4ce520b7cab3bf86b6fec46788efdc11cd6c336033c8f6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ab561194802fea4e2a4ce520b7cab3bf86b6fec46788efdc11cd6c336033c8f6->enter($__internal_ab561194802fea4e2a4ce520b7cab3bf86b6fec46788efdc11cd6c336033c8f6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        ";
        // line 5
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/open.css.twig");
        echo "
    </style>
";
        
        $__internal_ab561194802fea4e2a4ce520b7cab3bf86b6fec46788efdc11cd6c336033c8f6->leave($__internal_ab561194802fea4e2a4ce520b7cab3bf86b6fec46788efdc11cd6c336033c8f6_prof);

        
        $__internal_01ec2031724a450e83437323613622ffec37ae147f3c4de78b5c576bd4154644->leave($__internal_01ec2031724a450e83437323613622ffec37ae147f3c4de78b5c576bd4154644_prof);

    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        $__internal_1b766e71e7464f5928622658e023f7709ab4cbe426e13b4acc5663cd1820ac15 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1b766e71e7464f5928622658e023f7709ab4cbe426e13b4acc5663cd1820ac15->enter($__internal_1b766e71e7464f5928622658e023f7709ab4cbe426e13b4acc5663cd1820ac15_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_b8626a39009280c4c94aec05abee62a0411833ecb1224aff0d7049cf7c2ed5dd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b8626a39009280c4c94aec05abee62a0411833ecb1224aff0d7049cf7c2ed5dd->enter($__internal_b8626a39009280c4c94aec05abee62a0411833ecb1224aff0d7049cf7c2ed5dd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 10
        echo "<div class=\"header\">
    <a class=\"doc\" href=\"https://symfony.com/doc/";
        // line 11
        echo twig_escape_filter($this->env, twig_constant("Symfony\\Component\\HttpKernel\\Kernel::VERSION"), "html", null, true);
        echo "/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
    <h1>";
        // line 12
        echo twig_escape_filter($this->env, ($context["file"] ?? $this->getContext($context, "file")), "html", null, true);
        echo " <small>line ";
        echo twig_escape_filter($this->env, ($context["line"] ?? $this->getContext($context, "line")), "html", null, true);
        echo "</small></h1>
</div>
<div class=\"source\">
    ";
        // line 15
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\CodeExtension')->fileExcerpt(($context["filename"] ?? $this->getContext($context, "filename")), ($context["line"] ?? $this->getContext($context, "line")),  -1);
        echo "
</div>
";
        
        $__internal_b8626a39009280c4c94aec05abee62a0411833ecb1224aff0d7049cf7c2ed5dd->leave($__internal_b8626a39009280c4c94aec05abee62a0411833ecb1224aff0d7049cf7c2ed5dd_prof);

        
        $__internal_1b766e71e7464f5928622658e023f7709ab4cbe426e13b4acc5663cd1820ac15->leave($__internal_1b766e71e7464f5928622658e023f7709ab4cbe426e13b4acc5663cd1820ac15_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/open.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 15,  82 => 12,  78 => 11,  75 => 10,  66 => 9,  53 => 5,  50 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/base.html.twig' %}

{% block head %}
    <style>
        {{ include('@WebProfiler/Profiler/open.css.twig') }}
    </style>
{% endblock %}

{% block body %}
<div class=\"header\">
    <a class=\"doc\" href=\"https://symfony.com/doc/{{ constant('Symfony\\\\Component\\\\HttpKernel\\\\Kernel::VERSION') }}/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
    <h1>{{ file }} <small>line {{ line }}</small></h1>
</div>
<div class=\"source\">
    {{ filename|file_excerpt(line, -1) }}
</div>
{% endblock %}
", "@WebProfiler/Profiler/open.html.twig", "/home/fonguen/symfony projet/infotels/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/open.html.twig");
    }
}
