<?php

/* @FOSUser/Group/list.html.twig */
class __TwigTemplate_0653f0bdb55cf2e18038a817b16edee2ca9e754c77072da3eabcba4352ef57f9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Group/list.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_35d438a2e995f9f6ae7fe482015861e88088fbf29b7f6cc9fdc0cc0ede2912bc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_35d438a2e995f9f6ae7fe482015861e88088fbf29b7f6cc9fdc0cc0ede2912bc->enter($__internal_35d438a2e995f9f6ae7fe482015861e88088fbf29b7f6cc9fdc0cc0ede2912bc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Group/list.html.twig"));

        $__internal_28d7e6e6a0f3e230d0d59dcd252caf7bac0cf8bad28493a73520bc420f7c22b3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_28d7e6e6a0f3e230d0d59dcd252caf7bac0cf8bad28493a73520bc420f7c22b3->enter($__internal_28d7e6e6a0f3e230d0d59dcd252caf7bac0cf8bad28493a73520bc420f7c22b3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Group/list.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_35d438a2e995f9f6ae7fe482015861e88088fbf29b7f6cc9fdc0cc0ede2912bc->leave($__internal_35d438a2e995f9f6ae7fe482015861e88088fbf29b7f6cc9fdc0cc0ede2912bc_prof);

        
        $__internal_28d7e6e6a0f3e230d0d59dcd252caf7bac0cf8bad28493a73520bc420f7c22b3->leave($__internal_28d7e6e6a0f3e230d0d59dcd252caf7bac0cf8bad28493a73520bc420f7c22b3_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_606225a015f914bf2635fc03b844656cfe6570cd1b0b2f7403225270b27b6d34 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_606225a015f914bf2635fc03b844656cfe6570cd1b0b2f7403225270b27b6d34->enter($__internal_606225a015f914bf2635fc03b844656cfe6570cd1b0b2f7403225270b27b6d34_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_4422043832552f2a6a07b936273269e4546d9323cb54da9193e96db1f3329d49 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4422043832552f2a6a07b936273269e4546d9323cb54da9193e96db1f3329d49->enter($__internal_4422043832552f2a6a07b936273269e4546d9323cb54da9193e96db1f3329d49_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/list_content.html.twig", "@FOSUser/Group/list.html.twig", 4)->display($context);
        
        $__internal_4422043832552f2a6a07b936273269e4546d9323cb54da9193e96db1f3329d49->leave($__internal_4422043832552f2a6a07b936273269e4546d9323cb54da9193e96db1f3329d49_prof);

        
        $__internal_606225a015f914bf2635fc03b844656cfe6570cd1b0b2f7403225270b27b6d34->leave($__internal_606225a015f914bf2635fc03b844656cfe6570cd1b0b2f7403225270b27b6d34_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Group/list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/list_content.html.twig\" %}
{% endblock fos_user_content %}
", "@FOSUser/Group/list.html.twig", "/home/fonguen/symfony projet/infotels/vendor/friendsofsymfony/user-bundle/Resources/views/Group/list.html.twig");
    }
}
