<?php

/* @App/Blog/affichvideo.html.twig */
class __TwigTemplate_cd2f6cd7f7792105e7607404ea8f49a71583ae0bb31bb2cc33fa96550a3dad85 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "@App/Blog/affichvideo.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3d16b84aca75fb7b9ca4c13314874941c7fbf703c986056e27f404ed63a6e4bf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3d16b84aca75fb7b9ca4c13314874941c7fbf703c986056e27f404ed63a6e4bf->enter($__internal_3d16b84aca75fb7b9ca4c13314874941c7fbf703c986056e27f404ed63a6e4bf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@App/Blog/affichvideo.html.twig"));

        $__internal_6840c54fdb7961925c575a7575852b948ce040e3a6f8d1ceb2ba2eed044448d0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6840c54fdb7961925c575a7575852b948ce040e3a6f8d1ceb2ba2eed044448d0->enter($__internal_6840c54fdb7961925c575a7575852b948ce040e3a6f8d1ceb2ba2eed044448d0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@App/Blog/affichvideo.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3d16b84aca75fb7b9ca4c13314874941c7fbf703c986056e27f404ed63a6e4bf->leave($__internal_3d16b84aca75fb7b9ca4c13314874941c7fbf703c986056e27f404ed63a6e4bf_prof);

        
        $__internal_6840c54fdb7961925c575a7575852b948ce040e3a6f8d1ceb2ba2eed044448d0->leave($__internal_6840c54fdb7961925c575a7575852b948ce040e3a6f8d1ceb2ba2eed044448d0_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_058bd383218a1b92c61e31395350331617d11194f3da41ce821bdf3ae65a8102 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_058bd383218a1b92c61e31395350331617d11194f3da41ce821bdf3ae65a8102->enter($__internal_058bd383218a1b92c61e31395350331617d11194f3da41ce821bdf3ae65a8102_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_87dc1c75a69dee4b065668d30c3b7f594ec826e8061e80f715aafbd391694b18 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_87dc1c75a69dee4b065668d30c3b7f594ec826e8061e80f715aafbd391694b18->enter($__internal_87dc1c75a69dee4b065668d30c3b7f594ec826e8061e80f715aafbd391694b18_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "AppBundle:Blog:affichvideo";
        
        $__internal_87dc1c75a69dee4b065668d30c3b7f594ec826e8061e80f715aafbd391694b18->leave($__internal_87dc1c75a69dee4b065668d30c3b7f594ec826e8061e80f715aafbd391694b18_prof);

        
        $__internal_058bd383218a1b92c61e31395350331617d11194f3da41ce821bdf3ae65a8102->leave($__internal_058bd383218a1b92c61e31395350331617d11194f3da41ce821bdf3ae65a8102_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_8c0f8e118530cf4b24dd143fe161eb516d0e2b7b232d3e98de209d293454babe = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8c0f8e118530cf4b24dd143fe161eb516d0e2b7b232d3e98de209d293454babe->enter($__internal_8c0f8e118530cf4b24dd143fe161eb516d0e2b7b232d3e98de209d293454babe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_28c66891bdc71c26651e1729ed73a7d41a561fb6b01499c6acfed8582e82742a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_28c66891bdc71c26651e1729ed73a7d41a561fb6b01499c6acfed8582e82742a->enter($__internal_28c66891bdc71c26651e1729ed73a7d41a561fb6b01499c6acfed8582e82742a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "<h1>Welcome to the Blog:affichvideo page</h1>

<div class=\"row\">
    ";
        // line 9
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["document"]);
        foreach ($context['_seq'] as $context["_key"] => $context["document"]) {
            // line 10
            echo "    <div class=\"col-sm-4 col-lg-4 col-md-4\">
        <div class=\"thumbnails\"></div>
        <video controls alt=\"DevAnclik\" height=\"450\"   width=\"300\">
                            <source  src=\"";
            // line 13
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl(("uploads/document/" . $this->getAttribute($context["document"], "path", array()))), "html", null, true);
            echo "\" />

                        </video>
                        <div class=\"caption\">
                            <p> ";
            // line 17
            echo twig_escape_filter($this->env, $this->getAttribute($context["document"], "description", array()), "html", null, true);
            echo "</p>

                        </div>


    </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['document'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 24
        echo "

</div>
<div class=\"navigation\">
    ";
        // line 28
        echo $this->env->getExtension('Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension')->render($this->env, ($context["document"] ?? $this->getContext($context, "document")));
        echo "
   </div>

";
        
        $__internal_28c66891bdc71c26651e1729ed73a7d41a561fb6b01499c6acfed8582e82742a->leave($__internal_28c66891bdc71c26651e1729ed73a7d41a561fb6b01499c6acfed8582e82742a_prof);

        
        $__internal_8c0f8e118530cf4b24dd143fe161eb516d0e2b7b232d3e98de209d293454babe->leave($__internal_8c0f8e118530cf4b24dd143fe161eb516d0e2b7b232d3e98de209d293454babe_prof);

    }

    public function getTemplateName()
    {
        return "@App/Blog/affichvideo.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  108 => 28,  102 => 24,  89 => 17,  82 => 13,  77 => 10,  73 => 9,  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"::base.html.twig\" %}

{% block title %}AppBundle:Blog:affichvideo{% endblock %}

{% block body %}
<h1>Welcome to the Blog:affichvideo page</h1>

<div class=\"row\">
    {% for document in document %}
    <div class=\"col-sm-4 col-lg-4 col-md-4\">
        <div class=\"thumbnails\"></div>
        <video controls alt=\"DevAnclik\" height=\"450\"   width=\"300\">
                            <source  src=\"{{ asset('uploads/document/' ~ document.path) }}\" />

                        </video>
                        <div class=\"caption\">
                            <p> {{ document.description }}</p>

                        </div>


    </div>
    {% endfor %}


</div>
<div class=\"navigation\">
    {{ knp_pagination_render(document) }}
   </div>

{% endblock %}
", "@App/Blog/affichvideo.html.twig", "/home/fonguen/symfony projet/infotels/src/AppBundle/Resources/views/Blog/affichvideo.html.twig");
    }
}
