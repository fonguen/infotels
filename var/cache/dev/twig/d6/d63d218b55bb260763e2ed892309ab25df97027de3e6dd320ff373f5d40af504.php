<?php

/* @FOSUser/Registration/confirmed.html.twig */
class __TwigTemplate_47a97f807cbc93f6ae6ea46a0e3c2d904528bb0e93befe0f696dfd68066d14e9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Registration/confirmed.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_74fe4165d271113182284aa9d35db2e52e8147ebbd0d616c466d267f83c06b38 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_74fe4165d271113182284aa9d35db2e52e8147ebbd0d616c466d267f83c06b38->enter($__internal_74fe4165d271113182284aa9d35db2e52e8147ebbd0d616c466d267f83c06b38_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/confirmed.html.twig"));

        $__internal_2ce6f4e9c8b30a85ccc9c4f3924d787d6de2b9faea648bb0db5af7485dc9704f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2ce6f4e9c8b30a85ccc9c4f3924d787d6de2b9faea648bb0db5af7485dc9704f->enter($__internal_2ce6f4e9c8b30a85ccc9c4f3924d787d6de2b9faea648bb0db5af7485dc9704f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/confirmed.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_74fe4165d271113182284aa9d35db2e52e8147ebbd0d616c466d267f83c06b38->leave($__internal_74fe4165d271113182284aa9d35db2e52e8147ebbd0d616c466d267f83c06b38_prof);

        
        $__internal_2ce6f4e9c8b30a85ccc9c4f3924d787d6de2b9faea648bb0db5af7485dc9704f->leave($__internal_2ce6f4e9c8b30a85ccc9c4f3924d787d6de2b9faea648bb0db5af7485dc9704f_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_6af65f3e961e8b2596825b97ee9a5a037c506ddba62af2938e429aaea083a30b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6af65f3e961e8b2596825b97ee9a5a037c506ddba62af2938e429aaea083a30b->enter($__internal_6af65f3e961e8b2596825b97ee9a5a037c506ddba62af2938e429aaea083a30b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_22651219f3d6c7fc0ec57996ece52af659badce4652ac0e7576b0d1897fa8f93 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_22651219f3d6c7fc0ec57996ece52af659badce4652ac0e7576b0d1897fa8f93->enter($__internal_22651219f3d6c7fc0ec57996ece52af659badce4652ac0e7576b0d1897fa8f93_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "    <p>";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.confirmed", array("%username%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array())), "FOSUserBundle"), "html", null, true);
        echo "</p>
    ";
        // line 7
        if (($context["targetUrl"] ?? $this->getContext($context, "targetUrl"))) {
            // line 8
            echo "    <p><a href=\"";
            echo twig_escape_filter($this->env, ($context["targetUrl"] ?? $this->getContext($context, "targetUrl")), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.back", array(), "FOSUserBundle"), "html", null, true);
            echo "</a></p>
    ";
        }
        
        $__internal_22651219f3d6c7fc0ec57996ece52af659badce4652ac0e7576b0d1897fa8f93->leave($__internal_22651219f3d6c7fc0ec57996ece52af659badce4652ac0e7576b0d1897fa8f93_prof);

        
        $__internal_6af65f3e961e8b2596825b97ee9a5a037c506ddba62af2938e429aaea083a30b->leave($__internal_6af65f3e961e8b2596825b97ee9a5a037c506ddba62af2938e429aaea083a30b_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Registration/confirmed.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  56 => 8,  54 => 7,  49 => 6,  40 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% trans_default_domain 'FOSUserBundle' %}

{% block fos_user_content %}
    <p>{{ 'registration.confirmed'|trans({'%username%': user.username}) }}</p>
    {% if targetUrl %}
    <p><a href=\"{{ targetUrl }}\">{{ 'registration.back'|trans }}</a></p>
    {% endif %}
{% endblock fos_user_content %}
", "@FOSUser/Registration/confirmed.html.twig", "/home/fonguen/symfony projet/infotels/vendor/friendsofsymfony/user-bundle/Resources/views/Registration/confirmed.html.twig");
    }
}
