<?php

/* document/new.html.twig */
class __TwigTemplate_95697fc69e1a1fb042b63ef47c9d66346bd803ecca5417d80cea013b0c106632 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "document/new.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_24dabb146b55c6cb4b5953f9004eb3e2fd39ad0d592cb69456586806284a88f9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_24dabb146b55c6cb4b5953f9004eb3e2fd39ad0d592cb69456586806284a88f9->enter($__internal_24dabb146b55c6cb4b5953f9004eb3e2fd39ad0d592cb69456586806284a88f9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "document/new.html.twig"));

        $__internal_530da4742c4ae20406c414d8ddbf297ffb50b2d5e46fb1988bcb08e16ba31e93 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_530da4742c4ae20406c414d8ddbf297ffb50b2d5e46fb1988bcb08e16ba31e93->enter($__internal_530da4742c4ae20406c414d8ddbf297ffb50b2d5e46fb1988bcb08e16ba31e93_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "document/new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_24dabb146b55c6cb4b5953f9004eb3e2fd39ad0d592cb69456586806284a88f9->leave($__internal_24dabb146b55c6cb4b5953f9004eb3e2fd39ad0d592cb69456586806284a88f9_prof);

        
        $__internal_530da4742c4ae20406c414d8ddbf297ffb50b2d5e46fb1988bcb08e16ba31e93->leave($__internal_530da4742c4ae20406c414d8ddbf297ffb50b2d5e46fb1988bcb08e16ba31e93_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_6183d6e8ecb19616b7f20fbe9aec13419dff3aa5f47cab466a8e9f15cd832597 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6183d6e8ecb19616b7f20fbe9aec13419dff3aa5f47cab466a8e9f15cd832597->enter($__internal_6183d6e8ecb19616b7f20fbe9aec13419dff3aa5f47cab466a8e9f15cd832597_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_43e7620c0363b4ef023982b2cc4cf37b4cdfb3727368ecb0c37a35ff7a8d64b4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_43e7620c0363b4ef023982b2cc4cf37b4cdfb3727368ecb0c37a35ff7a8d64b4->enter($__internal_43e7620c0363b4ef023982b2cc4cf37b4cdfb3727368ecb0c37a35ff7a8d64b4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<div class=\"container\">


    <h4 class=\"list-group-item active\">Document creation</h4>

    ";
        // line 9
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
        ";
        // line 10
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo "
        <input type=\"submit\" value=\"Create\" />
    ";
        // line 12
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
    <br>


            <a href=\"";
        // line 16
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_document_index");
        echo "\" class=\"btn btn-large btn-info\">Back to the list</a>
    </div>
";
        
        $__internal_43e7620c0363b4ef023982b2cc4cf37b4cdfb3727368ecb0c37a35ff7a8d64b4->leave($__internal_43e7620c0363b4ef023982b2cc4cf37b4cdfb3727368ecb0c37a35ff7a8d64b4_prof);

        
        $__internal_6183d6e8ecb19616b7f20fbe9aec13419dff3aa5f47cab466a8e9f15cd832597->leave($__internal_6183d6e8ecb19616b7f20fbe9aec13419dff3aa5f47cab466a8e9f15cd832597_prof);

    }

    public function getTemplateName()
    {
        return "document/new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 16,  65 => 12,  60 => 10,  56 => 9,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
<div class=\"container\">


    <h4 class=\"list-group-item active\">Document creation</h4>

    {{ form_start(form) }}
        {{ form_widget(form) }}
        <input type=\"submit\" value=\"Create\" />
    {{ form_end(form) }}
    <br>


            <a href=\"{{ path('admin_document_index') }}\" class=\"btn btn-large btn-info\">Back to the list</a>
    </div>
{% endblock %}
", "document/new.html.twig", "/home/fonguen/symfony projet/infotels/app/Resources/views/document/new.html.twig");
    }
}
