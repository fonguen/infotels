<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appDevDebugProjectContainerUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevDebugProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if (rtrim($pathinfo, '/') === '/_profiler') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_profiler_home');
                    }

                    return array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                }

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ($pathinfo === '/_profiler/search') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ($pathinfo === '/_profiler/search_bar') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_info
                if (0 === strpos($pathinfo, '/_profiler/info') && preg_match('#^/_profiler/info/(?P<about>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_info')), array (  '_controller' => 'web_profiler.controller.profiler:infoAction',));
                }

                // _profiler_phpinfo
                if ($pathinfo === '/_profiler/phpinfo') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler_open_file
                if ($pathinfo === '/_profiler/open') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:openAction',  '_route' => '_profiler_open_file',);
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            // _twig_error_test
            if (0 === strpos($pathinfo, '/_error') && preg_match('#^/_error/(?P<code>\\d+)(?:\\.(?P<_format>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_twig_error_test')), array (  '_controller' => 'twig.controller.preview_error:previewErrorPageAction',  '_format' => 'html',));
            }

        }

        if (0 === strpos($pathinfo, '/user_affich')) {
            // app_blog_affichvideo
            if ($pathinfo === '/user_affichvideo') {
                return array (  '_controller' => 'AppBundle\\Controller\\BlogController::affichvideoAction',  '_route' => 'app_blog_affichvideo',);
            }

            if (0 === strpos($pathinfo, '/user_affiche')) {
                // app_blog_affichecour
                if ($pathinfo === '/user_affichecour') {
                    return array (  '_controller' => 'AppBundle\\Controller\\BlogController::affichecourAction',  '_route' => 'app_blog_affichecour',);
                }

                // app_blog_affichepragramme
                if ($pathinfo === '/user_affichepragramme') {
                    return array (  '_controller' => 'AppBundle\\Controller\\BlogController::affichepragrammeAction',  '_route' => 'app_blog_affichepragramme',);
                }

                // app_blog_afficheresultat
                if ($pathinfo === '/user_afficheresultat') {
                    return array (  '_controller' => 'AppBundle\\Controller\\BlogController::afficheresultatAction',  '_route' => 'app_blog_afficheresultat',);
                }

            }

        }

        // app_blog_menu
        if ($pathinfo === '/informatique') {
            return array (  '_controller' => 'AppBundle\\Controller\\BlogController::menuAction',  '_route' => 'app_blog_menu',);
        }

        // app_blog_telecom
        if ($pathinfo === '/telecom') {
            return array (  '_controller' => 'AppBundle\\Controller\\BlogController::telecomAction',  '_route' => 'app_blog_telecom',);
        }

        // app_blog_menuadmin
        if ($pathinfo === '/admin_menuadmin') {
            return array (  '_controller' => 'AppBundle\\Controller\\BlogController::menuadminAction',  '_route' => 'app_blog_menuadmin',);
        }

        // app_blog_ajouuser
        if ($pathinfo === '/inscription') {
            return array (  '_controller' => 'AppBundle\\Controller\\BlogController::ajouuserAction',  '_route' => 'app_blog_ajouuser',);
        }

        // app_blog_evenementafich
        if ($pathinfo === '/evenentaffich') {
            return array (  '_controller' => 'AppBundle\\Controller\\BlogController::evenementafichAction',  '_route' => 'app_blog_evenementafich',);
        }

        // app_blog_rechercheeven
        if ($pathinfo === '/rechercherevenment') {
            return array (  '_controller' => 'AppBundle\\Controller\\BlogController::rechercheevenAction',  '_route' => 'app_blog_rechercheeven',);
        }

        if (0 === strpos($pathinfo, '/admin/cours')) {
            // admin_cours_index
            if (rtrim($pathinfo, '/') === '/admin/cours') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_admin_cours_index;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'admin_cours_index');
                }

                return array (  '_controller' => 'AppBundle\\Controller\\CoursController::indexAction',  '_route' => 'admin_cours_index',);
            }
            not_admin_cours_index:

            // admin_cours_new
            if ($pathinfo === '/admin/cours/new') {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_admin_cours_new;
                }

                return array (  '_controller' => 'AppBundle\\Controller\\CoursController::newAction',  '_route' => 'admin_cours_new',);
            }
            not_admin_cours_new:

            // admin_cours_show
            if (preg_match('#^/admin/cours/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_admin_cours_show;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_cours_show')), array (  '_controller' => 'AppBundle\\Controller\\CoursController::showAction',));
            }
            not_admin_cours_show:

            // admin_cours_edit
            if (preg_match('#^/admin/cours/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_admin_cours_edit;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_cours_edit')), array (  '_controller' => 'AppBundle\\Controller\\CoursController::editAction',));
            }
            not_admin_cours_edit:

            // admin_cours_delete
            if (preg_match('#^/admin/cours/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'DELETE') {
                    $allow[] = 'DELETE';
                    goto not_admin_cours_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_cours_delete')), array (  '_controller' => 'AppBundle\\Controller\\CoursController::deleteAction',));
            }
            not_admin_cours_delete:

        }

        // homepage
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'homepage');
            }

            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::indexAction',  '_route' => 'homepage',);
        }

        if (0 === strpos($pathinfo, '/admin')) {
            if (0 === strpos($pathinfo, '/admin/d')) {
                if (0 === strpos($pathinfo, '/admin/departement')) {
                    // admin_departement_index
                    if (rtrim($pathinfo, '/') === '/admin/departement') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_admin_departement_index;
                        }

                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'admin_departement_index');
                        }

                        return array (  '_controller' => 'AppBundle\\Controller\\DepartementController::indexAction',  '_route' => 'admin_departement_index',);
                    }
                    not_admin_departement_index:

                    // admin_departement_new
                    if ($pathinfo === '/admin/departement/new') {
                        if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                            goto not_admin_departement_new;
                        }

                        return array (  '_controller' => 'AppBundle\\Controller\\DepartementController::newAction',  '_route' => 'admin_departement_new',);
                    }
                    not_admin_departement_new:

                    // admin_departement_show
                    if (preg_match('#^/admin/departement/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_admin_departement_show;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_departement_show')), array (  '_controller' => 'AppBundle\\Controller\\DepartementController::showAction',));
                    }
                    not_admin_departement_show:

                    // admin_departement_edit
                    if (preg_match('#^/admin/departement/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                            goto not_admin_departement_edit;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_departement_edit')), array (  '_controller' => 'AppBundle\\Controller\\DepartementController::editAction',));
                    }
                    not_admin_departement_edit:

                    // admin_departement_delete
                    if (preg_match('#^/admin/departement/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if ($this->context->getMethod() != 'DELETE') {
                            $allow[] = 'DELETE';
                            goto not_admin_departement_delete;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_departement_delete')), array (  '_controller' => 'AppBundle\\Controller\\DepartementController::deleteAction',));
                    }
                    not_admin_departement_delete:

                }

                if (0 === strpos($pathinfo, '/admin/document')) {
                    // admin_document_index
                    if (rtrim($pathinfo, '/') === '/admin/document') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_admin_document_index;
                        }

                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'admin_document_index');
                        }

                        return array (  '_controller' => 'AppBundle\\Controller\\DocumentController::indexAction',  '_route' => 'admin_document_index',);
                    }
                    not_admin_document_index:

                    // admin_document_new
                    if ($pathinfo === '/admin/document/new') {
                        if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                            goto not_admin_document_new;
                        }

                        return array (  '_controller' => 'AppBundle\\Controller\\DocumentController::newAction',  '_route' => 'admin_document_new',);
                    }
                    not_admin_document_new:

                    // admin_document_show
                    if (preg_match('#^/admin/document/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_admin_document_show;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_document_show')), array (  '_controller' => 'AppBundle\\Controller\\DocumentController::showAction',));
                    }
                    not_admin_document_show:

                    // admin_document_edit
                    if (preg_match('#^/admin/document/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                            goto not_admin_document_edit;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_document_edit')), array (  '_controller' => 'AppBundle\\Controller\\DocumentController::editAction',));
                    }
                    not_admin_document_edit:

                    // admin_document_delete
                    if (preg_match('#^/admin/document/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if ($this->context->getMethod() != 'DELETE') {
                            $allow[] = 'DELETE';
                            goto not_admin_document_delete;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_document_delete')), array (  '_controller' => 'AppBundle\\Controller\\DocumentController::deleteAction',));
                    }
                    not_admin_document_delete:

                }

            }

            if (0 === strpos($pathinfo, '/admin/evenement')) {
                // admin_evenement_index
                if (rtrim($pathinfo, '/') === '/admin/evenement') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_admin_evenement_index;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'admin_evenement_index');
                    }

                    return array (  '_controller' => 'AppBundle\\Controller\\EvenementController::indexAction',  '_route' => 'admin_evenement_index',);
                }
                not_admin_evenement_index:

                // admin_evenement_new
                if ($pathinfo === '/admin/evenement/new') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_admin_evenement_new;
                    }

                    return array (  '_controller' => 'AppBundle\\Controller\\EvenementController::newAction',  '_route' => 'admin_evenement_new',);
                }
                not_admin_evenement_new:

                // admin_evenement_show
                if (preg_match('#^/admin/evenement/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_admin_evenement_show;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_evenement_show')), array (  '_controller' => 'AppBundle\\Controller\\EvenementController::showAction',));
                }
                not_admin_evenement_show:

                // admin_evenement_edit
                if (preg_match('#^/admin/evenement/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_admin_evenement_edit;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_evenement_edit')), array (  '_controller' => 'AppBundle\\Controller\\EvenementController::editAction',));
                }
                not_admin_evenement_edit:

                // admin_evenement_delete
                if (preg_match('#^/admin/evenement/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'DELETE') {
                        $allow[] = 'DELETE';
                        goto not_admin_evenement_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_evenement_delete')), array (  '_controller' => 'AppBundle\\Controller\\EvenementController::deleteAction',));
                }
                not_admin_evenement_delete:

            }

            if (0 === strpos($pathinfo, '/admin/formation')) {
                // admin_formation_index
                if (rtrim($pathinfo, '/') === '/admin/formation') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_admin_formation_index;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'admin_formation_index');
                    }

                    return array (  '_controller' => 'AppBundle\\Controller\\FormationController::indexAction',  '_route' => 'admin_formation_index',);
                }
                not_admin_formation_index:

                // admin_formation_new
                if ($pathinfo === '/admin/formation/new') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_admin_formation_new;
                    }

                    return array (  '_controller' => 'AppBundle\\Controller\\FormationController::newAction',  '_route' => 'admin_formation_new',);
                }
                not_admin_formation_new:

                // admin_formation_show
                if (preg_match('#^/admin/formation/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_admin_formation_show;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_formation_show')), array (  '_controller' => 'AppBundle\\Controller\\FormationController::showAction',));
                }
                not_admin_formation_show:

                // admin_formation_edit
                if (preg_match('#^/admin/formation/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_admin_formation_edit;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_formation_edit')), array (  '_controller' => 'AppBundle\\Controller\\FormationController::editAction',));
                }
                not_admin_formation_edit:

                // admin_formation_delete
                if (preg_match('#^/admin/formation/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'DELETE') {
                        $allow[] = 'DELETE';
                        goto not_admin_formation_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_formation_delete')), array (  '_controller' => 'AppBundle\\Controller\\FormationController::deleteAction',));
                }
                not_admin_formation_delete:

            }

            if (0 === strpos($pathinfo, '/admin/niveau')) {
                // admin_niveau_index
                if (rtrim($pathinfo, '/') === '/admin/niveau') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_admin_niveau_index;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'admin_niveau_index');
                    }

                    return array (  '_controller' => 'AppBundle\\Controller\\NiveauController::indexAction',  '_route' => 'admin_niveau_index',);
                }
                not_admin_niveau_index:

                // admin_niveau_new
                if ($pathinfo === '/admin/niveau/new') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_admin_niveau_new;
                    }

                    return array (  '_controller' => 'AppBundle\\Controller\\NiveauController::newAction',  '_route' => 'admin_niveau_new',);
                }
                not_admin_niveau_new:

                // admin_niveau_show
                if (preg_match('#^/admin/niveau/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_admin_niveau_show;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_niveau_show')), array (  '_controller' => 'AppBundle\\Controller\\NiveauController::showAction',));
                }
                not_admin_niveau_show:

                // admin_niveau_edit
                if (preg_match('#^/admin/niveau/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_admin_niveau_edit;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_niveau_edit')), array (  '_controller' => 'AppBundle\\Controller\\NiveauController::editAction',));
                }
                not_admin_niveau_edit:

                // admin_niveau_delete
                if (preg_match('#^/admin/niveau/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'DELETE') {
                        $allow[] = 'DELETE';
                        goto not_admin_niveau_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_niveau_delete')), array (  '_controller' => 'AppBundle\\Controller\\NiveauController::deleteAction',));
                }
                not_admin_niveau_delete:

            }

            if (0 === strpos($pathinfo, '/admin/user')) {
                // admin_user_index
                if (rtrim($pathinfo, '/') === '/admin/user') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_admin_user_index;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'admin_user_index');
                    }

                    return array (  '_controller' => 'AppBundle\\Controller\\UserController::indexAction',  '_route' => 'admin_user_index',);
                }
                not_admin_user_index:

                // admin_user_new
                if ($pathinfo === '/admin/user/new') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_admin_user_new;
                    }

                    return array (  '_controller' => 'AppBundle\\Controller\\UserController::newAction',  '_route' => 'admin_user_new',);
                }
                not_admin_user_new:

                // admin_user_show
                if (preg_match('#^/admin/user/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_admin_user_show;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_user_show')), array (  '_controller' => 'AppBundle\\Controller\\UserController::showAction',));
                }
                not_admin_user_show:

                // admin_user_edit
                if (preg_match('#^/admin/user/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_admin_user_edit;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_user_edit')), array (  '_controller' => 'AppBundle\\Controller\\UserController::editAction',));
                }
                not_admin_user_edit:

                // admin_user_delete
                if (preg_match('#^/admin/user/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'DELETE') {
                        $allow[] = 'DELETE';
                        goto not_admin_user_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_user_delete')), array (  '_controller' => 'AppBundle\\Controller\\UserController::deleteAction',));
                }
                not_admin_user_delete:

            }

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
