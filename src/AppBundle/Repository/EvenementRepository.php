<?php

namespace AppBundle\Repository;

/**
 * EvenementRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class EvenementRepository extends \Doctrine\ORM\EntityRepository
{
  public function afficheevenement(){
		$query=$this->createQueryBuilder('f')
		->select('f')
		->orderBy('f.dateEvenement','DESC')
		->getQuery();
		return $query->getResult();
	}
  public function rechercheevenement($motcle){

		$query=$this->createQueryBuilder('f')
		->select('f')
		->where('f.description like :description')
		->setParameter('description','%'.$motcle.'%')
		->orderBy('f.dateEvenement','DESC')
		->getQuery();
		return $query->getResult();
	}
}
