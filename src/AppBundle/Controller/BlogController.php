<?php

namespace AppBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use AppBundle\Entity\User;

use AppBundle\Entity\Document;
use AppBundle\Form\DocumentType;

class BlogController extends Controller
{
    /**
     * @Route("/user_affichvideo")
     */
    public function affichvideoAction(Request $request)
    {
      $em = $this->getDoctrine()->getManager();

      $listedocuments = $em->getRepository('AppBundle:Document')->affichevideo();
        $documents  = $this->get('knp_paginator')->paginate(
        $listedocuments, /* query NOT result */
        $request->query->getInt('page', 1)/*page number*/,
        6/*limit per page*/
    );
        return $this->render('AppBundle:Blog:affichvideo.html.twig', array(
          'document' => $documents,


        ));
    }

    /**
     * @Route("/user_affichecour")
     */
    public function affichecourAction(Request $request)
    {
      $em = $this->getDoctrine()->getManager();

      $listedocument= $em->getRepository('AppBundle:Document')->affichecour();
      $documents  = $this->get('knp_paginator')->paginate(
      $listedocument, /* query NOT result */
      $request->query->getInt('page', 1)/*page number*/,
      10/*limit per page*/
  );
        return $this->render('AppBundle:Blog:affichecour.html.twig', array(
          'document' => $documents,
        ));
    }

    /**
     * @Route("/user_affichepragramme")
     */
    public function affichepragrammeAction(Request $request)
    {
      $em = $this->getDoctrine()->getManager();
      $listedocument = $em->getRepository('AppBundle:Document')->afficheprogramme();
      $documents  = $this->get('knp_paginator')->paginate(
      $listedocument, /* query NOT result */
      $request->query->getInt('page', 1)/*page number*/,
      10/*limit per page*/
  );
        return $this->render('AppBundle:Blog:affichepragramme.html.twig', array(
            'document' => $documents,
        ));
    }

    /**
     * @Route("/user_afficheresultat")
     */
    public function afficheresultatAction(Request $request)
    {
      $em = $this->getDoctrine()->getManager();
      $listedocument = $em->getRepository('AppBundle:Document')->affiresultat();
      $documents  = $this->get('knp_paginator')->paginate(
      $listedocument, /* query NOT result */
      $request->query->getInt('page', 1)/*page number*/,
      6/*limit per page*/
  );
        return $this->render('AppBundle:Blog:resultat.html.twig', array(
            'document' => $documents,
        ));
    }

    /**
     * @Route("/informatique")
     */
    public function menuAction(Request $request)
    {
      $em = $this->getDoctrine()->getManager();

      $listeformations = $em->getRepository('AppBundle:Formation')->afficheinfo();
      $formations  = $this->get('knp_paginator')->paginate(
      $listeformations, /* query NOT result */
      $request->query->getInt('page', 1)/*page number*/,
      10/*limit per page*/
  );
        return $this->render('AppBundle:Blog:menu.html.twig', array(
          'formation'=>$formations,
        ));
    }
    /**
     * @Route("/telecom")
     */
    public function telecomAction(Request $request)
    {
      $em = $this->getDoctrine()->getManager();

      $listeformations = $em->getRepository('AppBundle:Formation')->affichetele();
      $formations  = $this->get('knp_paginator')->paginate(
      $listeformations, /* query NOT result */
      $request->query->getInt('page', 1)/*page number*/,
      10/*limit per page*/
  );
        return $this->render('AppBundle:Blog:telecom.html.twig', array(
          'formation'=>$formations,
        ));
    }
    /**
     * @Route("/admin_menuadmin")
     */
    public function menuadminAction(Request $request)
    {

        return $this->render('AppBundle:Blog:menuadmin.html.twig', array(

        ));
    }

    /**
     * @Route("/inscription")
     */
    public function ajouuserAction(Request $request){
      $user =new User;
      $form = $this->createFormBuilder($user)
      ->add('username',TextType::class,[
        'attr'=>[
          'placeholder'=>"entrer votre nom",
          'class'=>'form-control'

        ]
      ])
      ->add('email',TextType::class,[
        'attr'=>[
          'placeholder'=>"entrer le votre prenom",
          'class'=>'form-control'

        ]
      ])
        ->add('matricule',TextType::class,[
          'attr'=>[
            'placeholder'=>"entrer le matricule",
            'class'=>'form-control'

          ]
        ])
      ->add('password',PasswordType::class,[
        'attr'=>[
          'placeholder'=>"",
          'class'=>'form-control'

        ]
      ])
      ->add('Departement')
      ->getForm();
      if ($request->getMethod() == 'POST') {
        $form->bind($request);
        if ($form ->isValid()){
          $em = $this->getDoctrine()->getManager();
          $em->persist($user);
          $em->flush();
        }
      }
      return $this->render('AppBundle:Blog:insciption.html.twig',
     array(
       'form' => $form->createView(),
));




    }

    /**
     * @Route("/evenentaffich")
     */
    public function evenementafichAction(Request $request)
    {
      $em = $this->getDoctrine()->getManager();

      $listevenements = $em->getRepository('AppBundle:Evenement')->afficheevenement();
      $even  = $this->get('knp_paginator')->paginate(
      $listevenements, /* query NOT result */
      $request->query->getInt('page', 1)/*page number*/,
      10/*limit per page*/
  );
        return $this->render('AppBundle:Blog:even.html.twig', array(
          'even'=>$even,
        ));
    }
    /**
     * @Route("/rechercherevenment")
     */
    public function rechercheevenAction(Request $request)
    {
      $em = $this->getDoctrine()->getManager();
      $motcle=$request->get('motcle');

      $listevenements = $em->getRepository('AppBundle:Evenement')->rechercheevenement($motcle);
      $even  = $this->get('knp_paginator')->paginate(
      $listevenements, /* query NOT result */
      $request->query->getInt('page', 1)/*page number*/,
      10/*limit per page*/
  );
        return $this->render('AppBundle:Blog:even.html.twig', array(
          'even'=>$even,
        ));
    }

}
