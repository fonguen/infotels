<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\FormationType;
use AppBundle\Entity\Formtion;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        $em = $this->getDoctrine()->getManager();

        $listeformations = $em->getRepository('AppBundle:Formation')->findAll();
        $formations  = $this->get('knp_paginator')->paginate(
        $listeformations, /* query NOT result */
        $request->query->getInt('page', 1)/*page number*/,
        4/*limit per page*/
    );
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
            'formation'=>$formations,

        ]);
    }

}
