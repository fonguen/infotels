<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class DocumentType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nom',TextType::class,[
          'attr'=>[
            'placeholder'=>"entrer le nom du doccument",
            'class'=>'form-control'

          ]
        ])
        ->add('file',FileType::class,[
          'attr'=>[
            'placeholder'=>"entrer le nom du doccument",
            'class'=>'form-control'

          ]
        ])
        ->add('type', ChoiceType::class, [
    'choices' => [
        'Publier Cours' => [
            'cours video' => 'video',
            'cours pdf ou doc' => 'cour',
        ],
        'Publier un Resultat' => [

            'resultat' => 'resultat',
        ],

    ],
])
        ->add('datePublication',BirthdayType::class,[
          'widget' => 'single_text',
    // this is actually the default format for single_text
     'format' => 'yyyy-MM-dd',
        ])
        ->add('description',TextareaType::class,[
          'attr'=>[
            'placeholder'=>"entrer la description du doccument",
            'class'=>'form-control'

          ]
        ])
        ->add('Cours')        ;
    }

    /**
     * {@inheritdoc}-
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Document'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_document';
    }


}
