<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;


class EvenementType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('intituleEvenement',TextType::class,[
          'attr'=>[
            'placeholder'=>"entrer l'intitulé de l'évenement",
            'class'=>'form-control'

          ]
        ])
        ->add('dateEvenement',BirthdayType::class,[
          'widget' => 'single_text',
    // this is actually the default format for single_text
     'format' => 'yyyy-MM-dd',
        ])
        ->add('description',TextareaType::class,[
          'attr'=>[
            'placeholder'=>"entrer la description de l'événement",
            'class'=>'form-control'

          ]
        ])
        ->add('Departement')        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Evenement'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_evenement';
    }


}
