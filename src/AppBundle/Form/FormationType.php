<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;


class FormationType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('nom',ChoiceType::class,[
          'multiple' =>false,
          'expanded' =>true,
          'choices'  =>[
            'INFORMATIQUE'    =>'INFORMATIQUE',
            'TELECOMMUNICATION'  =>'TELECOMMUNICATION',
            'CODITION ADMISISION'  =>'ADMISISION',
          ]
        ])
        ->add('description',TextareaType::class,[
          'attr'=>[
            'placeholder'=>"entrer l'intitulé du cours",
            'class'=>'form-control'

          ]
        ])
        ->add('file')
        ->add('Departement')        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Formation'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_formation';
    }


}
