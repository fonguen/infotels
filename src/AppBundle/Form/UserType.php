<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;


class UserType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('username',TextType::class,[
          'attr'=>[
            'placeholder'=>"entrer votre nom",
            'class'=>'form-control'

          ]
        ])
        ->add('email',TextType::class,[
          'attr'=>[
            'placeholder'=>"entrer votre nom",
            'class'=>'form-control'

          ]
        ])
        ->add('matricule',TextType::class,[
            'attr'=>[
              'placeholder'=>"entrer le matricule",
              'class'=>'form-control'

            ]
          ])

        ->add('password',PasswordType::class,[
          'attr'=>[
            'placeholder'=>"",
            'class'=>'form-control'

          ]
        ])
        ->add('role',ChoiceType::class,[
          'multiple' =>false,
          'expanded' =>true,
          'choices'  =>[
            'creator'    =>'REOLE_CREATOR',
            'mederator'  =>'ROLE_MODERATOR',
            'admin'      =>'ROLE_ADMIN',
            'super'      =>'ROLE_SUPER_ADMIN'
            ]

          ])
        ->add('Departement')        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_user';
    }


}
