<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;

class NiveauType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nom',ChoiceType::class, [
    'choices' => [
        'Choix du Niveau' => [
            'nveveau 1' => 'nveveau1',
            'nveveau 2' => 'nveveau2',
            'nveveau 3' => 'nveveau3',
            'nveveau 4' => 'nveveau4',
            'nveveau 5' => 'nveveau5',
        ],
    ],
])
        ->add('options',ChoiceType::class, [
          'choices' => [
              'Choix options' => [
                  'TRONC COMMUN' => 'commun',
                  'SAR' => 'sar',
                  'IG/AP' => 'ig',
                  'TELECOM' => 'telecom',
                  'RESEAU' => 'reseau',
                  'CRYPTO' => 'crypto',
                  'GLO' => 'glo',
              ],
          ],
      ])
          ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Niveau'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_niveau';
    }


}
