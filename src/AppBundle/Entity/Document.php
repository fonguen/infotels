<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;


/**
 * Document
 *
 * @ORM\Table(name="document")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DocumentRepository")
 */
class Document
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @Assert\File(maxSize="60000000000")
     */
    public $file;
    /**
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Cours",cascade={"persist"})
    */
    private $Cours;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255)
     */
    private $path;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_publication", type="date")
     */
    private $datePublication;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Document
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return Document
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Document
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set datePublication
     *
     * @param \DateTime $datePublication
     *
     * @return Document
     */
    public function setDatePublication($datePublication)
    {
        $this->datePublication = $datePublication;

        return $this;
    }

    /**
     * Get datePublication
     *
     * @return \DateTime
     */
    public function getDatePublication()
    {
        return $this->datePublication;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Document
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set cours
     *
     * @param \AppBundle\Entity\Cours $cours
     *
     * @return Document
     */
    public function setCours(\AppBundle\Entity\Cours $cours = null)
    {
        $this->Cours = $cours;

        return $this;
    }

    /**
     * Get cours
     *
     * @return \AppBundle\Entity\Cours
     */
    public function getCours()
    {
        return $this->Cours;
    }


  public function getAbsolutePath()
 {
     return null === $this->path
         ? null
         : $this->getUploadRootDir().'/'.$this->path;
 }

 public function getWebPath()
 {
     return null === $this->path
         ? null
         : $this->getUploadDir().'/'.$this->path;
 }

 protected function getUploadRootDir()
 {
     // the absolute directory path where uploaded
     // documents should be saved
     return __DIR__.'/../../../web/'.$this->getUploadDir();
 }

 protected function getUploadDir()
 {
     // get rid of the __DIR__ so it doesn't screw up
     // when displaying uploaded doc/image in the view.
     return 'uploads/document';
 }

 /**
  * Get file.
  *
  * @return UploadedFile
  */
 public function getFile()
 {
     return $this->file;
 }

 public function upload()
 {
 // the file property can be empty if the field is not required
 if (null === $this->getFile()) {
     return;
 }

 // use the original file name here but you should
 // sanitize it at least to avoid any security issues

 // move takes the target directory and then the
 // target filename to move to
 $this->getFile()->move(
     $this->getUploadRootDir(),
     $this->getFile()->getClientOriginalName()
 );

 // set the path property to the filename where you've saved the file
 $this->path = $this->getFile()->getClientOriginalName();

 // clean up the file property as you won't need it anymore
 $this->file = null;
}


}
