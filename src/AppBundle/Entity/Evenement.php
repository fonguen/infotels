<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Evenement
 *
 * @ORM\Table(name="evenement")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EvenementRepository")
 */
class Evenement
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Departement",cascade={"persist"})
     */
    private $Departement;

    /**
     * @var string
     *
     * @ORM\Column(name="intitule_evenement", type="string", length=255)
     */
    private $intituleEvenement;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_evenement", type="date")
     */
    private $dateEvenement;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set intituleEvenement
     *
     * @param string $intituleEvenement
     *
     * @return Evenement
     */
    public function setIntituleEvenement($intituleEvenement)
    {
        $this->intituleEvenement = $intituleEvenement;

        return $this;
    }

    /**
     * Get intituleEvenement
     *
     * @return string
     */
    public function getIntituleEvenement()
    {
        return $this->intituleEvenement;
    }

    /**
     * Set dateEvenement
     *
     * @param \DateTime $dateEvenement
     *
     * @return Evenement
     */
    public function setDateEvenement($dateEvenement)
    {
        $this->dateEvenement = $dateEvenement;

        return $this;
    }

    /**
     * Get dateEvenement
     *
     * @return \DateTime
     */
    public function getDateEvenement()
    {
        return $this->dateEvenement;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Evenement
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set departement
     *
     * @param \AppBundle\Entity\Departement $departement
     *
     * @return Evenement
     */
    public function setDepartement(\AppBundle\Entity\Departement $departement = null)
    {
        $this->Departement = $departement;

        return $this;
    }

    /**
     * Get departement
     *
     * @return \AppBundle\Entity\Departement
     */
    public function getDepartement()
    {
        return $this->Departement;
    }
}
