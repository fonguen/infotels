<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cours
 *
 * @ORM\Table(name="cours")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CoursRepository")
 */
class Cours
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Niveau",cascade={"persist"})
     */
    private $Niveau;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Formation",cascade={"persist"})
     */
    private $Formation;

    /**
     * @var string
     *
     * @ORM\Column(name="intitule_cours", type="string", length=255)
     */
    private $intituleCours;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255)
     */
    private $code;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set intituleCours
     *
     * @param string $intituleCours
     *
     * @return Cours
     */
    public function setIntituleCours($intituleCours)
    {
        $this->intituleCours = $intituleCours;

        return $this;
    }

    /**
     * Get intituleCours
     *
     * @return string
     */
    public function getIntituleCours()
    {
        return $this->intituleCours;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Cours
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }
    public function __toString()
    {
    return $this->getIntituleCours();
    }

    /**
     * Set niveau
     *
     * @param \AppBundle\Entity\Niveau $niveau
     *
     * @return Cours
     */
    public function setNiveau(\AppBundle\Entity\Niveau $niveau = null)
    {
        $this->Niveau = $niveau;

        return $this;
    }

    /**
     * Get niveau
     *
     * @return \AppBundle\Entity\Niveau
     */
    public function getNiveau()
    {
        return $this->Niveau;
    }

    /**
     * Set formation
     *
     * @param \AppBundle\Entity\Formation $formation
     *
     * @return Cours
     */
    public function setFormation(\AppBundle\Entity\Formation $formation = null)
    {
        $this->Formation = $formation;

        return $this;
    }

    /**
     * Get formation
     *
     * @return \AppBundle\Entity\Formation
     */
    public function getFormation()
    {
        return $this->Formation;
    }
}
